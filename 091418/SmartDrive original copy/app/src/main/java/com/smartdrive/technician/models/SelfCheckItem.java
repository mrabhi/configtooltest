package com.smartdrive.technician.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.smartdrive.technician.R;

import java.util.List;

@Table(name = "SelfCheckItem")
public class SelfCheckItem extends Model implements Parcelable {

    public static final Parcelable.Creator<SelfCheckItem> CREATOR = new Parcelable.Creator<SelfCheckItem>() {
        @Override
        public SelfCheckItem createFromParcel(Parcel source) {
            return new SelfCheckItem(source);
        }

        @Override
        public SelfCheckItem[] newArray(int size) {
            return new SelfCheckItem[size];
        }
    };

    @Column
    public long selfCheckId;
    @Column
    public String name;
    @Column
    public String details;
    @Column
    public boolean alertFlag;
    @Column
    public String background;

    public SelfCheckItem() {
    }

    protected SelfCheckItem(Parcel in) {
        this.selfCheckId = in.readLong();
        this.name = in.readString();
        this.details = in.readString();
        this.alertFlag = in.readByte() != 0;
        this.background =  in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.selfCheckId);
        dest.writeString(this.name);
        dest.writeString(this.details);
        dest.writeByte(this.alertFlag ? (byte) 1 : (byte) 0);
        dest.writeString(this.background);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static List<SelfCheckItem> getListFromDB(long selfCheckId) {
        return new Select().from(SelfCheckItem.class).where("selfCheckId = ?", selfCheckId).execute();
    }


    private static final String RED = "red";
    private static final String YELLOW = "yellow";
    public int getBackgroundColorRes() {
        if (!TextUtils.isEmpty(background)) {
            if (background.equals(YELLOW)) {
                return R.color.yellow;
            } else if (background.equals(RED)) {
                return android.R.color.holo_red_dark;
            }
        }
        return android.R.color.transparent;
    }
}