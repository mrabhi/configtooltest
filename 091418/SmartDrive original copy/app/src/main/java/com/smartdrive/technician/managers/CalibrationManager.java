package com.smartdrive.technician.managers;

import android.support.annotation.StringRes;
import android.util.Log;

import com.neusoft.smartdrive.CabExtriParam;
import com.neusoft.smartdrive.CabIntriParam;
import com.neusoft.smartdrive.CalibLines;
import com.neusoft.smartdrive.FrontCalibrationInfo;
import com.neusoft.smartdrive.NativeJNI;
import com.smartdrive.technician.R;
import com.smartdrive.technician.controller.BleConstants;
import com.smartdrive.technician.models.VehicleMeasurements;

public class CalibrationManager {

    public enum Status {
        ALL_DONE(0, R.string.calibration_done),
        STEP_COMPLETE(1, R.string.success_calibration_step),
        NOT_CENTERED(2, R.string.calibration_error_2),
        MISMATCH_HEIGHT(3, R.string.calibration_error_3),
        INCORRECT_MOUNT_ANGLE(4, R.string.calibration_error_4),
        ERROR_INVALID_IMAGE(5, R.string.calibration_error_5),
        OK(6, android.R.string.ok),
        VERTICAL_NO(111, R.string.calibration_error_2);

        int code;
        int textRes;

        Status(int i, @StringRes int res) {
            code = i;
            textRes = res;
        }

        public static Status byCode(int b) {
            for (Status status : Status.values()) {
                if (status.code == b) {
                    return status;
                }
            }
            return null;
        }

        public int getTextRes() {
            return textRes;
        }
    }

    private static CalibrationManager instance;
    private VehicleMeasurements mVehicleMeasurements;
    private CabIntriParam mIntriParam;
    private CabExtriParam mExtriParam;
    private CalibLines mLines;
    private FrontCalibrationInfo mFrontCalibrationInfo;
    private NativeJNI mNativeJNI;

    private CalibrationManager(VehicleMeasurements vehicleMeasurements) {
        this.mVehicleMeasurements = vehicleMeasurements;

        mIntriParam = new CabIntriParam();
        mExtriParam = new CabExtriParam();
        mLines = new CalibLines();
        mFrontCalibrationInfo = new FrontCalibrationInfo();
        mNativeJNI = new NativeJNI();
    }

    public static CalibrationManager getInstance(VehicleMeasurements vehicleMeasurements) {
        if (instance == null) {
            synchronized (CalibrationManager.class) {
                if (instance == null) {
                    instance = new CalibrationManager(vehicleMeasurements);
                }
            }
        }
        return instance;
    }

    public Status init() {
        mExtriParam.setBoard_height(mVehicleMeasurements.getBoardHeightFloat());
        mExtriParam.setVehicle_Width(mVehicleMeasurements.getVehicleWidthFloat());
        mExtriParam.setTy(mVehicleMeasurements.getTy());
        mExtriParam.setTz(mVehicleMeasurements.getTz());
        mExtriParam.setTx(mVehicleMeasurements.getTx());

        mIntriParam.setU0(680);
        mIntriParam.setV0(393);
        mIntriParam.setAx(1571);
        mIntriParam.setAy(1571);

        return Status.byCode(mNativeJNI.CameraParamInit(mIntriParam, mExtriParam));
    }

    public Status initLines() {
        return Status.byCode(mNativeJNI.InitCalibLines(mLines));
    }

    public CalibLines getLines() {
        return mLines;
    }

    public Status calibrate(String filePath,
                            BleConstants.DistanceCaptured step) {
//        // TODO: for testing only
//        return Status.STEP_COMPLETE;
       try {
            int i = mNativeJNI.ServiceCalibration(filePath, step.ordinal(),
                                                  mFrontCalibrationInfo,
                                                  mExtriParam);
            Log.d("STATUS", "CODE_" + i);
            return Status.byCode(i);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Calibration", e.getLocalizedMessage());
        }
        return Status.ERROR_INVALID_IMAGE;
    }
}
