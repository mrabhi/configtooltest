package com.smartdrive.technician.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartdrive.technician.EventsSubscriber;
import com.smartdrive.technician.Layout;
import com.smartdrive.technician.activities.BaseActivity;
import com.smartdrive.technician.activities.MainActivity;
import com.smartdrive.technician.managers.BleManager;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;

public abstract class BaseFragment extends Fragment {

    private View toolbarButton;
    private boolean isEventsSubscriber;
    protected BleManager bleManager = BleManager.getInstance();

    protected abstract void initView();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Class cls = getClass();
        Layout layout = (Layout) cls.getAnnotation(Layout.class);
        if (layout == null) {
            layout = (Layout) cls.getSuperclass().getAnnotation(Layout.class);
            if (layout == null) {
                throw new RuntimeException("Fragment must use @Layout (R.layout.xxx) annotation");
            }
        }
        View view = inflater.inflate(layout.id(), null);
        ButterKnife.bind(this, view);
        toolbarButton = createToolbarButton();
        if (toolbarButton != null) {
            ((MainActivity) getActivity()).addToolbarButton(toolbarButton);
        }
        isEventsSubscriber = cls.isAnnotationPresent(EventsSubscriber.class)
                || cls.getSuperclass().isAnnotationPresent(EventsSubscriber.class);
        if (isEventsSubscriber) {
            EventBus.getDefault().register(this);
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    @Override
    public void onDestroyView() {
        if (isEventsSubscriber) {
            EventBus.getDefault().unregister(this);
        }
        if (toolbarButton != null) {
            ((MainActivity) getActivity()).removeToolbarButton(toolbarButton);
        }
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (toolbarButton == null) return;
        MainActivity activity = (MainActivity) getActivity();
        if (hidden) {
            activity.removeToolbarButton(toolbarButton);
        } else {
            activity.addToolbarButton(toolbarButton);
        }
    }

    protected View createToolbarButton() {
        return null;
    }

    protected void showProgress(@StringRes int textRes) {
        ((BaseActivity) getActivity()).showProgress(textRes);
    }

    protected void hideProgress() {
        ((BaseActivity) getActivity()).hideProgress();
    }

}