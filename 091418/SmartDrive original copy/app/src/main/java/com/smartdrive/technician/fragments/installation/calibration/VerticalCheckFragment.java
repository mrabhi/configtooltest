package com.smartdrive.technician.fragments.installation.calibration;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;
import android.widget.ImageView;

import com.neusoft.smartdrive.CalibLines;
import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.activities.AdasCalibrationActivity;
import com.smartdrive.technician.fragments.BaseFragment;
import com.smartdrive.technician.managers.CalibrationManager;

import butterknife.Bind;

@Layout(id = R.layout.fragment_adas_vertical_check)
public class VerticalCheckFragment extends BaseFragment {
    @Bind(R.id.vYes)
    View vYes;
    @Bind(R.id.vNo)
    View vNo;
    @Bind(R.id.ivGeneratedImage)
    ImageView ivGeneratedImage;
    @Bind(R.id.ivInstructions)
    ImageView ivInstructions;

    AdasCalibrationActivity mActivity;
    CalibrationManager mCalibrationManager;
    Bitmap mBitmap;

    @Override
    protected void initView() {
        mActivity = (AdasCalibrationActivity) getActivity();
        if (mActivity == null) {
            return;
        }
        mBitmap = mActivity.getBitmap();
        mCalibrationManager = mActivity.getCalibrationManager();
        vNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AdasCalibrationActivity) getActivity()).openFragment(MessageFragment.newInstance(
                        CalibrationManager.Status.VERTICAL_NO));
            }
        });
        vYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AdasCalibrationActivity) getActivity()).openFragment(new PatternCheckFragment());
            }
        });

        if (mBitmap != null) {
            ivGeneratedImage.setImageBitmap(mBitmap);
        }
        if (mCalibrationManager != null) {
            if (mCalibrationManager
                    .init() == CalibrationManager.Status.OK || mCalibrationManager
                    .init() == CalibrationManager.Status.STEP_COMPLETE) {
                if (mCalibrationManager.initLines() == CalibrationManager.Status.OK
                        || mCalibrationManager.initLines()
                        == CalibrationManager.Status.STEP_COMPLETE) {
                    CalibLines lines = mCalibrationManager.getLines();
                    drawLines(lines.getTopLine(), lines.getBottomLine());
                }
            }
        }
    }

    private void drawLines(float top, float bottom) {
        Bitmap mutable = mBitmap.copy(Bitmap.Config.ARGB_8888, true);

        Canvas c = new Canvas(mutable);
        ivGeneratedImage.draw(c);

        Paint p = new Paint();
        p.setColor(Color.RED);
        p.setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.default_line_thickness));
        c.drawLine(0, top, mutable.getWidth(), top, p);
        c.drawLine(0, bottom, mutable.getWidth(), bottom, p);
        ivGeneratedImage.setImageBitmap(mutable);
    }

}