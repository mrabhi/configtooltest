package com.smartdrive.technician.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.smartdrive.technician.R;

import java.util.List;

@Table(name = "AdasCheckItem")
public class AdasCheckItem extends Model implements Parcelable {

    public static final Creator<AdasCheckItem> CREATOR = new Creator<AdasCheckItem>() {
        @Override
        public AdasCheckItem createFromParcel(Parcel source) {
            return new AdasCheckItem(source);
        }

        @Override
        public AdasCheckItem[] newArray(int size) {
            return new AdasCheckItem[size];
        }
    };
    private static final String RED = "red";
    private static final String YELLOW = "yellow";
    @Column
    public long selfCheckId;
    @Column
    public String name;
    @Column
    public String details;
    @Column
    private boolean alertFlag;
    @Column
    public String background;

    public AdasCheckItem() {
    }

    private AdasCheckItem(Parcel in) {
        this.selfCheckId = in.readLong();
        this.name = in.readString();
        this.details = in.readString();
        this.alertFlag = in.readByte() != 0;
        this.background = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.selfCheckId);
        dest.writeString(this.name);
        dest.writeString(this.details);
        dest.writeByte(this.alertFlag
                       ? (byte) 1
                       : (byte) 0);
        dest.writeString(this.background);
    }

    public static List<AdasCheckItem> getListFromDB(long selfCheckId) {
        return new Select().from(AdasCheckItem.class)
                .where("selfCheckId = ?", selfCheckId)
                .execute();
    }

    public int getBackgroundColorRes() {
        if (!TextUtils.isEmpty(background)) {
            if (background.equals(YELLOW)) {
                return R.color.yellow;
            } else if (background.equals(RED)) {
                return android.R.color.holo_red_dark;
            }
        }
        return android.R.color.transparent;
    }
}