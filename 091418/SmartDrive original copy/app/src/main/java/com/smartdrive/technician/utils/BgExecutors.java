package com.smartdrive.technician.utils;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public final class BgExecutors {

    public static final ScheduledExecutorService EXECUTOR = new ScheduledThreadPoolExecutor(
            Runtime.getRuntime()
                    .availableProcessors());

    private BgExecutors() {
        throw new IllegalStateException("this is utility class");
    }
}
