package com.smartdrive.technician.fragments.installation;

import android.view.View;
import android.widget.TextView;

import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.fragments.SelfCheckFragment;
import com.smartdrive.technician.models.report.Report;

import butterknife.Bind;

import static com.smartdrive.technician.Constants.REPORT;

@Layout(id = R.layout.fragment_self_check_report)
public class SelfCheckReportFragment extends ChildInstallationFragment {

    @Bind(R.id.runSelfCheckContainer)
    View runSelfCheckContainer;
    @Bind(R.id.btnRunSelfCheck)
    TextView btnRunSelfCheck;

    @Override
    protected void initView() {
        btnRunSelfCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSelfCheckFragment();
            }
        });
        Report report = getArguments().getParcelable(REPORT);
        if (report != null && report.selfCheck != null) {
            showSelfCheckFragment();
        }
    }

    @Override
    protected void updateReport(Report report) {
        //do nothing
    }

    private void showSelfCheckFragment() {
        runSelfCheckContainer.setVisibility(View.GONE);
        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.selfCheckReportContainer, SelfCheckFragment.newInstance())
                .commit();
    }

}