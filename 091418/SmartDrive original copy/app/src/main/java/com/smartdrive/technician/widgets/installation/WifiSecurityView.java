package com.smartdrive.technician.widgets.installation;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import com.smartdrive.technician.OnChangeListener;
import com.smartdrive.technician.R;
import com.smartdrive.technician.SmartDriveApp;
import com.smartdrive.technician.activities.MainActivity;
import com.smartdrive.technician.managers.BleManager;
import com.smartdrive.technician.models.report.WifiSecurity;
import com.smartdrive.technician.utils.CsvParser;
import com.smartdrive.technician.widgets.BaseEditText;
import com.smartdrive.technician.widgets.BaseSpinner;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.smartdrive.technician.Constants.USER_MODE;

public class WifiSecurityView extends LinearLayout {

    @Bind(R.id.etSsid)
    BaseEditText etSsid;
    @Bind(R.id.spinEncryption)
    BaseSpinner spinEncryption;
    @Bind(R.id.etPassword)
    BaseEditText etPassword;

    private String noneEncryption;

    public WifiSecurityView(Context context) {
        super(context);
        create();
    }

    public WifiSecurityView(Context context, AttributeSet attrs) {
        super(context, attrs);
        create();
    }

    public WifiSecurityView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        create();
    }

    private void create() {
        inflate(getContext(), R.layout.view_wifi_security, this);
        ButterKnife.bind(this);
        noneEncryption = SmartDriveApp.getContext().getString(R.string.none);
        etSsid.init(R.string.ssid, true);
        spinEncryption.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0 || selectedNoneEncryption()) {
                    etPassword.setVisibility(GONE);
                    etPassword.init(R.string.password, false);
                } else {
                    etPassword.setVisibility(VISIBLE);
                    etPassword.init(R.string.password, true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spinEncryption.init(CsvParser.parseEncryption(), R.string.encryption, true);
        byte userMode = ((MainActivity) getContext()).getIntent().getByteExtra(USER_MODE, (byte) 0);
        if (BleManager.getInstance().isMaintenanceMode(userMode)) {
            etSsid.setEnabled(false);
            etPassword.setEnabled(false);
        }
    }

    private boolean selectedNoneEncryption() {
        return spinEncryption.getSelectedItem() != null
                && spinEncryption.getSelectedItem().toLowerCase().equals(noneEncryption.toLowerCase());
    }

    public void init(WifiSecurity wifiSecurity) {
        if (wifiSecurity == null) return;
        etSsid.setText(wifiSecurity.ssid);
        spinEncryption.setSelectedItem(wifiSecurity.encryption);
        etPassword.setText(wifiSecurity.password);
    }

    public WifiSecurity getWifiSecurity() {
        WifiSecurity wifiSecurity = new WifiSecurity();
        wifiSecurity.ssid = etSsid.getText();
        wifiSecurity.encryption = spinEncryption.getSelectedItem();
        if (!selectedNoneEncryption()) {
            wifiSecurity.password = etPassword.getText();
        }
        return wifiSecurity;
    }

    public boolean isFieldsValid(boolean pickOut) {
        return !etSsid.isRequiredAndEmpty(pickOut)
                & !spinEncryption.isRequiredAndEmpty(pickOut)
                & !etPassword.isRequiredAndEmpty(pickOut);
    }

    public void setOnChangeListener(OnChangeListener onChangeListener) {
        etSsid.setOnChangeListener(onChangeListener);
        spinEncryption.setOnChangeListener(onChangeListener);
        etPassword.setOnChangeListener(onChangeListener);
    }

}