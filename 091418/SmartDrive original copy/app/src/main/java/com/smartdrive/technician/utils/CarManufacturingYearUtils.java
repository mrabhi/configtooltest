package com.smartdrive.technician.utils;

import com.smartdrive.technician.R;
import com.smartdrive.technician.SmartDriveApp;

import java.util.ArrayList;
import java.util.List;

import static com.smartdrive.technician.Constants.MAX_CAR_MANUFACTURING_YEAR;
import static com.smartdrive.technician.Constants.MIN_CAR_MANUFACTURING_YEAR;

public class CarManufacturingYearUtils {

    public CarManufacturingYearUtils() {
        throw new UnsupportedOperationException("This is utility class!");
    }

    public static List<String> generateYears() {
        List<String> years = new ArrayList<>();
        for (int i = MAX_CAR_MANUFACTURING_YEAR; i >= MIN_CAR_MANUFACTURING_YEAR; i--) {
            years.add(String.valueOf(i));
        }
        years.add(SmartDriveApp.getContext()
                          .getString(R.string.other));
        return years;
    }

}
