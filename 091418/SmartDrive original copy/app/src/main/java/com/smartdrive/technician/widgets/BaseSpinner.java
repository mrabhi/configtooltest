package com.smartdrive.technician.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.smartdrive.technician.OnChangeListener;
import com.smartdrive.technician.R;

import java.util.ArrayList;
import java.util.List;

public class BaseSpinner extends LinearLayout {

    private Spinner spinner;
    private TextView tvStar;
    private View fieldLine;

    private int defaultTextColor;
    private int defaultLineColor;
    private int errorColor;
    private String defaultHint;
    private String requiredHint;
    private ArrayAdapter<String> adapter;
    private AdapterView.OnItemSelectedListener onItemSelectedListener;
    private OnChangeListener onChangeListener;
    private List<String> list;

    public BaseSpinner(Context context) {
        super(context);
        create();
    }

    public BaseSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        create();
    }

    public BaseSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        create();
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void setOnTouchListener(OnTouchListener l) {
        spinner.setOnTouchListener(l);
        super.setOnTouchListener(l);
    }

    @Override
    public void setEnabled(boolean enabled) {
        spinner.setEnabled(enabled);
    }

    public void init(List<String> elements, @StringRes int hintRes, boolean isFieldRequired) {
        defaultHint = getContext().getString(hintRes);
        if (isFieldRequired) {
            requiredHint = getContext().getString(R.string.is_required, defaultHint);
        }

        list = new ArrayList<>();
        list.add(defaultHint);
        list.addAll(elements);
        adapter = new ArrayAdapter<>(getContext(), R.layout.item_base_spinner, R.id.textV, list);
        adapter.setDropDownViewResource(R.layout.item_spinner);
        spinner.setAdapter(adapter);
    }

    public void setOnItemSelectedListener(
            AdapterView.OnItemSelectedListener onItemSelectedListener) {
        this.onItemSelectedListener = onItemSelectedListener;
    }

    public void setOnChangeListener(OnChangeListener onChangeListener) {
        this.onChangeListener = onChangeListener;
    }

    public String getSelectedItem() {
        if (spinner.getSelectedItem() != null) {
            String selectedItem = spinner.getSelectedItem()
                    .toString();
            if (!selectedItem.equals(defaultHint)) {
                return selectedItem;
            }
        }
        return null;
    }

    public void setSelectedItem(final String selectedItem) {
        if (list.contains(selectedItem)) {
            spinner.setSelection(TextUtils.isEmpty(selectedItem)
                                 ? 0
                                 : adapter.getPosition(selectedItem));
        } else {
            spinner.setSelection(0);
        }
    }

    public boolean isRequiredAndEmpty(boolean pickOut) {
        if (adapter != null && isRequired() && spinner.getSelectedItemPosition() == 0) {
            if (pickOut) {
                defaultHint = requiredHint;
                initSelectedItemView();
                tvStar.setTextColor(errorColor);
            }
            return true;
        }
        return false;
    }

    private void create() {
        inflate(getContext(), R.layout.view_base_spinner, this);
        setOrientation(VERTICAL);
        spinner = findViewById(R.id.spinner);
        tvStar = findViewById(R.id.tvStar);
        fieldLine = findViewById(R.id.fieldLine);
        spinner.setId(View.generateViewId());
        defaultTextColor = ContextCompat.getColor(getContext(), R.color.hint);
        defaultLineColor = ContextCompat.getColor(getContext(), R.color.field_line);
        errorColor = ContextCompat.getColor(getContext(), R.color.yellow);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tvStar.setVisibility(isRequired() && position < 1
                                     ? VISIBLE
                                     : GONE);
                initSelectedItemView();
                if (onItemSelectedListener != null) {
                    onItemSelectedListener.onItemSelected(parent, view, position, id);
                }
                if (onChangeListener != null) {
                    onChangeListener.onChange();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void initSelectedItemView() {
        View selectedView = spinner.getSelectedView();
        if (selectedView == null) {
            return;
        }
        TextView tv = selectedView.findViewById(R.id.textV);
        ImageView iv = selectedView.findViewById(R.id.image1);
        if (spinner.getSelectedItemPosition() == 0) {
            tv.setText(defaultHint);
            if (defaultHint.equals(requiredHint)) {
                tv.setTextColor(errorColor);
                fieldLine.setBackgroundColor(errorColor);
                iv.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_down_yellow));
                return;
            }
        }
        tv.setTextColor(defaultTextColor);
        fieldLine.setBackgroundColor(defaultLineColor);
        iv.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_down_gray));
    }

    private boolean isRequired() {
        return !TextUtils.isEmpty(requiredHint);
    }
}