package com.smartdrive.technician.fragments;

import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.adapters.BaseAdapter;
import com.smartdrive.technician.adapters.EventDetailsAdapter;
import com.smartdrive.technician.managers.VideoManager;
import com.smartdrive.technician.models.Event;
import com.smartdrive.technician.models.report.Camera;
import com.smartdrive.technician.utils.DateUtils;
import com.smartdrive.technician.utils.Dialogs;
import com.smartdrive.technician.utils.Utils;
import com.smartdrive.technician.widgets.BaseRecyclerView;

import java.util.List;
import java.util.Locale;

import butterknife.Bind;

import static com.smartdrive.technician.Constants.EVENT;
import static com.smartdrive.technician.Constants.SHOW_VEHICLE_ID;
import static com.smartdrive.technician.Constants.SPACE;
import static com.smartdrive.technician.Constants.TOOLBAR_TITLE;

@Layout(id = R.layout.fragment_event_details)
public class EventDetailsFragment extends BaseFragment {

    @Bind(R.id.tvTime)
    TextView tvTime;
    @Bind(R.id.rvEventDetails)
    BaseRecyclerView rvEventDetails;

    private EventDetailsAdapter adapter;

    public static Fragment newInstance(Event event) {
        Fragment fragment = new EventDetailsFragment();
        Bundle args = new Bundle();
        args.putString(TOOLBAR_TITLE, event.name);
        args.putBoolean(SHOW_VEHICLE_ID, true);
        args.putParcelable(EVENT, event);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initView() {
        final Event event = getArguments().getParcelable(EVENT);
        if (event == null) return;
        tvTime.setText(DateUtils.longToString(event.time, DateUtils.is24HourFormat()
                ? DateUtils.TIME_FORMAT_24 : DateUtils.TIME_FORMAT_12));
        adapter = new EventDetailsAdapter();
        adapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                VideoManager.getInstance().playVideoFile(getContext(), adapter.getItem(position).videoUrl);
            }
        });
        adapter.setOnLocationClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                Camera item = adapter.getItem(position);
                Utils.openMap(getContext(), event.latitude, event.longitude, Uri.encode(item.name));
            }
        });
        rvEventDetails.setAdapter(adapter);
        adapter.update(event.cameras);
        initLocation(event);
    }

    private void initLocation(final Event event) {
        if (Utils.checkInternetConnection()) {
            new AsyncTask<Void, Void, String>() {
                @Override
                protected void onPreExecute() {
                    rvEventDetails.setVisibility(View.GONE);
                    showProgress(R.string.loading_event_location);
                }

                @Override
                protected String doInBackground(Void... voids) {
                    StringBuilder location = new StringBuilder();
                    Geocoder geocoder = new Geocoder(getContext(), Locale.US);
                    try {
                        List<Address> addresses = geocoder.getFromLocation(event.latitude, event.longitude, 1);
                        if (!addresses.isEmpty()) {
                            Address address = addresses.get(0);
                            if (!TextUtils.isEmpty(address.getSubThoroughfare())) {
                                location.append(address.getSubThoroughfare());
                                location.append(SPACE);
                            }
                            if (!TextUtils.isEmpty(address.getThoroughfare())) {
                                location.append(address.getThoroughfare());
                                location.append(SPACE);
                            }
                            if (!TextUtils.isEmpty(address.getLocality())) {
                                location.append(address.getLocality());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return location.toString();
                }

                @Override
                protected void onPostExecute(String location) {
                    adapter.setLocation(location);
                    hideProgress();
                    rvEventDetails.setVisibility(View.VISIBLE);
                }
            }.execute();
        } else {
            Dialogs.showAlert(getContext(), getString(R.string.address_location_loading_error),
                    getString(R.string.check_internet_connection), null, R.string.cancel,
                    R.string.try_again, true, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            initLocation(event);
                        }
                    }
            );
        }
    }

}