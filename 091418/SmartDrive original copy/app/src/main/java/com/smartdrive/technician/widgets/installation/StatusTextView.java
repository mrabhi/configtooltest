package com.smartdrive.technician.widgets.installation;

import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;

import com.smartdrive.technician.R;

import static android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE;

public class StatusTextView extends android.support.v7.widget.AppCompatTextView {

    private boolean isAvailable = false;

    public StatusTextView(Context context) {
        super(context);
        initView();
    }

    public StatusTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public StatusTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public static SpannableStringBuilder setColorToText(Context context,
                                                        String textWithMarkedColoredPart,
                                                        @ColorRes int colorId) {
        int ppStartIndex = textWithMarkedColoredPart.indexOf("[");
        int ppEndIndex = textWithMarkedColoredPart.indexOf("]");
        SpannableStringBuilder spannable = new SpannableStringBuilder(textWithMarkedColoredPart);
        spannable.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, colorId)),
                          ppStartIndex, ppEndIndex, SPAN_EXCLUSIVE_EXCLUSIVE);

        spannable.delete(ppStartIndex, ppStartIndex + 1);
        spannable.delete(ppEndIndex - 1, ppEndIndex);
        return spannable;
    }

    public void setTextWithStatusPrefix(String statusValue) {
        super.setText(setColorToText(getContext(),
                                     getContext().getString(R.string.status, statusValue),
                                     getColorByStatus()));
    }

    public boolean isTurnSignalAvailable() {
        return isAvailable;
    }

    public void setTurnStatus(boolean isAvailable) {
        this.isAvailable = isAvailable;
        setTextWithStatusPrefix(getContext().getString(getStatusStringByStatus()));
    }

    public void resetDefault(){
        this.isAvailable = false;
        initView();
    }

    private void initView() {
        setTextWithStatusPrefix(getContext().getString(getStatusStringByStatus()));
    }

    private @ColorRes
    int getColorByStatus() {
        if (isAvailable) {
            return R.color.green;
        } else {
            return R.color.gray_text;
        }
    }

    private @StringRes
    int getStatusStringByStatus() {
        if (isAvailable) {
            return R.string.available;
        } else {
            return R.string.unavailable;
        }
    }


}
