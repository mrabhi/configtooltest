package com.smartdrive.technician.fragments.installation;

import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.smartdrive.technician.EventsSubscriber;
import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.events.ble.installation.GpsCheckedEvent;
import com.smartdrive.technician.models.report.Accessories;
import com.smartdrive.technician.models.report.Report;
import com.smartdrive.technician.utils.CsvParser;
import com.smartdrive.technician.widgets.BaseEditText;
import com.smartdrive.technician.widgets.BaseSpinner;
import com.smartdrive.technician.widgets.installation.CheckCommunicationView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;

import static com.smartdrive.technician.Constants.REPORT;

@EventsSubscriber
@Layout(id = R.layout.fragment_accessories)
public class AccessoriesFragment extends ChildInstallationFragment {

    @Bind(R.id.cbKeypadState)
    CheckBox cbKeypadState;
    @Bind(R.id.spinKeypadMountingLocation)
    BaseSpinner spinKeypadMountingLocation;

    @Bind(R.id.cbGpsPuckState)
    CheckBox cbGpsPuckState;
    @Bind(R.id.gpsPuckDetails)
    View gpsPuckDetails;
    @Bind(R.id.spinGpsPuckMountingLocation)
    BaseSpinner spinGpsPuckMountingLocation;
    @Bind(R.id.etGpsPuckNotes)
    BaseEditText etGpsPuckNotes;
    @Bind(R.id.checkGpsView)
    CheckCommunicationView checkGpsView;

    @Bind(R.id.cbRemotePushButtonState)
    CheckBox cbRemotePushButtonState;
    @Bind(R.id.spinRemotePushButtonMountingLocation)
    BaseSpinner spinRemotePushButtonMountingLocation;

    @Override
    protected void initView() {
        cbKeypadState.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                spinKeypadMountingLocation.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });
        spinKeypadMountingLocation.init(CsvParser.parseKeypadMountingLocation(), R.string.mounting_location, false);
        cbGpsPuckState.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                gpsPuckDetails.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });
        spinGpsPuckMountingLocation.init(CsvParser.parseGpsMountingLocation(), R.string.mounting_location, false);
        etGpsPuckNotes.init(R.string.add_notes, false);
        etGpsPuckNotes.setPadding((int) getContext().getResources().getDimension(R.dimen.default_padding));
        checkGpsView.init(R.string.gps_connection, R.string.check_gps, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(R.string.checking_gps);
                bleManager.checkGps();
            }
        });
        cbRemotePushButtonState.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                spinRemotePushButtonMountingLocation.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });
        spinRemotePushButtonMountingLocation.init(CsvParser.parseRemotePushButtonMountingLocation(), R.string.mounting_location, false);
        Report report = getArguments().getParcelable(REPORT);
        if (report == null || report.accessories == null) return;
        cbKeypadState.setChecked(report.accessories.keypadConnected);
        spinKeypadMountingLocation.setSelectedItem(report.accessories.keypadMountingLocation);
        cbGpsPuckState.setChecked(report.accessories.gpsPuckConnected);
        spinGpsPuckMountingLocation.setSelectedItem(report.accessories.gpsPuckMountingLocation);
        etGpsPuckNotes.setText(report.accessories.gpsPuckNotes);
        checkGpsView.initCheckResult(report.accessories.gpsSignalStrength, report.accessories.gpsConnection);
        cbRemotePushButtonState.setChecked(report.accessories.remotePushButtonConnected);
        spinRemotePushButtonMountingLocation.setSelectedItem(report.accessories.remotePushButtonMountingLocation);
    }

    @Override
    protected void updateReport(Report report) {
        report.accessories = new Accessories();
        if (cbKeypadState.isChecked()) {
            report.accessories.keypadConnected = true;
            report.accessories.keypadMountingLocation = spinKeypadMountingLocation.getSelectedItem();
        } else {
            report.accessories.keypadConnected = false;
        }
        if (cbGpsPuckState.isChecked()) {
            report.accessories.gpsPuckConnected = true;
            report.accessories.gpsPuckMountingLocation = spinGpsPuckMountingLocation.getSelectedItem();
            report.accessories.gpsPuckNotes = etGpsPuckNotes.getText();
            report.accessories.gpsConnection = checkGpsView.getPing();
            report.accessories.gpsSignalStrength = checkGpsView.getSignalStrength();
        } else {
            report.accessories.gpsPuckConnected = false;
        }
        if (cbRemotePushButtonState.isChecked()) {
            report.accessories.remotePushButtonConnected = true;
            report.accessories.remotePushButtonMountingLocation = spinRemotePushButtonMountingLocation.getSelectedItem();
        } else {
            report.accessories.remotePushButtonConnected = false;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(GpsCheckedEvent event) {
        hideProgress();
        checkGpsView.initCheckResult(event.signalStrength, event.connection);
    }

}