package com.smartdrive.technician.events.ble.installation;

public class GpsCheckedEvent {

    public final String signalStrength;
    public final String connection;

    public GpsCheckedEvent(String signalStrength, String connection) {
        this.signalStrength = signalStrength;
        this.connection = connection;
    }
    
}