package com.smartdrive.technician.models.report;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "CommunicationProtocol")
public class CommunicationProtocol extends Model implements Parcelable {

    public static final Parcelable.Creator<CommunicationProtocol> CREATOR = new Parcelable.Creator<CommunicationProtocol>() {
        @Override
        public CommunicationProtocol createFromParcel(Parcel source) {
            return new CommunicationProtocol(source);
        }

        @Override
        public CommunicationProtocol[] newArray(int size) {
            return new CommunicationProtocol[size];
        }
    };

    public enum Type {
        DHCP,
        STATIC;

        @Override
        public String toString() {
            return name().toLowerCase();
        }
    }

    @Column
    public Type type;
    @Column
    public String ipAddress;
    @Column
    public String subnetMask;
    @Column
    public String gateway;
    @Column
    public String dns;

    public CommunicationProtocol() {
    }

    protected CommunicationProtocol(Parcel in) {
        this.type = Type.values()[in.readInt()];
        this.ipAddress = in.readString();
        this.subnetMask = in.readString();
        this.gateway = in.readString();
        this.dns = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type.ordinal());
        dest.writeString(this.ipAddress);
        dest.writeString(this.subnetMask);
        dest.writeString(this.gateway);
        dest.writeString(this.dns);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommunicationProtocol that = (CommunicationProtocol) o;
        return type == that.type
                && ((ipAddress != null ? ipAddress.equals(that.ipAddress) : that.ipAddress == null)
                && ((subnetMask != null ? subnetMask.equals(that.subnetMask) : that.subnetMask == null)
                && ((gateway != null ? gateway.equals(that.gateway) : that.gateway == null)
                && ((dns != null ? dns.equals(that.dns) : that.dns == null)))));
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + (ipAddress != null ? ipAddress.hashCode() : 0);
        result = 31 * result + (subnetMask != null ? subnetMask.hashCode() : 0);
        result = 31 * result + (gateway != null ? gateway.hashCode() : 0);
        result = 31 * result + (dns != null ? dns.hashCode() : 0);
        return result;
    }

}