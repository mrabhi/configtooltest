package com.smartdrive.technician.fragments.installation;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartdrive.technician.EventsSubscriber;
import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.controller.BleConstants;
import com.smartdrive.technician.events.ble.installation.ImageGeneratedEvent;
import com.smartdrive.technician.fragments.BaseFragment;
import com.smartdrive.technician.models.report.Camera;
import com.smartdrive.technician.utils.Utils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;

import static com.smartdrive.technician.Constants.CAMERA;
import static com.smartdrive.technician.Constants.SHOW_SERIAL_NUMBER;
import static com.smartdrive.technician.Constants.TOOLBAR_TITLE;

@EventsSubscriber
@Layout(id = R.layout.fragment_generate_image)
public class GenerateImageFragment extends BaseFragment {

    @Bind(R.id.ivGeneratedImage)
    ImageView ivGeneratedImage;
    @Bind(R.id.tvTestImage)
    TextView tvTestImage;

    private Camera camera;

    @Override
    protected void initView() {
        if (getArguments() != null) {
            camera = getArguments().getParcelable(CAMERA);
        }
        if (camera == null) {
            return;
        }
        camera.testImageBase64 = null;
        showProgress(R.string.generating_of_test_image);
        bleManager.generateTestImage(BleConstants.CameraInfo.byValue((byte) camera.cameraId));
    }

    public static GenerateImageFragment newInstance(Camera camera) {
        Bundle args = new Bundle();
        args.putParcelable(CAMERA, camera);
        args.putString(TOOLBAR_TITLE, camera.name);
        args.putBoolean(SHOW_SERIAL_NUMBER, false);
        GenerateImageFragment fragment = new GenerateImageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onBleEvent(ImageGeneratedEvent event) {
        if (camera != null && event.image != null) {
            camera.testImageBase64 = Utils.bitmapToStringBase64(event.image);
        }
        showGeneratedImage(event.image);
    }

    private void showGeneratedImage(final Bitmap image) {
        if (getActivity() == null) {
            return;
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgress();
                if (image != null) {
                    tvTestImage.setVisibility(View.VISIBLE);
                    ivGeneratedImage.setImageBitmap(image);
                }
            }
        });
    }

}