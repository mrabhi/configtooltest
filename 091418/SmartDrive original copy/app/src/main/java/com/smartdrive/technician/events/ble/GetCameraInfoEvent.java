package com.smartdrive.technician.events.ble;

import com.smartdrive.technician.controller.BleConstants;

public class GetCameraInfoEvent {
    private boolean isConnected;
    private BleConstants.CameraType cameraType;
    private String serialNumber;

    public GetCameraInfoEvent(boolean isConnected,
                              BleConstants.CameraType cameraType, String serialNumber) {
        this.isConnected = isConnected;
        this.cameraType = cameraType;
        this.serialNumber = serialNumber;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public BleConstants.CameraType getCameraType() {
        return cameraType;
    }

    public String getSerialNumber() {
        return serialNumber;
    }
}
