package com.smartdrive.technician.models;

import android.os.Parcel;
import android.os.Parcelable;

public class VehicleMeasurements implements Parcelable {
    public static final Parcelable.Creator<VehicleMeasurements> CREATOR = new Parcelable.Creator<VehicleMeasurements>() {
        @Override
        public VehicleMeasurements createFromParcel(Parcel source) {
            return new VehicleMeasurements(source);
        }

        @Override
        public VehicleMeasurements[] newArray(int size) {
            return new VehicleMeasurements[size];
        }
    };
    private static final double BOARD_OFFSET = 18.75;
    private static final double H1_OFFSET = 25;
    public static final double MIN_VEHICLE_WIDTH = 100;
    public static final double MAX_VEHICLE_WIDTH = 300;
    public static final double MIN_FRONT_OF_HOOD = 0;
    public static final double MAX_FRONT_OF_HOOD = 412;
    public static final double MIN_CAMERA_OFFSET = -50;
    public static final double MAX_CAMERA_OFFSET = 50;
    public static final double MIN_CAMERA_HEIGHT = 90;
    public static final double MAX_CAMERA_HEIGHT = 280;
    private double mVehicleWidth;
    private double mFrontOfHood;
    private double mCameraOffset;
    private boolean isOffsetRight;
    private double mCameraHeight;
    private String mMake;
    private String mModel;

    public VehicleMeasurements() {
    }

    private VehicleMeasurements(Parcel in) {
        this.mVehicleWidth = in.readDouble();
        this.mFrontOfHood = in.readDouble();
        this.mCameraOffset = in.readDouble();
        this.isOffsetRight = in.readByte() != 0;
        this.mCameraHeight = in.readDouble();
        this.mMake = in.readString();
        this.mModel = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.mVehicleWidth);
        dest.writeDouble(this.mFrontOfHood);
        dest.writeDouble(this.mCameraOffset);
        dest.writeByte(this.isOffsetRight
                       ? (byte) 1
                       : (byte) 0);
        dest.writeDouble(this.mCameraHeight);
        dest.writeString(this.mMake);
        dest.writeString(this.mModel);
    }

    public String getMake() {
        return mMake;
    }

    public void setMake(String make) {
        mMake = make;
    }

    public String getModel() {
        return mModel;
    }

    public void setModel(String model) {
        mModel = model;
    }

    public double getVehicleWidth() {
        return mVehicleWidth;
    }

    public void setVehicleWidth(double vehicleWidth) {
        mVehicleWidth = vehicleWidth;
    }

    public float getVehicleWidthFloat() {
        return (float) (mVehicleWidth * 10);
    }

    public double getFrontOfHood() {
        return mFrontOfHood;
    }

    public void setFrontOfHood(double frontOfHood) {
        mFrontOfHood = frontOfHood;
    }

    public double getCameraOffset() {
        return mCameraOffset;
    }

    public void setCameraOffset(double cameraOffset) {
        mCameraOffset = cameraOffset;
    }

    public boolean isOffsetRight() {
        return isOffsetRight;
    }

    public void setOffsetRight(boolean offsetRight) {
        isOffsetRight = offsetRight;
    }

    public double getCameraHeight() {
        return mCameraHeight;
    }

    public void setCameraHeight(double cameraHeight) {
        mCameraHeight = cameraHeight;
    }

    public boolean isVehicleWidthValid() {
        return MIN_VEHICLE_WIDTH <= mVehicleWidth && mVehicleWidth <= MAX_VEHICLE_WIDTH;
    }

    public boolean isFrontOfHoodValid() {
        return MIN_FRONT_OF_HOOD <= mFrontOfHood && mFrontOfHood <= MAX_FRONT_OF_HOOD;
    }

    public boolean isCameraOffsetValid() {
        return MIN_CAMERA_OFFSET <= getRealCameraOffset()
                && getRealCameraOffset() <= MAX_CAMERA_OFFSET;
    }

    public boolean isCameraHeightValid() {
        return MIN_CAMERA_HEIGHT <= mCameraHeight && mCameraHeight <= MAX_CAMERA_HEIGHT;
    }

    public double getBoardHeight() {
        return getCameraHeight() - BOARD_OFFSET;
    }

    public float getBoardHeightFloat() {
        return (float) ((getCameraHeight() - BOARD_OFFSET) * 10);
    }

    public double getH1() {
        return getCameraHeight() - H1_OFFSET;
    }

    public float getTy() {
        return (float) (this.mCameraHeight * 10);
    }

    public float getTx() {
        return (float) (getRealCameraOffset() * 10);
    }

    public float getTz() {
        return (float) (this.mFrontOfHood * 10);
    }

    private double getRealCameraOffset() {
        if (isOffsetRight) {
            return mCameraOffset > 0
                   ? mCameraOffset
                   : -mCameraOffset;
        } else {
            return mCameraOffset < 0
                   ? mCameraOffset
                   : -mCameraOffset;
        }
    }
}
