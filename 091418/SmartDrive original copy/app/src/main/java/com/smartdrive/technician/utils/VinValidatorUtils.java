package com.smartdrive.technician.utils;

import android.text.TextUtils;

public class VinValidatorUtils {

    private static final int VALID_VIN_LENGTH = 17;
    private static final int CHECK_DIGIT_POSITION = 8;
    private static final int VOCABULARY_DIVIDER = 10;
    private static final int TRANSLITERATE_SUM_DIVIDER = 11;
    private static final String MAP = "0123456789X";
    private static final String WEIGHTS = "8765432X098765432";
    private static final String VOCABULARY = "0123456789.ABCDEFGH..JKLMN.P.R..STUVWXYZ";

    private static int transliterate(char c) {
        return VOCABULARY.indexOf(c) % VOCABULARY_DIVIDER;
    }

    private static char getCheckDigit(String vin) {
        int sum = 0;
        for (int i = 0; i < VALID_VIN_LENGTH; ++i) {
            sum += transliterate(vin.charAt(i)) * MAP.indexOf(WEIGHTS.charAt(i));
        }
        return MAP.charAt(sum % TRANSLITERATE_SUM_DIVIDER);
    }

    public static boolean validate(String vin) {
        if (!TextUtils.isEmpty(vin)) {
            try {
                return vin.length() == VALID_VIN_LENGTH && getCheckDigit(vin) == vin.charAt(CHECK_DIGIT_POSITION);
            } catch (Throwable throwable) {
                return false;
            }
        } else {
            return false;
        }
    }
}
