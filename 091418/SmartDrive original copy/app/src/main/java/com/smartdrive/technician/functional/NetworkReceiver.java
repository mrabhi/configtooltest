package com.smartdrive.technician.functional;

public interface NetworkReceiver<T> {
    void receive(T data, boolean isSuccessful);
}
