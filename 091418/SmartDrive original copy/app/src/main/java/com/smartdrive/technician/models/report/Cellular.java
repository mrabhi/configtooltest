package com.smartdrive.technician.models.report;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "Cellular")
public class Cellular extends Model implements Parcelable {

    public static final Parcelable.Creator<Cellular> CREATOR = new Parcelable.Creator<Cellular>() {
        @Override
        public Cellular createFromParcel(Parcel source) {
            return new Cellular(source);
        }

        @Override
        public Cellular[] newArray(int size) {
            return new Cellular[size];
        }
    };

    @Column
    public String country;
    @Column
    public String carrier;
    @Column
    public String apn;

    public Cellular() {
    }

    protected Cellular(Parcel in) {
        this.country = in.readString();
        this.carrier = in.readString();
        this.apn = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.country);
        dest.writeString(this.carrier);
        dest.writeString(this.apn);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cellular cellular = (Cellular) o;
        return (country != null ? country.equals(cellular.country) : cellular.country == null)
                && ((carrier != null ? carrier.equals(cellular.carrier) : cellular.carrier == null)
                && ((apn != null ? apn.equals(cellular.apn) : cellular.apn == null)));

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (carrier != null ? carrier.hashCode() : 0);
        result = 31 * result + (apn != null ? apn.hashCode() : 0);
        return result;
    }

}