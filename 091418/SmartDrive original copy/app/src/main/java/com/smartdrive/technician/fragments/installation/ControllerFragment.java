package com.smartdrive.technician.fragments.installation;

import android.widget.EditText;
import android.widget.TextView;

import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.models.report.Controller;
import com.smartdrive.technician.models.report.Report;
import com.smartdrive.technician.utils.CsvParser;
import com.smartdrive.technician.widgets.BaseSpinner;

import butterknife.Bind;

import static com.smartdrive.technician.Constants.REPORT;
import static com.smartdrive.technician.Constants.SERIAL_NUMBER;

@Layout(id = R.layout.fragment_controller)
public class ControllerFragment extends ChildInstallationFragment {

    @Bind(R.id.tvSerialNumber)
    TextView tvSerialNumber;
    @Bind(R.id.spinMountingLocation)
    BaseSpinner spinMountingLocation;
    @Bind(R.id.etNotes)
    EditText etNotes;

    @Override
    protected void initView() {
        tvSerialNumber.setText(getActivity().getIntent().getStringExtra(SERIAL_NUMBER));
        spinMountingLocation.init(CsvParser.parseControllerMountingLocations(), R.string.mounting_location, false);
        Report report = getArguments().getParcelable(REPORT);
        if (report != null && report.controller != null) {
            spinMountingLocation.setSelectedItem(report.controller.mountingLocation);
            etNotes.setText(report.controller.notes);
        }
    }

    @Override
    protected void updateReport(Report report) {
        report.controller = new Controller();
        report.controller.mountingLocation = spinMountingLocation.getSelectedItem();
        report.controller.notes = etNotes.getText().toString().trim();
    }

}