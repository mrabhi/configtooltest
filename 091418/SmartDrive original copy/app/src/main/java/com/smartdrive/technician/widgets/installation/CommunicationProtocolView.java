package com.smartdrive.technician.widgets.installation;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.smartdrive.technician.OnChangeListener;
import com.smartdrive.technician.R;
import com.smartdrive.technician.models.report.CommunicationProtocol;
import com.smartdrive.technician.widgets.BaseEditText;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.smartdrive.technician.Constants.IP_ADDRESS_REGEXP;

public class CommunicationProtocolView extends LinearLayout {

    @Bind(R.id.rgProtocol)
    RadioGroup rgProtocol;
    @Bind(R.id.staticIpContainer)
    View staticIpContainer;
    @Bind(R.id.etIpAddress)
    BaseEditText etIpAddress;
    @Bind(R.id.etSubnetMask)
    BaseEditText etSubnetMask;
    @Bind(R.id.etGateway)
    BaseEditText etGateway;
    @Bind(R.id.etDns)
    BaseEditText etDns;

    private OnChangeListener onChangeListener;

    public CommunicationProtocolView(Context context) {
        super(context);
        create();
    }

    public CommunicationProtocolView(Context context, AttributeSet attrs) {
        super(context, attrs);
        create();
    }

    public CommunicationProtocolView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        create();
    }

    private void create() {
        inflate(getContext(), R.layout.view_communication_protocol, this);
        ButterKnife.bind(this);
        rgProtocol.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                staticIpContainer.setVisibility(checkedId == R.id.rbStaticIp ? VISIBLE : GONE);
                if (onChangeListener != null) {
                    onChangeListener.onChange();
                }
            }
        });
        etIpAddress.init(R.string.ip_address, false);
        etSubnetMask.init(R.string.subnet_mask, false);
        etGateway.init(R.string.gateway, false);
        etDns.init(R.string.dns, false);
    }

    public void init(CommunicationProtocol protocol) {
        if (protocol.type.equals(CommunicationProtocol.Type.DHCP)) {
            rgProtocol.check(R.id.rbDhcp);
        } else {
            rgProtocol.check(R.id.rbStaticIp);
            etIpAddress.setText(protocol.ipAddress);
            etSubnetMask.setText(protocol.subnetMask);
            etGateway.setText(protocol.gateway);
            etDns.setText(protocol.dns);
        }
    }

    public CommunicationProtocol getProtocol() {
        CommunicationProtocol protocol = new CommunicationProtocol();
        if (rgProtocol.getCheckedRadioButtonId() == R.id.rbStaticIp) {
            protocol.type = CommunicationProtocol.Type.STATIC;
            protocol.ipAddress = etIpAddress.getText();
            protocol.subnetMask = etSubnetMask.getText();
            protocol.gateway = etGateway.getText();
            protocol.dns = etDns.getText();
        } else {
            protocol.type = CommunicationProtocol.Type.DHCP;
        }
        return protocol;
    }

    public boolean isFieldsValid(boolean pickOut) {
        if (rgProtocol.getCheckedRadioButtonId() == R.id.rbStaticIp) {
            return etIpAddress.isValid(IP_ADDRESS_REGEXP, R.string.invalid_ip_address, pickOut)
                    && etSubnetMask.isValid(IP_ADDRESS_REGEXP, R.string.invalid_subnet_mask, pickOut)
                    && etGateway.isValid(IP_ADDRESS_REGEXP, R.string.invalid_gateway, pickOut)
                    && etDns.isValid(IP_ADDRESS_REGEXP, R.string.invalid_dns, pickOut);
        } else {
            return true;
        }
    }

    public void setOnChangeListener(OnChangeListener onChangeListener) {
        this.onChangeListener = onChangeListener;
        etIpAddress.setOnChangeListener(onChangeListener);
        etSubnetMask.setOnChangeListener(onChangeListener);
        etGateway.setOnChangeListener(onChangeListener);
        etDns.setOnChangeListener(onChangeListener);
    }

}