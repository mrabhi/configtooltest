package com.smartdrive.technician.widgets;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import com.smartdrive.technician.R;

public class BaseRecyclerView extends RecyclerView {

    private AdapterDataObserver dataObserver = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            setEmptyViewVisibility(getAdapter() != null && getAdapter().getItemCount() == 0);
        }
    };

    private int emptyViewId;
    private View emptyView;

    public BaseRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public BaseRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.BaseRecyclerView, 0, 0);
        try {
            emptyViewId = ta.getResourceId(R.styleable.BaseRecyclerView_emptyView, -1);
        } finally {
            ta.recycle();
        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        setLayoutManager(layoutManager);
    }

    @Override
    public void setVisibility(int visibility) {
        if (visibility == View.GONE) {
            setEmptyViewVisibility(false);
        }
        super.setVisibility(visibility);
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter) {
        if (emptyViewId != -1) {
            emptyView = ((Activity) getContext()).findViewById(emptyViewId);
            setEmptyViewVisibility(false);
        }
        if (getAdapter() != null) {
            getAdapter().unregisterAdapterDataObserver(dataObserver);
        }
        if (adapter != null) {
            adapter.registerAdapterDataObserver(dataObserver);
        }
        super.setAdapter(adapter);
    }

    private void setEmptyViewVisibility(boolean needShow) {
        if (emptyView != null) {
            emptyView.setVisibility(needShow ? VISIBLE : GONE);
        }
    }

}
