package com.smartdrive.technician.adapters;

import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.smartdrive.technician.R;
import com.smartdrive.technician.models.report.InterchangeItem;
import com.smartdrive.technician.widgets.BaseEditText;

import butterknife.Bind;

public class InterchangeAdapter extends BaseAdapter<InterchangeItem> {

    protected class InterchangeViewHolder extends BaseViewHolder {
        @Bind(R.id.tvName)
        TextView tvName;
        @Bind(R.id.cbState)
        CheckBox cbState;
        @Bind(R.id.etNotes)
        BaseEditText etNotes;

        private InterchangeViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
        }

        @Override
        protected void bind(int position) {
            final InterchangeItem interchangeItem = getItem(position);
            tvName.setText(tvName.getContext().getString(R.string.wire_input, interchangeItem.name));
            cbState.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        interchangeItem.connected = true;
                        etNotes.setVisibility(View.VISIBLE);
                    } else {
                        interchangeItem.connected = false;
                        etNotes.setVisibility(View.GONE);
                    }
                }
            });
            cbState.setChecked(interchangeItem.connected);
            etNotes.init(R.string.add_notes, false);
            etNotes.setText(interchangeItem.notes);
            etNotes.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    interchangeItem.notes = s.toString().trim();
                }
            });
        }
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_interchange, parent, false);
        return new InterchangeViewHolder(itemView);
    }

}