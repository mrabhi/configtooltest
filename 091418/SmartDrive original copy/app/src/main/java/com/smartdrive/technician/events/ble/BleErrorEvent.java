package com.smartdrive.technician.events.ble;

import android.text.TextUtils;

import com.smartdrive.technician.R;
import com.smartdrive.technician.SmartDriveApp;

public class BleErrorEvent {

    public final String message;

    public BleErrorEvent() {
        this(null);
    }

    public BleErrorEvent(String message) {
        if (TextUtils.isEmpty(message)) {
            this.message = SmartDriveApp.getContext().getString(R.string.try_again_later);
        } else {
            this.message = message;
        }
    }

}