package com.smartdrive.technician.fragments;

import android.support.annotation.StringRes;
import android.view.View;
import android.widget.TextView;

import com.smartdrive.technician.EventsSubscriber;
import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.activities.BaseActivity;
import com.smartdrive.technician.activities.MainActivity;
import com.smartdrive.technician.adapters.PendingReportsAdapter;
import com.smartdrive.technician.events.AddPendingReportEvent;
import com.smartdrive.technician.events.PendingReportsEvent;
import com.smartdrive.technician.events.ReportSentEvent;
import com.smartdrive.technician.managers.ReportSendingManager;
import com.smartdrive.technician.models.report.Report;
import com.smartdrive.technician.utils.Dialogs;
import com.smartdrive.technician.widgets.BaseRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

import static com.smartdrive.technician.Constants.SERIAL_NUMBER;

@EventsSubscriber
@Layout(id = R.layout.fragment_pending_reports)
public class PendingReportsFragment extends BaseFragment {

    @Bind(R.id.pendingReportsProgress)
    View pendingReportsProgress;
    @Bind(R.id.pendingReportsContent)
    View pendingReportsContent;
    @Bind(R.id.rvPendingReports)
    BaseRecyclerView rvPendingReports;

    private PendingReportsAdapter adapter;

    @Override
    protected void initView() {
        adapter = new PendingReportsAdapter();
        rvPendingReports.setAdapter(adapter);
        loadPendingReports();
    }

    @Override
    protected void showProgress(@StringRes int textRes) {
        ((TextView) pendingReportsProgress.findViewById(R.id.tvProgress)).setText(textRes);
        pendingReportsProgress.setVisibility(View.VISIBLE);
        pendingReportsContent.setVisibility(View.GONE);
    }

    @Override
    protected void hideProgress() {
        pendingReportsProgress.setVisibility(View.GONE);
        pendingReportsContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden && adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    @OnClick(R.id.tvCancel)
    public void onHide() {
        ((BaseActivity) getActivity()).hidePendingReportsFragment();
    }

    @OnClick(R.id.tvSend)
    public void onSend() {
        showProgress(R.string.sending_of_reports);
        final List<Report> reports = new ArrayList<>();
        for (int i = 0; i < adapter.getItemCount(); i++) {
            reports.add(adapter.getItem(i));
        }
        ReportSendingManager.getInstance().send(reports);
    }

    private void loadPendingReports() {
        if (getActivity() instanceof MainActivity) {
            adapter.update(Report.getListFromDB(getActivity().getIntent().getStringExtra(SERIAL_NUMBER)));
        } else {
            adapter.update(Report.getListFromDB());
        }
        EventBus.getDefault().post(new PendingReportsEvent(adapter.getItemCount()));
    }

    @Subscribe
    public void onAddPendingReport(AddPendingReportEvent event) {
        adapter.add(event.report);
        EventBus.getDefault().postSticky(new PendingReportsEvent(adapter.getItemCount()));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReportSent(ReportSentEvent event) {
        if (event.reportId == null) return;
        if (event.isSuccessfully) {
            for (int i = 0; i < adapter.getItemCount(); i++) {
                Report item = adapter.getItem(i);
                if (event.reportId.equals(item.getId())) {
                    item.deleteFromDB();
                    adapter.remove(i);
                    break;
                }
            }
            EventBus.getDefault().post(new PendingReportsEvent(adapter.getItemCount()));
            if (adapter.isEmpty()) {
                hideProgress();
                onHide();
                String title = getString(R.string.reports_successfully_sent);
                Dialogs.showToast(getContext(), title, null, R.drawable.ic_check_circle_green);
            }
        } else {
            hideProgress();
            String title = getString(R.string.unable_to_send_reports);
            Dialogs.showToast(getContext(), title, null, R.drawable.ic_warning);
        }
    }

}