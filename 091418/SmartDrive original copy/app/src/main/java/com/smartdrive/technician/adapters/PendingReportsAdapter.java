package com.smartdrive.technician.adapters;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smartdrive.technician.R;
import com.smartdrive.technician.models.report.Report;
import com.smartdrive.technician.utils.DateUtils;

import butterknife.Bind;

public class PendingReportsAdapter extends BaseAdapter<Report> {

    protected class PendingReportsHolder extends BaseViewHolder {
        @Bind(R.id.tvName)
        TextView tvName;
        @Bind(R.id.tvVehicle)
        TextView tvVehicle;
        @Bind(R.id.tvTime)
        TextView tvTime;

        private PendingReportsHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void bind(int position) {
            Report item = getItem(position);
            tvName.setText(item.serialNumber);
            if (TextUtils.isEmpty(item.vehicleId)) {
                tvVehicle.setVisibility(View.GONE);
            } else {
                tvVehicle.setText(itemView.getContext().getString(R.string.assigned_vehicle, item.vehicleId));
                tvVehicle.setVisibility(View.VISIBLE);
            }
            tvTime.setText(DateUtils.longToString(item.time, DateUtils.is24HourFormat() ?
                    DateUtils.PENDING_REPORT_TIME_FORMAT_24 : DateUtils.PENDING_REPORT_TIME_FORMAT_12));
        }
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pending_reports, parent, false);
        return new PendingReportsHolder(itemView);
    }

}
