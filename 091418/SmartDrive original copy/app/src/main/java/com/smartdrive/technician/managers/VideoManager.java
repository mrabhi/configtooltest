package com.smartdrive.technician.managers;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;

import com.smartdrive.technician.events.VideosLoadedEvent;
import com.smartdrive.technician.models.Event;
import com.smartdrive.technician.models.report.Camera;
import com.smartdrive.technician.utils.XmlHelper;

import org.greenrobot.eventbus.EventBus;
import org.kamranzafar.jtar.TarEntry;
import org.kamranzafar.jtar.TarInputStream;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import static com.smartdrive.technician.Constants.BUFFER;
import static com.smartdrive.technician.Constants.HEADER_XML;
import static com.smartdrive.technician.Constants.MP4_VIDEO;
import static com.smartdrive.technician.Constants.ROOT_FOLDER_NAME;
import static com.smartdrive.technician.Constants.SLASH;
import static com.smartdrive.technician.Constants.VIDEO_FRAME;

public class VideoManager {

    private static VideoManager instance;

    private VideoManager() {
    }

    public static VideoManager getInstance() {
        if (instance == null) {
            synchronized (VideoManager.class) {
                if (instance == null) {
                    instance = new VideoManager();
                }
            }
        }
        return instance;
    }

    public void saveVideos(String eventId, byte[] buffer) throws Exception {
        if (TextUtils.isEmpty(eventId)) {
            throw new RuntimeException();
        }
        Event event = null;
        File folder = new File(getRootFolder(), eventId);
        if (folder.exists() || folder.mkdirs()) {
            TarInputStream is = new TarInputStream(new ByteArrayInputStream(buffer));
            TarEntry tarEntry;
            while ((tarEntry = is.getNextEntry()) != null) {
                byte data[] = new byte[BUFFER];
                String fileName = tarEntry.getName().toLowerCase();
                File file = new File(folder, fileName);
                BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(file));
                int count;
                while ((count = is.read(data)) != -1) {
                    os.write(data, 0, count);
                }
                os.flush();
                os.close();
                if (fileName.equals((HEADER_XML))) {
                    event = XmlHelper.parseVideoFilesHeader(eventId, file);
                }
            }
            is.close();
        }
        if (event == null) {
            throw new RuntimeException();
        } else {
            for (Camera camera : event.cameras) {
                camera.firstFrame = getFrameFromVideo(camera.videoUrl);
            }
            EventBus.getDefault().post(new VideosLoadedEvent(event));
        }
    }

    public void getLoadedVideos() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<Event> events = new ArrayList<>();
                File rootFolder = getRootFolder();
                if (rootFolder.exists()) {
                    File[] files = rootFolder.listFiles();
                    if (files != null) {
                        for (File eventFolder : files) {
                            if (eventFolder.list().length == 0) continue;
                            for (File file : eventFolder.listFiles()) {
                                if (file.getName().toLowerCase().equals((HEADER_XML))) {
                                    Event event = XmlHelper.parseVideoFilesHeader(eventFolder.getName(), file);
                                    if (event != null) {
                                        for (Camera camera : event.cameras) {
                                            camera.firstFrame = getFrameFromVideo(camera.videoUrl);
                                        }
                                        events.add(event);
                                    }
                                }
                            }
                        }
                    }
                }
                EventBus.getDefault().post(new VideosLoadedEvent(events));

            }
        }).start();
    }

    public void playVideoFile(Context context, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(new File(url)), MP4_VIDEO);
        context.startActivity(intent);
    }

    private File getRootFolder() {
        return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + SLASH + ROOT_FOLDER_NAME);
    }

    private Bitmap getFrameFromVideo(String url) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            retriever.setDataSource(url);
            return retriever.getFrameAtTime(VIDEO_FRAME);
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            try {
                retriever.release();
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void deleteAllSessionVideos() {
        deleteFile(getRootFolder());
    }

    private void deleteFile(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File child : files) {
                    deleteFile(child);
                }
            }
        }
        file.delete();
    }

    public boolean isEnoughMemory(int byteSize) {
        return getAvailableExternalMemorySize() >= byteSize;
    }

    private long getAvailableExternalMemorySize() {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSizeLong();
            long availableBlocks = stat.getAvailableBlocksLong();
            return (availableBlocks * blockSize);
        } else {
            return -1;
        }
    }

}