package com.smartdrive.technician.fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.SmartDriveApp;
import com.smartdrive.technician.activities.BaseActivity;
import com.smartdrive.technician.adapters.BaseAdapter;
import com.smartdrive.technician.adapters.EventLibraryAdapter;
import com.smartdrive.technician.events.NavigationEvent;
import com.smartdrive.technician.events.VideosLoadedEvent;
import com.smartdrive.technician.events.ble.BleErrorEvent;
import com.smartdrive.technician.events.ble.CurrentTimeGotEvent;
import com.smartdrive.technician.events.ble.EventsLibraryLoadedEvent;
import com.smartdrive.technician.managers.VideoManager;
import com.smartdrive.technician.models.Event;
import com.smartdrive.technician.utils.DateUtils;
import com.smartdrive.technician.utils.Dialogs;
import com.smartdrive.technician.widgets.BaseRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;

import static com.smartdrive.technician.Constants.RC_STORAGE;
import static com.smartdrive.technician.Constants.TOOLBAR_TITLE;

@Layout(id = R.layout.fragment_event_library)
public class EventLibraryFragment extends BaseTriggerEventFragment {

    private enum Filter {
        ALL, EXTENDED_RECORDING, ALL_EXCEPT_EXTENDED_RECORDING;

        @Override
        public String toString() {
            Context context = SmartDriveApp.getContext();
            switch (values()[ordinal()]) {
                case EXTENDED_RECORDING:
                    return context.getString(R.string.extended_recording_only);
                case ALL_EXCEPT_EXTENDED_RECORDING:
                    return context.getString(R.string.all_except_extended_recording);
                default:
                    return context.getString(R.string.show_all);
            }
        }

        private static List<String> strList() {
            List<String> list = new ArrayList<>();
            for (int i = 0; i < values().length; i++) {
                list.add(values()[i].toString());
            }
            return list;
        }
    }

    @Bind(R.id.tvTimeInterval)
    TextView tvTimeInterval;
    @Bind(R.id.spinFilter)
    Spinner spinFilter;
    @Bind(R.id.rvEventLibrary)
    BaseRecyclerView rvEventLibrary;

    private EventLibraryAdapter adapter;
    private Filter selectedFilter;
    private Calendar filterFrom;
    private Calendar filterTo;

    public static Fragment newInstance() {
        Fragment fragment = new EventLibraryFragment();
        Bundle args = new Bundle();
        args.putString(TOOLBAR_TITLE, SmartDriveApp.getContext().getString(R.string.event_library));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initView() {
        adapter = new EventLibraryAdapter();
        adapter.setOnDownloadClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onClick(View view, final int position) {
                if (isDownloading()) return;
                Event event = adapter.getItem(position);
                event.downloaded = null;
                adapter.notifyDataSetChanged();
                bleManager.loadEventVideos(event.id);
            }
        });
        adapter.setOnPlayClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                Fragment fragment = EventDetailsFragment.newInstance(adapter.getItem(position));
                EventBus.getDefault().post(new NavigationEvent(fragment));
            }
        });
        rvEventLibrary.setAdapter(adapter);
        initFilterSpinner();
        initTimeInterval();
        if (bleManager.getEvents() != null) {
            filterFrom = bleManager.getFilterFrom();
            filterTo = bleManager.getFilterTo();
            selectedFilter = Filter.values()[bleManager.getFilter()];
            spinFilter.setSelection(bleManager.getFilter());
            updateTimeInterval();
            adapter.update(bleManager.getEvents());
            VideoManager.getInstance().getLoadedVideos();
        } else {
            checkStoragePermission();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_STORAGE) {
            checkStoragePermission();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == RC_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadEventsLibrary();
            } else {
                Dialog dialog = Dialogs.showAlert(getContext(), getString(R.string.attention),
                        getString(R.string.required_storage_permission), null, R.string.cancel,
                        R.string.open_app_settings, true, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                BaseActivity activity = (BaseActivity) getActivity();
                                if (activity != null) {
                                    activity.showAppSettings(RC_STORAGE);
                                }
                            }
                        }
                );
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        loadEventsLibrary();
                    }
                });
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void checkStoragePermission() {
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            loadEventsLibrary();
        } else {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, RC_STORAGE);
        }
    }

    @Override
    protected View createToolbarButton() {
        ImageView view = new ImageView(getContext());
        view.setImageResource(R.drawable.selector_btn_add);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                triggerManualEvent();
            }
        });
        return view;
    }

    private void initFilterSpinner() {
        selectedFilter = Filter.ALL;
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.item_filter_spinner, Filter.strList());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinFilter.setAdapter(adapter);
        spinFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Filter filter = Filter.values()[position];
                if (filter != selectedFilter) {
                    if (isDownloading()) {
                        spinFilter.setSelection(selectedFilter.ordinal());
                    } else {
                        selectedFilter = filter;
                        checkStoragePermission();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void initTimeInterval() {
        tvTimeInterval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date localDateFilterFrom = filterFrom.getTime();
                Date localDateFilterTo = filterTo.getTime();
                Dialog dialog = Dialogs.showTimeIntervalPicker(getContext(),
                        !isTimeIntervalInitialized(),
                        localDateFilterFrom.getYear() + 1900,
                        localDateFilterFrom.getMonth(),
                        localDateFilterFrom.getDate(),
                        localDateFilterFrom.getHours(),
                        localDateFilterFrom.getMinutes(),
                        localDateFilterTo.getHours(),
                        localDateFilterTo.getMinutes(),
                        new Dialogs.OnTimeIntervalChangedListener() {
                            @Override
                            public void onTimeIntervalChanged(int year, int month, int day,
                                                              int fromHour, int fromMinute,
                                                              int toHour, int toMinute) {
                                if (isDownloading()) return;
                                year -= 1900;
                                filterFrom.setTime(new Date(year, month, day, fromHour, fromMinute));
                                filterTo.setTime(new Date(year, month, day, toHour, toMinute));
                                updateTimeInterval();
                                checkStoragePermission();
                            }
                        }
                );
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (isTimeIntervalInitialized()) return;
                        getActivity().onBackPressed();
                    }
                });
            }
        });
    }

    private boolean isTimeIntervalInitialized() {
        return tvTimeInterval.getText().length() != 0;
    }

    private void updateTimeInterval() {
        tvTimeInterval.setText(DateUtils.timeIntervalToString(filterFrom, filterTo));
    }

    private void loadEventsLibrary() {
        if (isDownloading()) return;
        showProgress(R.string.loading_event_library);
        rvEventLibrary.setVisibility(View.GONE);
        if (filterFrom == null) {
            bleManager.getCurrentTime();
        } else {
            bleManager.loadEventsLibrary(filterFrom, filterTo, (byte) selectedFilter.ordinal());
        }
    }

    private boolean isDownloading() {
        for (int i = 0; i < adapter.getItemCount(); i++) {
            Event item = adapter.getItem(i);
            if (item.downloaded == null) {
                Dialogs.showToast(getContext(), getString(R.string.error), getString(R.string.waiting_attention));
                return true;
            }
        }
        return false;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onVideosLoadedEvent(VideosLoadedEvent videosLoadedEvent) {
        for (Event event : videosLoadedEvent.events) {
            for (int i = 0; i < adapter.getItemCount(); i++) {
                Event item = adapter.getItem(i);
                if (item.id.equals(event.id)) {
                    item.latitude = event.latitude;
                    item.longitude = event.longitude;
                    item.cameras = event.cameras;
                    item.downloaded = true;
                    break;
                }
            }
        }
        hideProgress();
        rvEventLibrary.setVisibility(View.VISIBLE);
        adapter.notifyDataSetChanged();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(EventsLibraryLoadedEvent event) {
        adapter.update(event.eventsLibrary);
        VideoManager.getInstance().getLoadedVideos();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(BleErrorEvent event) {
        for (int i = 0; i < adapter.getItemCount(); i++) {
            Event item = adapter.getItem(i);
            if (item.downloaded == null) {
                item.downloaded = false;
                adapter.notifyDataSetChanged();
                break;
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(CurrentTimeGotEvent event) {
        hideProgress();
        filterTo = event.currentTime;
        filterFrom = Calendar.getInstance();
        filterFrom.setTime(filterTo.getTime());
        filterFrom.setTimeZone(filterTo.getTimeZone());
        filterFrom.add(Calendar.HOUR_OF_DAY, -1);
        tvTimeInterval.performClick();
    }

}