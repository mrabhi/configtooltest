package com.smartdrive.technician.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.smartdrive.technician.models.report.Camera;

import java.util.List;

public class Event implements Parcelable {

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel source) {
            return new Event(source);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    public String id;
    public String name;
    public long time;
    public double latitude;
    public double longitude;
    public List<Camera> cameras;
    public Boolean downloaded = false;

    public Event() {
    }

    protected Event(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.time = in.readLong();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.cameras = in.createTypedArrayList(Camera.CREATOR);
        this.downloaded = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeLong(this.time);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeTypedList(this.cameras);
        dest.writeValue(this.downloaded);
    }

    @Override
    public int describeContents() {
        return 0;
    }

}