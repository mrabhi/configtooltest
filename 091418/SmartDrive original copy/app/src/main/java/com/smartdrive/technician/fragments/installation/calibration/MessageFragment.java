package com.smartdrive.technician.fragments.installation.calibration;


import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.activities.AdasCalibrationActivity;
import com.smartdrive.technician.fragments.BaseFragment;
import com.smartdrive.technician.managers.CalibrationManager;

import butterknife.Bind;

@Layout(id = R.layout.fragment_adas_messages)
public class MessageFragment extends BaseFragment {
    private final static String RES = "res";

    @Bind(R.id.vContinue)
    View vContinue;


    @Bind(R.id.vStatusOk)
    View vStatusOk;
    @Bind(R.id.tvStatusOkMessage)
    TextView tvStatusOkMessage;

    @Bind(R.id.vVertical)
    View vVertical;
    @Bind(R.id.tvVerticalMessage)
    TextView tvVerticalMessage;

    @Bind(R.id.vVerticalNo)
    View vVerticalNo;
    @Bind(R.id.vVerticalHelp1)
    View vVerticalNoHelp1;
    @Bind(R.id.vVerticalHelp2)
    View vVerticalNoHelp2;
    @Bind(R.id.vVerticalHelp3)
    View vVerticalNoHelp3;
    @Bind(R.id.tvVerticalNoMessage)
    TextView tvVerticalNoMessage;

    @Bind(R.id.vDefault)
    View vDefault;
    @Bind(R.id.tvDefaultMessage)
    TextView tvDefaultMessage;

    @Bind(R.id.vAngle)
    View vAngle;
    @Bind(R.id.vAngleHelp1)
    View vAngleHelp1;
    @Bind(R.id.vAngleHelp2)
    View vAngleHelp2;

    AdasCalibrationActivity activity;
    CalibrationManager.Status mStatus;

    @Override
    protected void initView() {
        activity = (AdasCalibrationActivity) getActivity();
        if (getArguments() != null && getArguments().containsKey(RES)) {
            mStatus = (CalibrationManager.Status) getArguments().getSerializable(RES);

            vVerticalNo.setVisibility(View.GONE);
            vVertical.setVisibility(View.GONE);
            vDefault.setVisibility(View.GONE);
            vAngle.setVisibility(View.GONE);
            vStatusOk.setVisibility(View.VISIBLE);
            vContinue.setVisibility(View.GONE);

            if (mStatus != null) {
                switch (mStatus) {
                    case OK:
                        tvStatusOkMessage.setText(mStatus.getTextRes());
                        vStatusOk.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                activity.finishCalibration();
                            }
                        }, 2000);
                        break;
                    case STEP_COMPLETE:
                        tvStatusOkMessage.setText(
                                getResources().getString(mStatus.getTextRes(), activity.getStep()
                                        .ordinal() + 1));
                        vStatusOk.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                activity.finishCalibration();
                            }
                        }, 2000);
                        break;
                    case ALL_DONE:
                        tvStatusOkMessage.setText(R.string.calibration_done);
                        vStatusOk.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                activity.finishCalibration();
                            }
                        }, 2000);
                        break;
                    case NOT_CENTERED:
                        vVerticalNo.setVisibility(View.GONE);
                        vVertical.setVisibility(View.VISIBLE);
                        vDefault.setVisibility(View.GONE);
                        vAngle.setVisibility(View.GONE);
                        vStatusOk.setVisibility(View.GONE);
                        vContinue.setVisibility(View.VISIBLE);

                        tvVerticalMessage.setText(
                                getResources().getString(mStatus.getTextRes(), activity.getH1()));
                        break;
                    case VERTICAL_NO:
                        vVerticalNo.setVisibility(View.VISIBLE);
                        vVertical.setVisibility(View.GONE);
                        vDefault.setVisibility(View.GONE);
                        vAngle.setVisibility(View.GONE);
                        vStatusOk.setVisibility(View.GONE);
                        vContinue.setVisibility(View.VISIBLE);
                        tvVerticalNoMessage.setText(
                                getResources().getString(mStatus.getTextRes(), activity.getH1()));
                        break;
                    case INCORRECT_MOUNT_ANGLE:
                        vVerticalNo.setVisibility(View.GONE);
                        vVertical.setVisibility(View.GONE);
                        vAngle.setVisibility(View.VISIBLE);
                        vDefault.setVisibility(View.GONE);
                        vStatusOk.setVisibility(View.GONE);
                        vContinue.setVisibility(View.VISIBLE);
                        break;
                    case MISMATCH_HEIGHT:
                    case ERROR_INVALID_IMAGE:
                        vVerticalNo.setVisibility(View.GONE);
                        vVertical.setVisibility(View.GONE);
                        vAngle.setVisibility(View.GONE);
                        vDefault.setVisibility(View.VISIBLE);
                        vStatusOk.setVisibility(View.GONE);
                        vContinue.setVisibility(View.VISIBLE);
                        tvDefaultMessage.setText(mStatus.getTextRes());
                        break;
                    default:
                        break;
                }
            }
        }

        vContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activity != null) {
                    activity.openFragment(new InstructionsFragment());
                }
            }
        });

        //todo
        vAngleHelp1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, "step 1 angle help", Toast.LENGTH_SHORT)
                        .show();
            }
        });
        vAngleHelp2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, "step 2 angle help", Toast.LENGTH_SHORT)
                        .show();
            }
        });

        //todo
        vVerticalNoHelp1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, "step 1 vertical help", Toast.LENGTH_SHORT)
                        .show();
            }
        });
        vVerticalNoHelp2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, "step 2 vertical help", Toast.LENGTH_SHORT)
                        .show();
            }
        });
        vVerticalNoHelp3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, "step 3 vertical help", Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    public static MessageFragment newInstance(CalibrationManager.Status status) {
        Bundle args = new Bundle();
        args.putSerializable(RES, status);
        MessageFragment fragment = new MessageFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
