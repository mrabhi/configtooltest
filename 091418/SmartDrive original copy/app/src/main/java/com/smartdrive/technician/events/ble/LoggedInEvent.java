package com.smartdrive.technician.events.ble;

import com.smartdrive.technician.controller.BleConstants;
import com.smartdrive.technician.models.Device;

public class LoggedInEvent {

    public final Device device;
    public final byte mode;

    public LoggedInEvent(Device device, byte mode) {
        this.device = device;
        this.mode = mode;
    }

    public boolean isInvalidPassword() {
        return mode == BleConstants.AuthenticationMode.INVALID_PASSWORD;
    }

}