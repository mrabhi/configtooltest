package com.smartdrive.technician.models;

public class Device {

    private static final String SR4 = "SR4 ";
    private static final String SEPARATOR = "-";

    public String name;
    public String serialNumber;
    public String vehicleId;

    public Device(String name) {
        this.name = name;
        String[] parts = name.split(SEPARATOR);
        serialNumber = SR4 + parts[0];
        if (parts.length > 1) {
            vehicleId = parts[1];
        }
    }

    @Override
    public String toString() {
        return name;
    }

}