package com.smartdrive.technician;

public interface OnChangeListener {

    void onChange();

}