package com.smartdrive.technician.widgets;

public enum AdasMeasurement {
    VEHICLE_WIDTH, FRONT_OF_HOOD, CAMERA_OFFSET, CAMERA_HEIGHT
}
