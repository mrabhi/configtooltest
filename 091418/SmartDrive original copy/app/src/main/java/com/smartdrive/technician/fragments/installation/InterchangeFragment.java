package com.smartdrive.technician.fragments.installation;

import android.support.v7.widget.RecyclerView;

import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.adapters.InterchangeAdapter;
import com.smartdrive.technician.models.report.InterchangeItem;
import com.smartdrive.technician.models.report.Report;
import com.smartdrive.technician.utils.CsvParser;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

import static com.smartdrive.technician.Constants.REPORT;

@Layout(id = R.layout.fragment_interchange)
public class InterchangeFragment extends ChildInstallationFragment {

    @Bind(R.id.rvInterchange)
    RecyclerView rvInterchange;

    private InterchangeAdapter adapter;

    @Override
    protected void initView() {
        adapter = new InterchangeAdapter();
        rvInterchange.setAdapter(adapter);
        Report report = getArguments().getParcelable(REPORT);
        if (report == null || report.interchangeItems == null || report.interchangeItems.isEmpty()) {
            adapter.update(CsvParser.parseInterchange());
        } else {
            adapter.update(report.interchangeItems);
        }
    }

    @Override
    protected void updateReport(Report report) {
        List<InterchangeItem> interchangeItems = new ArrayList<>();
        for (int i = 0; i < adapter.getItemCount(); i++) {
            interchangeItems.add(adapter.getItem(i));
        }
        report.interchangeItems = interchangeItems;
    }

}