package com.smartdrive.technician.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.smartdrive.technician.R;

import static com.smartdrive.technician.Constants.AUTO_SCAN;
import static com.smartdrive.technician.Constants.SPLASH_DELAY;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isFinishing()) return;
                Intent intent = new Intent(SplashActivity.this, ConnectionActivity.class);
                intent.putExtra(AUTO_SCAN, true);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        }, SPLASH_DELAY);
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

}