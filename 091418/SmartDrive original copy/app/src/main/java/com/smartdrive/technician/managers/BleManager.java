package com.smartdrive.technician.managers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.smartdrive.technician.R;
import com.smartdrive.technician.SmartDriveApp;
import com.smartdrive.technician.controller.BleConstants;
import com.smartdrive.technician.controller.BleController;
import com.smartdrive.technician.controller.BleControllerInterface;
import com.smartdrive.technician.controller.BleException;
import com.smartdrive.technician.controller.BluetoothClassicController;
import com.smartdrive.technician.events.ble.AdasCheckGeneratedEvent;
import com.smartdrive.technician.events.ble.AdasFlagCheckedEvent;
import com.smartdrive.technician.events.ble.AdasImageGeneratedEvent;
import com.smartdrive.technician.events.ble.BleErrorEvent;
import com.smartdrive.technician.events.ble.CurrentTimeGotEvent;
import com.smartdrive.technician.events.ble.DebugInfoLoadedEvent;
import com.smartdrive.technician.events.ble.EventsLibraryLoadedEvent;
import com.smartdrive.technician.events.ble.GetCameraInfoEvent;
import com.smartdrive.technician.events.ble.LoggedInEvent;
import com.smartdrive.technician.events.ble.LoggedOutEvent;
import com.smartdrive.technician.events.ble.ScanCompletedEvent;
import com.smartdrive.technician.events.ble.ScanResultEvent;
import com.smartdrive.technician.events.ble.SelfCheckGeneratedEvent;
import com.smartdrive.technician.events.ble.SyncCompletedEvent;
import com.smartdrive.technician.events.ble.TestEventGeneratedEvent;
import com.smartdrive.technician.events.ble.installation.CheckTurnSignalEvent;
import com.smartdrive.technician.events.ble.installation.CommunicationSettingsGotEvent;
import com.smartdrive.technician.events.ble.installation.CommunicationSettingsSetEvent;
import com.smartdrive.technician.events.ble.installation.CommunicationSettingsTestedEvent;
import com.smartdrive.technician.events.ble.installation.GpsCheckedEvent;
import com.smartdrive.technician.events.ble.installation.ImageGeneratedEvent;
import com.smartdrive.technician.events.ble.installation.SensorBarCheckedEvent;
import com.smartdrive.technician.events.ble.installation.VehicleIdEvent;
import com.smartdrive.technician.models.AdasCheck;
import com.smartdrive.technician.models.DebugInfoItem;
import com.smartdrive.technician.models.Device;
import com.smartdrive.technician.models.Event;
import com.smartdrive.technician.models.SelfCheck;
import com.smartdrive.technician.models.report.CommunicationSettings;
import com.smartdrive.technician.utils.XmlHelper;

import org.apache.commons.lang3.StringUtils;
import org.greenrobot.eventbus.EventBus;

import java.util.Calendar;
import java.util.List;

import static com.smartdrive.technician.Constants.ADASCHECK_SECTION_OPEN;
import static com.smartdrive.technician.Constants.SECTION_CLOSE;
import static com.smartdrive.technician.Constants.SELFCHECK_SECTION_OPEN;

public class BleManager implements BleControllerInterface {

    private static BleManager instance;

    private BleController bleController;
    private Device device;
    private String devicePassword;
    private int devicePosition;
    private String serialNumber;
    private String vehicleId;
    private CommunicationSettings communicationSettings;
    private boolean needTestCommunicationSettings;
    private boolean needGenerateSelfCheck;
    private boolean needGenerateADASCheck;
    private long selfCheckRunTime;
    private long adasCheckRunTime;
    private List<DebugInfoItem> debugInfo;
    private String eventId;
    private BleConstants.DistanceCaptured mDistanceCaptured;
    private List<Event> events;
    private Calendar filterFrom;
    private Calendar filterTo;
    private byte filter;

    private BluetoothClassicController btClassicController;
    private byte[] btClassicBuffer;
    private int btClassicBufferLength;
    private int btClassicBufferLocation;

    private int tempCounter;

    @SuppressLint("HandlerLeak")
    private BleManager() {
        bleController = new BleController(SmartDriveApp.getContext(), this);
        btClassicController = new BluetoothClassicController(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case BluetoothClassicController.CREATE_RFCOMM_SOCKET_ERROR:
                    case BluetoothClassicController.SOCKET_CONNECTION_ERROR:
                    case BluetoothClassicController.INPUT_OUTPUT_STREAM_ERROR:
                    case BluetoothClassicController.DATA_RECEIVED_ERROR:
                    case BluetoothClassicController.MESSAGE_RECEIVE_TIMEOUT:
                        eventId = null;
                        postEvent(new BleErrorEvent());
                        break;
                    case BluetoothClassicController.STATE_CONNECTED:
                        if (mDistanceCaptured == null) {
                            loadEventVideos();
                        } else {
                            captureAdasImage(mDistanceCaptured);
                        }
                        break;
                    case BluetoothClassicController.MESSAGE_READ:
                        byte[] incomingData = (byte[]) msg.obj;
                        int bytesReceived = msg.arg1;
                        if ((btClassicBufferLocation + bytesReceived) > btClassicBufferLength) {
                            btClassicController.stop();
                            eventId = null;
                            postEvent(new BleErrorEvent());
                        } else {
                            tempCounter++;
                            if (tempCounter == 1) {
                                System.out.println(
                                        "DOWNLOADING: " + (btClassicBufferLocation / 1024) + "KB / "
                                                + (btClassicBufferLength / 1024) + "KB");
                            } else if (tempCounter == 100) {
                                tempCounter = 0;
                            }
                            System.arraycopy(incomingData, 0, btClassicBuffer,
                                             btClassicBufferLocation, bytesReceived);
                            btClassicBufferLocation += bytesReceived;
                            if (btClassicBufferLocation == btClassicBufferLength) {
                                btClassicController.finishDownload();

                                if (mDistanceCaptured == null) {
                                    try {
                                        VideoManager.getInstance()
                                                .saveVideos(eventId, btClassicBuffer);
                                    } catch (Exception e) {
                                        EventBus.getDefault()
                                                .post(new BleErrorEvent());
                                    }
                                    eventId = null;
                                } else {
                                    EventBus.getDefault()
                                            .post(new AdasImageGeneratedEvent(btClassicBuffer));
                                    mDistanceCaptured = null;
                                }
                            }
                        }
                        break;
                }
            }
        });
    }

    @Override
    public void scanResult(String deviceName) {
        postEvent(new ScanResultEvent(deviceName));
    }

    @Override
    public void scanCompleted() {
        postEvent(new ScanCompletedEvent());
    }

    @Override
    public void connectionComplete() {
        try {
            bleController.executeAuthenticate_Login(devicePassword);
        } catch (Exception exception) {
            postEvent(new BleErrorEvent());
        }
    }

    @Override
    public void connectionDisconnected(BleException.DisconnectException exception) {
        btClassicController.unpairAndRemoveBluetoothDevice();
        postEvent(new LoggedOutEvent(exception));
    }

    @Override
    public void responseSegmentReceived(int segmentNumber) {
    }

    @Override
    public void authenticateResponse_Login(Exception exception, byte mode) {
        if (!checkError(exception)) {
            postEvent(new LoggedInEvent(device, mode));
        }
    }

    @Override
    public void authenticateResponse_GetCurrentTime(Exception exception, Calendar date) {
        if (!checkError(exception)) {
            if (needGenerateSelfCheck) {
                selfCheckRunTime = date.getTimeInMillis();
                try {
                    bleController.executeSelfCheck_GenerateSelfCheck();
                } catch (Exception e) {
                    postEvent(new BleErrorEvent());
                }
            } else if (needGenerateADASCheck) {
                adasCheckRunTime = date.getTimeInMillis();
                try {
                    bleController.executeSelfCheck_GenerateSelfCheck();
                } catch (Exception e) {
                    postEvent(new BleErrorEvent());
                }
            } else {
                postEvent(new CurrentTimeGotEvent(date));
            }
        }
        needGenerateSelfCheck = false;
        needGenerateADASCheck = false;
    }

    @Override
    public void authenticateResponse_Logout(Exception exception) {
        checkError(exception);
    }

    @Override
    public void authenticateResponse_SyncComplete(Exception exception) {
        if (!checkError(exception)) {
            postEvent(new SyncCompletedEvent());
        }
    }

    @Override
    public void commandResponse_SetVehicleId(Exception exception) {
        if (!checkError(exception)) {
            postEvent(new VehicleIdEvent(vehicleId));
        }
    }

    @Override
    public void commandResponse_GetVehicleId(Exception exception, String vehicleId) {
        if (!checkError(exception)) {
            postEvent(new VehicleIdEvent(vehicleId));
        }
    }


    //---BleController API---

    @Override
    public void commandResponse_GetControllerSerialNumber(Exception exception,
                                                          String serialNumber) {
    }

    @Override
    public void commandResponse_CheckSensorBar(Exception exception, String message) {
        if (!checkError(exception)) {
            postEvent(new SensorBarCheckedEvent(message));
        }
    }

    @Override
    public void commandResponse_GetAdasFlag(Exception exception, Boolean adasFlag) {
        if (!checkError(exception)) {
            postEvent(new AdasFlagCheckedEvent(adasFlag));
        }
    }

    @Override
    public void commandResponse_GetCameraInfo(Exception exception, boolean isConnected,
                                              BleConstants.CameraType cameraType,
                                              String serialNumber) {
        if (!checkError(exception)) {
            postEvent(new GetCameraInfoEvent(isConnected, cameraType, serialNumber));
        }
    }

    @Override
    public void commandResponse_CheckTurnSignal(Exception exception, boolean isAvailable) {
        if (!checkError(exception)) {
            postEvent(new CheckTurnSignalEvent(isAvailable));
        }
    }

    @Override
    public void commandResponse_GetCommunicationsSetting(Exception exception, String xml) {
        if (!checkError(exception)) {
            CommunicationSettings settings = XmlHelper.parseCommunicationSettings(xml);
            postEvent(new CommunicationSettingsGotEvent(settings));
        }
    }

    @Override
    public void commandResponse_SetCommunicationsSetting(Exception exception) {
        postEvent(new CommunicationSettingsSetEvent(communicationSettings, exception != null,
                                                    needTestCommunicationSettings));
        if (exception == null && needTestCommunicationSettings) {
            testWithoutSetCommunicationSettings();
        }
    }

    @Override
    public void commandResponse_TestCommunicationsSetting(Exception exception, Boolean successFlag,
                                                          byte signalStrength) {
        if (!checkError(exception)) {
            Context context = SmartDriveApp.getContext();
            String signalStrengthStr;
            if (signalStrength < 1) {
                signalStrengthStr = context.getString(R.string.none);
            } else if (signalStrength < 2) {
                signalStrengthStr = context.getString(R.string.low);
            } else if (signalStrength < 4) {
                signalStrengthStr = context.getString(R.string.medium);
            } else {
                signalStrengthStr = context.getString(R.string.high);
            }
            String ping;
            if (successFlag != null && successFlag) {
                ping = context.getString(R.string.successful);
            } else {
                ping = context.getString(R.string.failed);
            }
            postEvent(new CommunicationSettingsTestedEvent(signalStrengthStr, ping));
        }
    }

    @Override
    public void commandResponse_GenerateImageOnCamera(Exception exception, Bitmap bitmap) {
        if (!checkError(exception)) {
            postEvent(new ImageGeneratedEvent(bitmap));
        }
    }

    @Override
    public void commandResponse_GenerateTestEvent(Exception exception) {
        if (!checkError(exception)) {
            postEvent(new TestEventGeneratedEvent());
        }
    }

    @Override
    public void commandResponse_ViewEventLibrary(Exception exception, String xml) {
        if (!checkError(exception)) {
            xml = xml.replaceAll(">\\s*<", "><");
            events = XmlHelper.parseEventsLibrary(xml);
            if (eventId != null) {
                for (Event event : events) {
                    if (eventId.equals(event.id)) {
                        event.downloaded = null;
                        break;
                    }
                }
            }
            postEvent(new EventsLibraryLoadedEvent(events));
        }
    }

    @Override
    public void commandResponse_InitiateBluetoothFileSharing(Exception exception, String macAddress,
                                                             String uuid) {
        if (!checkError(exception)) {
            btClassicController.connectToDeviceAtAddress(macAddress, uuid);
        } else {
            eventId = null;
        }
    }

    @Override
    public void commandResponse_RequestEventOverBluetooth(Exception exception, int byteSize) {
        btClassicBuffer = new byte[byteSize];
        btClassicBufferLength = byteSize;
        btClassicBufferLocation = 0;
        if (!checkError(exception)) {
            if (!VideoManager.getInstance()
                    .isEnoughMemory(byteSize)) {
                postEvent(new BleErrorEvent(SmartDriveApp.getContext()
                                                    .getString(R.string.not_enough_memory_space)));
                btClassicController.stop();
                eventId = null;
            } else {
                btClassicController.startDownload();
            }
        } else {
            eventId = null;
        }
    }

    @Override
    public void commandResponse_RequestAdasImageOverBluetooth(Exception exception, int byteSize) {
        btClassicBuffer = new byte[byteSize];
        btClassicBufferLength = byteSize;
        btClassicBufferLocation = 0;
        if (!checkError(exception)) {
            btClassicController.startDownload();
        } else {
            mDistanceCaptured = null;
        }
    }

    @Override
    public void commandResponse_SendAdasStats(Exception exception) {
        checkError(exception);
    }

    @Override
    public void commandResponse_CheckGps(Exception exception, byte testResult,
                                         byte signalStrength) {
        if (!checkError(exception)) {
            Context context = SmartDriveApp.getContext();
            String signalStrengthStr;
            if (signalStrength < 1) {
                signalStrengthStr = context.getString(R.string.none);
            } else if (signalStrength < 2) {
                signalStrengthStr = context.getString(R.string.low);
            } else if (signalStrength < 4) {
                signalStrengthStr = context.getString(R.string.medium);
            } else {
                signalStrengthStr = context.getString(R.string.high);
            }
            String connection;
            if (testResult < 1) {
                connection = context.getString(R.string.success);
            } else if (testResult < 2) {
                connection = context.getString(R.string.fail_gps);
            } else {
                connection = context.getString(R.string.fail_nmea);
            }
            postEvent(new GpsCheckedEvent(signalStrengthStr, connection));
        }
    }

    @Override
    public void commandResponse_GetDebugInformation(Exception exception, String xml) {
        if (!checkError(exception)) {
            xml = xml.replaceAll(">\\s*<", "><");
            debugInfo = XmlHelper.parseDebugInfo(xml);
            postEvent(new DebugInfoLoadedEvent(debugInfo));
        }
    }

    @Override
    public void selfCheckResponse_GenerateSelfCheck(Exception exception) {
        checkError(exception);
    }

    @Override
    public void selfCheckResponse_SelfCheckComplete(Exception exception, String xml) {
        if (!checkError(exception)) {
            xml = xml.replaceAll(">\\s*<", "><");
            if (selfCheckRunTime!=0) {
                SelfCheck selfCheck = XmlHelper.parseSelfCheck(xml.replace(StringUtils.substringBetween(xml, ADASCHECK_SECTION_OPEN, SECTION_CLOSE),""));
                selfCheck.serialNumber = serialNumber;
                selfCheck.runTime = selfCheckRunTime;
                selfCheck.saveToDB();

                postEvent(new SelfCheckGeneratedEvent(selfCheck));
            } else if (adasCheckRunTime!=0) {
                AdasCheck adasCheck = XmlHelper.parseAdasCheck(xml.replace(StringUtils.substringBetween(xml, SELFCHECK_SECTION_OPEN, SECTION_CLOSE),""));
                adasCheck.serialNumber = serialNumber;
                adasCheck.runTime = adasCheckRunTime;
                adasCheck.saveToDB();

                postEvent(new AdasCheckGeneratedEvent(adasCheck));
            }
        }
        selfCheckRunTime = 0;
        adasCheckRunTime = 0;
    }

    public static BleManager getInstance() {
        if (instance == null) {
            synchronized (BleManager.class) {
                if (instance == null) {
                    instance = new BleManager();
                }
            }
        }
        return instance;
    }

    public void clear() {
        device = null;
        devicePassword = null;
        devicePosition = -1;
        serialNumber = null;
        vehicleId = null;
        communicationSettings = null;
        needTestCommunicationSettings = false;
        needGenerateSelfCheck = false;
        selfCheckRunTime = 0;
        debugInfo = null;
        eventId = null;
        events = null;
        filterFrom = null;
        filterTo = null;
        filter = 0;
    }

    public boolean isGuestMode(byte mode) {
        return mode == BleConstants.AuthenticationMode.GUEST_MODE;
    }

    public boolean isMaintenanceMode(byte mode) {
        return mode == BleConstants.AuthenticationMode.MAINTENANCE_USER;
    }

    public boolean isLoggedIn() {
        return device != null;
    }

    public List<Event> getEvents() {
        return events;
    }

    public byte getFilter() {
        return filter;
    }


    //---BleControllerInterface callbacks---

    public Calendar getFilterFrom() {
        return filterFrom;
    }

    public Calendar getFilterTo() {
        return filterTo;
    }

    public boolean isBluetoothEnabled() {
        return bleController.isBluetoothEnabled();
    }

    public void startScan() {
        bleController.startScan();
    }

    public void login(int devicePosition, Device device, String devicePassword) {
        this.devicePosition = devicePosition;
        this.device = device;
        this.devicePassword = devicePassword;
        bleController.connectToDevice(devicePosition);
    }

    public void logout() {
        try {
            bleController.executeAuthenticate_Logout();
        } catch (Exception exception) {
            postEvent(new BleErrorEvent());
        }
    }

    public void reconnect() {
        bleController.connectToDevice(devicePosition);
    }

    public void getCurrentTime() {
        try {
            bleController.executeAuthenticate_GetCurrentTime();
        } catch (Exception exception) {
            needGenerateSelfCheck = false;
            postEvent(new BleErrorEvent());
        }
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
        try {
            bleController.executeCommand_SetVehicleId(vehicleId);
        } catch (Exception exception) {
            postEvent(new BleErrorEvent());
        }
    }

    public void checkSensorBar() {
        try {
            bleController.executeCommand_CheckSensorBar();
        } catch (Exception exception) {
            postEvent(new BleErrorEvent());
        }
    }

    public void checkGps() {
        try {
            bleController.executeCommand_CheckGps();
        } catch (Exception exception) {
            postEvent(new BleErrorEvent());
        }
    }


    public void sendAdasStats(String xml) {
        try {
            bleController.executeCommand_SendAdasStats(xml);
        } catch (Exception exception) {
            postEvent(new BleErrorEvent());
        }
    }


    public void checkAdas() {
        try {
            bleController.executeCommand_GetAdasFlag();
        } catch (Exception exception) {
            postEvent(new BleErrorEvent());
        }
    }

    public void getCameraInfo(BleConstants.CameraInfo camera) {
        try {
            bleController.executeCommand_GetCameraInfo(camera);
        } catch (Exception exception) {
            postEvent(new BleErrorEvent());
        }
    }

    public void getCommunicationSettings() {
        try {
            bleController.executeCommand_GetCommunicationsSetting();
        } catch (Exception exception) {
            postEvent(new BleErrorEvent());
        }
    }

    public void setCommunicationSettings(CommunicationSettings settings,
                                         boolean needTestCommunicationSettings) {
        this.communicationSettings = settings;
        this.needTestCommunicationSettings = needTestCommunicationSettings;
        try {
            bleController.executeCommand_SetCommunicationsSetting(
                    XmlHelper.serializeCommunicationSettings(settings));
        } catch (Exception exception) {
            postEvent(new CommunicationSettingsSetEvent(settings, true,
                                                        needTestCommunicationSettings));
        }
    }

    public void checkLeftTurnStatus() {
        try {
            bleController.executeCommand_CheckTurnSignal(BleConstants.TurnSignal.SIGNAL_LEFT);
        } catch (Exception exception) {
            postEvent(new BleErrorEvent());
        }
    }

    public void checkRightTurnStatus() {
        try {
            bleController.executeCommand_CheckTurnSignal(BleConstants.TurnSignal.SIGNAL_RIGHT);
        } catch (Exception exception) {
            postEvent(new BleErrorEvent());
        }
    }

    public void testWithoutSetCommunicationSettings() {
        try {
            bleController.executeCommand_TestCommunicationsSetting();
        } catch (Exception exception) {
            postEvent(new BleErrorEvent());
        }
    }

    public void generateTestImage(BleConstants.CameraInfo cameraId) {
        try {
            bleController.executeCommand_GenerateImageOnCamera(cameraId);
        } catch (Exception exception) {
            postEvent(new BleErrorEvent());
        }
    }

    public void captureAdasImage(BleConstants.DistanceCaptured distanceCaptured) {
        mDistanceCaptured = distanceCaptured;
        if (btClassicController.getState() == BluetoothClassicController.STATE_CONNECTED) {
            try {
                bleController.executeCommand_RequestAdasImageOverBluetooth(distanceCaptured);
            } catch (Exception exception) {
                mDistanceCaptured = null;
                postEvent(new BleErrorEvent(exception.getMessage()));
            }
        } else {
            try {
                bleController.executeCommand_InitiateBluetoothFileSharing();
            } catch (Exception exception) {
                this.mDistanceCaptured = null;
                postEvent(new BleErrorEvent(exception.getMessage()));
            }
        }
    }

    public void generateSelfCheck(String serialNumber) {
        this.serialNumber = serialNumber;
        needGenerateSelfCheck = true;
        getCurrentTime();
    }

    public void generateAdasCheck(String serialNumber) {
        this.serialNumber = serialNumber;
        needGenerateADASCheck = true;
        getCurrentTime();
    }

    public void generateTestEvent() {
        try {
            bleController.executeCommand_GenerateTestEvent();
        } catch (Exception exception) {
            postEvent(new BleErrorEvent());
        }
    }

    public void loadEventsLibrary(Calendar filterFrom, Calendar filterTo, byte filter) {
        try {
            bleController.executeCommand_ViewEventLibrary(filterFrom, filterTo, filter);
            this.filterFrom = filterFrom;
            this.filterTo = filterTo;
            this.filter = filter;
        } catch (Exception exception) {
            postEvent(new BleErrorEvent());
        }
    }

    public void loadDebugInfo() {
        try {
            debugInfo = null;
            bleController.executeCommand_GetDebugInformation();
        } catch (Exception exception) {
            postEvent(new BleErrorEvent());
        }
    }

    public List<DebugInfoItem> getDebugInfo() {
        return debugInfo;
    }

    public void loadEventVideos(String eventId) {
        this.eventId = eventId;
        if (btClassicController.getState() == BluetoothClassicController.STATE_CONNECTED) {
            loadEventVideos();
        } else {
            try {
                bleController.executeCommand_InitiateBluetoothFileSharing();
            } catch (Exception exception) {
                this.eventId = null;
                postEvent(new BleErrorEvent(exception.getMessage()));
            }
        }
    }

    private boolean checkError(Exception exception) {
        if (exception != null) {
            postEvent(new BleErrorEvent(exception.getMessage()));
            return true;
        }
        return false;
    }

    private void postEvent(Object event) {
        EventBus.getDefault()
                .post(event);
    }

    private void loadEventVideos() {
        if (TextUtils.isEmpty(eventId)) {
            return;
        }
        try {
            bleController.executeCommand_RequestEventOverBluetooth(eventId);
        } catch (Exception exception) {
            eventId = null;
            postEvent(new BleErrorEvent(exception.getMessage()));
        }
    }

}
