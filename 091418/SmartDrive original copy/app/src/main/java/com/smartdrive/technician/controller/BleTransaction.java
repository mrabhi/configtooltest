package com.smartdrive.technician.controller;

import android.util.Log;

//import com.smartdrive.technician.BuildConfig;
import java.util.ArrayList;

public class BleTransaction {
    public static final String TAG = "BleTransaction";

    BleConstants.FunctionCode mFunctionCode;

    ArrayList<byte[]> mRawRequestSegmentArray;
    int mCurrentRequestSegmentNumber;

    ArrayList<Byte> mResponseData;
    int mCurrentResponseSegmentNumber;

    BleConstants.TransactionStatus mStatus;
    boolean mAwaitingRequestWriteConfirmationFlag;
    boolean mAwaitingAcknowledgementWriteConfirmationFlag;

    public BleTransaction(BleConstants.FunctionCode functionCode) {
        mFunctionCode = functionCode;

        mRawRequestSegmentArray = new ArrayList<>();
        mCurrentRequestSegmentNumber = 0;

        mResponseData = new ArrayList<>();
        mCurrentResponseSegmentNumber = 0;

        mStatus = BleConstants.TransactionStatus.CREATING;
        mAwaitingRequestWriteConfirmationFlag = false;
        mAwaitingAcknowledgementWriteConfirmationFlag = false;
    }

    public byte[] getNextRequestSegment() {

        //if (BuildConfig.DEBUG && !(mCurrentRequestSegmentNumber < (mRawRequestSegmentArray.size()))) {
        //    throw new AssertionError("Cannot getNextRequestSegment() on an index that is out of range");
        //}

        byte[] toReturn = mRawRequestSegmentArray.get(mCurrentRequestSegmentNumber);
        mCurrentRequestSegmentNumber++;
        return toReturn;
    }

    public boolean isAtEndOfRequest() {
        return (mCurrentRequestSegmentNumber >= mRawRequestSegmentArray.size());
    }

    public boolean isAwaitingWriteConfirmation() {
        return (mAwaitingRequestWriteConfirmationFlag || mAwaitingAcknowledgementWriteConfirmationFlag);
    }

    public boolean isAwaitingResponse() {
        return (mStatus == BleConstants.TransactionStatus.AWAITING_RESPONSE);
    }

    public BleConstants.ProcessResponseSegmentResult processResponseSegment(byte[] responseSegment) {
        //if (BuildConfig.DEBUG && !(isAwaitingResponse())) {
        //    throw new AssertionError("Cannot processResponseSegment() on a BleTransaction that is not currently in AWAITING_RESPONSE status");
        //}

        if (responseSegment[0] != mFunctionCode.value) {
            Log.v(TAG, "Mismatched Function Codes");
            return BleConstants.ProcessResponseSegmentResult.ERROR;
        }

        int segmentNumberModulo = (0xFF & responseSegment[1]);
        if (segmentNumberModulo != (mCurrentResponseSegmentNumber % 256)) {
            Log.v(TAG, "Mismatched Segment Numbers Modulo");
            return BleConstants.ProcessResponseSegmentResult.ERROR;
        }

        int length = (0x7F & responseSegment[2]);

        if (BleUtility.calculateChecksum(responseSegment, length + 3) != responseSegment[length + 3]) {
            Log.v(TAG, "Mismatched Checksum");
            return BleConstants.ProcessResponseSegmentResult.ERROR;
        }

        BleConstants.ProcessResponseSegmentResult returnCode;
        if ((0x80 & responseSegment[2]) == 0x80) {
            // Message Continues
            returnCode = BleConstants.ProcessResponseSegmentResult.MESSAGE_CONTINUES;
        } else {
            // Message Terminates, let's process after this!
            returnCode = BleConstants.ProcessResponseSegmentResult.MESSAGE_COMPLETE;
            mStatus = BleConstants.TransactionStatus.PROCESSING_RESPONSE;
        }

        for (int index = 0; index < length; index++) {
            mResponseData.add(responseSegment[index + 3]);
        }

        mCurrentResponseSegmentNumber++;
        return returnCode;
    }

    public void setRequestData(byte[] data) {
        //if (BuildConfig.DEBUG && !(mStatus == BleConstants.TransactionStatus.CREATING)) {
        //    throw new AssertionError("Cannot setRequestData() on a BleTransaction that is not currently in CREATING status");
        //}

        byte[] segment;

        if ((data == null) || (data.length == 0)) {
            segment = new byte[4];
            segment[0] = mFunctionCode.value;
            segment[1] = (byte) 0x00;
            segment[2] = (byte) 0x00;
            segment[3] = mFunctionCode.value;

            mRawRequestSegmentArray.add(segment);
        } else {
            int bytePosition = 0;
            int sequenceNumber = 0;

            while (bytePosition < data.length) {
                int sequenceLength = Math.min(16, data.length - bytePosition);

                segment = new byte[sequenceLength + 4];
                segment[0] = mFunctionCode.value;
                segment[1] = (byte) (sequenceNumber % 256);
                segment[2] = (byte) sequenceLength;

                System.arraycopy(data, bytePosition, segment, 3, sequenceLength);

                bytePosition += sequenceLength;

                if (bytePosition >= data.length) {
                    // FINAL SEQUENCE
                    // Do Nothing
                } else {
                    // SEQUENCE CONTINUES
                    // Add to the 2nd Byte
                    segment[2] += (byte) 0x80;
                    sequenceNumber++;
                }

                segment[3 + sequenceLength] = BleUtility.calculateChecksum(segment, 3 + sequenceLength);

                mRawRequestSegmentArray.add(segment);
            }
        }

    }
}
