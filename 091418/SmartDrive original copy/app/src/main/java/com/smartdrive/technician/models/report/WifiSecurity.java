package com.smartdrive.technician.models.report;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.smartdrive.technician.R;
import com.smartdrive.technician.SmartDriveApp;

@Table(name = "WifiSecurity")
public class WifiSecurity extends Model implements Parcelable {

    public static final Parcelable.Creator<WifiSecurity> CREATOR = new Parcelable.Creator<WifiSecurity>() {
        @Override
        public WifiSecurity createFromParcel(Parcel source) {
            return new WifiSecurity(source);
        }

        @Override
        public WifiSecurity[] newArray(int size) {
            return new WifiSecurity[size];
        }
    };

    @Column
    public String ssid;
    @Column
    public String encryption;
    @Column
    public String password;

    public WifiSecurity() {
    }

    protected WifiSecurity(Parcel in) {
        this.ssid = in.readString();
        this.encryption = in.readString();
        this.password = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ssid);
        dest.writeString(this.encryption);
        dest.writeString(this.password);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WifiSecurity that = (WifiSecurity) o;
        return (ssid != null ? ssid.equals(that.ssid) : that.ssid == null)
                && ((encryption != null ? encryption.equals(that.encryption) : that.encryption == null)
                && ((password != null ? password.equals(that.password) : that.password == null)));

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (ssid != null ? ssid.hashCode() : 0);
        result = 31 * result + (encryption != null ? encryption.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }

    public boolean isEncryptionNone() {
        return encryption != null && encryption.toLowerCase().equals(SmartDriveApp.getContext().getString(R.string.none).toLowerCase());
    }

}