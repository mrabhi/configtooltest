package com.smartdrive.technician.fragments.installation;

import android.view.View;
import android.widget.RadioButton;

import com.smartdrive.technician.EventsSubscriber;
import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.events.ble.BleErrorEvent;
import com.smartdrive.technician.events.ble.installation.CheckTurnSignalEvent;
import com.smartdrive.technician.managers.BleManager;
import com.smartdrive.technician.models.report.Report;
import com.smartdrive.technician.widgets.installation.StatusTextView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;
import butterknife.OnClick;

import static com.smartdrive.technician.Constants.REPORT;

@EventsSubscriber
@Layout(id = R.layout.fragment_verify_turn_signals)
public class VerifyTurnSignalsFragment extends ChildInstallationFragment {

    private static final int DEFALT_STATUS = 0;
    private static final int STATUS_RIGHT_TURN_PROCESSING = 1;
    private static final int STATUS_LEFT_TURN_PROCESSING = 2;

    @Bind(R.id.stvLeftTurnSignal)
    StatusTextView stvLeftTurnSignal;
    @Bind(R.id.stvRightTurnSignal)
    StatusTextView stvRightTurnSignal;
    @Bind(R.id.vError)
    View vError;
    @Bind(R.id.vPolarityLeft)
    View vPolarityLeft;
    @Bind(R.id.vPolarityRight)
    View vPolarityRight;
    @Bind(R.id.rbLowRight)
    RadioButton rbLowRight;
    @Bind(R.id.rbHighRight)
    RadioButton rbHighRight;
    @Bind(R.id.rbLowLeft)
    RadioButton rbLowLeft;
    @Bind(R.id.rbHighLeft)
    RadioButton rbHighLeft;

    private BleManager bleManager;
    private Report report;
    private int status;

    @Override
    protected void initView() {
        report = getArguments().getParcelable(REPORT);
        if (report == null) {
            return;
        }
        bleManager = BleManager.getInstance();
        stvLeftTurnSignal.setTurnStatus(report.leftTurnStatus);
        stvRightTurnSignal.setTurnStatus(report.rightTurnStatus);
        status = DEFALT_STATUS;
    }

    @Override
    protected void updateReport(Report report) {
        report.leftTurnStatus = stvLeftTurnSignal.isTurnSignalAvailable();
        report.rightTurnStatus = stvRightTurnSignal.isTurnSignalAvailable();
    }

    @Override
    protected boolean areFieldsValid() {
        if (vPolarityLeft.getVisibility() == View.VISIBLE
                && vPolarityRight.getVisibility() == View.VISIBLE) {
            report.rightTurnPolarityHigh = rbHighRight.isChecked();
            report.rightTurnPolarityLow = rbLowRight.isChecked();
            report.leftTurnPolarityHigh = rbHighLeft.isChecked();
            report.leftTurnPolarityLow = rbLowLeft.isChecked();
        }

        return super.areFieldsValid();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (report.leftTurnPolarityLow || report.leftTurnPolarityHigh || report.rightTurnPolarityLow
                || report.rightTurnPolarityHigh) {
            vError.setVisibility(View.VISIBLE);
            vPolarityRight.setVisibility(View.VISIBLE);
            vPolarityLeft.setVisibility(View.VISIBLE);

            rbLowLeft.setChecked(report.leftTurnPolarityLow);
            rbHighLeft.setChecked(report.leftTurnPolarityHigh);
            rbLowRight.setChecked(report.rightTurnPolarityLow);
            rbHighRight.setChecked(report.rightTurnPolarityHigh);
        }
    }

    @OnClick(R.id.btnCheckLeftTurnStatus)
    public void onBtnCheckLeftTurnStatus() {
        status = STATUS_LEFT_TURN_PROCESSING;
        showProgress(R.string.scanning);
        bleManager.checkLeftTurnStatus();
        stvLeftTurnSignal.resetDefault();
        stvRightTurnSignal.setEnabled(false);
    }

    @OnClick(R.id.btnCheckRightTurnStatus)
    public void onBtnCheckRightTurnStatus() {
        status = STATUS_RIGHT_TURN_PROCESSING;
        showProgress(R.string.scanning);
        bleManager.checkRightTurnStatus();
        stvRightTurnSignal.resetDefault();
        stvLeftTurnSignal.setEnabled(false);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(CheckTurnSignalEvent event) {
        if (status == STATUS_LEFT_TURN_PROCESSING) {
            stvLeftTurnSignal.setTurnStatus(event.isTurnSignalAvailable);
            report.leftTurnStatus = event.isTurnSignalAvailable;
        } else {
            stvRightTurnSignal.setTurnStatus(event.isTurnSignalAvailable);
            report.rightTurnStatus = event.isTurnSignalAvailable;
        }
        if (!event.isTurnSignalAvailable) {
            vError.setVisibility(View.VISIBLE);
            vPolarityLeft.setVisibility(View.VISIBLE);
            vPolarityRight.setVisibility(View.VISIBLE);
        }
        hideProgress();
        updateReport(report);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(BleErrorEvent event) {
        if (status == STATUS_LEFT_TURN_PROCESSING) {
            stvLeftTurnSignal.setTurnStatus(false);
        } else {
            stvRightTurnSignal.setTurnStatus(false);
        }
        stvRightTurnSignal.setEnabled(true);
        stvLeftTurnSignal.setEnabled(true);
    }
}
