package com.smartdrive.technician.models.report;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.smartdrive.technician.controller.BleConstants;

import java.util.List;

import static com.smartdrive.technician.models.report.Camera.Type.DIGITAL;

@Table(name = "Camera")
public class Camera extends Model implements Parcelable {

    public static final Parcelable.Creator<Camera> CREATOR = new Parcelable.Creator<Camera>() {
        @Override
        public Camera createFromParcel(Parcel source) {
            return new Camera(source);
        }

        @Override
        public Camera[] newArray(int size) {
            return new Camera[size];
        }
    };

    public enum Type {
        DIGITAL,
        ANALOG,
        WABCO;

        @Override
        public String toString() {
            return (name().substring(0, 1) + name().substring(1).toLowerCase());
        }
    }

    @Column
    public Type type = DIGITAL;
    @Column
    public long reportId;
    @Column
    public boolean connected;
    @Column
    public int cameraId;
    @Column
    public String name;
    @Column
    public String serial;
    @Column
    public String mountingLocation;
    @Column
    public String fieldOfView;
    @Column
    public String notes;
    @Column
    public String testImageBase64;

    public String videoUrl;
    public Bitmap firstFrame;
    public BleConstants.CameraType cameraType;

    public Camera() {
    }

    protected Camera(Parcel in) {
        this.type = Type.values()[in.readInt()];
        this.reportId = in.readLong();
        this.connected = in.readByte() != 0;
        this.cameraId = in.readInt();
        this.name = in.readString();
        this.serial = in.readString();
        this.mountingLocation = in.readString();
        this.fieldOfView = in.readString();
        this.notes = in.readString();
        this.testImageBase64 = in.readString();
        this.videoUrl = in.readString();
        this.firstFrame = in.readParcelable(Bitmap.class.getClassLoader());
        this.cameraType = BleConstants.CameraType.valueOf(in.readString());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type.ordinal());
        dest.writeLong(this.reportId);
        dest.writeByte(this.connected ? (byte) 1 : (byte) 0);
        dest.writeInt(this.cameraId);
        dest.writeString(this.name);
        dest.writeString(this.serial);
        dest.writeString(this.mountingLocation);
        dest.writeString(this.fieldOfView);
        dest.writeString(this.notes);
        dest.writeString(this.testImageBase64);
        dest.writeString(this.videoUrl);
        dest.writeParcelable(this.firstFrame, flags);
        dest.writeString(this.cameraType.name());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static List<Camera> getListFromDB(long reportId) {
        return new Select().from(Camera.class).where("reportId = ?", reportId).execute();
    }

}