package com.smartdrive.technician.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.smartdrive.technician.R;

import java.util.Calendar;

import static com.smartdrive.technician.Constants.TOAST_DELAY;

public abstract class Dialogs {

    public interface OnTimeIntervalChangedListener {
        void onTimeIntervalChanged(int year, int month, int day, int fromHour, int fromMinute,
                                   int toHour, int toMinute);
    }

    public static void showToast(Context context, String title, String message) {
        showToast(context, title, message, -1);
    }

    public static Dialog showToast(Context context, String title, String message,
                                   @DrawableRes int imageRes) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View contentView = LayoutInflater.from(context)
                .inflate(R.layout.dialog_toast, null);
        TextView tvTitle = contentView.findViewById(R.id.tvTitle);
        tvTitle.setText(title);
        if (!TextUtils.isEmpty(message)) {
            TextView tvMessage = contentView.findViewById(R.id.tvMessage);
            tvMessage.setText(message);
            tvMessage.setVisibility(View.VISIBLE);
        }
        if (imageRes != -1) {
            ImageView ivStatus = contentView.findViewById(R.id.ivStatus);
            ivStatus.setImageResource(imageRes);
            ivStatus.setVisibility(View.VISIBLE);
        }

        setContentView(context, dialog, contentView);
        contentView.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, TOAST_DELAY);
        dialog.show();
        return dialog;
    }

    private static void setContentView(Context context, Dialog dialog, View contentView) {
        dialog.setContentView(contentView);
        dialog.getWindow()
                .setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        int margin = (int) context.getResources()
                .getDimension(R.dimen.x_large_padding);
        WindowManager.LayoutParams params = dialog.getWindow()
                .getAttributes();
        params.width = context.getResources()
                .getDisplayMetrics().widthPixels - 2 * margin;
        dialog.getWindow()
                .setAttributes(params);
    }

    public static Dialog showAlert(Context context, String title, String message,
                                   final View view, int cancelRes, final int actionRes,
                                   final boolean dismissAfterAction,
                                   final View.OnClickListener actionListener) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View contentView = LayoutInflater.from(context)
                .inflate(R.layout.dialog_alert, null);
        if (!TextUtils.isEmpty(title)) {
            TextView tvTitle = contentView.findViewById(R.id.tvTitle);
            tvTitle.setText(title);
            tvTitle.setVisibility(View.VISIBLE);
        }
        if (!TextUtils.isEmpty(message)) {
            TextView tvMessage = contentView.findViewById(R.id.tvMessage);
            tvMessage.setText(message);
            tvMessage.setVisibility(View.VISIBLE);
        }
        if (view != null) {
            ViewGroup viewContainer = contentView.findViewById(R.id.viewContainer);
            viewContainer.addView(view);
            viewContainer.setVisibility(View.VISIBLE);
        }
        TextView tvCancel = contentView.findViewById(R.id.tvCancel);
        tvCancel.setText(cancelRes);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        TextView tvAction = contentView.findViewById(R.id.tvAction);
        if (actionRes > 0) {
            tvAction.setText(actionRes);
            tvAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    actionListener.onClick(v);
                    if (dismissAfterAction) {
                        dialog.dismiss();
                    }
                }
            });
        } else {
            tvAction.setVisibility(View.GONE);
            tvCancel.setBackgroundResource(R.drawable.selector_rounded_btn);
        }

        setContentView(context, dialog, contentView);
        dialog.show();
        return dialog;
    }

    public static void showProgress(Dialog dialog, @StringRes int textRes) {
        if (dialog == null) {
            return;
        }
        View dialogContent = dialog.findViewById(R.id.dialogContent);
        View dialogProgress = dialog.findViewById(R.id.dialogProgress);
        if (dialogContent == null || dialogProgress == null) {
            return;
        }
        ((TextView) dialog.findViewById(R.id.tvProgress)).setText(textRes);
        dialogContent.setVisibility(View.INVISIBLE);
        dialogProgress.setVisibility(View.VISIBLE);
        dialog.setCancelable(false);
    }

    public static void hideProgress(Dialog dialog) {
        if (dialog == null) {
            return;
        }
        View dialogContent = dialog.findViewById(R.id.dialogContent);
        View dialogProgress = dialog.findViewById(R.id.dialogProgress);
        if (dialogContent == null || dialogProgress == null) {
            return;
        }
        dialogContent.setVisibility(View.VISIBLE);
        dialogProgress.setVisibility(View.GONE);
        dialog.setCancelable(true);
    }

    public static Dialog showTimeIntervalPicker(Context context, final boolean firstSelection,
                                                final int year, final int month, final int day,
                                                final int fromHour, final int fromMinute,
                                                final int toHour, final int toMinute,
                                                final OnTimeIntervalChangedListener listener) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.dialog_time_interval, null);

        final DatePicker datePicker = view.findViewById(R.id.datePicker);
        Calendar minDate = Calendar.getInstance();
        minDate.set(Calendar.DAY_OF_MONTH, 1);
        minDate.set(Calendar.MONTH, Calendar.JANUARY);
        minDate.set(Calendar.YEAR, 2017);
        datePicker.setMinDate(minDate.getTimeInMillis());
        datePicker.updateDate(year, month, day);
        final TimePicker fromTimePicker = view.findViewById(R.id.fromTimePicker);
        fromTimePicker.setIs24HourView(DateUtils.is24HourFormat());
        fromTimePicker.setCurrentHour(fromHour);
        fromTimePicker.setCurrentMinute(fromMinute);
        final TimePicker toTimePicker = view.findViewById(R.id.toTimePicker);
        toTimePicker.setIs24HourView(DateUtils.is24HourFormat());
        toTimePicker.setCurrentHour(toHour);
        toTimePicker.setCurrentMinute(toMinute);

        return showAlert(context, null, null, view, R.string.cancel, R.string.ok, true,
                         new View.OnClickListener() {
                             @Override
                             public void onClick(View v) {
                                 int selectedYear = datePicker.getYear();
                                 int selectedMonth = datePicker.getMonth();
                                 int selectedDay = datePicker.getDayOfMonth();
                                 int selectedFromHour = fromTimePicker.getCurrentHour();
                                 int selectedFromMinute = fromTimePicker.getCurrentMinute();
                                 int selectedToHour = toTimePicker.getCurrentHour();
                                 int selectedToMinute = toTimePicker.getCurrentMinute();
                                 if (!firstSelection && year == selectedYear
                                         && month == selectedMonth && day == selectedDay
                                         && fromHour == selectedFromHour
                                         && fromMinute == selectedFromMinute
                                         && toHour == selectedToHour
                                         && toMinute == selectedToMinute) {
                                     return;
                                 }
                                 listener.onTimeIntervalChanged(selectedYear, selectedMonth,
                                                                selectedDay,
                                                                selectedFromHour,
                                                                selectedFromMinute, selectedToHour,
                                                                selectedToMinute);
                             }
                         });
    }

}