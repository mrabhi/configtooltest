package com.smartdrive.technician.events;

import android.support.v4.app.Fragment;

public class NavigationEvent {

    public final Fragment fragment;

    public NavigationEvent(Fragment fragment) {
        this.fragment = fragment;
    }

    public String getFragmentTag() {
        return fragment == null ? null : fragment.getClass().getName();
    }

}