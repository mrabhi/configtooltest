package com.smartdrive.technician.widgets.installation;

import android.content.Context;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smartdrive.technician.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CheckCommunicationView extends LinearLayout {

    @Bind(R.id.btnCheck)
    TextView btnCheck;
    @Bind(R.id.checkResultContainer)
    View checkResultContainer;
    @Bind(R.id.tvSignalStrengthValue)
    TextView tvSignalStrengthValue;
    @Bind(R.id.tvPing)
    TextView tvPing;
    @Bind(R.id.tvPingValue)
    TextView tvPingValue;

    public CheckCommunicationView(Context context) {
        super(context);
        create();
    }

    public CheckCommunicationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        create();
    }

    public CheckCommunicationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        create();
    }

    private void create() {
        inflate(getContext(), R.layout.view_check_communication, this);
        ButterKnife.bind(this);
    }

    public void init(@StringRes int pingTextRes, @StringRes int checkTextRes, final OnClickListener onCheckClickListener) {
        checkResultContainer.setVisibility(GONE);
        tvPing.setText(pingTextRes);
        btnCheck.setText(checkTextRes);
        btnCheck.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                checkResultContainer.setVisibility(GONE);
                onCheckClickListener.onClick(v);
            }
        });
    }

    public void initCheckResult(String signalStrength, String ping) {
        if (TextUtils.isEmpty(signalStrength) || TextUtils.isEmpty(ping)) {
            checkResultContainer.setVisibility(View.GONE);
        } else {
            tvSignalStrengthValue.setText(signalStrength);
            tvPingValue.setText(ping);
            checkResultContainer.setVisibility(View.VISIBLE);
        }
    }

    public String getSignalStrength() {
        return checkResultContainer.getVisibility() == VISIBLE ? tvSignalStrengthValue.getText().toString() : null;
    }

    public String getPing() {
        return checkResultContainer.getVisibility() == VISIBLE ? tvPingValue.getText().toString() : null;
    }

}