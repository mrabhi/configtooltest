package com.smartdrive.technician.events.ble;

import com.smartdrive.technician.models.DebugInfoItem;

import java.util.List;

public class DebugInfoLoadedEvent {

    public final List<DebugInfoItem> debugInfo;

    public DebugInfoLoadedEvent(List<DebugInfoItem> debugInfo) {
        this.debugInfo = debugInfo;
    }

}