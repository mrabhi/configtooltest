package com.smartdrive.technician.controller;

import android.graphics.Bitmap;

import java.util.Calendar;

public interface BleControllerInterface {
    void scanResult(String deviceName);
    void scanCompleted();
    void connectionComplete();
    void connectionDisconnected(BleException.DisconnectException exception);

    void responseSegmentReceived(int segmentNumber);

    void authenticateResponse_Login(Exception exception, byte mode);
    void authenticateResponse_GetCurrentTime(Exception exception, Calendar date);
    void authenticateResponse_Logout(Exception exception);
    void authenticateResponse_SyncComplete(Exception exception);

    void commandResponse_SetVehicleId(Exception exception);
    void commandResponse_GetVehicleId(Exception exception, String vehicleId);

    void commandResponse_GetControllerSerialNumber(Exception exception, String serialNumber);
    void commandResponse_CheckSensorBar(Exception exception, String message);
    void commandResponse_GetAdasFlag(Exception exception, Boolean AdasFlag);
    void commandResponse_GetCameraInfo(Exception exception, boolean isConnected, BleConstants.CameraType CameraType, String serialNumber);
    void commandResponse_CheckTurnSignal(Exception exception, boolean isAvailable);
    void commandResponse_GetCommunicationsSetting(Exception exception, String xmlSettings);
    void commandResponse_SetCommunicationsSetting(Exception exception);
    void commandResponse_TestCommunicationsSetting(Exception exception, Boolean successFlag, byte signalStrength);
    void commandResponse_GenerateImageOnCamera(Exception exception, Bitmap bitmap);
    void commandResponse_GenerateTestEvent(Exception exception);
    void commandResponse_ViewEventLibrary(Exception exception, String xmlLibrary);
    void commandResponse_InitiateBluetoothFileSharing(Exception exception, String deviceMacAddress, String deviceRfcommSocketServiceUuid);
    void commandResponse_RequestEventOverBluetooth(Exception exception, int byteSize);
    void commandResponse_RequestAdasImageOverBluetooth(Exception exception, int ByteSize);
    void commandResponse_SendAdasStats(Exception exception);
    void commandResponse_CheckGps(Exception exception, byte testResult, byte signalStrength);
    void commandResponse_GetDebugInformation(Exception exception, String xmlReport);

    void selfCheckResponse_GenerateSelfCheck(Exception exception);
    void selfCheckResponse_SelfCheckComplete(Exception exception, String xmlReport);
}
