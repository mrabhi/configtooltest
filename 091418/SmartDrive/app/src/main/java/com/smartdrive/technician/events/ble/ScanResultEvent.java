package com.smartdrive.technician.events.ble;

public class ScanResultEvent {

    public final String deviceName;

    public ScanResultEvent(String deviceName) {
        this.deviceName = deviceName;
    }

}