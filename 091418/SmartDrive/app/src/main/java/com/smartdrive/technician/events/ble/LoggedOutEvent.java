package com.smartdrive.technician.events.ble;

import android.text.TextUtils;

import com.smartdrive.technician.R;
import com.smartdrive.technician.SmartDriveApp;

public class LoggedOutEvent {

    public final String message;

    public LoggedOutEvent(Exception exception) {
        if (exception != null && !TextUtils.isEmpty(exception.getMessage())) {
            message = exception.getMessage();
        } else {
            message = SmartDriveApp.getContext().getString(R.string.disconnected);
        }
    }

}