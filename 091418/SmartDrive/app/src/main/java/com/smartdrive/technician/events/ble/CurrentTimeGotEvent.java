package com.smartdrive.technician.events.ble;

import java.util.Calendar;

public class CurrentTimeGotEvent {

    public final Calendar currentTime;

    public CurrentTimeGotEvent(Calendar currentTime) {
        this.currentTime = currentTime;
    }

}