package com.smartdrive.technician.functional;

public interface Receiver<T> {
    void receive(T data);
}
