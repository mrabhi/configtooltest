package com.smartdrive.technician.fragments.installation;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.smartdrive.technician.EventsSubscriber;
import com.smartdrive.technician.Layout;
import com.smartdrive.technician.OnChangeListener;
import com.smartdrive.technician.R;
import com.smartdrive.technician.activities.BaseActivity;
import com.smartdrive.technician.events.ChangeFieldsEvent;
import com.smartdrive.technician.events.ble.installation.VehicleIdEvent;
import com.smartdrive.technician.functional.Receiver;
import com.smartdrive.technician.functional.UiReceiver;
import com.smartdrive.technician.managers.VehicleInfoManager;
import com.smartdrive.technician.managers.VinImageManager;
import com.smartdrive.technician.models.pojo.VehicleInfo;
import com.smartdrive.technician.models.pojo.Vin;
import com.smartdrive.technician.models.report.Report;
import com.smartdrive.technician.utils.CarManufacturingYearUtils;
import com.smartdrive.technician.utils.CsvParser;
import com.smartdrive.technician.utils.Dialogs;
import com.smartdrive.technician.utils.VinValidatorUtils;
import com.smartdrive.technician.widgets.BaseEditText;
import com.smartdrive.technician.widgets.BaseSpinner;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;

import static android.app.Activity.RESULT_OK;
import static com.smartdrive.technician.Constants.INTENT_DEFAULT_DATA_OBJECT;
import static com.smartdrive.technician.Constants.MAKE_REGEXP;
import static com.smartdrive.technician.Constants.REPORT;
import static com.smartdrive.technician.Constants.VEHICLE_ID;
import static com.smartdrive.technician.Constants.VEHICLE_ID_REGEXP;

@EventsSubscriber
@Layout(id = R.layout.fragment_vehicle_info)
public class VehicleInfoFragment extends ChildInstallationFragment {

    static final int REQUEST_IMAGE_CAPTURE = 1;

    @Bind(R.id.fieldsContainer)
    View fieldsContainer;
    @Bind(R.id.etVehicleId)
    BaseEditText etVehicleId;
    @Bind(R.id.spinVehicleType)
    BaseSpinner spinVehicleType;
    @Bind(R.id.spinYear)
    BaseSpinner spinYear;
    @Bind(R.id.etMake)
    BaseEditText etMake;
    @Bind(R.id.etModel)
    BaseEditText etModel;
    @Bind(R.id.etVin)
    BaseEditText etVin;
    @Bind(R.id.etCompany)
    BaseEditText etCompany;
    @Bind(R.id.btnRetrieveVehicleInfo)
    Button btnRetrieveVehicleInfo;

    private Report mReport;

    private OnChangeListener onChangeListener = new OnChangeListener() {
        @Override
        public void onChange() {
            EventBus.getDefault()
                    .post(new ChangeFieldsEvent(checkFields(false)));
        }
    };

    @Override
    protected void initView() {
        etVehicleId.init(R.string.vehicle_id, true);
        etVehicleId.setOnChangeListener(onChangeListener);
        if (getActivity().getIntent()
                .getStringExtra(VEHICLE_ID) != null) {
            etVehicleId.setText(getActivity().getIntent()
                                        .getStringExtra(VEHICLE_ID));
        }
        spinVehicleType.init(CsvParser.parseVehicleTypes(), R.string.vehicle_type, true);
        spinVehicleType.setOnChangeListener(onChangeListener);
        spinYear.init(CarManufacturingYearUtils.generateYears(), R.string.year, true);
        spinYear.setOnChangeListener(onChangeListener);
        etMake.init(R.string.make, true);
        etMake.setOnChangeListener(onChangeListener);
        etModel.init(R.string.model, true);
        etModel.setOnChangeListener(onChangeListener);
        etCompany.init(R.string.company, true);
        etCompany.setOnChangeListener(onChangeListener);
        etVin.init(R.string.vin, true);
        etVin.setOnChangeListener(onChangeListener);
        etVin.setOnActionClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });
        Report report = getArguments().getParcelable(REPORT);
        if (report == null) {
            return;
        }
        mReport=report;
        initData(report);

        onChangeListener.onChange();
        btnRetrieveVehicleInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(etVin.getText()) && VinValidatorUtils.validate(
                        etVin.getText())) {
                    getVehicleData();
                } else {
                    Dialogs.showToast(getContext(), getString(R.string.invalid_vin), null,
                                      R.drawable.ic_error);
                }
            }
        });
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden){
            initData(mReport);
        }
    }

    @Override
    protected void updateReport(Report report) {
        String vehicleId = getActivity().getIntent()
                .getStringExtra(VEHICLE_ID);
        if (vehicleId == null) {
            vehicleId = "";
        }
        String inputVehicleId = etVehicleId.getText();
        if (!vehicleId.equals(inputVehicleId)) {
            bleManager.setVehicleId(inputVehicleId);
        }
        report.vehicleType = spinVehicleType.getSelectedItem();
        report.year = spinYear.getSelectedItem();
        report.make = etMake.getText();
        report.model = etModel.getText();
        report.company = etCompany.getText();
        report.vin = etVin.getText();
    }

    @Override
    protected boolean areFieldsValid() {
        return checkFields(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap;
            if (extras != null) {
                imageBitmap = (Bitmap) extras.get(INTENT_DEFAULT_DATA_OBJECT);
                showFullscreenProgress(R.drawable.ic_close, R.string.scanning_image);
                VinImageManager.getInstance()
                        .send(imageBitmap, new UiReceiver<>(new Receiver<Vin>() {
                            @Override
                            public void receive(Vin data) {
                                hideFullscreenProgress();
                                if (data.getVin() != null) {
                                    etVin.setText(data.getVin());
                                } else {
                                    Dialogs.showToast(getContext(), getString(R.string.invalid_vin),
                                                      null,
                                                      R.drawable.ic_error);
                                }
                            }
                        }), new UiReceiver<>(new Receiver<Throwable>() {
                            @Override
                            public void receive(Throwable data) {
                                hideFullscreenProgress();
                                Dialogs.showToast(getContext(), getString(R.string.failed), null,
                                                  R.drawable.ic_error);
                            }
                        }));
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(VehicleIdEvent event) {
        etVehicleId.setText(event.vehicleId);
    }

    private void initData(Report report) {
        spinVehicleType.setSelectedItem(report.vehicleType);
        spinYear.setSelectedItem(report.year);
        etMake.setText(report.make);
        etModel.setText(report.model);
        etCompany.setText(report.company);
        etVin.setText(report.vin);
    }

    private boolean checkFields(boolean pickOut) {
        if (pickOut) {
            fieldsContainer.requestFocus();
        }
        return !etVehicleId.isRequiredAndEmpty(pickOut)
                & !spinVehicleType.isRequiredAndEmpty(pickOut)
                & !spinYear.isRequiredAndEmpty(pickOut)
                & !etMake.isRequiredAndEmpty(pickOut)
                & !etModel.isRequiredAndEmpty(pickOut)
                & !etVin.isRequiredAndEmpty(pickOut)
                & !etCompany.isRequiredAndEmpty(pickOut)
                && etVehicleId.isValid(VEHICLE_ID_REGEXP, R.string.invalid_vehicle_id, pickOut)
                && etMake.isValid(MAKE_REGEXP, R.string.invalid_make, pickOut)
                && etVin.validateVinEditText(R.string.invalid_vin, pickOut);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void getVehicleData() {
        showFullscreenProgress(R.drawable.ic_close, R.string.searching_vin_info);
        VehicleInfoManager.getInstance()
                .send(etVin.getText(),
                      new UiReceiver<>(new Receiver<VehicleInfo>() {
                          @Override
                          public void receive(VehicleInfo data) {
                              if (data != null) {
                                  spinVehicleType.setSelectedItem(
                                          data.getVehicleType());
                                  spinYear.setSelectedItem(data.getYear());
                                  etMake.setText(data.getManufacturer());
                                  etModel.setText(data.getModel());
                                  hideFullscreenProgress();
                              }
                          }
                      }), new UiReceiver<>(new Receiver<Throwable>() {
                            @Override
                            public void receive(Throwable data) {
                                hideFullscreenProgress();
                                Dialogs.showToast(getContext(), getString(R.string.failed), null,
                                                  R.drawable.ic_error);
                            }
                        }));
    }

    private void showFullscreenProgress(@DrawableRes int drawableRes,
                                        @StringRes int messageString) {
        ((BaseActivity) getActivity()).showFullscreenProgress(drawableRes, messageString);
    }

    private void hideFullscreenProgress() {
        ((BaseActivity) getActivity()).hideFullscreenProgress();
    }

}