package com.smartdrive.technician.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartdrive.technician.R;
import com.smartdrive.technician.SmartDriveApp;
import com.smartdrive.technician.controller.BleConstants;
import com.smartdrive.technician.models.report.Camera;
import com.smartdrive.technician.utils.Utils;
import com.smartdrive.technician.widgets.BaseEditText;
import com.smartdrive.technician.widgets.BaseSpinner;

import java.util.Arrays;
import java.util.List;

import butterknife.Bind;

public class CamerasAdapter extends BaseAdapter<Camera> {

    private Activity activity;

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    protected class CamerasViewHolder extends BaseViewHolder {
        @Bind(R.id.tvCameraName)
        TextView tvCamera;
        @Bind(R.id.tvCameraSerial)
        TextView tvCameraSerial;
        @Bind(R.id.tvState)
        TextView tvState;
        @Bind(R.id.cameraDetails)
        View cameraDetails;
        @Bind(R.id.spinMountingLocation)
        BaseSpinner spinMountingLocation;
        @Bind(R.id.spinFieldOfView)
        BaseSpinner spinFieldOfView;
        @Bind(R.id.etNotes)
        BaseEditText etNotes;
        @Bind(R.id.btnGenerateTestImage)
        TextView btnGenerateTestImage;
        @Bind(R.id.vWrongPort)
        View vWrongPort;
        @Bind(R.id.ivState)
        ImageView ivState;

        View.OnTouchListener mListener = new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                removeFocus();
                return false;
            }
        };

        private CamerasViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
        }

        @SuppressLint("ClickableViewAccessibility")
        @Override
        protected void bind(final int position) {
            final Camera camera = getItem(position);
            tvCamera.setOnTouchListener(mListener);
            tvCameraSerial.setOnTouchListener(mListener);
            tvState.setOnTouchListener(mListener);
            cameraDetails.setOnTouchListener(mListener);
            spinMountingLocation.setOnTouchListener(mListener);
            spinFieldOfView.setOnTouchListener(mListener);
            btnGenerateTestImage.setOnTouchListener(mListener);
            vWrongPort.setOnTouchListener(mListener);


            tvCamera.setText(camera.name);
            tvCameraSerial.setText(camera.serial);
            tvCameraSerial.setVisibility(TextUtils.isEmpty(camera.serial)
                                         ? View.GONE
                                         : View.VISIBLE);


            if (camera.cameraType == null) {
                ivState.setImageResource(R.drawable.ic_gray_circle);
                tvState.setText(R.string.disconnected);
                cameraDetails.setVisibility(View.GONE);
            } else {
                ivState.setImageResource(camera.connected
                                         ? R.drawable.ic_green_circle
                                         : R.drawable.ic_gray_circle);
                tvState.setText(camera.connected
                                ? R.string.connected
                                : R.string.disconnected);
                cameraDetails.setVisibility(camera.connected
                                            ? View.VISIBLE
                                            : View.GONE);

                if (camera.cameraType.equals(BleConstants.CameraType.TYPE_WABCO)) {
                    spinMountingLocation.setVisibility(View.GONE);
                    spinFieldOfView.setVisibility(View.GONE);
                } else {
                    spinMountingLocation.setVisibility(View.VISIBLE);
                    spinFieldOfView.setVisibility(View.VISIBLE);
                    spinMountingLocation.init(mountingLocationList, R.string.mounting_location,
                                              false);
                    spinMountingLocation.setSelectedItem(camera.mountingLocation);
                    spinMountingLocation.setOnItemSelectedListener(
                            new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view,
                                                           int position, long id) {
                                    camera.mountingLocation = spinMountingLocation.getSelectedItem();
                                    removeFocus();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    removeFocus();
                                }
                            });

                    switch (camera.cameraType) {
                        case TYPE_SONY:
                            fieldOfViewList = Arrays.asList(SmartDriveApp.getContext()
                                                                    .getResources()
                                                                    .getStringArray(
                                                                            R.array.fov_sony_no_ir));
                            break;
                        case TYPE_SONY_IR:
                            fieldOfViewList = Arrays.asList(SmartDriveApp.getContext()
                                                                    .getResources()
                                                                    .getStringArray(
                                                                            R.array.fov_sony_ir));

                            break;
                        case TYPE_ANALOG:
                            fieldOfViewList = Arrays.asList(SmartDriveApp.getContext()
                                                                    .getResources()
                                                                    .getStringArray(
                                                                            R.array.fov_analog));

                            break;
                        default:
                            break;
                    }

                    spinFieldOfView.init(fieldOfViewList, R.string.field_of_view, false);
                    spinFieldOfView.setSelectedItem(camera.fieldOfView);
                    spinFieldOfView.setOnItemSelectedListener(
                            new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view,
                                                           int position,
                                                           long id) {
                                    camera.fieldOfView = spinFieldOfView.getSelectedItem();
                                    removeFocus();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    removeFocus();
                                }
                            });
                }
                etNotes.init(R.string.add_notes, false);
                etNotes.setPadding((int) etNotes.getContext()
                        .getResources()
                        .getDimension(R.dimen.default_padding));
                etNotes.setText(camera.notes);
                etNotes.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        camera.notes = s.toString()
                                .trim();
                    }
                });
                btnGenerateTestImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onGenerateTestImageClickListener.onClick(v, position);
                        removeFocus();
                    }
                });
                checkWrongPort(vWrongPort, camera.cameraId, camera.cameraType);
            }
        }

        private void removeFocus() {
            etNotes.clearFocus();
            //cameraDetails.requestFocus();
            Utils.hideKeyboard(activity);
        }
    }

    private List<String> mountingLocationList;
    private List<String> fieldOfViewList;
    private OnItemClickListener onGenerateTestImageClickListener;

    public CamerasAdapter(List<String> mountingLocationList, List<String> fieldOfViewList,
                          OnItemClickListener onGenerateTestImageClickListener) {
        this.mountingLocationList = mountingLocationList;
        this.fieldOfViewList = fieldOfViewList;
        this.onGenerateTestImageClickListener = onGenerateTestImageClickListener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_camera, parent, false);
        return new CamerasViewHolder(itemView);
    }

    private void checkWrongPort(View view, int cameraId, BleConstants.CameraType cameraType) {
        BleConstants.CameraInfo info = BleConstants.CameraInfo.byValue((byte) cameraId);
        if (info == null) {
            return;
        }

        switch (info) {
            case WABCO_CAMERA:
                view.setVisibility(cameraType == BleConstants.CameraType.TYPE_WABCO
                                   ? View.GONE
                                   : View.VISIBLE);
                break;
            case DIGITAL_CAMERA_POSITION_1:
            case DIGITAL_CAMERA_POSITION_2:
            case DIGITAL_CAMERA_POSITION_4:
                view.setVisibility((cameraType == BleConstants.CameraType.TYPE_SONY
                        || cameraType == BleConstants.CameraType.TYPE_SONY_IR)
                                   ? View.GONE
                                   : View.VISIBLE);
                break;
            case ANALOG_CAMERA_6:
            case ANALOG_CAMERA_7:
            case ANALOG_CAMERA_8:
            case ANALOG_CAMERA_9:
                view.setVisibility(cameraType == BleConstants.CameraType.TYPE_ANALOG
                                   ? View.GONE
                                   : View.VISIBLE);
                break;
            default:
                view.setVisibility(View.GONE);
        }

    }
}