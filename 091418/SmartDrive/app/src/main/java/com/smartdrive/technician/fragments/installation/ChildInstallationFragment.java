package com.smartdrive.technician.fragments.installation;

import com.smartdrive.technician.fragments.BaseFragment;
import com.smartdrive.technician.models.report.Report;

import static com.smartdrive.technician.Constants.REPORT;

public abstract class ChildInstallationFragment extends BaseFragment {

    protected abstract void updateReport(Report report);

    @Override
    public void onDestroyView() {
        Report report = null;
        if (getArguments() != null) {
            report = getArguments().getParcelable(REPORT);
        }
        updateReport(report);
        super.onDestroyView();
    }

    protected boolean areFieldsValid() {
        return true;
    }

}