package com.smartdrive.technician.events;

public class ChangeFieldsEvent {

    public final boolean fieldsValid;

    public ChangeFieldsEvent(boolean fieldsValid) {
        this.fieldsValid = fieldsValid;
    }

}