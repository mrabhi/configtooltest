package com.smartdrive.technician.fragments;

import android.view.View;

import com.smartdrive.technician.EventsSubscriber;
import com.smartdrive.technician.R;
import com.smartdrive.technician.events.ble.TestEventGeneratedEvent;
import com.smartdrive.technician.utils.Dialogs;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

@EventsSubscriber
public abstract class BaseTriggerEventFragment extends BaseFragment {

    protected void triggerManualEvent() {
        Dialogs.showAlert(getContext(), null, getString(R.string.trigger_manual_event_message),
                null, R.string.cancel, R.string.yes, true, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bleManager.generateTestEvent();
                    }
                }
        );
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(TestEventGeneratedEvent event) {
        if (isHidden()) return;
        Dialogs.showToast(getContext(), getString(R.string.trigger_manual_event), getString(R.string.event_successfully_triggered));
    }

}