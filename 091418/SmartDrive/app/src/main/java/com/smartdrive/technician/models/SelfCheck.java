package com.smartdrive.technician.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.smartdrive.technician.R;

import java.util.List;

@Table(name = "SelfCheck")
public class SelfCheck extends Model implements Parcelable {

    public static final Parcelable.Creator<SelfCheck> CREATOR = new Parcelable.Creator<SelfCheck>() {
        @Override
        public SelfCheck createFromParcel(Parcel source) {
            return new SelfCheck(source);
        }

        @Override
        public SelfCheck[] newArray(int size) {
            return new SelfCheck[size];
        }
    };

    private static final String RED = "red";
    private static final String YELLOW = "yellow";

    @Column
    public long reportId;
    @Column
    public String serialNumber;
    @Column
    public long runTime;
    @Column
    public String status;
    @Column
    public String background;
    public List<SelfCheckItem> items;

    public SelfCheck() {
    }

    protected SelfCheck(Parcel in) {
        this.serialNumber = in.readString();
        this.runTime = in.readLong();
        this.status = in.readString();
        this.background = in.readString();
        this.items = in.createTypedArrayList(SelfCheckItem.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.serialNumber);
        dest.writeLong(this.runTime);
        dest.writeString(this.status);
        dest.writeString(this.background);
        dest.writeTypedList(this.items);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getBackgroundColorRes() {
        if (!TextUtils.isEmpty(background)) {
            if (background.equals(YELLOW)) {
                return R.color.yellow;
            } else if (background.equals(RED)) {
                return android.R.color.holo_red_dark;
            }
        }
        return R.color.green;
    }

    public void saveToDB() {
        deleteOldFromDB();
        save();
        if (items != null) {
            for (SelfCheckItem item : items) {
                item.selfCheckId = getId();
                item.save();
            }
        }
    }

    public void deleteFromDB() {
        delete();
        if (items != null) {
            for (SelfCheckItem item : items) {
                item.delete();
            }
        }
    }

    private void deleteOldFromDB() {
        if (serialNumber == null) return;
        SelfCheck oldSelfCheck = new Select().from(SelfCheck.class).where("reportId = ?", reportId).and("serialNumber = ?", serialNumber).executeSingle();
        if (oldSelfCheck != null) {
            oldSelfCheck.items = SelfCheckItem.getListFromDB(oldSelfCheck.getId());
            oldSelfCheck.deleteFromDB();
        }
    }

    public static SelfCheck getLastFromDB(String deviceSerialNumber) {
        if (deviceSerialNumber == null) return null;
        SelfCheck selfCheck = new Select().from(SelfCheck.class).where("serialNumber = ?", deviceSerialNumber).orderBy("runTime DESC").executeSingle();
        if (selfCheck != null) {
            selfCheck.items = SelfCheckItem.getListFromDB(selfCheck.getId());
        }
        return selfCheck;
    }

}