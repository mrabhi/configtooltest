package com.smartdrive.technician.fragments.installation;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;

import com.smartdrive.technician.EventsSubscriber;
import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.SmartDriveApp;
import com.smartdrive.technician.events.AddPendingReportEvent;
import com.smartdrive.technician.events.ChangeFieldsEvent;
import com.smartdrive.technician.events.NavigationEvent;
import com.smartdrive.technician.events.ReportSentEvent;
import com.smartdrive.technician.events.ble.BleErrorEvent;
import com.smartdrive.technician.events.ble.CurrentTimeGotEvent;
import com.smartdrive.technician.events.ble.SelfCheckGeneratedEvent;
import com.smartdrive.technician.events.ble.installation.CommunicationSettingsSetEvent;
import com.smartdrive.technician.fragments.BaseFragment;
import com.smartdrive.technician.managers.ReportSendingManager;
import com.smartdrive.technician.models.report.Report;
import com.smartdrive.technician.utils.Dialogs;
import com.smartdrive.technician.utils.Utils;
import com.smartdrive.technician.widgets.installation.DotsView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

import static com.smartdrive.technician.Constants.REPORT;
import static com.smartdrive.technician.Constants.SERIAL_NUMBER;
import static com.smartdrive.technician.Constants.TOOLBAR_TITLE;
import static com.smartdrive.technician.Constants.USER_MODE;
import static com.smartdrive.technician.Constants.VEHICLE_ID;

@EventsSubscriber
@Layout(id = R.layout.fragment_installation)
public class InstallationFragment extends BaseFragment {

    @Bind(R.id.installationBack)
    ImageView installationBack;
    @Bind(R.id.installationNext)
    ImageView installationNext;
    @Bind(R.id.dotsView)
    DotsView dotsView;

    private Dialog sendReportDialog;
    private GetStartedFragment getStartedFragment;
    private List<Class<? extends ChildInstallationFragment>> childFragments;
    private Report report;

    @Override
    protected void initView() {
        report = new Report();
        getStartedFragment = (GetStartedFragment) getChildFragmentManager().findFragmentById(
                R.id.getStartedFragment);
        installationBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideKeyboard(getActivity());
                getChildFragmentManager().popBackStack();
                dotsView.selectPrevious();
                setNextButtonEnabled();
                if (!dotsView.hasDots()) {
                    installationBack.setVisibility(View.INVISIBLE);
                    getChildFragmentManager()
                            .beginTransaction()
                            .show(getStartedFragment)
                            .commit();
                }
            }
        });
        installationNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!dotsView.hasDots()) {
                    if (!getStartedFragment.areFieldsValid()) {
                        return;
                    }
                    if (!getStartedFragment.hasChecks()) {
                        Dialogs.showToast(getContext(), getString(R.string.error),
                                          getString(R.string.nothing_checked));
                    } else if (getStartedFragment.isSonEmpty()) {
                        Dialogs.showAlert(getContext(), null, getString(R.string.son_not_provided),
                                          null, R.string.no, R.string.yes, true,
                                          new View.OnClickListener() {
                                              @Override
                                              public void onClick(View v) {
                                                  onGetStartedConfigured();
                                              }
                                          }
                        );
                    } else {
                        onGetStartedConfigured();
                    }
                } else if (dotsView.isEndDotSelected()) {
                    showSendReportDialog();
                } else {
                    ChildInstallationFragment currentFragment = (ChildInstallationFragment) getChildFragmentManager()
                            .findFragmentById(R.id.installationContainer);
                    if (currentFragment == null || currentFragment.areFieldsValid()) {
                        dotsView.selectNext();
                        if (dotsView.isEndDotSelected() && report.selfCheck == null) {
                            setNextButtonDisabled(false);
                        }
                        showNextFragment();
                    } else {
                        setNextButtonDisabled(true);
                    }
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        hideSendReportDialog();
        super.onDestroyView();
    }

    public static InstallationFragment newInstance() {
        InstallationFragment fragment = new InstallationFragment();
        Bundle args = new Bundle();
        args.putString(TOOLBAR_TITLE, SmartDriveApp.getContext()
                .getString(R.string.verify_installation));
        fragment.setArguments(args);
        return fragment;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChangeFieldsEvent(ChangeFieldsEvent event) {
        if (event.fieldsValid) {
            setNextButtonEnabled();
        } else {
            setNextButtonDisabled(true);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReportSent(final ReportSentEvent event) {
        hideSendReportDialog();
        String title = getString(event.isSuccessfully
                                 ? R.string.report_successfully_sent
                                 : R.string.unable_to_send_report);
        String message = event.isSuccessfully
                         ? null
                         : getString(R.string.report_saved);
        int imageRes = event.isSuccessfully
                       ? R.drawable.ic_check_circle_green
                       : R.drawable.ic_warning;
        Dialog dialog = Dialogs.showToast(getContext(), title, message, imageRes);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                EventBus.getDefault()
                        .post(new NavigationEvent(null));
                if (!event.isSuccessfully) {
                    report.saveToDB();
                    EventBus.getDefault()
                            .post(new AddPendingReportEvent(report));
                }
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(final CommunicationSettingsSetEvent event) {
        if (!event.failed) {
            report.communicationSettings = event.settings;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(SelfCheckGeneratedEvent event) {
        report.selfCheck = event.selfCheck;
        setNextButtonEnabled();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(CurrentTimeGotEvent event) {
        report.time = event.currentTime.getTimeInMillis();
        Intent intent = getActivity().getIntent();
        report.serialNumber = intent.getStringExtra(SERIAL_NUMBER);
        String vehicleId = intent.getStringExtra(VEHICLE_ID);
        if (vehicleId == null) {
            vehicleId = "";
        }
        report.vehicleId = vehicleId;
        byte userMode = intent.getByteExtra(USER_MODE, (byte) 0);
        if (bleManager.isGuestMode(userMode)) {
            report.userMode = getString(R.string.guest);
        } else if (bleManager.isMaintenanceMode(userMode)) {
            report.userMode = getString(R.string.maintenance);
        } else {
            report.userMode = getString(R.string.admin);
        }
        ReportSendingManager.getInstance()
                .send(report);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(BleErrorEvent event) {
        hideSendReportDialog();
    }

    private void onGetStartedConfigured() {
        getStartedFragment.updateReport(report);
        initChildFragmentsList();
        dotsView.showDots(childFragments.size());
        installationBack.setVisibility(View.VISIBLE);
        getChildFragmentManager()
                .beginTransaction()
                .hide(getStartedFragment)
                .commit();
        showNextFragment();
    }

    private void initChildFragmentsList() {
        childFragments = new ArrayList<>();
        childFragments.add(VehicleInfoFragment.class);
        if (getStartedFragment.isControllerChecked()) {
            childFragments.add(ControllerFragment.class);
        }
        if (getStartedFragment.isSensorBarChecked()) {
            childFragments.add(SensorBarFragment.class);
        }
        if (getStartedFragment.isCamerasChecked()) {
            childFragments.add(CamerasFragment.class);
        }
        if (getStartedFragment.isAdasCameraChecked()) {
            childFragments.add(AdasCameraFragment.class);
        }
        if (getStartedFragment.isLaneDepartureChecked()) {
            childFragments.add(VerifyTurnSignalsFragment.class);
        }
        if (getStartedFragment.isOtherCamerasChecked()) {
            childFragments.add(OtherCamerasFragment.class);
        }
        if (getStartedFragment.isControllerChecked()) {
            childFragments.add(CommunicationsFragment.class);
        }
        if (getStartedFragment.isAccessoriesChecked()) {
            childFragments.add(AccessoriesFragment.class);
        }
        if (getStartedFragment.isInterchangeChecked()) {
            childFragments.add(InterchangeFragment.class);
        }
        childFragments.add(SelfCheckReportFragment.class);
    }

    private void showNextFragment() {
        Utils.hideKeyboard(getActivity());
        String fragmentClassName = childFragments.get(dotsView.getSelectedDotIndex())
                .getName();
        Bundle args = new Bundle();
        args.putParcelable(REPORT, report);
        Fragment childFragment = Fragment.instantiate(getContext(), fragmentClassName, args);
        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.installationContainer, childFragment, fragmentClassName)
                .addToBackStack(fragmentClassName)
                .commit();
    }

    private void showSendReportDialog() {
        sendReportDialog = Dialogs.showAlert(getContext(), getString(R.string.send_report),
                                             getString(R.string.report_finished),
                                             null, R.string.cancel, R.string.send_report, false,
                                             new View.OnClickListener() {
                                                 @Override
                                                 public void onClick(View v) {
                                                     Dialogs.showProgress(sendReportDialog,
                                                                          R.string.sending_of_report);
                                                     bleManager.getCurrentTime();
                                                 }
                                             }
        );
    }

    private void hideSendReportDialog() {
        if (sendReportDialog != null) {
            sendReportDialog.dismiss();
        }
    }

    private void setNextButtonEnabled() {
        installationNext.setEnabled(true);
        installationNext.setImageResource(R.drawable.selector_btn_next);
    }

    private void setNextButtonDisabled(boolean onlyColor) {
        if (onlyColor) {
            installationNext.setImageResource(R.drawable.ic_arrowgray_right);
        } else {
            installationNext.setEnabled(false);
        }
    }

}