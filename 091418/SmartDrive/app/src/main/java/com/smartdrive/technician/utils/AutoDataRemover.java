package com.smartdrive.technician.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.smartdrive.technician.managers.VideoManager;

public class AutoDataRemover extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        VideoManager.getInstance().deleteAllSessionVideos();
    }

}