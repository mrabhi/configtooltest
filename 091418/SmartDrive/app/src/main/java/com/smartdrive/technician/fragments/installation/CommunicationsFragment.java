package com.smartdrive.technician.fragments.installation;

import android.support.annotation.StringRes;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.ScrollView;

import com.smartdrive.technician.EventsSubscriber;
import com.smartdrive.technician.Layout;
import com.smartdrive.technician.OnChangeListener;
import com.smartdrive.technician.R;
import com.smartdrive.technician.events.ChangeFieldsEvent;
import com.smartdrive.technician.events.ble.installation.CommunicationSettingsGotEvent;
import com.smartdrive.technician.events.ble.installation.CommunicationSettingsTestedEvent;
import com.smartdrive.technician.models.report.CommunicationSettings;
import com.smartdrive.technician.models.report.Report;
import com.smartdrive.technician.widgets.installation.CellularView;
import com.smartdrive.technician.widgets.installation.CheckCommunicationView;
import com.smartdrive.technician.widgets.installation.CommunicationProtocolView;
import com.smartdrive.technician.widgets.installation.WifiSecurityView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;

import static com.smartdrive.technician.Constants.REPORT;

@EventsSubscriber
@Layout(id = R.layout.fragment_communications)
public class CommunicationsFragment extends ChildInstallationFragment {

    @Bind(R.id.scrollCommunications)
    ScrollView scrollCommunications;
    @Bind(R.id.contentContainer)
    View contentContainer;
    @Bind(R.id.rgCommunications)
    RadioGroup rgCommunications;

    @Bind(R.id.cellularView)
    CellularView cellularView;
    @Bind(R.id.wifiSecurityView)
    WifiSecurityView wifiSecurityView;
    @Bind(R.id.wifiProtocolView)
    CommunicationProtocolView wifiProtocolView;
    @Bind(R.id.ethernetProtocolView)
    CommunicationProtocolView ethernetProtocolView;

    @Bind(R.id.checkCellularView)
    CheckCommunicationView checkCellularView;
    @Bind(R.id.checkWifiView)
    CheckCommunicationView checkWifiView;
    @Bind(R.id.checkEthernetView)
    CheckCommunicationView checkEthernetView;

    private OnChangeListener onChangeListener = new OnChangeListener() {
        @Override
        public void onChange() {
            EventBus.getDefault().post(new ChangeFieldsEvent(checkFields(false)));
        }
    };

    @Override
    protected void initView() {
        rgCommunications.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                cellularView.setVisibility(checkedId == R.id.rbCellular ? View.VISIBLE : View.GONE);
                checkCellularView.setVisibility(checkedId == R.id.rbCellular ? View.VISIBLE : View.GONE);

                wifiSecurityView.setVisibility(checkedId == R.id.rbWiFi ? View.VISIBLE : View.GONE);
                wifiProtocolView.setVisibility(checkedId == R.id.rbWiFi ? View.VISIBLE : View.GONE);
                checkWifiView.setVisibility(checkedId == R.id.rbWiFi ? View.VISIBLE : View.GONE);
                wifiSecurityView.setOnChangeListener(checkedId == R.id.rbWiFi ? onChangeListener : null);
                wifiProtocolView.setOnChangeListener(checkedId == R.id.rbWiFi ? onChangeListener : null);

                ethernetProtocolView.setVisibility(checkedId == R.id.rbEthernet ? View.VISIBLE : View.GONE);
                checkEthernetView.setVisibility(checkedId == R.id.rbEthernet ? View.VISIBLE : View.GONE);
                ethernetProtocolView.setOnChangeListener(checkedId == R.id.rbEthernet ? onChangeListener : null);
                onChangeListener.onChange();
            }
        });
        checkCellularView.init(R.string.ping_to_smartdrive, R.string.check_cellular_connection, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCommunicationSettings(R.string.checking_cellular_connection);
            }
        });
        checkWifiView.init(R.string.ping_to_smartdrive, R.string.check_wifi_connection, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCommunicationSettings(R.string.checking_wifi_connection);
            }
        });
        checkEthernetView.init(R.string.ping_to_smartdrive, R.string.check_ethernet_connection, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCommunicationSettings(R.string.checking_ethernet_connection);
            }
        });
        Report report = getArguments().getParcelable(REPORT);
        if (report == null) return;
        if (report.communicationSettings == null) {
            showProgress(R.string.loading_communications);
            bleManager.getCommunicationSettings();
        } else {
            showCommunicationSettings(report.communicationSettings);
        }
    }

    @Override
    protected void updateReport(Report report) {
        CommunicationSettings settings = getCommunicationSettings();
        if (settings != null && !settings.equals(report.communicationSettings)) {
            showProgress(R.string.setting_communications);
            bleManager.setCommunicationSettings(settings, false);
        } else {
            switch (rgCommunications.getCheckedRadioButtonId()) {
                case R.id.rbCellular:
                    report.communicationSettings.signalStrength = checkCellularView.getSignalStrength();
                    report.communicationSettings.ping = checkCellularView.getPing();
                    break;
                case R.id.rbWiFi:
                    report.communicationSettings.signalStrength = checkWifiView.getSignalStrength();
                    report.communicationSettings.ping = checkWifiView.getPing();
                    break;
                case R.id.rbEthernet:
                    report.communicationSettings.signalStrength = checkEthernetView.getSignalStrength();
                    report.communicationSettings.ping = checkEthernetView.getPing();
                    break;
            }
        }
    }

    @Override
    protected boolean areFieldsValid() {
        return checkFields(true);
    }

    private boolean checkFields(boolean pickOut) {
        if (pickOut) contentContainer.requestFocus();
        switch (rgCommunications.getCheckedRadioButtonId()) {
            case R.id.rbWiFi:
                return wifiSecurityView.isFieldsValid(pickOut) && wifiProtocolView.isFieldsValid(pickOut);
            case R.id.rbEthernet:
                return ethernetProtocolView.isFieldsValid(pickOut);
            default:
                return true;
        }
    }

    private void showCommunicationSettings(CommunicationSettings settings) {
        hideProgress();
        if (settings == null) return;
        switch (settings.type) {
            case CELLULAR:
                rgCommunications.check(R.id.rbCellular);
                cellularView.init(settings.cellular);
                checkCellularView.initCheckResult(settings.signalStrength, settings.ping);
                break;
            case WIFI:
                rgCommunications.check(R.id.rbWiFi);
                wifiSecurityView.init(settings.wifiSecurity);
                wifiProtocolView.init(settings.protocol);
                checkWifiView.initCheckResult(settings.signalStrength, settings.ping);
                break;
            case ETHERNET:
                rgCommunications.check(R.id.rbEthernet);
                ethernetProtocolView.init(settings.protocol);
                checkEthernetView.initCheckResult(settings.signalStrength, settings.ping);
                break;
        }
        onChangeListener.onChange();
    }

    private CommunicationSettings getCommunicationSettings() {
        CommunicationSettings settings = new CommunicationSettings();
        switch (rgCommunications.getCheckedRadioButtonId()) {
            case R.id.rbCellular:
                settings.type = CommunicationSettings.Type.CELLULAR;
                settings.cellular = cellularView.getCellular();
                settings.signalStrength = checkCellularView.getSignalStrength();
                settings.ping = checkCellularView.getPing();
                break;
            case R.id.rbWiFi:
                settings.type = CommunicationSettings.Type.WIFI;
                settings.wifiSecurity = wifiSecurityView.getWifiSecurity();
                settings.protocol = wifiProtocolView.getProtocol();
                settings.signalStrength = checkWifiView.getSignalStrength();
                settings.ping = checkWifiView.getPing();
                break;
            case R.id.rbEthernet:
                settings.type = CommunicationSettings.Type.ETHERNET;
                settings.protocol = ethernetProtocolView.getProtocol();
                settings.signalStrength = checkEthernetView.getSignalStrength();
                settings.ping = checkEthernetView.getPing();
                break;
            default:
                return null;
        }
        return settings;
    }

    private void checkCommunicationSettings(@StringRes int progressTextRes) {
        if (!areFieldsValid()) return;
        Report report = getArguments().getParcelable(REPORT);
        if (report == null) return;
        showProgress(progressTextRes);
        CommunicationSettings settings = getCommunicationSettings();
        if (settings != null && !settings.equals(report.communicationSettings)) {
            bleManager.setCommunicationSettings(settings, true);
        } else {
            bleManager.testWithoutSetCommunicationSettings();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(CommunicationSettingsGotEvent event) {
        Report report = getArguments().getParcelable(REPORT);
        if (report != null) {
            report.communicationSettings = event.settings;
        }
        showCommunicationSettings(event.settings);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(CommunicationSettingsTestedEvent event) {
        hideProgress();
        String signalStrength = event.signalStrength;
        String ping = event.ping;
        switch (rgCommunications.getCheckedRadioButtonId()) {
            case R.id.rbCellular:
                checkCellularView.initCheckResult(signalStrength, ping);
                break;
            case R.id.rbWiFi:
                checkWifiView.initCheckResult(signalStrength, ping);
                break;
            case R.id.rbEthernet:
                checkEthernetView.initCheckResult(signalStrength, ping);
                break;
        }
        scrollCommunications.post(new Runnable() {
            @Override
            public void run() {
                scrollCommunications.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

}