package com.smartdrive.technician.widgets.installation;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import com.smartdrive.technician.R;
import com.smartdrive.technician.SmartDriveApp;
import com.smartdrive.technician.models.report.Cellular;
import com.smartdrive.technician.utils.CsvParser;
import com.smartdrive.technician.widgets.BaseSpinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CellularView extends LinearLayout {

    @Bind(R.id.spinCountry)
    BaseSpinner spinCountry;
    @Bind(R.id.spinCarrier)
    BaseSpinner spinCarrier;

    private List<Cellular> availableCellulars;
    private String defaultCountry;
    private String defaultCarrier;

    private AdapterView.OnItemSelectedListener onCountrySelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String selectedItem = spinCountry.getSelectedItem();
            if (selectedItem == null) {
                selectedItem = defaultCountry;
            }
            initSpinCarrier(selectedItem, defaultCarrier);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    public CellularView(Context context) {
        super(context);
        create();
    }

    public CellularView(Context context, AttributeSet attrs) {
        super(context, attrs);
        create();
    }

    public CellularView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        create();
    }

    private void create() {
        inflate(getContext(), R.layout.view_cellular, this);
        ButterKnife.bind(this);
        availableCellulars = CsvParser.parseCellulars();
        defaultCountry = SmartDriveApp.getContext().getString(R.string.country);
        defaultCarrier = SmartDriveApp.getContext().getString(R.string.carrier);
        spinCountry.setOnItemSelectedListener(onCountrySelectedListener);
        spinCountry.init(getCountries(), R.string.country, false);
        initSpinCarrier(defaultCountry, defaultCarrier);
    }

    public void init(final Cellular cellular) {
        spinCountry.setOnItemSelectedListener(null);
        final String country = isValidCountry(cellular) ? cellular.country : defaultCountry;
        spinCountry.setSelectedItem(country);
        String carrier = isValidCarrier(cellular) ? cellular.carrier : defaultCarrier;
        initSpinCarrier(country, carrier);
        spinCountry.post(new Runnable() {
            @Override
            public void run() {
                spinCountry.setOnItemSelectedListener(onCountrySelectedListener);
            }
        });
    }

    private void initSpinCarrier(String country, String carrier) {
        spinCarrier.init(getCarriers(country), R.string.carrier, false);
        spinCarrier.setSelectedItem(carrier);
        spinCarrier.setEnabled(!country.equals(defaultCountry));
    }

    private List<String> getCountries() {
        List<String> countries = new ArrayList<>();
        for (Cellular item : availableCellulars) {
            if (!countries.contains(item.country)) {
                countries.add(item.country);
            }
        }
        return countries;
    }

    private List<String> getCarriers(String country) {
        List<String> carriers = new ArrayList<>();
        if (!country.equals(defaultCountry)) {
            for (Cellular item : availableCellulars) {
                if (item.country.equals(country)) {
                    carriers.add(item.carrier);
                }
            }
        }
        return carriers;
    }

    private boolean isValidCountry(Cellular cellular) {
        if (cellular != null && !TextUtils.isEmpty(cellular.country)) {
            for (Cellular item : availableCellulars) {
                if (cellular.country.equals(item.country)) return true;
            }
        }
        return false;
    }

    private boolean isValidCarrier(Cellular cellular) {
        if (cellular != null && !TextUtils.isEmpty(cellular.carrier)) {
            for (Cellular item : availableCellulars) {
                if (cellular.carrier.equals(item.carrier)) return true;
            }
        }
        return false;
    }

    public Cellular getCellular() {
        Cellular cellular = new Cellular();
        String selectedCountry = spinCountry.getSelectedItem();
        String selectedCarrier = spinCarrier.getSelectedItem();
        if (selectedCountry != null && !selectedCountry.equals(defaultCountry)) {
            cellular.country = selectedCountry;
            if (selectedCarrier != null && !selectedCarrier.equals(defaultCarrier)) {
                cellular.carrier = selectedCarrier;
                for (Cellular item : availableCellulars) {
                    if (item.country.equals(selectedCountry) && item.carrier.equals(selectedCarrier)) {
                        cellular.apn = item.apn;
                        return cellular;
                    }
                }
            }
        }
        return cellular;
    }

}
