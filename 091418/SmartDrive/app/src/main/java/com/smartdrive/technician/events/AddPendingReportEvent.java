package com.smartdrive.technician.events;

import com.smartdrive.technician.models.report.Report;

public class AddPendingReportEvent {

    public final Report report;

    public AddPendingReportEvent(Report report) {
        this.report = report;
    }
}
