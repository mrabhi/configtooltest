package com.smartdrive.technician.models.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class VehicleInfo {

    @SerializedName("VehicleType")
    @Expose
    private String vehicleType;
    @SerializedName("Model")
    @Expose
    private String model;
    @SerializedName("Year")
    @Expose
    private String year;
    @SerializedName("Make")
    @Expose
    private String make;

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(model)
                .append(vehicleType)
                .append(year)
                .append(make)
                .toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof VehicleInfo)) {
            return false;
        }
        VehicleInfo rhs = ((VehicleInfo) other);
        return new EqualsBuilder().append(model, rhs.model)
                .append(vehicleType, rhs.vehicleType)
                .append(year, rhs.year)
                .append(make, rhs.make)
                .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("vehicleType", vehicleType)
                .append("model", model)
                .append("year", year)
                .append("make", make)
                .toString();
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getManufacturer() {
        return make;
    }

    public void setManufacturer(String make) {
        this.make = make;
    }

}
