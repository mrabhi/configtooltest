package com.smartdrive.technician;

import android.support.v4.util.PatternsCompat;

public interface Constants {

    //String SERVER_URL = "http://ec2-54-165-251-75.compute-1.amazonaws.com:1234/";
    String SERVER_URL = "https://services-qa2.smartdrivesystems.com/SRConfigTool";
    String AUTH_HASH_KEY = "x-authorization-hash";
    String AUTH_HASH_VALUE = "abcdef1234567890";
    String CONTENT_TYPE_APPLICATION_XML = "application/xml";
    String CACHE_CONTROL_KEY = "cache-control";
    String CACHE_CONTROL_VALUE = "no-cache";

    String URL_SCHEMA = "https";
    String URL_HOST = "services-qa2.smartdrivesystems.com";
    String CONFIG_PATH = "SRConfigTool";
    String VIN_PATH = "vin";

    String IMAGE_PROCESSING_URL_SCHEMA = "http";
    String IMAGE_PROCESSING_HOST = "imgs-sandbox.intsig.net";
    String IMAGE_PROCESSING_PATH = "icr";
    String IMAGE_PROCESSING_PATH_SECOND_PART = "recognize_document";

    long INACTIVE_TIME = 1800000; //30 minutes
    long USER_DATA_SAVING_TIME = 86400000; //24 hours

    int SPLASH_DELAY = 2000;
    int TOAST_DELAY = 5000;
    int BUFFER = 2048;
    int VIDEO_FRAME = 5000;

    int DEFAULT_SERVER_TIMEOUT_DURATINO = 30;

    int RC_ENABLE_BLE = 1;
    int RC_ENABLE_BLE_WITH_SCAN = 2;
    int RC_ENABLE_BLE_WITH_RECONNECT = 3;
    int RC_LOCATION = 4;
    int RC_STORAGE = 5;

    String PASSWORD_REGEXP = "((?=.*[a-z])(?=.*[A-Z])(?=.*[\\Q~!@#$%^&*_-+=`|\\(){}[]:;\"'<>,.?/\\E])(?=\\S+$).{8,12})";
    String IP_ADDRESS_REGEXP = PatternsCompat.IP_ADDRESS.pattern();
    String VEHICLE_ID_REGEXP = "^$|^[a-zA-Z0-9\\Q~!@#$%^&*_-+=`|\\(){}[]:;\"'<>,.?/\\E]+$";
    String MAKE_REGEXP = "^$|^[a-zA-Z0-9\\Q~!@#$%^&*_-+=`|\\(){}[]:;\"'<>,.?/\\E\\s]+$";

    String INTENT_DEFAULT_DATA_OBJECT = "data";

    String LARGE_SPACE = "   ";
    String SPACE = " ";
    String PACKAGE = "package:%1$s";
    String HEADER_XML = "header.xml";
    String SLASH = "/";
    String SEPARATOR = ";";
    String ROOT_FOLDER_NAME = "SmartDrive";
    String MP4_VIDEO = "video/mp4";
    String USER_MODE = "user.mode";
    String SERIAL_NUMBER = "serial.number";
    String VEHICLE_ID = "vehicle.id";
    String TOOLBAR_TITLE = "toolbar.title";
    String SHOW_SERIAL_NUMBER = "show.serial.number";
    String SHOW_VEHICLE_ID = "show.vehicle.id";
    String CAMERA = "camera";
    String EVENT = "event";
    String REPORT = "report";
    String AUTO_SCAN = "auto.scan";

    int MAX_CAR_MANUFACTURING_YEAR = 2019;
    int MIN_CAR_MANUFACTURING_YEAR = 1990;
    int DIGITAL_CAMERAS_SCREEN = 1;
    int ANALOG_CAMERAS_SCREEN = 2;
    int ADAS_CAMERA_SCREEN = 3;


    String SELFCHECK_SECTION_OPEN="<section type=\"selfcheck\">";
    String SECTION_CLOSE="</section>";
    String ADASCHECK_SECTION_OPEN="<section type=\"adas\">";
}