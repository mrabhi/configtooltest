package com.smartdrive.technician.fragments.installation;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.smartdrive.technician.Constants;
import com.smartdrive.technician.EventsSubscriber;
import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.SmartDriveApp;
import com.smartdrive.technician.adapters.BaseAdapter;
import com.smartdrive.technician.adapters.CamerasAdapter;
import com.smartdrive.technician.controller.BleConstants;
import com.smartdrive.technician.events.NavigationEvent;
import com.smartdrive.technician.events.ble.GetCameraInfoEvent;
import com.smartdrive.technician.models.report.Camera;
import com.smartdrive.technician.models.report.Report;
import com.smartdrive.technician.utils.CsvParser;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

@EventsSubscriber
@Layout(id = R.layout.fragment_cameras)
public class OtherCamerasFragment extends ChildInstallationFragment {

    protected CamerasAdapter adapter;
    protected int currentCamera = 0;
    @Bind(R.id.camerasHeader)
    TextView camerasHeader;
    @Bind(R.id.rvCameras)
    RecyclerView rvCameras;
    @Bind(R.id.refresh)
    View refresh;

    @Override
    protected void initView() {
        camerasHeader.setText(getHeaderTextRes());
        adapter = new CamerasAdapter(CsvParser.parseCameraMountingLocations(),
                                     CsvParser.parseFieldOfView(),
                                     new BaseAdapter.OnItemClickListener() {
                                         @Override
                                         public void onClick(View view, int position) {
                                             Camera camera = adapter.getItem(position);
                                             EventBus.getDefault()
                                                     .post(new NavigationEvent(
                                                             GenerateImageFragment.newInstance(
                                                                     camera)));
                                         }
                                     }
        );
        adapter.setActivity(getActivity());
        rvCameras.setAdapter(adapter);
        Report report = null;
        if (getArguments() != null) {
            report = getArguments().getParcelable(Constants.REPORT);
        }
        loadCameras(report);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Report report = null;
                if (getArguments() != null) {
                    report = getArguments().getParcelable(Constants.REPORT);
                }
                loadCameras(report);
            }
        });
    }

    @Override
    protected void updateReport(Report report) {
        List<Camera> cameras = new ArrayList<>();
        for (int i = 0; i < adapter.getItemCount(); i++) {
            Camera camera = adapter.getItem(i);
            cameras.add(camera);
            SmartDriveApp.addCameraMountingLocation(camera.cameraId, camera.mountingLocation);
        }
        addCamerasToReport(report, cameras);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCameraCheckResult(GetCameraInfoEvent event) {
        Log.d("OTHER SERIAL", event.getSerialNumber());
        Camera camera = adapter.getItem(currentCamera);
        camera.connected = event.isConnected();
        camera.serial = event.getSerialNumber();
        camera.cameraType = event.getCameraType();
        adapter.updateItem(camera, currentCamera);

        currentCamera++;
        if (adapter.getList()
                .size() > currentCamera) {
            bleManager.getCameraInfo(
                    BleConstants.CameraInfo.byValue(
                            (byte) adapter.getItem(currentCamera).cameraId));
        } else {
            currentCamera = 0;
            hideProgress();
        }
    }

    protected int getHeaderTextRes() {
        return R.string.other_cameras;
    }

    protected void loadCameras(Report report) {
        showProgress(R.string.rescanning_for_cameras);

        if (report == null || report.otherCameras == null || report.otherCameras.isEmpty()) {
            adapter.update(CsvParser.parseCameras(Constants.ANALOG_CAMERAS_SCREEN));
        } else {
            adapter.update(report.otherCameras);
        }

        if (adapter.getList()
                .size() > 0) {
            currentCamera = 0;
            bleManager.getCameraInfo(
                    BleConstants.CameraInfo.byValue(
                            (byte) adapter.getItem(currentCamera).cameraId));
        }
    }

    protected void addCamerasToReport(Report report, List<Camera> cameras) {
        report.otherCameras = cameras;
    }
}
