package com.smartdrive.technician.adapters;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartdrive.technician.R;
import com.smartdrive.technician.models.report.Camera;

import butterknife.Bind;

public class EventDetailsAdapter extends BaseAdapter<Camera> {

    protected class EventDetailsHolder extends BaseViewHolder {
        @Bind(R.id.tvName)
        TextView tvName;
        @Bind(R.id.tvPlace)
        TextView tvPlace;
        @Bind(R.id.ivPreview)
        ImageView ivPreview;

        private EventDetailsHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void bind(final int position) {
            tvName.setText(getItem(position).name);
            tvPlace.setText(location);
            tvPlace.setVisibility(TextUtils.isEmpty(location) ? View.GONE : View.VISIBLE);
            ivPreview.setImageBitmap(getItem(position).firstFrame);
        }

        @Override
        protected void setListeners(final int position, OnItemClickListener onItemClickListener) {
            super.setListeners(position, onItemClickListener);
            tvPlace.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onLocationClickListener.onClick(v, position);
                }
            });
        }
    }

    private OnItemClickListener onLocationClickListener;
    private String location;

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event_details, parent, false);
        return new EventDetailsHolder(itemView);
    }

    public void setOnLocationClickListener(OnItemClickListener onLocationClickListener) {
        this.onLocationClickListener = onLocationClickListener;
    }

    public void setLocation(String location) {
        this.location = location;
        if (!TextUtils.isEmpty(location)) {
            notifyDataSetChanged();
        }
    }

}