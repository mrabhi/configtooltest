package com.smartdrive.technician.events.ble;

public class AdasFlagCheckedEvent {
    private boolean isEnabled;

    public AdasFlagCheckedEvent(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public boolean isEnabled() {
        return isEnabled;
    }
}
