package com.smartdrive.technician.events.ble.installation;

import com.smartdrive.technician.models.report.CommunicationSettings;

public class CommunicationSettingsGotEvent {

    public CommunicationSettings settings;

    public CommunicationSettingsGotEvent(CommunicationSettings settings) {
        this.settings = settings;
    }
}