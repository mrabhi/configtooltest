package com.smartdrive.technician.managers;

import android.util.Log;

import com.google.gson.Gson;
import com.smartdrive.technician.functional.Receiver;
import com.smartdrive.technician.models.pojo.VehicleInfo;
import com.smartdrive.technician.utils.BgExecutors;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

import static com.smartdrive.technician.Constants.CACHE_CONTROL_KEY;
import static com.smartdrive.technician.Constants.CACHE_CONTROL_VALUE;
import static com.smartdrive.technician.Constants.CONFIG_PATH;
import static com.smartdrive.technician.Constants.DEFAULT_SERVER_TIMEOUT_DURATINO;
import static com.smartdrive.technician.Constants.URL_HOST;
import static com.smartdrive.technician.Constants.URL_SCHEMA;
import static com.smartdrive.technician.Constants.VIN_PATH;

public class VehicleInfoManager {

    private static final int STATUS_202 = 202;
    private static final int RETRIES_COUNT = 3;
    private static final String TAG = VehicleInfoManager.class.getName();
    private static VehicleInfoManager instance;
    private Response response = null;
    private AtomicInteger retryCount = new AtomicInteger(0);

    public VehicleInfoManager() {
    }

    public static VehicleInfoManager getInstance() {
        if (instance == null) {
            synchronized (VehicleInfoManager.class) {
                if (instance == null) {
                    instance = new VehicleInfoManager();
                }
            }
        }
        return instance;
    }

    public void send(String vin, Receiver<VehicleInfo> networkReceiver,
                     Receiver<Throwable> throwableReceiver) {
        executeRequest(vin, networkReceiver, throwableReceiver);
    }

    private void executeRequest(final String vin,
                                final Receiver<VehicleInfo> networkReceiver,
                                final Receiver<Throwable> errorReceiver) {
        BgExecutors.EXECUTOR.execute(new Runnable() {
            @Override
            public void run() {
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

                OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor)
                        .connectTimeout(DEFAULT_SERVER_TIMEOUT_DURATINO, TimeUnit.SECONDS)
                        .readTimeout(DEFAULT_SERVER_TIMEOUT_DURATINO, TimeUnit.SECONDS)
                        .writeTimeout(DEFAULT_SERVER_TIMEOUT_DURATINO, TimeUnit.SECONDS)
                        .build();
                try {
                    HttpUrl.Builder httpUrl = new HttpUrl.Builder();
                    httpUrl.scheme(URL_SCHEMA);
                    httpUrl.host(URL_HOST);
                    httpUrl.addPathSegment(CONFIG_PATH);
                    httpUrl.addPathSegment(VIN_PATH);
                    httpUrl.addQueryParameter("vinNumber", vin);
                    HttpUrl build = httpUrl.build();

                    String result = build.toString();

                    Request request = new Request.Builder()
                            .url(result)
                            .get()
                            .addHeader("Content-Encoding", "gzip")
                            .addHeader(CACHE_CONTROL_KEY, CACHE_CONTROL_VALUE)
                            .build();
                    client.newCall(request)
                            .enqueue(new Callback() {
                                @Override
                                public void onFailure(Call call, IOException e) {
                                    Log.e(TAG,
                                          "Http report sending return code " + response.code());
                                    errorReceiver.receive(new Throwable(e.getMessage()));
                                }

                                @Override
                                public void onResponse(Call call,
                                                       Response response) throws IOException {
                                    if (response != null) {
                                        Log.e(TAG,
                                              "Http report sending return code " + response.code());
                                        if (response.isSuccessful() && response.code() == STATUS_202
                                                && retryCount.get() < RETRIES_COUNT) {
                                            retryCount.set(retryCount.get() + 1);
                                            BgExecutors.EXECUTOR.schedule(new Runnable() {
                                                @Override
                                                public void run() {
                                                    executeRequest(vin, networkReceiver,
                                                                   errorReceiver);
                                                }
                                            }, 2, TimeUnit.SECONDS);
                                        } else if (isSuccessfulVehicleInfoResponse(response)) {
                                            retryCount.set(0);
                                            networkReceiver.receive(
                                                    convertResponseToVehicleInfo(response.body()
                                                                                         .string()));
                                        } else {
                                            retryCount.set(0);
                                            errorReceiver.receive(new Throwable("Server error"));
                                        }
                                    } else {
                                        errorReceiver.receive(new Throwable("Server error"));
                                    }
                                }
                            });
                } catch (Exception e) {
                    Log.e(TAG, "Http report sending exception");
                    e.printStackTrace();
                    errorReceiver.receive(new Throwable(e.getMessage()));
                } finally {
                    if (response != null) {
                        response.body()
                                .close();
                    }
                }
            }
        });
    }

    private VehicleInfo convertResponseToVehicleInfo(String body) {
        Gson gson = new Gson();
        return gson.fromJson(body, VehicleInfo.class);
    }

    private boolean isSuccessfulVehicleInfoResponse(Response response) {
        return response != null && response.isSuccessful() && response.code() != STATUS_202;
    }

}
