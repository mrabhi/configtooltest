package com.smartdrive.technician.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.smartdrive.technician.SmartDriveApp;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public abstract class Utils {

    private static final String GOOGLE_MAPS_APP_PACKAGE = "com.google.android.apps.maps";
    private static final String GOOGLE_MAPS_APP_LOCATION = "geo:0,0?q=%1$s,%2$s(%3$s)";
    private static final String GOOGLE_MAPS_WEB_LOCATION = "https://www.google.com/maps/?q=%1$s,%2$s";

    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public static boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) SmartDriveApp.getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo network = cm != null
                              ? cm.getActiveNetworkInfo()
                              : null;
        return network != null && network.isConnected();
    }

    public static String bitmapToStringBase64(Bitmap image) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, bos);
        return Base64.encodeToString(bos.toByteArray(), Base64.DEFAULT);
    }

    public static void openMap(Context context, double latitude, double longitude, String name) {
        if (context == null) {
            return;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(
                String.format(GOOGLE_MAPS_APP_LOCATION, latitude, longitude, name)));
        intent.setPackage(GOOGLE_MAPS_APP_PACKAGE);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        } else {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(
                    String.format(GOOGLE_MAPS_WEB_LOCATION, latitude, longitude)));
            context.startActivity(intent);
        }
    }

    public static String writeToFile(byte[] array) {
        String path = Environment.getExternalStorageDirectory()
                .getAbsolutePath()
                + File.separator
                + "SmartDrive"
                + File.separator
                + System.currentTimeMillis() + ".png";
        try {
            FileOutputStream stream = new FileOutputStream(path);
            stream.write(array);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }

    public static void splitSelfCheck(String xml) {

    }


    public static ArrayList<EditText> getAllEditTexts(ViewGroup v) {
        ArrayList<EditText> arrayList = new ArrayList<>();

        for (int i = 0; i < v.getChildCount(); i++) {
            if (v.getChildAt(i) instanceof EditText) {
                arrayList.add((EditText) v.getChildAt(i));
            } else if (v.getChildAt(i) instanceof ViewGroup) {
                arrayList.addAll(getAllEditTexts((ViewGroup) v.getChildAt(i)));
            }
        }

        return arrayList;
    }

}