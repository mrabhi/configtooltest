package com.smartdrive.technician.managers;

import android.graphics.Bitmap;
import android.support.v4.graphics.BitmapCompat;
import android.util.Log;

import com.google.gson.Gson;
import com.smartdrive.technician.functional.Receiver;
import com.smartdrive.technician.models.pojo.Vin;
import com.smartdrive.technician.utils.BgExecutors;

import java.io.ByteArrayOutputStream;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

import static com.smartdrive.technician.Constants.CACHE_CONTROL_KEY;
import static com.smartdrive.technician.Constants.CACHE_CONTROL_VALUE;
import static com.smartdrive.technician.Constants.DEFAULT_SERVER_TIMEOUT_DURATINO;
import static com.smartdrive.technician.Constants.IMAGE_PROCESSING_HOST;
import static com.smartdrive.technician.Constants.IMAGE_PROCESSING_PATH;
import static com.smartdrive.technician.Constants.IMAGE_PROCESSING_PATH_SECOND_PART;
import static com.smartdrive.technician.Constants.IMAGE_PROCESSING_URL_SCHEMA;

public class VinImageManager {

    private static final String USER_NAME = "smartdrive";
    private static final String USER_PASSWORD = "6asTUsPAsuhu";
    private static final String TAG = VinImageManager.class.getName();
    private static final int MAX_PHOTO_SIZE = 4000000;
    private static VinImageManager instance;

    public VinImageManager() {
    }

    public static VinImageManager getInstance() {
        if (instance == null) {
            synchronized (VinImageManager.class) {
                if (instance == null) {
                    instance = new VinImageManager();
                }
            }
        }
        return instance;
    }

    public void send(Bitmap vinImage, Receiver<Vin> networkReceiver,
                     Receiver<Throwable> errorReceiver) {
        executeRequest(vinImage, networkReceiver, errorReceiver);
    }

    private void executeRequest(final Bitmap vinImage,
                                final Receiver<Vin> networkReceiver,
                                final Receiver<Throwable> errorReceiver) {
        BgExecutors.EXECUTOR.execute(new Runnable() {
            @Override
            public void run() {
                Response response = null;
                Bitmap bitmap = vinImage;
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor)
                        .connectTimeout(DEFAULT_SERVER_TIMEOUT_DURATINO, TimeUnit.SECONDS)
                        .readTimeout(DEFAULT_SERVER_TIMEOUT_DURATINO, TimeUnit.SECONDS)
                        .writeTimeout(DEFAULT_SERVER_TIMEOUT_DURATINO, TimeUnit.SECONDS)
                        .build();

                if (BitmapCompat.getAllocationByteCount(bitmap) > MAX_PHOTO_SIZE) {
                    bitmap = scaleImageDownToCorrectSize(bitmap);
                }
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                bitmap.recycle();

                try {
                    HttpUrl.Builder httpUrl = new HttpUrl.Builder();
                    httpUrl.scheme(IMAGE_PROCESSING_URL_SCHEMA);
                    httpUrl.host(IMAGE_PROCESSING_HOST);
                    httpUrl.addPathSegment(IMAGE_PROCESSING_PATH);
                    httpUrl.addPathSegment(IMAGE_PROCESSING_PATH_SECOND_PART);
                    httpUrl.addQueryParameter("user", USER_NAME);
                    httpUrl.addQueryParameter("password", USER_PASSWORD);
                    HttpUrl build = httpUrl.build();

                    String result = build.toString();

                    Request request = new Request.Builder()
                            .url(result)
                            .post(RequestBody.create(MediaType.parse("application/json"),
                                                     byteArray))
                            .addHeader(CACHE_CONTROL_KEY, CACHE_CONTROL_VALUE)
                            .build();
                    response = client.newCall(request)
                            .execute();
                    if (response != null) {
                        Log.e(TAG, "Http report sending return code " + response.code());
                        if (response.isSuccessful()) {
                            networkReceiver
                                    .receive(convertResponseToVin(response.body()
                                                                          .string()));
                        } else {
                            errorReceiver.receive(new Throwable("Server error"));
                        }
                    } else {
                        errorReceiver.receive(new Throwable("Server error"));
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Http report sending exception");
                    e.printStackTrace();
                    errorReceiver.receive(new Throwable("Server error"));
                } finally {
                    if (response != null) {
                        response.body()
                                .close();
                    }
                }
            }
        });
    }

    private Vin convertResponseToVin(String body) {
        Gson gson = new Gson();
        return gson.fromJson(body, Vin.class);
    }

    private Bitmap scaleImageDownToCorrectSize(Bitmap bitmap) {
        float sizeMultiplier = MAX_PHOTO_SIZE / BitmapCompat.getAllocationByteCount(bitmap);
        return Bitmap.createScaledBitmap(bitmap, (int) (bitmap.getWidth() * sizeMultiplier),
                                         (int) (bitmap.getHeight() * sizeMultiplier), false);
    }

}
