package com.smartdrive.technician.models.report;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.smartdrive.technician.models.AdasCheck;
import com.smartdrive.technician.models.AdasCheckItem;
import com.smartdrive.technician.models.SelfCheck;
import com.smartdrive.technician.models.SelfCheckItem;

import java.util.List;

@Table(name = "Report")
public class Report extends Model implements Parcelable {

    public static final Parcelable.Creator<Report> CREATOR = new Parcelable.Creator<Report>() {
        @Override
        public Report createFromParcel(Parcel source) {
            return new Report(source);
        }

        @Override
        public Report[] newArray(int size) {
            return new Report[size];
        }
    };

    @Column
    public long time;
    @Column
    public String selectedSettings;
    @Column
    public String userMode;
    @Column
    public String serialNumber;
    @Column
    public String firstName;
    @Column
    public String lastName;
    @Column
    public String phoneNumber;
    @Column
    public String son;
    @Column
    public String vehicleId;
    @Column
    public String vehicleType;
    @Column
    public String year;
    @Column
    public String make;
    @Column
    public String model;
    @Column
    public String vin;
    @Column
    public String company;
    @Column
    public boolean leftTurnStatus;
    @Column
    public boolean rightTurnStatus;
    @Column
    public boolean leftTurnPolarityHigh;
    @Column
    public boolean leftTurnPolarityLow;
    @Column
    public boolean rightTurnPolarityHigh;
    @Column
    public boolean rightTurnPolarityLow;

    @Column(onUpdate = Column.ForeignKeyAction.CASCADE, onDelete = Column.ForeignKeyAction.CASCADE)
    public Controller controller;
    @Column(onUpdate = Column.ForeignKeyAction.CASCADE, onDelete = Column.ForeignKeyAction.CASCADE)
    public SensorBar sensorBar;
    @Column(onUpdate = Column.ForeignKeyAction.CASCADE, onDelete = Column.ForeignKeyAction.CASCADE)
    public CommunicationSettings communicationSettings;
    @Column(onUpdate = Column.ForeignKeyAction.CASCADE, onDelete = Column.ForeignKeyAction.CASCADE)
    public Accessories accessories;
    @Column(onUpdate = Column.ForeignKeyAction.CASCADE, onDelete = Column.ForeignKeyAction.CASCADE)
    public SelfCheck selfCheck;
    @Column(onUpdate = Column.ForeignKeyAction.CASCADE, onDelete = Column.ForeignKeyAction.CASCADE)
    public AdasCheck adasCheck;

    public List<Camera> cameras;
    public List<Camera> otherCameras;
    public List<InterchangeItem> interchangeItems;
    public Camera adas;
    public boolean calibration1;
    public boolean calibration2;
    public boolean calibration3;
    public boolean adasTerms;

    public Report() {
    }

    protected Report(Parcel in) {
        this.time = in.readLong();
        this.selectedSettings = in.readString();
        this.userMode = in.readString();
        this.serialNumber = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.phoneNumber = in.readString();
        this.son = in.readString();
        this.vehicleId = in.readString();
        this.vehicleType = in.readString();
        this.year = in.readString();
        this.make = in.readString();
        this.model = in.readString();
        this.vin = in.readString();
        this.company = in.readString();
        this.controller = in.readParcelable(Controller.class.getClassLoader());
        this.sensorBar = in.readParcelable(SensorBar.class.getClassLoader());
        this.communicationSettings = in.readParcelable(
                CommunicationSettings.class.getClassLoader());
        this.accessories = in.readParcelable(Accessories.class.getClassLoader());
        this.selfCheck = in.readParcelable(SelfCheck.class.getClassLoader());
        this.adasCheck = in.readParcelable(AdasCheck.class.getClassLoader());
        this.leftTurnStatus = in.readByte() != 0;
        this.rightTurnStatus = in.readByte() != 0;
        this.leftTurnPolarityHigh = in.readByte() != 0;
        this.leftTurnPolarityLow = in.readByte() != 0;
        this.rightTurnPolarityHigh = in.readByte() != 0;
        this.rightTurnPolarityLow = in.readByte() != 0;
        this.cameras = in.createTypedArrayList(Camera.CREATOR);
        this.otherCameras = in.createTypedArrayList(Camera.CREATOR);
        this.interchangeItems = in.createTypedArrayList(InterchangeItem.CREATOR);
        this.adas = in.readParcelable(Camera.class.getClassLoader());
        this.calibration1 = in.readByte() != 0;
        this.calibration2 = in.readByte() != 0;
        this.calibration3 = in.readByte() != 0;
        this.adasTerms = in.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.time);
        dest.writeString(this.selectedSettings);
        dest.writeString(this.userMode);
        dest.writeString(this.serialNumber);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.phoneNumber);
        dest.writeString(this.son);
        dest.writeString(this.vehicleId);
        dest.writeString(this.vehicleType);
        dest.writeString(this.year);
        dest.writeString(this.make);
        dest.writeString(this.model);
        dest.writeString(this.vin);
        dest.writeString(this.company);
        dest.writeParcelable(this.controller, flags);
        dest.writeParcelable(this.sensorBar, flags);
        dest.writeParcelable(this.communicationSettings, flags);
        dest.writeParcelable(this.accessories, flags);
        dest.writeParcelable(this.selfCheck, flags);
        dest.writeParcelable(this.adasCheck, flags);
        dest.writeByte(this.leftTurnStatus
                       ? (byte) 1
                       : (byte) 0);
        dest.writeByte(this.rightTurnStatus
                       ? (byte) 1
                       : (byte) 0);
        dest.writeByte(this.leftTurnPolarityHigh
                       ? (byte) 1
                       : (byte) 0);
        dest.writeByte(this.leftTurnPolarityLow
                       ? (byte) 1
                       : (byte) 0);
        dest.writeByte(this.rightTurnPolarityHigh
                       ? (byte) 1
                       : (byte) 0);
        dest.writeByte(this.rightTurnPolarityLow
                       ? (byte) 1
                       : (byte) 0);
        dest.writeTypedList(this.cameras);
        dest.writeTypedList(this.otherCameras);
        dest.writeTypedList(this.interchangeItems);
        dest.writeParcelable(this.selfCheck, flags);
        dest.writeParcelable(this.adasCheck, flags);
        dest.writeByte(this.calibration1
                       ? (byte) 1
                       : (byte) 0);
        dest.writeByte(this.calibration2
                       ? (byte) 1
                       : (byte) 0);
        dest.writeByte(this.calibration3
                       ? (byte) 1
                       : (byte) 0);
        dest.writeByte(this.adasTerms
                       ? (byte) 1
                       : (byte) 0);
    }

    public static List<Report> getListFromDB() {
        return getListFromDB(null);
    }

    public static List<Report> getListFromDB(String deviceSerialNumber) {
        List<Report> reports;
        ActiveAndroid.beginTransaction();
        try {
            From query = new Select().from(Report.class);
            if (!TextUtils.isEmpty(deviceSerialNumber)) {
                query.where("serialNumber = ?", deviceSerialNumber);
            }
            reports = query.execute();
            if (reports != null) {
                for (Report report : reports) {
                    report.cameras = Camera.getListFromDB(report.getId());
                    report.otherCameras = Camera.getListFromDB(report.getId());
                    report.adas = Camera.getListFromDB(report.getId())
                            .get(0);
                    report.interchangeItems = InterchangeItem.getListFromDB(report.getId());
                    if (report.selfCheck != null) {
                        report.selfCheck.items = SelfCheckItem.getListFromDB(
                                report.selfCheck.getId());
                    }
                    if (report.adasCheck != null) {
                        report.adasCheck.items = AdasCheckItem.getListFromDB(
                                report.adasCheck.getId());
                    }
                }
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
        return reports;
    }

    public void saveToDB() {
        ActiveAndroid.beginTransaction();
        try {
            if (controller != null) {
                controller.save();
            }
            if (sensorBar != null) {
                sensorBar.save();
            }
            if (communicationSettings != null) {
                communicationSettings.saveToDB();
            }
            if (accessories != null) {
                accessories.save();
            }
            save();
            if (cameras != null) {
                for (Camera item : cameras) {
                    item.reportId = getId();
                    item.save();
                }
            }
            if (otherCameras != null) {
                for (Camera item : otherCameras) {
                    item.reportId = getId();
                    item.save();
                }
            }
            if (interchangeItems != null) {
                for (InterchangeItem item : interchangeItems) {
                    item.reportId = getId();
                    item.save();
                }
            }
            if (selfCheck != null) {
                selfCheck.reportId = getId();
                selfCheck.saveToDB();
            }
            if (adasCheck != null) {
                adasCheck.setReportId(getId());
                adasCheck.saveToDB();
            }
            if (adas != null) {
                adas.reportId = getId();
                adas.save();
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public void deleteFromDB() {
        ActiveAndroid.beginTransaction();
        try {
            delete();
            if (controller != null) {
                controller.delete();
            }
            if (sensorBar != null) {
                sensorBar.delete();
            }
            if (communicationSettings != null) {
                communicationSettings.deleteFromDB();
            }
            if (accessories != null) {
                accessories.delete();
            }
            if (cameras != null) {
                for (Camera item : cameras) {
                    item.delete();
                }
            }
            if (otherCameras != null) {
                for (Camera item : otherCameras) {
                    item.delete();
                }
            }
            if (interchangeItems != null) {
                for (InterchangeItem item : interchangeItems) {
                    item.delete();
                }
            }
            if (selfCheck != null) {
                selfCheck.deleteFromDB();
            }
            if (adasCheck != null) {
                adasCheck.deleteFromDB();
            }
            if (adas != null) {
                adas.delete();
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public void imageOneCalibrated(boolean b) {
        calibration1 = b;
    }

    public void imageTwoCalibrated(boolean b) {
        calibration2 = b;
    }

    public void imageThreeCalibrated(boolean b) {
        calibration3 = b;
    }

    public void adasTermsAccepted(boolean b) {
        adasTerms = b;
    }
}