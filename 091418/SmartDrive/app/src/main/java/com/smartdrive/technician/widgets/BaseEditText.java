package com.smartdrive.technician.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smartdrive.technician.OnChangeListener;
import com.smartdrive.technician.R;
import com.smartdrive.technician.utils.Dialogs;
import com.smartdrive.technician.utils.VinValidatorUtils;

public class BaseEditText extends LinearLayout {

    private EditText etField;
    private TextView tvStar;
    private View fieldLine;
    private TextView tvScan;

    private int defaultColor;
    private int focusColor;
    private int errorColor;
    private String requiredHint;
    private OnChangeListener onChangeListener;
    private boolean needPickOut = false;
    private boolean isValid = true;

    public BaseEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        create(attrs);
    }

    public BaseEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        create(attrs);
    }

    public void init(@StringRes int hintRes, boolean isFieldRequired) {
        String hint = getContext().getString(hintRes);
        if (isFieldRequired) {
            requiredHint = getContext().getString(R.string.is_required, hint);
        } else {
            requiredHint = null;
        }
        setHint(hint);
        setStarVisibility();
    }

    public void setHint(String hint) {
        etField.setHint(hint);
    }

    public void setInputType(int type) {
        etField.setInputType(type);
    }

    public void setPadding(int padding) {
        etField.setPadding(0, padding, 0, padding);
    }

    public void setOnChangeListener(OnChangeListener onChangeListener) {
        this.onChangeListener = onChangeListener;
    }

    public void addTextChangedListener(TextWatcher textWatcher) {
        etField.addTextChangedListener(textWatcher);
    }

    public void setOnActionClickListener(View.OnClickListener onActionClickListener) {
        tvScan.setOnClickListener(onActionClickListener);
    }

    public void setText(@StringRes int textRes) {
        setText(getContext().getString(textRes));
    }

    public String getText() {
        return etField.getText()
                .toString()
                .trim();
    }

    public void setText(final String text) {
        etField.setText(text);
        setStarVisibility();
    }

    public boolean isRequiredAndEmpty(boolean pickOut) {
        isValid = !(getVisibility() == VISIBLE && isRequired() && getText().isEmpty());
        if (!isValid && pickOut) {
            needPickOut = true;
            setHint(requiredHint);
            etField.setHintTextColor(errorColor);
            tvStar.setTextColor(errorColor);
            fieldLine.setBackgroundColor(etField.hasFocus()
                                         ? focusColor
                                         : errorColor);
        }
        return !isValid;
    }

    public boolean isValid(String regexp, @StringRes int errorRes, boolean showErrorDialog) {
        isValid = getText().matches(regexp);
        if (!isValid && showErrorDialog) {
            needPickOut = true;
            fieldLine.setBackgroundColor(etField.hasFocus()
                                         ? focusColor
                                         : errorColor);
            Dialogs.showToast(getContext(), getContext().getString(R.string.error),
                              getContext().getString(errorRes));
        }
        return isValid;
    }

    public void setNextFocusView(final View nextFocusView) {
        etField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    etField.clearFocus();
                    nextFocusView.requestFocus();
                }
                return false;
            }
        });
    }

    public boolean validateVinEditText(@StringRes int errorRes, boolean showErrorDialog) {
        isValid = VinValidatorUtils.validate(getText());
        if (!isValid && showErrorDialog) {
            needPickOut = true;
            fieldLine.setBackgroundColor(etField.hasFocus()
                                         ? focusColor
                                         : errorColor);
            Dialogs.showToast(getContext(), getContext().getString(R.string.error),
                              getContext().getString(errorRes));
        }
        return isValid;
    }

    private void create(AttributeSet attrs) {
        inflate(getContext(), R.layout.view_base_edit_text, this);
        setOrientation(VERTICAL);
        etField = findViewById(R.id.etField);
        tvStar = findViewById(R.id.tvStar);
        tvScan = findViewById(R.id.tvScan);
        fieldLine = findViewById(R.id.fieldLine);
        etField.setId(View.generateViewId());
        defaultColor = ContextCompat.getColor(getContext(), R.color.field_line);
        focusColor = ContextCompat.getColor(getContext(), R.color.green);
        errorColor = ContextCompat.getColor(getContext(), R.color.yellow);

        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.BaseEditText, 0, 0);
        try {
            int actionButtonVisibility = ta.getBoolean(
                    R.styleable.BaseEditText_shouldShowActionButton, false)
                                         ? VISIBLE
                                         : GONE;
            tvScan.setVisibility(actionButtonVisibility);
            if (actionButtonVisibility == VISIBLE) {
                int actionImage = ta.getResourceId(R.styleable.BaseEditText_actionButtonResource,
                                                   R.drawable.ic_camera);
                tvScan.setCompoundDrawablesWithIntrinsicBounds(0, actionImage, 0, 0);
            }
        } finally {
            ta.recycle();
        }
        etField.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    fieldLine.setBackgroundColor(focusColor);
                } else if (needPickOut && !isValid) {
                    fieldLine.setBackgroundColor(errorColor);
                } else {
                    fieldLine.setBackgroundColor(defaultColor);
                }
            }
        });
        etField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                setStarVisibility();
                if (onChangeListener != null) {
                    onChangeListener.onChange();
                }
            }
        });
    }

    private void setStarVisibility() {
        tvStar.setVisibility(isRequired() && getText().isEmpty()
                             ? VISIBLE
                             : GONE);
    }

    private boolean isRequired() {
        return !TextUtils.isEmpty(requiredHint);
    }

}