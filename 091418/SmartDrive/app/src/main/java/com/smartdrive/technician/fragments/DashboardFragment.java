package com.smartdrive.technician.fragments;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.activities.MainActivity;
import com.smartdrive.technician.events.NavigationEvent;
import com.smartdrive.technician.fragments.installation.InstallationFragment;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import butterknife.OnClick;

import static com.smartdrive.technician.Constants.SERIAL_NUMBER;
import static com.smartdrive.technician.Constants.USER_MODE;
import static com.smartdrive.technician.Constants.VEHICLE_ID;

@Layout(id = R.layout.fragment_dashboard)
public class DashboardFragment extends BaseTriggerEventFragment {

    @Bind(R.id.tvSerialNumber)
    TextView tvSerialNumber;
    @Bind(R.id.tvVehicleId)
    TextView tvVehicleId;
    @Bind(R.id.vehicleIdContainer)
    View vehicleIdContainer;
    @Bind(R.id.verifyInstallationDivider)
    View verifyInstallationDivider;
    @Bind(R.id.viewEventLibrary)
    View viewEventLibrary;
    @Bind(R.id.triggerManualEvent)
    View triggerManualEvent;

    @Override
    protected void initView() {
        byte userMode = getActivity().getIntent()
                .getByteExtra(USER_MODE, (byte) 0);
        if (bleManager.isGuestMode(userMode)) {
            verifyInstallationDivider.setVisibility(View.GONE);
            viewEventLibrary.setVisibility(View.GONE);
            triggerManualEvent.setVisibility(View.GONE);
        } else {
            verifyInstallationDivider.setVisibility(View.VISIBLE);
            if (bleManager.isMaintenanceMode(userMode)) {
                viewEventLibrary.setVisibility(View.GONE);
                triggerManualEvent.setVisibility(View.VISIBLE);
            } else {
                viewEventLibrary.setVisibility(View.VISIBLE);
                triggerManualEvent.setVisibility(View.GONE);
            }
        }
        Intent intent = getActivity().getIntent();
        tvSerialNumber.setText(intent.getStringExtra(SERIAL_NUMBER));
        String vehicleId = intent.getStringExtra(VEHICLE_ID);
        if (TextUtils.isEmpty(vehicleId)) {
            vehicleIdContainer.setVisibility(View.GONE);
        } else {
            tvVehicleId.setText(vehicleId);
            vehicleIdContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected View createToolbarButton() {
        ImageView view = new ImageView(getContext());
        view.setImageResource(R.drawable.selector_btn_logout);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).showLogoutDialog(R.string.logout_message,
                                                                R.string.cancel);
            }
        });
        return view;
    }

    public static Fragment newInstance() {
        return new DashboardFragment();
    }

    @OnClick({R.id.verifyInstallation, R.id.viewEventLibrary, R.id.triggerManualEvent, R.id.runSelfCheck, R.id.viewDebugInfo, R.id.runAdasCheck})
    public void onButtonClick(View view) {
        Fragment fragment = null;
        switch (view.getId()) {
            case R.id.verifyInstallation:
                fragment = InstallationFragment.newInstance();
                break;
            case R.id.viewEventLibrary:
                fragment = EventLibraryFragment.newInstance();
                break;
            case R.id.triggerManualEvent:
                triggerManualEvent();
                return;
            case R.id.runSelfCheck:
                fragment = SelfCheckFragment.newInstance();
                break;
            case R.id.viewDebugInfo:
                fragment = DebugInfoFragment.newInstance();
                break;
            case R.id.runAdasCheck:
                fragment = AdasCalibrationStatusFragment.newInstance();
                break;
        }
        EventBus.getDefault()
                .post(new NavigationEvent(fragment));
    }

}