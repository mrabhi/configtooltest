package com.smartdrive.technician.fragments.installation.calibration;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;
import android.widget.ImageView;

import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.activities.AdasCalibrationActivity;
import com.smartdrive.technician.fragments.BaseFragment;
import com.smartdrive.technician.managers.CalibrationManager;

import butterknife.Bind;

@Layout(id = R.layout.fragment_adas_pattern_check)
public class PatternCheckFragment extends BaseFragment {
    @Bind(R.id.vYes)
    View vYes;
    @Bind(R.id.vNo)
    View vNo;
    @Bind(R.id.ivGeneratedImage)
    ImageView ivGeneratedImage;

    AdasCalibrationActivity mActivity;
    CalibrationManager mCalibrationManager;
    Bitmap mBitmap;
    Rect mRect;

    @Override
    protected void initView() {
        mActivity = (AdasCalibrationActivity) getActivity();
        if (mActivity == null) {
            return;
        }
        mBitmap = mActivity.getBitmap();
        mRect = mActivity.getRect();
        mCalibrationManager = mActivity.getCalibrationManager();

        vNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AdasCalibrationActivity) getActivity()).openFragment(
                        new VerticalCheckFragment());
            }
        });
        vYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calibrate();
            }
        });
        drawLines();
    }

    void calibrate() {
        mActivity.openFragment(MessageFragment.newInstance(
                mCalibrationManager.calibrate(mActivity.getBitmapPath(),
                                              mActivity.getStep())));
    }


    private void drawLines() {
        Bitmap mutable = mBitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas c = new Canvas(mutable);
        ivGeneratedImage.draw(c);
        Paint p = new Paint();
        p.setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.default_line_thickness));
        p.setColor(Color.RED);
        p.setStyle(Paint.Style.STROKE);
        c.drawRect(mRect, p);

        p.setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.small_line_thickness));

        float thirdHeight = (mRect.bottom - mRect.top) / 3;
        float thirdWidth = (mRect.right - mRect.left) / 3;

        c.drawLine(mRect.left, mRect.top + thirdHeight, mRect.right, mRect.top + thirdHeight, p);
        c.drawLine(mRect.left, mRect.top + 2 * thirdHeight, mRect.right,
                   mRect.top + 2 * thirdHeight, p);

        c.drawLine(mRect.left + thirdWidth, mRect.top, mRect.left + thirdWidth, mRect.bottom, p);
        c.drawLine(mRect.left + 2 * thirdWidth, mRect.top, mRect.left + 2 * thirdWidth,
                   mRect.bottom, p);

        ivGeneratedImage.setImageBitmap(mutable);
    }
}