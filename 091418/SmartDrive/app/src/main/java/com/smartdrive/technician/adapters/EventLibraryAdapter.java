package com.smartdrive.technician.adapters;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.smartdrive.technician.R;
import com.smartdrive.technician.models.Event;
import com.smartdrive.technician.utils.DateUtils;

import butterknife.Bind;

public class EventLibraryAdapter extends BaseAdapter<Event> {

    protected class EventLibraryHolder extends BaseViewHolder {
        @Bind(R.id.tvName)
        TextView tvName;
        @Bind(R.id.tvTime)
        TextView tvTime;
        @Bind(R.id.itemProgress)
        ProgressBar itemProgress;
        @Bind(R.id.ivDownload)
        ImageView ivDownload;
        @Bind(R.id.ivPlay)
        ImageView ivPlay;

        private EventLibraryHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void bind(final int position) {
            Event item = getItem(position);
            tvName.setText(item.name);
            tvTime.setText(DateUtils.longToString(item.time, DateUtils.is24HourFormat()
                    ? DateUtils.TIME_FORMAT_24 : DateUtils.TIME_FORMAT_12));
            if (item.downloaded == null) {
                ivDownload.setVisibility(View.GONE);
                ivPlay.setVisibility(View.GONE);
                itemProgress.setVisibility(View.VISIBLE);
            } else if (item.downloaded) {
                itemProgress.setVisibility(View.GONE);
                ivDownload.setVisibility(View.GONE);
                ivPlay.setVisibility(View.VISIBLE);
            } else {
                itemProgress.setVisibility(View.GONE);
                ivPlay.setVisibility(View.GONE);
                ivDownload.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void setListeners(final int position, OnItemClickListener onItemClickListener) {
            ivDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onDownloadClickListener.onClick(v, position);
                }
            });
            ivPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onPlayClickListener.onClick(v, position);
                }
            });
        }
    }

    private OnItemClickListener onDownloadClickListener;
    private OnItemClickListener onPlayClickListener;

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event_library, parent, false);
        return new EventLibraryHolder(itemView);
    }

    public void setOnDownloadClickListener(OnItemClickListener onDownloadClickListener) {
        this.onDownloadClickListener = onDownloadClickListener;
    }

    public void setOnPlayClickListener(OnItemClickListener onPlayClickListener) {
        this.onPlayClickListener = onPlayClickListener;
    }

}