package com.smartdrive.technician.controller;

import java.util.UUID;

/**
 * Created by mikeho on 6/13/16.
 */
public class BleConstants {
    public static final String PRIVATE_KEY = "ABCD1234";

    public static class Uuid {
        public static final UUID SERVICE = UUID.fromString("447C0000-ED49-FE29-71E1-87A4E15ECFF6");
        public static final UUID AUTHENTICATION_NOTIFICATION = UUID.fromString("447C0010-ED49-FE29-71E1-87A4E15ECFF6");
        public static final UUID AUTHENTICATION_WRITE = UUID.fromString("447C0011-ED49-FE29-71E1-87A4E15ECFF6");
        public static final UUID COMMAND_NOTIFICATION = UUID.fromString("447C0020-ED49-FE29-71E1-87A4E15ECFF6");
        public static final UUID COMMAND_WRITE = UUID.fromString("447C0021-ED49-FE29-71E1-87A4E15ECFF6");
        public static final UUID SELFCHECK_NOTIFICATION = UUID.fromString("447C0030-ED49-FE29-71E1-87A4E15ECFF6");
        public static final UUID SELFCHECK_WRITE = UUID.fromString("447C0031-ED49-FE29-71E1-87A4E15ECFF6");

        public static final UUID DEFAULT_CONFIG = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    }

    public static class AuthenticationMode {
        public static final byte ADMIN = 0x00;
        public static final byte MAINTENANCE_USER = 0x01;
        public static final byte GUEST_MODE = 0x02;
        public static final byte INVALID_PASSWORD = 0x03;
    }

    public enum ProcessResponseSegmentResult {
        MESSAGE_CONTINUES,
        MESSAGE_COMPLETE,
        ERROR
    }

    public enum FunctionCode {
        LOGIN	                            (0x01, "Login"),
        SYNC_COMPLETE	                    (0x02, "SyncComplete"),
        LOGOUT	                            (0x03, "Logout"),
        GET_CURRENT_TIME           	        (0x04, "GetCurrentTime"),

        GET_VEHICLE_ID                      (0x10, "GetVehicleIdentifier"),
        SET_VEHICLE_ID                      (0x11, "SetVehicleIdentifier"),
        GET_CONTROLLER_SERIAL_NUMBER        (0x12, "GetControllerSerialNumber"),
        CHECK_SENSOR_BAR	                (0x13, "CheckSensorBar"),
        GET_ADAS_FLAG   	                (0x14, "GetAdasFlag"),
        GET_CAMERA_INFO   	                (0x15, "GetCameraInfo"),
        CHECK_TURN_SIGNAL  	                (0x16, "CheckTurnSignal"),
        GET_COMMUNICATIONS_SETTING	        (0x20, "GetCommunicationsSetting"),
        SET_COMMUNICATIONS_SETTING	        (0x21, "SetCommunicationsSetting"),
        TEST_COMMUNICATIONS_SETTING         (0x22, "TestCommunicationsSetting"),

        ACK_OR_ERROR                        (0x25, "AckOrError"),

        GENERATE_IMAGE_ON_CAMERA	        (0x30, "GenerateImageOnCamera"),
        GENERATE_TEST_EVENT                 (0x31, "GenerateTestEvent"),
        VIEW_EVENT_LIBRARY                  (0x32, "ViewEventLibrary"),
        INITIATE_BLUETOOTH_FILE_SHARING     (0x33, "InitiateBluetoothFileSharing"),
        REQUEST_EVENT_OVER_BLUETOOTH        (0x34, "RequestEventOverBluetooth"),
        REQUEST_ADAS_IMAGE_OVER_BLUETOOTH   (0x35, "RequestAdasImageOverBluetooth"),
        SEND_ADAS_STATS                     (0x36, "SendAdasStats"),
        CHECK_GPS                           (0x40, "CheckGps"),
        GET_DEBUG_INFORMATION               (0x50, "GetDebugInformation"),

        GENERATE_SELF_CHECK                 (0x60, "GenerateSelfCheck"),
        SELF_CHECK_COMPLETE                 (0x61, "SelfCheckComplete");

        byte value;
        String label;
        FunctionCode(int i, String s) {
            value = (byte) i;
            label = s;
        }
    }

    public enum ErrorCode {
        USER_NOT_LOGGED_IN(0xF0),
        UNKNOWN_FUNCTION_CODE(0xF1),
        OVERSIZED_RESPONSE_PAYLOAD_GENERATED(0xF2),
        INVALID_REQUEST_PAYLOAD_DATA(0xF3),
        INVALID_APPLICATION_PAYLOAD_DATA(0xF4);
        byte value;
        ErrorCode(int i) {
            value = (byte) i;
        }

        public static ErrorCode byValue(byte b) {
            for (ErrorCode codeItem : ErrorCode.values()) {
                if (codeItem.value == b) {
                    return codeItem;
                }
            }
            return null;
        }
    }

    public enum CameraInfo {
        WABCO_CAMERA(0x01),
        DIGITAL_CAMERA_POSITION_1(0x02),
        DIGITAL_CAMERA_POSITION_2(0x03),
        DIGITAL_CAMERA_POSITION_3(0x04),
        DIGITAL_CAMERA_POSITION_4(0x05),
        ANALOG_CAMERA_6(0x06),
        ANALOG_CAMERA_7(0x07),
        ANALOG_CAMERA_8(0x08),
        ANALOG_CAMERA_9(0x09);
        byte value;
        CameraInfo(int i) {
            value = (byte) i;
        }

        public static CameraInfo byValue(byte b) {
            for (CameraInfo codeItem : CameraInfo.values()) {
                if (codeItem.value == b) {
                    return codeItem;
                }
            }
            return null;
        }

        public static CameraInfo byId(int id) {
            for (CameraInfo codeItem : CameraInfo.values()) {
                if (codeItem.ordinal() == id) {
                    return codeItem;
                }
            }
            return null;
        }


        public byte[] getValue(){
            byte[] byteArray = {value};
            return byteArray;
        }
    }

    public enum CameraType {
        TYPE_WABCO(0x01),
        TYPE_ANALOG(0x02),
        TYPE_SONY(0x03),
        TYPE_SONY_IR(0x04),
        TYPE_ADAS(0x05);
        byte value;
        CameraType(int i) {
            value = (byte) i;
        }

        public static CameraType byValue(byte b) {
            for (CameraType codeItem : CameraType.values()) {
                if (codeItem.value == b) {
                    return codeItem;
                }
            }
            return null;
        }
    }

    public enum TurnSignal {
        SIGNAL_LEFT(0x01),
        SIGNAL_RIGHT(0x00);

        byte value;
        TurnSignal(int i) {
            value = (byte) i;
        }

        public static TurnSignal byValue(byte b) {
            for (TurnSignal codeItem : TurnSignal.values()) {
                if (codeItem.value == b) {
                    return codeItem;
                }
            }
            return null;
        }

        public byte[] getValue(){
            return new byte[] {value};
        }
    }

    public enum DistanceCaptured {
        DISTANCE_2_0_METERS(0x01),
        DISTANCE_2_5_METERS(0x02),
        DISTANCE_3_5_METERS(0x03);

        byte value;
        DistanceCaptured(int i) {
            value = (byte) i;
        }

        public static DistanceCaptured byValue(byte b) {
            for (DistanceCaptured codeItem : DistanceCaptured.values()) {
                if (codeItem.value == b) {
                    return codeItem;
                }
            }
            return null;
        }

        public byte[] getValue(){
            return new byte[] {value};
        }
    }

    public enum TransactionStatus {
        CREATING("Creating"),
        SENDING_REQUEST("Sending Request"),
        AWAITING_RESPONSE("Awaiting Response"),
        PROCESSING_RESPONSE("Processing Response");

        String value;
        TransactionStatus(String s) { value = s; }
    }
}