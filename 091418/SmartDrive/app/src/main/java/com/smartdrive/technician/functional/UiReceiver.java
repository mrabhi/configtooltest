package com.smartdrive.technician.functional;

import android.os.Handler;
import android.os.Looper;

public class UiReceiver<T> implements Receiver<T> {

    private final Receiver<T> target;

    public UiReceiver(final Receiver<T> target) {
        this.target = target;
    }

    @Override
    public void receive(final T data) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                target.receive(data);
            }
        });
    }
}
