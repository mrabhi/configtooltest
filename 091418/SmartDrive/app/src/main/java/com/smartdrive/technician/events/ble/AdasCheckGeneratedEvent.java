package com.smartdrive.technician.events.ble;

import com.smartdrive.technician.models.AdasCheck;

public class AdasCheckGeneratedEvent {

    public AdasCheck getAdasCheck() {
        return mAdasCheck;
    }

    private final AdasCheck mAdasCheck;

    public AdasCheckGeneratedEvent(AdasCheck adasCheck) {
        this.mAdasCheck = adasCheck;
    }

}