package com.smartdrive.technician.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;

import com.smartdrive.technician.EventsSubscriber;
import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.SmartDriveApp;
import com.smartdrive.technician.adapters.DebugInfoAdapter;
import com.smartdrive.technician.events.ble.DebugInfoLoadedEvent;
import com.smartdrive.technician.widgets.BaseRecyclerView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;

import static com.smartdrive.technician.Constants.TOOLBAR_TITLE;

@EventsSubscriber
@Layout(id = R.layout.fragment_debug_info)
public class DebugInfoFragment extends BaseFragment {

    @Bind(R.id.rvDebugInfo)
    BaseRecyclerView rvDebugInfo;

    private DebugInfoAdapter adapter;

    public static Fragment newInstance() {
        Fragment fragment = new DebugInfoFragment();
        Bundle args = new Bundle();
        args.putString(TOOLBAR_TITLE, SmartDriveApp.getContext().getString(R.string.debug_info));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initView() {
        adapter = new DebugInfoAdapter();
        rvDebugInfo.setAdapter(adapter);
        if (bleManager.getDebugInfo() != null) {
            adapter.update(bleManager.getDebugInfo());
        } else {
            loadDebugInfo();
        }
    }

    @Override
    protected View createToolbarButton() {
        ImageView view = new ImageView(getContext());
        view.setImageResource(R.drawable.selector_btn_refresh);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadDebugInfo();
            }
        });
        return view;
    }

    private void loadDebugInfo() {
        showProgress(R.string.loading_debug_info);
        rvDebugInfo.setVisibility(View.GONE);
        bleManager.loadDebugInfo();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(DebugInfoLoadedEvent event) {
        hideProgress();
        rvDebugInfo.setVisibility(View.VISIBLE);
        adapter.update(event.debugInfo);
    }

}