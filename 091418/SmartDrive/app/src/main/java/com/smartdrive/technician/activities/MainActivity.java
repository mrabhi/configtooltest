package com.smartdrive.technician.activities;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.events.NavigationEvent;
import com.smartdrive.technician.events.ble.BleErrorEvent;
import com.smartdrive.technician.events.ble.LoggedInEvent;
import com.smartdrive.technician.events.ble.LoggedOutEvent;
import com.smartdrive.technician.events.ble.SyncCompletedEvent;
import com.smartdrive.technician.events.ble.installation.CommunicationSettingsSetEvent;
import com.smartdrive.technician.events.ble.installation.VehicleIdEvent;
import com.smartdrive.technician.fragments.DashboardFragment;
import com.smartdrive.technician.fragments.installation.InstallationFragment;
import com.smartdrive.technician.utils.AutoDataRemover;
import com.smartdrive.technician.utils.Dialogs;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;
import butterknife.OnClick;

import static com.smartdrive.technician.Constants.INACTIVE_TIME;
import static com.smartdrive.technician.Constants.LARGE_SPACE;
import static com.smartdrive.technician.Constants.RC_ENABLE_BLE_WITH_RECONNECT;
import static com.smartdrive.technician.Constants.SERIAL_NUMBER;
import static com.smartdrive.technician.Constants.SHOW_SERIAL_NUMBER;
import static com.smartdrive.technician.Constants.SHOW_VEHICLE_ID;
import static com.smartdrive.technician.Constants.TOOLBAR_TITLE;
import static com.smartdrive.technician.Constants.USER_MODE;
import static com.smartdrive.technician.Constants.VEHICLE_ID;

@Layout(id = R.layout.activity_main)
public class MainActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    View toolbar;
    @Bind(R.id.toolbarShadow)
    View toolbarShadow;
    @Bind(R.id.toolbarLogo)
    View toolbarLogo;
    @Bind(R.id.toolbarDetails)
    View toolbarDetails;
    @Bind(R.id.toolbarTitle)
    TextView toolbarTitle;
    @Bind(R.id.toolbarSubtitle)
    TextView toolbarSubtitle;
    @Bind(R.id.toolbarButtonContainer)
    ViewGroup toolbarButtonContainer;
    @Bind(R.id.guestModePanel)
    View guestModePanel;

    private Dialog logoutDialog;
    private Dialog disconnectDialog;
    private long logoutTime = 0;

    @Override
    protected void onStart() {
        super.onStart();
        if (!bleManager.isLoggedIn()) {
            startConnectionActivity();
        } else if (logoutTime > 0 && System.currentTimeMillis() >= logoutTime) {
            startConnectionActivity();
            bleManager.logout();
        } else {
            initInactive(false);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!isFinishing()) {
            initInactive(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == RC_ENABLE_BLE_WITH_RECONNECT) {
            reconnect();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
            Fragment fragment = getSupportFragmentManager().findFragmentById(
                    R.id.fragmentContainer);
            if (fragment != null) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (isProgressVisible()) {
            Dialogs.showToast(this, getString(R.string.error),
                              getString(R.string.waiting_attention));
        } else if (getSupportFragmentManager().findFragmentById(
                R.id.fragmentContainer) instanceof InstallationFragment) {
            Dialogs.showAlert(this, null, getString(R.string.exit_installation), null, R.string.no,
                              R.string.yes, true,
                              new View.OnClickListener() {
                                  @Override
                                  public void onClick(View v) {
                                      MainActivity.super.onBackPressed();
                                  }
                              }
            );
        } else if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
            super.onBackPressed();
        }
    }

    @Override
    protected void initView() {
        getSupportFragmentManager().addOnBackStackChangedListener(new OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                updateToolbarAndReportsPanel();
            }
        });
        updateToolbarAndReportsPanel();
        boolean isGuestMode = bleManager.isGuestMode(getIntent().getByteExtra(USER_MODE, (byte) 0));
        guestModePanel.setVisibility(isGuestMode
                                     ? View.VISIBLE
                                     : View.GONE);
        showDashboardFragment();
    }

    @Override
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(BleErrorEvent event) {
        if (disconnectDialog != null && disconnectDialog.isShowing()) {
            Dialogs.hideProgress(disconnectDialog);
        } else {
            hideLogoutDialog();
            super.onBleEvent(event);
        }
    }

    @OnClick(R.id.toolbarBack)
    public void onToolbarBackPressed() {
        onBackPressed();
    }

    public void addToolbarButton(View toolbarButton) {
        toolbarButtonContainer.addView(toolbarButton);
    }

    public void removeToolbarButton(View toolbarButton) {
        toolbarButtonContainer.removeView(toolbarButton);
    }

    public void showLogoutDialog(int messageRes, int cancelRes) {
        hideLogoutDialog();
        logoutDialog = Dialogs.showAlert(this, null, getString(messageRes), null,
                                         cancelRes, R.string.logout, false,
                                         new View.OnClickListener() {
                                             @Override
                                             public void onClick(View v) {
                                                 Dialogs.showProgress(logoutDialog,
                                                                      R.string.logout_in_progress);
                                                 bleManager.logout();
                                             }
                                         }
        );
    }

    @Subscribe
    public void onNavigationEvent(NavigationEvent event) {
        if (event.fragment == null || event.fragment instanceof DashboardFragment) {
            clearBackStack();
            showDashboardFragment();
        } else {
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction()
                    .hide(fm.findFragmentById(R.id.fragmentContainer))
                    .add(R.id.fragmentContainer, event.fragment, event.getFragmentTag())
                    .addToBackStack(event.getFragmentTag())
                    .commit();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(LoggedInEvent event) {
        if (disconnectDialog != null && disconnectDialog.isShowing()) {
            disconnectDialog.setOnDismissListener(null);
            disconnectDialog.dismiss();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(LoggedOutEvent event) {
        if (isFinishing() || isDestroyed()) {
            return;
        }
        if (logoutDialog != null && logoutDialog.isShowing()) {
            startConnectionActivity();
        } else if (disconnectDialog != null && disconnectDialog.isShowing()) {
            Dialogs.hideProgress(disconnectDialog);
        } else {
            disconnectDialog = Dialogs.showAlert(this, getString(R.string.error),
                                                 getString(R.string.connection_interrupted),
                                                 null, R.string.no, R.string.yes, false,
                                                 new View.OnClickListener() {
                                                     @Override
                                                     public void onClick(View v) {
                                                         reconnect();
                                                     }
                                                 });
            disconnectDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    startConnectionActivity();
                }
            });
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(SyncCompletedEvent event) {
        showLogoutDialog(R.string.device_configured, R.string.dismiss);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(VehicleIdEvent event) {
        if (event.vehicleId != null) {
            getIntent().putExtra(VEHICLE_ID, event.vehicleId);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(final CommunicationSettingsSetEvent event) {
        if (event.failed) {
            hideProgress();
            Dialogs.showAlert(this, null, getString(R.string.error_set_communication_settings),
                              null, R.string.close, R.string.try_again, true,
                              new View.OnClickListener() {
                                  @Override
                                  public void onClick(View v) {
                                      showProgress(R.string.setting_communications);
                                      bleManager.setCommunicationSettings(event.settings,
                                                                          event.needTestCommunicationSettings);
                                  }
                              }
            );
        } else if (!event.needTestCommunicationSettings) {
            hideProgress();
        }
    }

    private void updateToolbarAndReportsPanel() {
        ViewGroup.LayoutParams toolbarParams = toolbar.getLayoutParams();
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            pendingReportsPanel.setVisibility(View.GONE);
            toolbarShadow.setVisibility(View.GONE);
            toolbarLogo.setVisibility(View.GONE);
            toolbarDetails.setVisibility(View.VISIBLE);
            Fragment currentFragment = fm.findFragmentById(R.id.fragmentContainer);
            if (currentFragment != null && currentFragment.getArguments() != null) {
                toolbarTitle.setText(currentFragment.getArguments()
                                             .getString(TOOLBAR_TITLE));
                StringBuilder subtitle = new StringBuilder();
                if (currentFragment.getArguments()
                        .getBoolean(SHOW_SERIAL_NUMBER, true)) {
                    subtitle.append(getIntent().getStringExtra(SERIAL_NUMBER));
                }
                if (currentFragment.getArguments()
                        .getBoolean(SHOW_VEHICLE_ID, false)) {
                    String vehicleId = getIntent().getStringExtra(VEHICLE_ID);
                    if (!TextUtils.isEmpty(vehicleId)) {
                        subtitle.append(LARGE_SPACE);
                        subtitle.append(getString(R.string.vehicle, vehicleId));
                    }
                }
                if (TextUtils.isEmpty(subtitle)) {
                    toolbarSubtitle.setVisibility(View.GONE);
                    toolbarParams.height = (int) getResources().getDimension(
                            R.dimen.toolbar_height_small);
                } else {
                    toolbarSubtitle.setText(subtitle);
                    toolbarSubtitle.setVisibility(View.VISIBLE);
                    toolbarParams.height = (int) getResources().getDimension(
                            R.dimen.toolbar_height_large);
                }
                toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.gray));
            }
        } else {
            pendingReportsPanel.setVisibility(TextUtils.isEmpty(tvPendingReports.getText())
                                              ? View.GONE
                                              : View.VISIBLE);
            toolbarShadow.setVisibility(View.VISIBLE);
            toolbarLogo.setVisibility(View.VISIBLE);
            toolbarDetails.setVisibility(View.GONE);
            toolbarTitle.setText(null);
            toolbarSubtitle.setText(null);
            toolbarParams.height = (int) getResources().getDimension(
                    R.dimen.toolbar_height_default);
            toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.dark_gray));
        }
        toolbar.setLayoutParams(toolbarParams);
    }

    private void showDashboardFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, DashboardFragment.newInstance())
                .commit();
    }

    private void hideLogoutDialog() {
        if (logoutDialog != null) {
            logoutDialog.dismiss();
        }
    }

    private void clearBackStack() {
        try {
            getSupportFragmentManager().popBackStack(null,
                                                     FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } catch (IllegalStateException ex) {
            ex.printStackTrace();
        }
    }

    private void startConnectionActivity() {
        hideLogoutDialog();
        finish();
        startActivity(new Intent(MainActivity.this, ConnectionActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    private void initInactive(boolean isInactive) {
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AutoDataRemover.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent,
                                                                 PendingIntent.FLAG_CANCEL_CURRENT);
        if (am != null) {
            if (isInactive) {
                logoutTime = System.currentTimeMillis() + INACTIVE_TIME;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    am.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, logoutTime, pendingIntent);
                } else {
                    am.set(AlarmManager.RTC_WAKEUP, logoutTime, pendingIntent);
                }
            } else {
                logoutTime = 0;
                am.cancel(pendingIntent);
            }
        }
    }

    private void reconnect() {
        if (bleManager.isBluetoothEnabled()) {
            Dialogs.showProgress(disconnectDialog, R.string.connection_in_progress);
            bleManager.reconnect();
        } else {
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),
                                   RC_ENABLE_BLE_WITH_RECONNECT);
        }
    }

}