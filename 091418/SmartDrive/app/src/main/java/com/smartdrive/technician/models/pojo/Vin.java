package com.smartdrive.technician.models.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartdrive.technician.utils.VinValidatorUtils;

import java.util.List;

public class Vin {

    @Expose
    @SerializedName("linesText")
    List<String> vinResponse;

    public String getVin() {
        String vin = null;
        if (vinResponse != null) {
            for (String item : vinResponse) {
                if (VinValidatorUtils.validate(item)) {
                    vin = item;
                }
            }
        }
        return vin;
    }

}
