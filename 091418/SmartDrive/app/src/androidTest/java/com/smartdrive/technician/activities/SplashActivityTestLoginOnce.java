package com.smartdrive.technician.activities;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.smartdrive.technician.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class SplashActivityTestLoginOnce {

    @Rule
    public ActivityTestRule<SplashActivity> mActivityTestRule = new ActivityTestRule<>(SplashActivity.class);

    @Test
    public void splashActivityTestLoginOnce() {
        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

/*        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.tvConnect), withText("Connect"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.rvSmartRecorders),
                                        3),
                                1),
                        isDisplayed()));
        appCompatTextView.perform(click());
*/
        ViewInteraction appCompatCheckBox = onView(
                allOf(withId(R.id.cbShowPassword), withText("Show Password"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.viewContainer),
                                        0),
                                3),
                        isDisplayed()));
        appCompatCheckBox.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.etPassword),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.viewContainer),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText.perform(click());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.etPassword),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.viewContainer),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText2.perform(replaceText("SDSI@Admin1"), closeSoftKeyboard());

        pressBack();

        ViewInteraction appCompatTextView2 = onView(
                allOf(withId(R.id.tvAction), withText("Login"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.dialogContent),
                                        1),
                                1),
                        isDisplayed()));
        appCompatTextView2.perform(click());

        ViewInteraction imageView = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.toolbarButtonContainer),
                                childAtPosition(
                                        withId(R.id.toolbar),
                                        2)),
                        0),
                        isDisplayed()));
        imageView.perform(click());

        // wait in logged in state for one minute
        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction appCompatTextView3 = onView(
                allOf(withId(R.id.tvAction), withText("Logout"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.dialogContent),
                                        1),
                                1),
                        isDisplayed()));
        appCompatTextView3.perform(click());

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
