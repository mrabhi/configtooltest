package com.smartdrive.technician.activities;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.smartdrive.technician.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class SplashActivityLoginLogout {

    @Rule
    public ActivityTestRule<SplashActivity> mActivityTestRule = new ActivityTestRule<>(SplashActivity.class);

    @Test
    public void splashActivityLoginLogout() {

        // Added a sleep statement so as to locate SR4 NA8CA028 on the screen
        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

// commenting out Connect so as to locate SR4 NA8CA028 and hit Connect for that SR4
/*
        ViewInteraction textView = onView(
                allOf(withId(R.id.tvConnect), withText("Connect"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.rvSmartRecorders),
                                        4),
                                1),
                        isDisplayed()));
        textView.check(matches(withText("Connect")));

        ViewInteraction textView2 = onView(
                allOf(withId(R.id.tvConnect), withText("Connect"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.rvSmartRecorders),
                                        4),
                                1),
                        isDisplayed()));
        textView2.check(matches(withText("Connect")));

        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.tvConnect), withText("Connect"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.rvSmartRecorders),
                                        4),
                                1),
                        isDisplayed()));
        appCompatTextView.perform(click());
*/

        ViewInteraction checkBox = onView(
                allOf(withId(R.id.cbShowPassword),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.viewContainer),
                                        0),
                                2),
                        isDisplayed()));
        checkBox.check(matches(isDisplayed()));

        ViewInteraction checkBox2 = onView(
                allOf(withId(R.id.cbShowPassword),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.viewContainer),
                                        0),
                                2),
                        isDisplayed()));
        checkBox2.check(matches(isDisplayed()));

        ViewInteraction appCompatCheckBox = onView(
                allOf(withId(R.id.cbShowPassword), withText("Show Password"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.viewContainer),
                                        0),
                                3),
                        isDisplayed()));
        appCompatCheckBox.perform(click());

        ViewInteraction editText = onView(
                allOf(withId(R.id.etPassword), withText("Password"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.viewContainer),
                                        0),
                                0),
                        isDisplayed()));
        editText.check(matches(withText("SDSI@Admin1")));

        ViewInteraction editText2 = onView(
                allOf(withId(R.id.etPassword), withText("Password"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.viewContainer),
                                        0),
                                0),
                        isDisplayed()));
        editText2.check(matches(withText("SDSI@Admin1")));

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.etPassword),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.viewContainer),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText.perform(click());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.etPassword),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.viewContainer),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText2.perform(replaceText("SDSI@admin1"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.etPassword), withText("SDSI@admin1"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.viewContainer),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText3.perform(click());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.etPassword), withText("SDSI@admin1"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.viewContainer),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText4.perform(click());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.etPassword), withText("SDSI@admin1"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.viewContainer),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText5.perform(replaceText("SDSI@Admin1"));

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.etPassword), withText("SDSI@Admin1"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.viewContainer),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText6.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.etPassword), withText("SDSI@Admin1"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.viewContainer),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText7.perform(pressImeActionButton());

        ViewInteraction appCompatEditText8 = onView(
                allOf(withId(R.id.etPassword), withText("SDSI@Admin1"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.viewContainer),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText8.perform(pressImeActionButton());

        ViewInteraction appCompatEditText9 = onView(
                allOf(withId(R.id.etPassword), withText("SDSI@Admin1"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.viewContainer),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText9.perform(pressImeActionButton());

        pressBack();

        pressBack();

        ViewInteraction appCompatTextView2 = onView(
                allOf(withId(R.id.tvAction), withText("Login"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.dialogContent),
                                        1),
                                1),
                        isDisplayed()));
        appCompatTextView2.perform(click());

        ViewInteraction imageView = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.toolbarButtonContainer),
                                childAtPosition(
                                        withId(R.id.toolbar),
                                        2)),
                        0),
                        isDisplayed()));
        imageView.perform(click());

        ViewInteraction appCompatTextView3 = onView(
                allOf(withId(R.id.tvAction), withText("Logout"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.dialogContent),
                                        1),
                                1),
                        isDisplayed()));
        appCompatTextView3.perform(click());

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
