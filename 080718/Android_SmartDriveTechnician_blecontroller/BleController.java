package com.smartdrive.technician.controller;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Camera;
import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class BleController implements BluetoothAdapter.LeScanCallback {
    private static final String TAG = "BleController";

    private static final long TIMEOUT_SCAN = 15000;
    private static final long TIMEOUT_CONNECT = 10000;
    private static final long TIMEOUT_TRANSACTION = 5000;

    public BluetoothAdapter mBluetoothAdapter;

    private Context mContext;
    private BleControllerInterface mInterface;

    private Timer mTimer;
    private TimerTask mTimerTask;

    private ArrayList<BluetoothDevice> mFoundDeviceArray;
    private Hashtable<String, Boolean> mScannedDeviceHashTable;

    private BluetoothDevice mBluetoothDevice;
    private BluetoothGattService mService;

    private BluetoothGatt mGatt;

    private enum TransactionType {
        AUTHENTICATE(0),
        COMMAND(1),
        SELFCHECK(2),
        AUTHENTICATE_SYNC_COMPLETE(3),
        SELFCHECK_COMPLETE(4);
        int value;

        TransactionType(int i) {
            value = i;
        }
    }

    private enum CharacteristicType {
        AUTHENTICATE(0),
        COMMAND(1),
        SELFCHECK(2);
        int value;

        CharacteristicType(int i) {
            value = i;
        }
    }

    private BluetoothGattCharacteristic[] mWriteCharacteristicArray;
    private BleTransaction[] mTransactionArray;


    // region Construction and Bookkeeping
    // --------------------------------------------------------------------------------------------

    public BleController(final Context context, BleControllerInterface bleInterface) {
        mContext = context;
        mInterface = bleInterface;
        BluetoothManager manager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = manager.getAdapter();
    }

    public boolean isBluetoothEnabled() {
        return mBluetoothAdapter.isEnabled();
    }

    private void clearTimer() {
        if (mTimerTask != null) {
            mTimerTask.cancel();
            mTimer.cancel();

            mTimer = null;
            mTimerTask = null;
        }
    }

    private void setTimeout(long period, TimerTask task) {
        clearTimer();

        mTimer = new Timer();
        mTimerTask = task;
        mTimer.schedule(mTimerTask, period);
    }


    // --------------------------------------------------------------------------------------------
    // endregion


    // region Scanning for / Connecting to Devices
    // --------------------------------------------------------------------------------------------

    public void connectToDevice(int index) {
        mWriteCharacteristicArray = new BluetoothGattCharacteristic[3];
        mTransactionArray = new BleTransaction[5];

        mBluetoothDevice = mFoundDeviceArray.get(index);
        mGatt = mBluetoothDevice.connectGatt(mContext, false, mGattCallback);

        setTimeout(TIMEOUT_CONNECT, new TimerTask() {
            @Override
            public void run() {
                connectTimeout();
            }
        });
    }

    public void disconnectFromDevice() {
        if (mGatt != null) {
            mGatt.disconnect();
        }
    }

    public void startScan() {
        mFoundDeviceArray = new ArrayList<>();
        mScannedDeviceHashTable = new Hashtable<>();
        mBluetoothAdapter.startLeScan(this);
        setTimeout(TIMEOUT_SCAN, new TimerTask() {
            @Override
            public void run() {
                scanTimeout();
            }
        });
    }

    private void scanTimeout() {
        Log.v(TAG, "scanTimeout()");
        clearTimer();
        mBluetoothAdapter.stopLeScan(this);
        mInterface.scanCompleted();
    }

    private void connectTimeout() {
        Log.v(TAG, "connectTimeout()");
        clearTimer();

        if (mGatt != null) mGatt.close();
        mInterface.connectionDisconnected(new BleException.DisconnectException("Connection Timeout on Connect"));
    }

    /**
     * Given a "deviceName" as provided by the controller, this will pull out the "Serial Number"
     * for that deviceName.
     *
     * @param deviceName the deviceName as provied by the controller during the BLE scan process
     * @return String
     */
    public String getSerialNumberFromDeviceName(String deviceName) {
        int delimiterPosition = deviceName.indexOf("-");
        if (delimiterPosition < 0) return deviceName;
        return deviceName.substring(0, delimiterPosition);
    }

    /**
     * Given a "deviceName" as provided by the controller, this will pull out the "Vehicle Identifier"
     * for that deviceName, if it exists.  If not, it will return NULL
     * <p>
     * Note that due to limitations on the advertising data in the BLE spec for deviceName, we will
     * only return up to the first 19 characters of the vehicleId.
     * <p>
     * So if we are already connected to the device and need to use the full vehicle ID, we should instead
     * be calling executeCommand_GetVehicleId()
     *
     * @param deviceName the deviceName as provided by the controller during the BLE scan process
     * @return String
     */
    public String getVehicleIdentifierFromDeviceName(String deviceName) {
        int delimiterPosition = deviceName.indexOf("-");
        if (delimiterPosition < 0) return null;
        return deviceName.substring(delimiterPosition + 1);
    }

    @Override
    public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
        UUID serviceUuid = BleUtility.parseUuidFromAdvertisementData(scanRecord);
        String deviceName = device.getName();
        String deviceIdentifier = device.getAddress();

        if (mScannedDeviceHashTable.containsKey(deviceIdentifier)) return;
        mScannedDeviceHashTable.put(deviceIdentifier, true);

        if ((deviceName != null) && (serviceUuid != null) && serviceUuid.equals(BleConstants.Uuid.SERVICE)) {
            Log.v(TAG, deviceIdentifier + " - " + deviceName);
            Log.v(TAG, BleUtility.getHexStringFromBytes(scanRecord));
            mFoundDeviceArray.add(device);
            mInterface.scanResult(deviceName);
        } else {
            Log.v(TAG, deviceIdentifier + " - NOT A SMARTDRIVE");
        }

        setTimeout(TIMEOUT_SCAN, new TimerTask() {
            @Override
            public void run() {
                scanTimeout();
            }
        });
    }

    // --------------------------------------------------------------------------------------------
    // endregion


    // region ExecuteTransaction / Send Request
    // --------------------------------------------------------------------------------------------

    private void executeTransaction(final TransactionType transactionType, BleConstants.FunctionCode functionCode, byte[] data) throws BleException.CannotExecuteMethod {
        BleTransaction transaction = mTransactionArray[transactionType.value];

        if (transaction != null) {
            throw new BleException.CannotExecuteMethod(functionCode, transaction.mStatus, transaction.mFunctionCode);
        }

        transaction = new BleTransaction(functionCode);
        mTransactionArray[transactionType.value] = transaction;
        transaction.setRequestData(data);

        // Perform Request
        transaction.mStatus = BleConstants.TransactionStatus.SENDING_REQUEST;
        sendRawSegment(transactionType);
        setTimeout(TIMEOUT_TRANSACTION, new TimerTask() {
            @Override
            public void run() {
                transactionTimeout(transactionType);
            }
        });
    }

    private void transactionTimeout(TransactionType transactionType) {
        Log.v(TAG, String.format("transactionTimeout for %d", transactionType.value));
        clearTimer();

        BleTransaction transaction = mTransactionArray[transactionType.value];
        if (transaction != null) {
            processResponse(transactionType, new BleException.TransactionTimeoutException(transaction));
        } else {
            processResponse(transactionType, new BleException.TransactionTimeoutException(transactionType.value));
        }
    }

    private BleTransaction getOrCreateReadOnlyTransaction(TransactionType transactionType, BleConstants.FunctionCode functionCode) {
        BleTransaction transaction = mTransactionArray[transactionType.value];

        if (transaction != null) {
            return transaction;
        }

        transaction = new BleTransaction(functionCode);
        mTransactionArray[transactionType.value] = transaction;
        transaction.mStatus = BleConstants.TransactionStatus.AWAITING_RESPONSE;
        return transaction;
    }

    private void sendRawSegment(TransactionType transactionType) {
        BleTransaction transaction = mTransactionArray[transactionType.value];
        BluetoothGattCharacteristic writeCharacteristic = mWriteCharacteristicArray[transactionType.value];

        byte[] payload = transaction.getNextRequestSegment();

        Log.v(TAG, String.format("SendRawSegment(%s): %s", transaction.mFunctionCode.label, BleUtility.getHexStringFromBytes(payload)));
        writeCharacteristic.setValue(payload);
        transaction.mAwaitingRequestWriteConfirmationFlag = true;
        mGatt.writeCharacteristic(writeCharacteristic);
    }

    private void confirmWrite(TransactionType transactionType) {
        BleTransaction transaction = mTransactionArray[transactionType.value];

        // Logic to run when we are confirming writing of a Request (or Request Segment)
        if (transaction.mAwaitingRequestWriteConfirmationFlag) {
            transaction.mAwaitingRequestWriteConfirmationFlag = false;
            if (transaction.isAtEndOfRequest()) {
                transaction.mStatus = BleConstants.TransactionStatus.AWAITING_RESPONSE;
            } else {
                sendRawSegment(transactionType);
            }

            // Logic to run when we are confirming writing of an ACK/ERROR
        } else if (transaction.mAwaitingAcknowledgementWriteConfirmationFlag) {
            transaction.mAwaitingAcknowledgementWriteConfirmationFlag = false;
            if (transaction.mStatus == BleConstants.TransactionStatus.PROCESSING_RESPONSE) {
                // Let's go ahead and process the response
                processResponse(transactionType, null);
            } else {
                // report
                processResponse(transactionType, new BleException.InvalidResponseException());
            }
        } else {
            throw new AssertionError("confirmWrite on transaction that is not awaiting write confirmation");
        }
    }

    private void processResponse(TransactionType transactionType, BleException exc) {
        BleTransaction transaction = mTransactionArray[transactionType.value];
        byte[] data = null;
        if (exc == null)
            data = BleUtility.getBytesFromByteArrayList(transaction.mResponseData);
        mTransactionArray[transactionType.value] = null;

        clearTimer();

        // Did the SR4 report an error when processing our request?
        if (exc == null && (data.length == 5) &&
                ((0xFF & data[0]) == 0xFE) &&
                ((0xFF & data[1]) == 0xEF) &&
                ((0xFF & data[2]) == 0xFE) &&
                ((0xFF & data[3]) == 0xEF)) {
            switch (BleConstants.ErrorCode.byValue(data[4])) {
                case USER_NOT_LOGGED_IN:
                    exc = new BleException.UserNotLoggedInSessionTimeoutException(transaction.mFunctionCode);
                    break;
                case UNKNOWN_FUNCTION_CODE:
                    exc = new BleException.UnknownFunctionCodeException(transaction.mFunctionCode);
                    break;
                case OVERSIZED_RESPONSE_PAYLOAD_GENERATED:
                    exc = new BleException.OversizedResponsePayloadGenerated(transaction.mFunctionCode);
                    break;
                case INVALID_REQUEST_PAYLOAD_DATA:
                    exc = new BleException.InvalidRequestPayloadData(transaction.mFunctionCode);
                    break;
                case INVALID_APPLICATION_PAYLOAD_DATA:
                    exc = new BleException.InvalidApplicationRequestData(transaction.mFunctionCode);
                    break;
                default:
                    exc = new BleException("An Unidentified Error Occurred");
                    break;
            }
        }

        if (transaction == null) return;

        // Delegate to the Function-specific method to process
        switch (transaction.mFunctionCode) {
            case LOGIN:
                processResponse_Login(exc, data);
                break;
            case LOGOUT:
                processResponse_Logout(exc, data);
                break;
            case GET_CURRENT_TIME:
                processResponse_GetCurrentTime(exc, data);
                break;
            case SYNC_COMPLETE:
                processResponse_SyncComplete(exc, data);
                break;

            case SET_VEHICLE_ID:
                processResponse_SetVehicleId(exc, data);
                break;
            case GET_VEHICLE_ID:
                processResponse_GetVehicleId(exc, data);
                break;
            case GET_CONTROLLER_SERIAL_NUMBER:
                processResponse_GetControllerSerialNumber(exc, data);
                break;
            case CHECK_SENSOR_BAR:
                processResponse_CheckSensorBar(exc, data);
                break;
            case GET_ADAS_FLAG:
                processResponse_GetAdasFlag(exc, data);
                break;
            case GET_CAMERA_INFO:
                processResponse_GetCameraInfo(exc, data);
                break;
            case CHECK_TURN_SIGNAL:
                processResponse_CheckTurnSignal(exc, data);
                break;
            case GET_COMMUNICATIONS_SETTING:
                processResponse_GetCommunicationsSetting(exc, data);
                break;
            case SET_COMMUNICATIONS_SETTING:
                processResponse_SetCommunicationsSetting(exc, data);
                break;
            case TEST_COMMUNICATIONS_SETTING:
                processResponse_TestCommunicationsSetting(exc, data);
                break;
            case GENERATE_IMAGE_ON_CAMERA:
                processResponse_GenerateImageOnCamera(exc, data);
                break;
            case GENERATE_TEST_EVENT:
                processResponse_GenerateTestEvent(exc, data);
                break;
            case VIEW_EVENT_LIBRARY:
                processResponse_ViewEventLibrary(exc, data);
                break;
            case INITIATE_BLUETOOTH_FILE_SHARING:
                processResponse_InitiateBluetoothFileSharing(exc, data);
                break;
            case REQUEST_EVENT_OVER_BLUETOOTH:
                processResponse_RequestEventOverBluetooth(exc, data);
                break;
            case REQUEST_ADAS_IMAGE_OVER_BLUETOOTH:
                processResponse_RequestAdasImageOverBluetooth(exc, data);
                break;
            case SEND_ADAS_STATS:
                processResponse_SendAdasStats(exc, data);
                break;
            case CHECK_GPS:
                processResponse_CheckGps(exc, data);
                break;
            case GET_DEBUG_INFORMATION:
                processResponse_GetDebugInformation(exc, data);
                break;

            case GENERATE_SELF_CHECK:
                processResponse_GenerateSelfCheck(exc, data);
                break;
            case SELF_CHECK_COMPLETE:
                processResponse_SelfCheckComplete(exc, data);
                break;
        }
    }

    // --------------------------------------------------------------------------------------------
    // endregion


    // region Public Facing Execute Methods
    // --------------------------------------------------------------------------------------------

    public void executeAuthenticate_Login(String password) throws BleException.CannotExecuteMethod {
        String payload = BleConstants.PRIVATE_KEY + password;
        byte[] data = payload.getBytes();
        executeTransaction(TransactionType.AUTHENTICATE, BleConstants.FunctionCode.LOGIN, data);
        mBluetoothAdapter.stopLeScan(this);
    }

    public void executeAuthenticate_Logout() throws BleException.CannotExecuteMethod {
        executeTransaction(TransactionType.AUTHENTICATE, BleConstants.FunctionCode.LOGOUT, null);
    }

    public void executeAuthenticate_GetCurrentTime() throws BleException.CannotExecuteMethod {
        executeTransaction(TransactionType.AUTHENTICATE, BleConstants.FunctionCode.GET_CURRENT_TIME, null);
    }

    public void executeCommand_GetVehicleId() throws BleException.CannotExecuteMethod {
        executeTransaction(TransactionType.COMMAND, BleConstants.FunctionCode.GET_VEHICLE_ID, null);
    }

    public void executeCommand_SetVehicleId(String vehicleId) throws BleException.CannotExecuteMethod {
        byte[] data = vehicleId.getBytes();
        executeTransaction(TransactionType.COMMAND, BleConstants.FunctionCode.SET_VEHICLE_ID, data);
    }

    public void executeCommand_GetControllerSerialNumber() throws BleException.CannotExecuteMethod {
        executeTransaction(TransactionType.COMMAND, BleConstants.FunctionCode.GET_CONTROLLER_SERIAL_NUMBER, null);
    }

    public void executeCommand_CheckSensorBar() throws BleException.CannotExecuteMethod {
        executeTransaction(TransactionType.COMMAND, BleConstants.FunctionCode.CHECK_SENSOR_BAR, null);
    }

    public void executeCommand_GetAdasFlag() throws BleException.CannotExecuteMethod {
        executeTransaction(TransactionType.COMMAND, BleConstants.FunctionCode.GET_ADAS_FLAG, null);
    }

    public void executeCommand_GetCameraInfo(BleConstants.CameraInfo cameraInfo) throws BleException.CannotExecuteMethod {
        byte[] data = cameraInfo.getValue();
        executeTransaction(TransactionType.COMMAND, BleConstants.FunctionCode.GET_CAMERA_INFO, data);
    }

    public void executeCommand_CheckTurnSignal(BleConstants.TurnSignal turnSignal) throws BleException.CannotExecuteMethod {
        byte[] data = turnSignal.getValue();
        executeTransaction(TransactionType.COMMAND, BleConstants.FunctionCode.CHECK_TURN_SIGNAL, data);
    }

    public void executeCommand_GetCommunicationsSetting() throws BleException.CannotExecuteMethod {
        executeTransaction(TransactionType.COMMAND, BleConstants.FunctionCode.GET_COMMUNICATIONS_SETTING, null);
    }

    public void executeCommand_SetCommunicationsSetting(String xmlString) throws BleException.CannotExecuteMethod {
        byte[] data = xmlString.getBytes();
        executeTransaction(TransactionType.COMMAND, BleConstants.FunctionCode.SET_COMMUNICATIONS_SETTING, data);
    }

    public void executeCommand_TestCommunicationsSetting() throws BleException.CannotExecuteMethod {
        executeTransaction(TransactionType.COMMAND, BleConstants.FunctionCode.TEST_COMMUNICATIONS_SETTING, null);
    }

    public void executeCommand_GenerateImageOnCamera(BleConstants.CameraInfo cameraNumber) throws BleException.CannotExecuteMethod {
        byte[] data;
        try{
            data = cameraNumber.getValue();
        } catch (NullPointerException e){
            e.printStackTrace();
            data = new byte[] {0x10};
        }

        executeTransaction(TransactionType.COMMAND, BleConstants.FunctionCode.GENERATE_IMAGE_ON_CAMERA, data);
    }

    public void executeCommand_GenerateTestEvent() throws BleException.CannotExecuteMethod {
        executeTransaction(TransactionType.COMMAND, BleConstants.FunctionCode.GENERATE_TEST_EVENT, null);
    }

    public void executeCommand_ViewEventLibrary(Calendar filterFrom, Calendar filterTo, byte filterExtendedRecording) throws BleException.CannotExecuteMethod {
        byte[] data = new byte[9];
        data[0] = (byte) (filterFrom.get(Calendar.YEAR) / 256);
        data[1] = (byte) (filterFrom.get(Calendar.YEAR) % 256);
        data[2] = (byte) (filterFrom.get(Calendar.MONTH) + 1);
        data[3] = (byte) (filterFrom.get(Calendar.DAY_OF_MONTH));
        data[4] = (byte) (filterFrom.get(Calendar.HOUR_OF_DAY));
        data[5] = (byte) (filterFrom.get(Calendar.MINUTE));
        data[6] = (byte) (filterTo.get(Calendar.HOUR_OF_DAY));
        data[7] = (byte) (filterTo.get(Calendar.MINUTE));
        data[8] = filterExtendedRecording;

        executeTransaction(TransactionType.COMMAND, BleConstants.FunctionCode.VIEW_EVENT_LIBRARY, data);
    }

    public void executeCommand_InitiateBluetoothFileSharing() throws BleException.CannotExecuteMethod {
        executeTransaction(TransactionType.COMMAND, BleConstants.FunctionCode.INITIATE_BLUETOOTH_FILE_SHARING, null);
    }

    public void executeCommand_RequestEventOverBluetooth(String eventUniqueIdentifier) throws BleException.CannotExecuteMethod {
        byte[] data = eventUniqueIdentifier.getBytes();
        executeTransaction(TransactionType.COMMAND, BleConstants.FunctionCode.REQUEST_EVENT_OVER_BLUETOOTH, data);
    }

    public void executeCommand_RequestAdasImageOverBluetooth(BleConstants.DistanceCaptured distanceCaptured) throws BleException.CannotExecuteMethod {
        byte[] data = distanceCaptured.getValue();
        executeTransaction(TransactionType.COMMAND, BleConstants.FunctionCode.REQUEST_ADAS_IMAGE_OVER_BLUETOOTH, data);
    }

    public void executeCommand_SendAdasStats(String xmlString) throws BleException.CannotExecuteMethod {
        byte[] data = xmlString.getBytes();
        executeTransaction(TransactionType.COMMAND, BleConstants.FunctionCode.SEND_ADAS_STATS, data);
    }

    public void executeCommand_CheckGps() throws BleException.CannotExecuteMethod {
        executeTransaction(TransactionType.COMMAND, BleConstants.FunctionCode.CHECK_GPS, null);
    }

    public void executeCommand_GetDebugInformation() throws BleException.CannotExecuteMethod {
        executeTransaction(TransactionType.COMMAND, BleConstants.FunctionCode.GET_DEBUG_INFORMATION, null);
    }

    public void executeSelfCheck_GenerateSelfCheck() throws BleException.CannotExecuteMethod {
        executeTransaction(TransactionType.SELFCHECK, BleConstants.FunctionCode.GENERATE_SELF_CHECK, null);
    }

    // --------------------------------------------------------------------------------------------
    // endregion


    // region Private Facing Callbackers
    // --------------------------------------------------------------------------------------------

    private void processResponse_Login(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.authenticateResponse_Login(exc, (byte) 0x00);
            return;
        }

        mInterface.authenticateResponse_Login(null, data[0]);
        mBluetoothAdapter.stopLeScan(this);

    }

    private void processResponse_Logout(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.authenticateResponse_Logout(exc);
            return;
        }

        mInterface.authenticateResponse_Logout(null);
        mGatt.disconnect();
    }

    private void processResponse_SyncComplete(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.authenticateResponse_SyncComplete(exc);
            return;
        }

        mInterface.authenticateResponse_SyncComplete(null);
    }

    private void processResponse_GetCurrentTime(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.authenticateResponse_GetCurrentTime(exc, null);
            return;
        }

        String dateString = String.format(Locale.getDefault(), "%d-%02d-%02dT%02d:%02d:%02d.000",
                BleUtility.getIntegerFromByte(data[0]) * 256 + BleUtility.getIntegerFromByte(data[1]),
                BleUtility.getIntegerFromByte(data[2]),
                BleUtility.getIntegerFromByte(data[3]),
                BleUtility.getIntegerFromByte(data[4]),
                BleUtility.getIntegerFromByte(data[5]),
                BleUtility.getIntegerFromByte(data[6]));
        Log.v(TAG, dateString);

        TimeZone timeZone = TimeZone.getTimeZone( String.format(Locale.getDefault(),"GMT%s%02d:%02d",
                (data[7] == 0x01) ? "-" : "+",
                BleUtility.getIntegerFromByte(data[8]),
                BleUtility.getIntegerFromByte(data[9])));

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault());
        sdf.setTimeZone(timeZone);
        Calendar date = Calendar.getInstance();
        try {
            date.setTime(sdf.parse(dateString));
            date.setTimeZone(timeZone);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        mInterface.authenticateResponse_GetCurrentTime(null, date);
    }

    private void processResponse_SetVehicleId(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.commandResponse_SetVehicleId(exc);
            return;
        }

        mInterface.commandResponse_SetVehicleId(null);
    }


    private void processResponse_GetVehicleId(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.commandResponse_GetVehicleId(exc, null);
            return;
        }

        mInterface.commandResponse_GetVehicleId(null, new String(data));
    }

    private void processResponse_GetControllerSerialNumber(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.commandResponse_GetControllerSerialNumber(exc, null);
            return;
        }

        mInterface.commandResponse_GetControllerSerialNumber(null, new String(data));
    }

    private void processResponse_CheckSensorBar(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.commandResponse_CheckSensorBar(exc, null);
            return;
        }

        mInterface.commandResponse_CheckSensorBar(null, new String(data));
    }

    private void processResponse_GetAdasFlag(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.commandResponse_GetAdasFlag(exc, false);
            return;
        }

        if (data[0] == 0x01)
            mInterface.commandResponse_GetAdasFlag(exc, true);
        else
            mInterface.commandResponse_GetAdasFlag(exc, false);
    }

    private void processResponse_GetCameraInfo(BleException exc, byte[] data) {

        if (exc != null) {
            mInterface.commandResponse_GetCameraInfo(exc,false, null,  "");
            return;
        }

        mInterface.commandResponse_GetCameraInfo(exc,data[0] != 0x00, BleConstants.CameraType.byValue(data[1]), BleUtility.getCameraInfoSerialNumber(data));
    }

    private void processResponse_CheckTurnSignal(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.commandResponse_CheckTurnSignal(exc,false);
            return;
        }

        if (data[0] == 0x01)
            mInterface.commandResponse_CheckTurnSignal(exc, true);
        else
            mInterface.commandResponse_CheckTurnSignal(exc, false);
    }

    private void processResponse_GetCommunicationsSetting(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.commandResponse_GetCommunicationsSetting(exc, null);
            return;
        }

        mInterface.commandResponse_GetCommunicationsSetting(null, new String(data));
    }

    private void processResponse_SetCommunicationsSetting(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.commandResponse_SetCommunicationsSetting(exc);
            return;
        }

        mInterface.commandResponse_SetCommunicationsSetting(null);
    }

    private void processResponse_TestCommunicationsSetting(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.commandResponse_TestCommunicationsSetting(exc, false, (byte) 0x00);
            return;
        }

        if (data.length == 1)
            mInterface.commandResponse_TestCommunicationsSetting(null, data[0] != 0x00, (byte) 0x00);
        else
            mInterface.commandResponse_TestCommunicationsSetting(null, data[0] != 0x00, data[1]);
    }

    private void processResponse_GenerateImageOnCamera(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.commandResponse_GenerateImageOnCamera(exc, null);
            return;
        }

        try {
            byte[] jpegData = BleUtility.gzDecompress(data);
            Bitmap bitmap = BitmapFactory.decodeByteArray(jpegData, 0, jpegData.length);
            mInterface.commandResponse_GenerateImageOnCamera(null, bitmap);
        } catch (IOException ioException) {
            Log.v(TAG, ioException.getMessage());
            mInterface.commandResponse_GenerateImageOnCamera(ioException, null);
        }
    }

    private void processResponse_GenerateTestEvent(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.commandResponse_GenerateTestEvent(exc);
            return;
        }

        mInterface.commandResponse_GenerateTestEvent(null);
    }

    private void processResponse_ViewEventLibrary(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.commandResponse_ViewEventLibrary(exc, null);
            return;
        }

        try {
            byte[] uncompressedData = BleUtility.gzDecompress(data);
            mInterface.commandResponse_ViewEventLibrary(null, new String(uncompressedData));
        } catch (IOException ioException) {
            Log.v(TAG, ioException.getMessage());
            mInterface.commandResponse_ViewEventLibrary(ioException, null);
        }
    }

    private void processResponse_InitiateBluetoothFileSharing(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.commandResponse_InitiateBluetoothFileSharing(exc, null, null);
            return;
        }

        String payload = new String(data);
        String[] parts = payload.split("/");
        mInterface.commandResponse_InitiateBluetoothFileSharing(null, parts[0], parts[1]);
    }

    private void processResponse_RequestEventOverBluetooth(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.commandResponse_RequestEventOverBluetooth(exc, 0);
            return;
        }

        mInterface.commandResponse_RequestEventOverBluetooth(null, BleUtility.byteArrayToInt(data));
    }

    private void processResponse_RequestAdasImageOverBluetooth(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.commandResponse_RequestAdasImageOverBluetooth(exc,0);
            return;
        }

        mInterface.commandResponse_RequestAdasImageOverBluetooth(null, BleUtility.byteArrayToInt(data));
    }

    private void processResponse_SendAdasStats(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.commandResponse_SendAdasStats(exc);
            return;
        }

        mInterface.commandResponse_SendAdasStats(null);
    }

    private void processResponse_CheckGps(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.commandResponse_CheckGps(exc, (byte) 0x00, (byte) 0x00);
            return;
        }

        mInterface.commandResponse_CheckGps(null, data[0], data[1]);
    }

    private void processResponse_GetDebugInformation(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.commandResponse_GetDebugInformation(exc, null);
            return;
        }

        try {
            byte[] uncompressedData = BleUtility.gzDecompress(data);
            mInterface.commandResponse_GetDebugInformation(null, new String(uncompressedData));
        } catch (IOException ioException) {
            Log.v(TAG, ioException.getMessage());
            mInterface.commandResponse_GetDebugInformation(ioException, null);
        }
    }


    private void processResponse_GenerateSelfCheck(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.selfCheckResponse_GenerateSelfCheck(exc);
            return;
        }

        mInterface.selfCheckResponse_GenerateSelfCheck(null);
    }


    private void processResponse_SelfCheckComplete(BleException exc, byte[] data) {
        if (exc != null) {
            mInterface.selfCheckResponse_SelfCheckComplete(exc, null);
            return;
        }

        try {
            byte[] uncompressedData = BleUtility.gzDecompress(data);
            mInterface.selfCheckResponse_SelfCheckComplete(null, new String(uncompressedData));
        } catch (IOException ioException) {
            Log.v(TAG, ioException.getMessage());
            mInterface.selfCheckResponse_SelfCheckComplete(ioException, null);
        }
    }

    // --------------------------------------------------------------------------------------------
    // endregion


    // region GATT Management
    // --------------------------------------------------------------------------------------------

    private void subscribeToNotificationCharacteristic(BluetoothGattCharacteristic notificationCharacteristic) {
        BluetoothGattDescriptor config = notificationCharacteristic.getDescriptor(BleConstants.Uuid.DEFAULT_CONFIG);
        mGatt.setCharacteristicNotification(notificationCharacteristic, true);
        config.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mGatt.writeDescriptor(config);
    }

    private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);
            Log.v(TAG, "onDescriptorWrite()");

            if (mWriteCharacteristicArray[CharacteristicType.COMMAND.value] == null) {
                mWriteCharacteristicArray[CharacteristicType.COMMAND.value] = mService.getCharacteristic(BleConstants.Uuid.COMMAND_WRITE);
                subscribeToNotificationCharacteristic(mService.getCharacteristic(BleConstants.Uuid.COMMAND_NOTIFICATION));
            } else if (mWriteCharacteristicArray[CharacteristicType.SELFCHECK.value] == null) {
                mWriteCharacteristicArray[CharacteristicType.SELFCHECK.value] = mService.getCharacteristic(BleConstants.Uuid.SELFCHECK_WRITE);
                subscribeToNotificationCharacteristic(mService.getCharacteristic(BleConstants.Uuid.SELFCHECK_NOTIFICATION));
            } else {
                mInterface.connectionComplete();
                clearTimer();
            }
        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            Log.v(TAG, "onConnectionStateChange()");

            if (status == BluetoothGatt.GATT_SUCCESS && newState == BluetoothProfile.STATE_CONNECTED) {
                // Connection has been established
                // Now, let's discover services
                gatt.discoverServices();

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                // Disconnected
                gatt.close();

                if (status == BluetoothGatt.GATT_SUCCESS) {
                    // Handle a successful disconnect event
                    mInterface.connectionDisconnected(null);
                } else if (status == BluetoothGatt.GATT_FAILURE) {
                    // Handle a unsuccessful connection attempt
                    mInterface.connectionDisconnected(new BleException.DisconnectException("Unable to connect to SR4. If you are having trouble connecting, please go to your device’s settings and turn Bluetooth off and then back on"));
                } else {
                    mInterface.connectionDisconnected(new BleException.DisconnectException(String.format("Unexpected Disconnect. \n\nTrouble connecting? \nPlease turn Bluetooth OFF and then back ON. \n\nStatus: %d", status)));
                }


                mGatt = null;

            } else {
                Log.v(TAG, String.format("Unhandled onConnectionStateChange [Status: %d, newState = %d]", status, newState));
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            Log.v(TAG, "onServicesDiscovered()");

            mService = gatt.getService(BleConstants.Uuid.SERVICE);
            if (mService != null) {
                mGatt = gatt;

                mWriteCharacteristicArray[CharacteristicType.AUTHENTICATE.value] = mService.getCharacteristic(BleConstants.Uuid.AUTHENTICATION_WRITE);
                subscribeToNotificationCharacteristic(mService.getCharacteristic(BleConstants.Uuid.AUTHENTICATION_NOTIFICATION));
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            Log.v(TAG, String.format("onCharacteristicChanged(%s): %s", characteristic.getUuid().toString(), BleUtility.getHexStringFromBytes(characteristic.getValue())));

            BleTransaction transaction = null;
            BluetoothGattCharacteristic writeCharacteristic = null;
            byte[] payload = characteristic.getValue();

            // Special Case/Circumstances for SyncComplete and SelfCheckComplete methods
            if (payload[0] == BleConstants.FunctionCode.SYNC_COMPLETE.value) {
                transaction = getOrCreateReadOnlyTransaction(TransactionType.AUTHENTICATE_SYNC_COMPLETE, BleConstants.FunctionCode.SYNC_COMPLETE);
                writeCharacteristic = mWriteCharacteristicArray[TransactionType.AUTHENTICATE.value];
                setTimeout(TIMEOUT_TRANSACTION, new TimerTask() {
                    @Override
                    public void run() {
                        transactionTimeout(TransactionType.AUTHENTICATE_SYNC_COMPLETE);
                    }
                });
            } else if (payload[0] == BleConstants.FunctionCode.SELF_CHECK_COMPLETE.value) {
                transaction = getOrCreateReadOnlyTransaction(TransactionType.SELFCHECK_COMPLETE, BleConstants.FunctionCode.SELF_CHECK_COMPLETE);
                writeCharacteristic = mWriteCharacteristicArray[TransactionType.SELFCHECK.value];
                setTimeout(TIMEOUT_TRANSACTION, new TimerTask() {
                    @Override
                    public void run() {
                        transactionTimeout(TransactionType.SELFCHECK_COMPLETE);
                    }
                });
            } else {
                if (characteristic.getUuid().equals(BleConstants.Uuid.AUTHENTICATION_NOTIFICATION)) {
                    transaction = mTransactionArray[TransactionType.AUTHENTICATE.value];
                    writeCharacteristic = mWriteCharacteristicArray[TransactionType.AUTHENTICATE.value];
                    setTimeout(TIMEOUT_TRANSACTION, new TimerTask() {
                        @Override
                        public void run() {
                            transactionTimeout(TransactionType.AUTHENTICATE);
                        }
                    });
                } else if (characteristic.getUuid().equals(BleConstants.Uuid.COMMAND_NOTIFICATION)) {
                    transaction = mTransactionArray[TransactionType.COMMAND.value];
                    writeCharacteristic = mWriteCharacteristicArray[TransactionType.COMMAND.value];
                    setTimeout(TIMEOUT_TRANSACTION, new TimerTask() {
                        @Override
                        public void run() {
                            transactionTimeout(TransactionType.COMMAND);
                        }
                    });
                } else if (characteristic.getUuid().equals(BleConstants.Uuid.SELFCHECK_NOTIFICATION)) {
                    transaction = mTransactionArray[TransactionType.SELFCHECK.value];
                    writeCharacteristic = mWriteCharacteristicArray[TransactionType.SELFCHECK.value];
                    setTimeout(TIMEOUT_TRANSACTION, new TimerTask() {
                        @Override
                        public void run() {
                            transactionTimeout(TransactionType.SELFCHECK);
                        }
                    });
                }
            }

            if (transaction == null) {
                Log.v(TAG, String.format("onCharacteristicChanged(%s): Unable to correlate", characteristic.getUuid().toString()));
                clearTimer();
                return;
            }

            if (!transaction.isAwaitingResponse()) {
                Log.v(TAG, String.format("onCharacteristicChanged(%s): Transaction not in AwaitingResponse state", characteristic.getUuid().toString()));
                clearTimer();
                return;
            }

            BleConstants.ProcessResponseSegmentResult result = transaction.processResponseSegment(payload);

            switch (result) {
                case MESSAGE_COMPLETE:
                    // Send ACK
                    sendAckOrError(transaction, writeCharacteristic, (byte) 0x00);
                    break;
                case MESSAGE_CONTINUES:
                    // Do Nothing
                    mInterface.responseSegmentReceived(transaction.mCurrentResponseSegmentNumber);
                    break;
                case ERROR:
                default:
                    // Send ERROR
                    sendAckOrError(transaction, writeCharacteristic, (byte) 0x02);
                    break;
            }
        }

        private void sendAckOrError(BleTransaction transaction, BluetoothGattCharacteristic writeCharacteristic, byte ackOrErrorCode) {
            byte[] segment = new byte[8];
            int segmentNumber = transaction.mCurrentResponseSegmentNumber - 1;

            segment[0] = BleConstants.FunctionCode.ACK_OR_ERROR.value;
            segment[1] = (byte) 0x00; // Sequence Order
            segment[2] = (byte) 0x04; // Length, 4 Bytes
            segment[3] = transaction.mFunctionCode.value; // Original Function COde
            segment[4] = (byte) (segmentNumber / 256);
            segment[5] = (byte) (segmentNumber % 256);
            segment[6] = ackOrErrorCode;

            segment[7] = BleUtility.calculateChecksum(segment, 7);

            Log.v(TAG, String.format("SendAckOrErrorSegment(%s): %s", transaction.mFunctionCode.label, BleUtility.getHexStringFromBytes(segment)));
            writeCharacteristic.setValue(segment);
            transaction.mAwaitingAcknowledgementWriteConfirmationFlag = true;
            mGatt.writeCharacteristic(writeCharacteristic);
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            Log.v(TAG, String.format("onCharacteristicWrite(%s, %d)", characteristic.getUuid().toString(), status));

            BleTransaction transaction;

            if (characteristic.getUuid().equals(BleConstants.Uuid.AUTHENTICATION_WRITE)) {
                transaction = mTransactionArray[TransactionType.AUTHENTICATE.value];
                if ((transaction != null) && (transaction.isAwaitingWriteConfirmation())) {
                    confirmWrite(TransactionType.AUTHENTICATE);
                    return;
                }

                transaction = mTransactionArray[TransactionType.AUTHENTICATE_SYNC_COMPLETE.value];
                if ((transaction != null) && (transaction.isAwaitingWriteConfirmation())) {
                    confirmWrite(TransactionType.AUTHENTICATE_SYNC_COMPLETE);
                    return;
                }
            }

            if (characteristic.getUuid().equals(BleConstants.Uuid.COMMAND_WRITE)) {
                transaction = mTransactionArray[TransactionType.COMMAND.value];
                if ((transaction != null) && (transaction.isAwaitingWriteConfirmation())) {
                    confirmWrite(TransactionType.COMMAND);
                    return;
                }
            }

            if (characteristic.getUuid().equals(BleConstants.Uuid.SELFCHECK_WRITE)) {
                transaction = mTransactionArray[TransactionType.SELFCHECK.value];
                if ((transaction != null) && (transaction.isAwaitingWriteConfirmation())) {
                    confirmWrite(TransactionType.SELFCHECK);
                    return;
                }

                transaction = mTransactionArray[TransactionType.SELFCHECK_COMPLETE.value];
                if ((transaction != null) && (transaction.isAwaitingWriteConfirmation())) {
                    confirmWrite(TransactionType.SELFCHECK_COMPLETE);
                    return;
                }
            }

            // If we are here, then something went wrong -- we are confirming a write, but cannot correlate it to a transaction
            Log.v(TAG, String.format("onCharacteristicWrite(%s, %d): Unable to Correlate", characteristic.getUuid().toString(), status));
        }
    };

    // --------------------------------------------------------------------------------------------
    // endregion
}
