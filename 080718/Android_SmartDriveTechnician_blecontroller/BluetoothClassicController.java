package com.smartdrive.technician.controller;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

/**
 * Created by Barefoot Solutions on 8/1/16.
 */
public class BluetoothClassicController {

    private BluetoothSocketThread mBluetoothSocketThread;
    private int mState;
    private final Handler mHandler;
    private String mUuid;
    private String mAddress;

    private Timer mTimer;
    private TimerTask mTimerTask;

    public static final int STATE_NONE = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;

    public static final int MESSAGE_READ = 3;
    public static final int CREATE_RFCOMM_SOCKET_ERROR = 4;
    public static final int SOCKET_CONNECTION_ERROR = 5;
    public static final int CLOSE_SOCKET_CONNECTION_ERROR = 6;
    public static final int INPUT_OUTPUT_STREAM_ERROR = 7;
    public static final int DATA_RECEIVED_ERROR = 8;
    public static final int UNPAIR_AND_REMOVE_BLUETOOTH_DEVICE_ERROR = 9;
    public static final int MESSAGE_RECEIVE_TIMEOUT = 10;

    private static final long TIMEOUT_RECEIVE = 5000;


    /**
     * Constructor
     *
     * @param handler A Handler to send messages back to the UI Activity
     */
    public BluetoothClassicController(Handler handler) {
        mState = STATE_NONE;
        mHandler = handler;
    }

    private synchronized void setState(int state) {
        mState = state;
        mHandler.obtainMessage(state).sendToTarget();
    }

    public synchronized int getState() {
        return mState;
    }

    public synchronized void connectToDeviceAtAddress(String address, String uuid) {
        if (mState == STATE_NONE) {
            mUuid = uuid;
            mAddress = address.toUpperCase();
            connectToDevice();
        }
    }

    private void connectToDevice() {
        setState(STATE_CONNECTING);
        mBluetoothSocketThread = new BluetoothSocketThread();
        mBluetoothSocketThread.start();
    }

    public synchronized void startDownload() {
        if (mState == STATE_NONE) {
            connectToDevice();
        }
        mBluetoothSocketThread.downloadingStarted = true;
    }

    public synchronized void finishDownload() {
        if (mBluetoothSocketThread != null) {
            mBluetoothSocketThread.downloadingStarted = false;
        }
    }

    public synchronized void unpairAndRemoveBluetoothDevice() {
        stop();

		if (mAddress == null) return;

        try {
            BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(mAddress);
            Method m = device.getClass().getMethod("removeBond", (Class[]) null);
            m.invoke(device, (Object[]) null);
        } catch (Exception unpairException) {
            unpairException.printStackTrace();
            mHandler.obtainMessage(UNPAIR_AND_REMOVE_BLUETOOTH_DEVICE_ERROR, unpairException).sendToTarget();
        }
    }

    public synchronized void stop() {
        if (mBluetoothSocketThread != null) {
            mBluetoothSocketThread.cancel();
        }
    }

    private void setTimer() {
        if (mTimerTask == null) {
            mTimer = new Timer();
            mTimerTask = new TimerTask() {
                @Override
                public void run() {
                    clearTimer();
                    mHandler.obtainMessage(MESSAGE_RECEIVE_TIMEOUT).sendToTarget();
                    if (mBluetoothSocketThread != null) {
                        mBluetoothSocketThread.cancel();
                    }
                }
            };
            mTimer.schedule(mTimerTask, TIMEOUT_RECEIVE);
        }
    }

    private void clearTimer() {
        if (mTimerTask != null) {
            mTimerTask.cancel();
            mTimer.cancel();
            mTimer = null;
            mTimerTask = null;
        }
    }

    private class BluetoothSocketThread extends Thread {

        private boolean downloadingStarted = false;
        private BluetoothSocket socket;

        public void run() {
            BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
            try {
                BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(mAddress);
                socket = device.createRfcommSocketToServiceRecord(UUID.fromString(mUuid));
            } catch (Exception createSocketException) {
                createSocketException.printStackTrace();
                mHandler.obtainMessage(CREATE_RFCOMM_SOCKET_ERROR, createSocketException).sendToTarget();
                cancel();
                return;
            }
            try {
                socket.connect();
            } catch (IOException connectionException) {
                connectionException.printStackTrace();
                mHandler.obtainMessage(SOCKET_CONNECTION_ERROR, connectionException).sendToTarget();
                cancel();
                return;
            }
            setState(STATE_CONNECTED);
            InputStream inputStream;
            try {
                inputStream = socket.getInputStream();
            } catch (IOException inputOutputException) {
                inputOutputException.printStackTrace();
                mHandler.obtainMessage(INPUT_OUTPUT_STREAM_ERROR, inputOutputException).sendToTarget();
                cancel();
                return;
            }
            byte[] buffer;
            int available;
            while (!isInterrupted()) {
                try {
                    if (downloadingStarted) {
                        setTimer();
                    } else {
                        clearTimer();
                    }
                    available = inputStream.available();
                    if (available == 0) {
                        try {
                            sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            return;
                        }
                        continue;
                    }
                    buffer = new byte[available];
                    available = inputStream.read(buffer);
                    clearTimer();
                    mHandler.obtainMessage(MESSAGE_READ, available, -1, buffer).sendToTarget();
                } catch (IOException readException) {
                    readException.printStackTrace();
                    mHandler.obtainMessage(DATA_RECEIVED_ERROR, readException).sendToTarget();
                    cancel();
                    return;
                }
            }
        }

        private void cancel() {
            if (mBluetoothSocketThread != null) {
                mBluetoothSocketThread.interrupt();
                mBluetoothSocketThread = null;
            }
            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException closeException) {
                closeException.printStackTrace();
                mHandler.obtainMessage(CLOSE_SOCKET_CONNECTION_ERROR, closeException).sendToTarget();
            }
            setState(STATE_NONE);
        }
    }

}