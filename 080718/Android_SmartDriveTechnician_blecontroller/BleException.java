package com.smartdrive.technician.controller;

/**
 * Created by mikeho on 7/1/16.
 */
public class BleException extends Exception {
    public BleException(String detailMessage) {
        super(detailMessage);
    }

    public static class DisconnectException extends BleException {
        public DisconnectException(String detailMessage) {
            super(detailMessage);
        }
    }

    public static class CannotExecuteMethod extends BleException {
        public CannotExecuteMethod(BleConstants.FunctionCode functionCode, BleConstants.TransactionStatus currentStatus, BleConstants.FunctionCode currentFunctionCode) {
            super("Cannot execute [" + functionCode.label + "]: a transaction already exists at [" + currentStatus.value + "] for [" + currentFunctionCode.value + "]");
        }
    }

    public static class TransactionTimeoutException extends BleException {
        public TransactionTimeoutException(BleTransaction transaction) {
            super("Timeout on executing [" + transaction.mFunctionCode.label + "]");
        }
        public TransactionTimeoutException(int transactionType) {
            super(String.format("Timeout on executing transaction type: %d", transactionType));
        }
    }

    public static class InvalidResponseException extends BleException {
        public InvalidResponseException() {
            super("Invalid Response Received");
        }
    }

    public static class UserNotLoggedInSessionTimeoutException extends BleException {
        public UserNotLoggedInSessionTimeoutException(BleConstants.FunctionCode functionCode) {
            super("User Not Logged In / Session Timeout");
        }
    }

    public static class UnknownFunctionCodeException extends BleException {
        public UnknownFunctionCodeException(BleConstants.FunctionCode functionCode) {
            super("Unknown Function Code: " + functionCode.label);
        }
    }

    public static class OversizedResponsePayloadGenerated extends BleException {
        public OversizedResponsePayloadGenerated(BleConstants.FunctionCode functionCode) {
            super("Oversized Response Payload Generated");
        }
    }

    public static class InvalidRequestPayloadData extends BleException {
        public InvalidRequestPayloadData(BleConstants.FunctionCode functionCode) {
            super("Invalid Request Payload - Raw Data Invalid");
        }
    }

    public static class InvalidApplicationRequestData extends BleException {
        public InvalidApplicationRequestData(BleConstants.FunctionCode functionCode) {
            super("Invalid Request Payload - Application Data Invalid");
        }
    }
}