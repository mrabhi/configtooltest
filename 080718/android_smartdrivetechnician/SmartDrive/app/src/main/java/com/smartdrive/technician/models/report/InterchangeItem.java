package com.smartdrive.technician.models.report;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "InterchangeItem")
public class InterchangeItem extends Model implements Parcelable {

    public static final Parcelable.Creator<InterchangeItem> CREATOR = new Parcelable.Creator<InterchangeItem>() {
        @Override
        public InterchangeItem createFromParcel(Parcel source) {
            return new InterchangeItem(source);
        }

        @Override
        public InterchangeItem[] newArray(int size) {
            return new InterchangeItem[size];
        }
    };

    @Column
    public long reportId;
    @Column
    public boolean connected;
    @Column
    public String name;
    @Column
    public String notes;

    public InterchangeItem() {
    }

    public InterchangeItem(String name) {
        this.name = name;
    }

    protected InterchangeItem(Parcel in) {
        this.reportId = in.readLong();
        this.connected = in.readByte() != 0;
        this.name = in.readString();
        this.notes = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.reportId);
        dest.writeByte(this.connected ? (byte) 1 : (byte) 0);
        dest.writeString(this.name);
        dest.writeString(this.notes);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static List<InterchangeItem> getListFromDB(long reportId) {
        return new Select().from(InterchangeItem.class).where("reportId = ?", reportId).execute();
    }

}