package com.smartdrive.technician.fragments.installation.calibration;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.smartdrive.technician.EventsSubscriber;
import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.activities.AdasCalibrationActivity;
import com.smartdrive.technician.controller.BleConstants;
import com.smartdrive.technician.events.ble.AdasImageGeneratedEvent;
import com.smartdrive.technician.fragments.BaseFragment;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;

@EventsSubscriber
@Layout(id = R.layout.fragment_adas_instructions)
public class InstructionsFragment extends BaseFragment {

    @Bind(R.id.vCaptureImage)
    View vCaptureImage;
    @Bind(R.id.tvLabel1)
    TextView tvLabel1;
    @Bind(R.id.tvLabel2)
    TextView tvLabel2;
    @Bind(R.id.ivInstructions)
    ImageView ivInstructions;

    BleConstants.DistanceCaptured mDistanceCaptured;

    @SuppressLint("StringFormatMatches")
    @Override
    protected void initView() {
        AdasCalibrationActivity activity = ((AdasCalibrationActivity) getActivity());
        if (activity == null) {
            return;
        }
        mDistanceCaptured = activity.getStep();

        tvLabel1.setText(getResources().getString(R.string.calibrations_instructions_label1,
                                                  activity.getStepString()));
        tvLabel2.setText(getResources().getString(R.string.calibrations_instructions_label2));
        ivInstructions.setImageDrawable(getResources().getDrawable(activity.getStepImage()));

        vCaptureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bleManager.captureAdasImage(mDistanceCaptured);
                ((AdasCalibrationActivity) getActivity()).showProgress(R.string.capturing_image);
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAdasImageRequest(AdasImageGeneratedEvent event) {
        ((AdasCalibrationActivity) getActivity()).hideProgress();
        if (event.getBytes() != null && event.getBytes().length > 0) {
            Toast.makeText(getActivity(), "bytes loaded: " + event.getBytes().length,
                           Toast.LENGTH_SHORT)
                    .show();

            ((AdasCalibrationActivity) getActivity()).saveBitmap(event.getBytes());
        }
        nextFragment();
    }

    void nextFragment() {
        if (mDistanceCaptured == BleConstants.DistanceCaptured.DISTANCE_2_0_METERS) {
            ((AdasCalibrationActivity) getActivity()).openFragment(new VerticalCheckFragment());
        } else {
            ((AdasCalibrationActivity) getActivity()).openFragment(new BoxDrawFragment());
        }
    }

    @SuppressLint("DefaultLocale")
    private String formatDouble(double d) {
        if (d == (long) d) {
            return String.format("%d", (long) d);
        } else {
            return String.format("%s", d);
        }
    }
}