package com.smartdrive.technician.fragments.installation;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;

import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.SmartDriveApp;
import com.smartdrive.technician.events.ChangeFieldsEvent;
import com.smartdrive.technician.fragments.BaseFragment;
import com.smartdrive.technician.models.report.Report;
import com.smartdrive.technician.widgets.BaseEditText;
import com.smartdrive.technician.widgets.BaseSpinner;

import org.greenrobot.eventbus.EventBus;

import java.util.Arrays;

import butterknife.Bind;

import static com.smartdrive.technician.Constants.SEPARATOR;
import static com.smartdrive.technician.Constants.USER_DATA_SAVING_TIME;

@Layout(id = R.layout.fragment_get_started)
public class GetStartedFragment extends BaseFragment {

    @Bind(R.id.scrollGetStarted)
    ScrollView scrollGetStarted;
    @Bind(R.id.etFirstName)
    BaseEditText etFirstName;
    @Bind(R.id.etLastName)
    BaseEditText etLastName;
    @Bind(R.id.etPhoneNumber)
    BaseEditText etPhoneNumber;
    @Bind(R.id.etCompany)
    BaseEditText etCompany;
    @Bind(R.id.etSon)
    BaseEditText etSon;
    @Bind(R.id.cbSon)
    CheckBox cbSon;
    @Bind(R.id.cbController)
    CheckBox cbController;
    @Bind(R.id.cbSensorBar)
    CheckBox cbSensorBar;
    @Bind(R.id.cbCameras)
    CheckBox cbCameras;
    @Bind(R.id.cbAdasCamera)
    CheckBox cbAdasCamera;
    @Bind(R.id.cbOtherCameras)
    CheckBox cbOtherCameras;
    @Bind(R.id.cbAccessories)
    CheckBox cbAccessories;
    @Bind(R.id.cbInterchange)
    CheckBox cbInterchange;
    @Bind(R.id.cbLaneDeparture)
    CheckBox cbLaneDeparture;

    @Bind(R.id.rgWorkType)
    RadioGroup rgWorkType;
    @Bind(R.id.rbNewInstall)
    RadioButton rbNewInstall;
    @Bind(R.id.rbService)
    RadioButton rbService;
    @Bind(R.id.spServices)
    BaseSpinner spServices;
    @Bind(R.id.vService)
    View vService;
    @Bind(R.id.ivHelp)
    ImageView ivHelp;
    @Bind(R.id.etNotes)
    EditText etNotes;


    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            SmartDriveApp.firstName = etFirstName.getText();
            SmartDriveApp.lastName = etLastName.getText();
            SmartDriveApp.phoneNumber = etPhoneNumber.getText();
            SmartDriveApp.company = etCompany.getText();
            SmartDriveApp.inputUserDataTime = System.currentTimeMillis();
        }
    };

    private CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                buttonView.setTextColor(Color.WHITE);
            } else {
                if (getContext() != null) {
                    buttonView.setTextColor(ContextCompat.getColor(getContext(), R.color.hint));
                }
            }
            EventBus.getDefault()
                    .post(new ChangeFieldsEvent(hasChecks()));
        }
    };

    @Override
    protected void initView() {
        cbSon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etSon.setVisibility(View.VISIBLE);
                } else {
                    etSon.setVisibility(View.GONE);
                    etSon.setText("");
                }
            }
        });
        etFirstName.init(R.string.first_name, false);
        etFirstName.setNextFocusView(etLastName);
        etLastName.init(R.string.last_name, false);
        etPhoneNumber.init(R.string.phone_number, false);
        etPhoneNumber.setInputType(InputType.TYPE_CLASS_PHONE);
        etCompany.init(R.string.installer_company, false);
        etSon.init(R.string.input_son, false);
        if (SmartDriveApp.inputUserDataTime != 0
                && (System.currentTimeMillis() - SmartDriveApp.inputUserDataTime
                < USER_DATA_SAVING_TIME)) {
            etFirstName.setText(SmartDriveApp.firstName);
            etLastName.setText(SmartDriveApp.lastName);
            etPhoneNumber.setText(SmartDriveApp.phoneNumber);
            etCompany.setText(SmartDriveApp.company);
        } else {
            SmartDriveApp.clearUserData();
        }
        etFirstName.addTextChangedListener(textWatcher);
        etLastName.addTextChangedListener(textWatcher);
        etPhoneNumber.addTextChangedListener(textWatcher);
        etCompany.addTextChangedListener(textWatcher);
        cbController.setOnCheckedChangeListener(onCheckedChangeListener);
        cbSensorBar.setOnCheckedChangeListener(onCheckedChangeListener);
        cbCameras.setOnCheckedChangeListener(onCheckedChangeListener);
        cbAdasCamera.setOnCheckedChangeListener(onCheckedChangeListener);
        cbOtherCameras.setOnCheckedChangeListener(onCheckedChangeListener);
        cbAccessories.setOnCheckedChangeListener(onCheckedChangeListener);
        cbInterchange.setOnCheckedChangeListener(onCheckedChangeListener);


        //new
        spServices.init(Arrays.asList(getResources().getStringArray(R.array.service_types)),
                        R.string.service_hint, true);
        rbService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vService.setVisibility(View.VISIBLE);
                etNotes.setVisibility(View.VISIBLE);
            }
        });
        rbNewInstall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vService.setVisibility(View.GONE);
                etNotes.setVisibility(View.GONE);
            }
        });
        ivHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO:
            }
        });
    }

    public void updateReport(Report report) {
        report.firstName = etFirstName.getText();
        report.lastName = etLastName.getText();
        report.phoneNumber = etPhoneNumber.getText();
        report.company = etCompany.getText();
        report.son = etSon.getText();
        StringBuilder selectedSettings = new StringBuilder();
        if (isControllerChecked()) {
            selectedSettings.append(cbController.getText()
                                            .toString());
        } else {
            report.controller = null;
        }
        if (isSensorBarChecked()) {
            if (selectedSettings.length() != 0) {
                selectedSettings.append(SEPARATOR);
            }
            selectedSettings.append(cbSensorBar.getText()
                                            .toString());
        } else {
            report.sensorBar = null;
        }
        if (!isControllerChecked() && !isSensorBarChecked()) {
            report.communicationSettings = null;
        }
        if (isCamerasChecked()) {
            if (selectedSettings.length() != 0) {
                selectedSettings.append(SEPARATOR);
            }
            selectedSettings.append(cbCameras.getText()
                                            .toString());
        } else {
            report.cameras = null;
        }
        if (isAdasCameraChecked()) {
            if (selectedSettings.length() != 0) {
                selectedSettings.append(SEPARATOR);
            }
            selectedSettings.append(cbAdasCamera.getText()
                                            .toString());
        } else {
            report.adas = null;
        }
        if (isOtherCamerasChecked()) {
            if (selectedSettings.length() != 0) {
                selectedSettings.append(SEPARATOR);
            }
            selectedSettings.append(cbOtherCameras.getText()
                                            .toString());
        } else {
            report.otherCameras = null;
        }
        if (isAccessoriesChecked()) {
            if (selectedSettings.length() != 0) {
                selectedSettings.append(SEPARATOR);
            }
            selectedSettings.append(cbAccessories.getText()
                                            .toString());
        } else {
            report.accessories = null;
        }
        if (isInterchangeChecked()) {
            if (selectedSettings.length() != 0) {
                selectedSettings.append(SEPARATOR);
            }
            selectedSettings.append(cbInterchange.getText()
                                            .toString());
        } else {
            report.interchangeItems = null;
        }
        report.selectedSettings = selectedSettings.toString();
    }

    public boolean isControllerChecked() {
        return cbController.isChecked();
    }

    public boolean isSensorBarChecked() {
        return cbSensorBar.isChecked();
    }

    public boolean isCamerasChecked() {
        return cbCameras.isChecked();
    }

    public boolean isAdasCameraChecked() {
        return cbAdasCamera.isChecked();
    }

    public boolean isOtherCamerasChecked() {
        return cbOtherCameras.isChecked();
    }

    public boolean isAccessoriesChecked() {
        return cbAccessories.isChecked();
    }

    public boolean isInterchangeChecked() {
        return cbInterchange.isChecked();
    }

    public boolean isLaneDepartureChecked() {
        return cbLaneDeparture.isChecked();
    }

    public boolean areFieldsValid() {
        boolean areFieldsValid =
                !etFirstName.isRequiredAndEmpty(true) & !etLastName.isRequiredAndEmpty(true);
        if (!areFieldsValid) {
            scrollGetStarted.fullScroll(View.FOCUS_UP);
        }
        return areFieldsValid;
    }

    public boolean hasChecks() {
        return isControllerChecked() || isSensorBarChecked() || isCamerasChecked()
                || isAdasCameraChecked()
                || isOtherCamerasChecked() || isAccessoriesChecked() || isInterchangeChecked();
    }

    public boolean isSonEmpty() {
        return cbSon.isChecked() && TextUtils.isEmpty(etSon.getText());
    }

}