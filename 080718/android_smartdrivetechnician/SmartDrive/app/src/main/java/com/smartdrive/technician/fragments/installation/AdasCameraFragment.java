package com.smartdrive.technician.fragments.installation;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.smartdrive.technician.Constants;
import com.smartdrive.technician.EventsSubscriber;
import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.SimpleTextWatcher;
import com.smartdrive.technician.activities.AdasCalibrationActivity;
import com.smartdrive.technician.controller.BleConstants;
import com.smartdrive.technician.events.ble.GetCameraInfoEvent;
import com.smartdrive.technician.models.VehicleMeasurements;
import com.smartdrive.technician.models.report.Camera;
import com.smartdrive.technician.models.report.Report;
import com.smartdrive.technician.utils.CsvParser;
import com.smartdrive.technician.utils.Dialogs;
import com.smartdrive.technician.utils.Utils;
import com.smartdrive.technician.utils.XmlHelper;
import com.smartdrive.technician.widgets.BaseSpinner;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.smartdrive.technician.activities.AdasCalibrationActivity.ERROR;
import static com.smartdrive.technician.activities.AdasCalibrationActivity.STEP;

@EventsSubscriber
@Layout(id = R.layout.fragment_camera_adas)
public class AdasCameraFragment extends ChildInstallationFragment {

    private static final String LAST_MEASUREMENTS = "last_measurements";
    private static final int CALIB_REQ_CODE = 10101;

    @Bind(R.id.vRootContainer)
    RelativeLayout vRootContainer;
    @Bind(R.id.camerasHeader)
    TextView tvHeader;
    @Bind(R.id.tvCameraName)
    TextView tvCameraName;
    @Bind(R.id.tvCameraSerial)
    TextView tvCameraSerial;
    @Bind(R.id.tvState)
    TextView tvState;
    @Bind(R.id.ivState)
    ImageView ivState;
    @Bind(R.id.spFieldOfView)
    BaseSpinner spFieldOfView;
    @Bind(R.id.etNotes)
    EditText etNotes;
    @Bind(R.id.bReadInstallationInstructions)
    View bReadInstallationInstructions;
    @Bind(R.id.cbAgreement)
    CheckBox cbAgreement;
    @Bind(R.id.vAgreement)
    View vAgreement;
    @Bind(R.id.ivHelpCameraHeight)
    View ivHelpCameraHeight;
    @Bind(R.id.bUseMeasurements)
    View bUseMeasurements;
    @Bind(R.id.etVehicleWidth)
    EditText etVehicleWidth;
    @Bind(R.id.etFrontOfHood)
    EditText etFrontOfHood;
    @Bind(R.id.etCameraOffset)
    EditText etCameraOffset;
    @Bind(R.id.etCameraHeight)
    EditText etCameraHeight;
    @Bind(R.id.rbOffsetLeft)
    RadioButton rbOffsetLeft;
    @Bind(R.id.rbOffsetRight)
    RadioButton rbOffsetRight;
    @Bind(R.id.bCalibrate1)
    TextView bCalibrate1;
    @Bind(R.id.bCalibrate2)
    TextView bCalibrate2;
    @Bind(R.id.bCalibrate3)
    TextView bCalibrate3;
    @Bind(R.id.tvCalibrated1)
    View tvCalibrated1;
    @Bind(R.id.tvCalibrated2)
    View tvCalibrated2;
    @Bind(R.id.tvCalibrated3)
    View tvCalibrated3;
    @Bind(R.id.container)
    RelativeLayout vContainer;
    @Bind(R.id.refresh)
    View vRefresh;
    @Bind(R.id.vWrongPort)
    View vWrongPort;
    @Bind(R.id.erVehicleWidth)
    TextView erVehicleWidth;
    @Bind(R.id.erFrontOfHood)
    TextView erFrontOfHood;
    @Bind(R.id.erCameraOffset)
    TextView erCameraOffset;
    @Bind(R.id.erCameraHeight)
    TextView erCameraHeight;
    @Bind(R.id.cmVehicleWidth)
    TextView cmVehicleWidth;
    @Bind(R.id.cmFrontOfHood)
    TextView cmFrontOfHood;
    @Bind(R.id.cmCameraOffset)
    TextView cmCameraOffset;
    @Bind(R.id.cmCameraHeight)
    TextView cmCameraHeight;
    @Bind(R.id.vFocus)
    FrameLayout vFocus;
    private Report mReport;
    private VehicleMeasurements mMeasurements;
    private Camera mAdasCamera;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initView() {
        if (getArguments() != null && getArguments().containsKey(Constants.REPORT)) {
            mReport = getArguments().getParcelable(Constants.REPORT);
        }
        if (mMeasurements == null) {
            mMeasurements = new VehicleMeasurements();
            mMeasurements.setMake(mReport.make);
            mMeasurements.setModel(mReport.model);
        }

        tvHeader.setText(getHeaderTextRes());

        int inputType = InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL
                | InputType.TYPE_NUMBER_FLAG_SIGNED;

        etVehicleWidth.setInputType(inputType);
        etVehicleWidth.setGravity(Gravity.END);
        etVehicleWidth.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                etVehicleWidth.setSelected(false);
                String string = s.toString();
                if (!TextUtils.isEmpty(string)) {
                    if (string.contentEquals(".")) {
                        string = "0.";
                        etVehicleWidth.setText(string);
                        etVehicleWidth.setSelection(string.length());
                    }
                    if (!string.contentEquals("-")) {
                        mMeasurements.setVehicleWidth(
                                Double.parseDouble(string));
                    }
                }
                if (TextUtils.isEmpty(etVehicleWidth.getText())
                        || !mMeasurements.isVehicleWidthValid()) {
                    etVehicleWidth.setSelected(true);
                    erVehicleWidth.setVisibility(View.VISIBLE);
                    cmVehicleWidth.setTextColor(getResources().getColor(R.color.yellow));
                } else {
                    erVehicleWidth.setVisibility(View.GONE);
                    cmVehicleWidth.setTextColor(getResources().getColor(R.color.white));
                }
            }
        });

        etCameraHeight.setInputType(inputType);
        etCameraHeight.setGravity(Gravity.END);
        etCameraHeight.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                etCameraHeight.setSelected(false);
                String string = s.toString();
                if (!TextUtils.isEmpty(string)) {
                    if (string.contentEquals(".")) {
                        string = "0.";
                        etCameraHeight.setText(string);
                        etCameraHeight.setSelection(string.length());
                    }
                    if (!string.contentEquals("-")) {
                        mMeasurements.setCameraHeight(
                                Double.parseDouble(string));
                    }
                }
                if (TextUtils.isEmpty(etCameraHeight.getText())
                        || !mMeasurements.isCameraHeightValid()) {
                    etCameraHeight.setSelected(true);
                    erCameraHeight.setVisibility(View.VISIBLE);
                    cmCameraHeight.setTextColor(getResources().getColor(R.color.yellow));
                } else {
                    erCameraHeight.setVisibility(View.GONE);
                    cmCameraHeight.setTextColor(getResources().getColor(R.color.white));
                }
            }
        });

        etFrontOfHood.setInputType(inputType);
        etFrontOfHood.setGravity(Gravity.END);
        etFrontOfHood.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                etFrontOfHood.setSelected(false);
                String string = s.toString();
                if (!TextUtils.isEmpty(string)) {
                    if (string.contentEquals(".")) {
                        string = "0.";
                        etFrontOfHood.setText(string);
                        etFrontOfHood.setSelection(string.length());
                    }
                    if (!string.contentEquals("-")) {
                        mMeasurements.setFrontOfHood(
                                Double.parseDouble(string));
                    }
                }
                if (TextUtils.isEmpty(etFrontOfHood.getText())
                        || !mMeasurements.isFrontOfHoodValid()) {
                    etFrontOfHood.setSelected(true);
                    erFrontOfHood.setVisibility(View.VISIBLE);
                    cmFrontOfHood.setTextColor(getResources().getColor(R.color.yellow));
                } else {
                    erFrontOfHood.setVisibility(View.GONE);
                    cmFrontOfHood.setTextColor(getResources().getColor(R.color.white));
                }
            }
        });

        etCameraOffset.setInputType(inputType);
        etCameraOffset.setGravity(Gravity.END);
        etCameraOffset.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                etCameraOffset.setSelected(false);
                String string = s.toString();
                if (!TextUtils.isEmpty(string)) {
                    if (string.contentEquals(".")) {
                        string = "0.";
                        etCameraOffset.setText(string);
                        etCameraOffset.setSelection(string.length());
                    }
                    if (!string.contentEquals("-")) {
                        mMeasurements.setCameraOffset(
                                Double.parseDouble(string));
                    }
                }
                if (TextUtils.isEmpty(etCameraOffset.getText())
                        || !mMeasurements.isCameraOffsetValid()) {
                    etCameraOffset.setSelected(true);
                    erCameraOffset.setVisibility(View.VISIBLE);
                    cmCameraOffset.setTextColor(getResources().getColor(R.color.yellow));
                } else {
                    erCameraOffset.setVisibility(View.GONE);
                    cmCameraOffset.setTextColor(getResources().getColor(R.color.white));
                }
            }
        });

        rbOffsetLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMeasurements.setOffsetRight(false);
            }
        });
        rbOffsetRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMeasurements.setOffsetRight(true);
            }
        });

        bUseMeasurements.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VehicleMeasurements vehicleMeasurements = getLastMeasurements();
                if (vehicleMeasurements != null) {
                    if (vehicleMeasurements.getMake() != null
                            && vehicleMeasurements.getMake()
                            .contentEquals(mReport.make)
                            && vehicleMeasurements.getModel() != null
                            && vehicleMeasurements.getModel()
                            .contentEquals(mReport.model)) {
                        mMeasurements = vehicleMeasurements;
                        etVehicleWidth.setText(
                                String.valueOf((int) mMeasurements.getVehicleWidth()));
                        etFrontOfHood.setText(String.valueOf((int) mMeasurements.getFrontOfHood()));
                        etCameraOffset.setText(
                                String.valueOf((int) mMeasurements.getCameraOffset()));
                        etCameraHeight.setText(
                                String.valueOf((int) mMeasurements.getCameraHeight()));
                        rbOffsetRight.setChecked(mMeasurements.isOffsetRight());
                        rbOffsetLeft.setChecked(!mMeasurements.isOffsetRight());
                    } else {
                        if (getContext() != null) {
                            Dialogs.showToast(getContext(), getContext().getString(R.string.error),
                                              getContext().getString(
                                                      R.string.measurements_not_match));
                        }
                    }
                } else {
                    if (getContext() != null) {
                        Dialogs.showToast(getContext(), getContext().getString(R.string.error),
                                          getContext().getString(
                                                  R.string.measurements_not_available));
                    }
                }
            }
        });

        vRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAdas();
            }
        });

        spFieldOfView.init(Arrays.asList(getResources().getStringArray(R.array.fov_adas)),
                           R.string.field_of_view, false);

        checkAdas();

        bCalibrate1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkMeasurements()) {
                    Intent intent = new Intent(getActivity(), AdasCalibrationActivity.class);
                    intent.putExtra(STEP,
                                    BleConstants.DistanceCaptured.DISTANCE_2_0_METERS);
                    intent.putExtra(AdasCalibrationActivity.MEASUREMENTS, mMeasurements);
                    startActivityForResult(intent, CALIB_REQ_CODE);
                }
            }
        });
        bCalibrate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkMeasurements()) {
                    if (tvCalibrated1.getVisibility() == View.GONE) {
                        Dialogs.showToast(getActivity(), getContext().getString(R.string.error),
                                          getContext().getString(R.string.calibrate_previous));
                    } else {
                        Intent intent = new Intent(getActivity(), AdasCalibrationActivity.class);
                        intent.putExtra(STEP,
                                        BleConstants.DistanceCaptured.DISTANCE_2_5_METERS);
                        intent.putExtra(AdasCalibrationActivity.MEASUREMENTS, mMeasurements);
                        startActivityForResult(intent, CALIB_REQ_CODE);
                    }
                }
            }
        });
        bCalibrate3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkMeasurements()) {
                    if (tvCalibrated2.getVisibility() == View.GONE) {
                        Dialogs.showToast(getActivity(), getContext().getString(R.string.error),
                                          getContext().getString(R.string.calibrate_previous));
                    } else {
                        Intent intent = new Intent(getActivity(), AdasCalibrationActivity.class);
                        intent.putExtra(STEP,
                                        BleConstants.DistanceCaptured.DISTANCE_3_5_METERS);
                        intent.putExtra(AdasCalibrationActivity.MEASUREMENTS, mMeasurements);
                        startActivityForResult(intent, CALIB_REQ_CODE);
                    }
                }
            }
        });

        erVehicleWidth.setText(getString(R.string.error_vehicle_measurements,
                                         (int) VehicleMeasurements.MIN_VEHICLE_WIDTH,
                                         (int) VehicleMeasurements.MAX_VEHICLE_WIDTH));
        erCameraHeight.setText(getString(R.string.error_vehicle_measurements,
                                         (int) VehicleMeasurements.MIN_CAMERA_HEIGHT,
                                         (int) VehicleMeasurements.MAX_CAMERA_HEIGHT));
        erCameraOffset.setText(getString(R.string.error_vehicle_measurements,
                                         (int) VehicleMeasurements.MIN_CAMERA_OFFSET,
                                         (int) VehicleMeasurements.MAX_CAMERA_OFFSET));
        erFrontOfHood.setText(getString(R.string.error_vehicle_measurements,
                                        (int) VehicleMeasurements.MIN_FRONT_OF_HOOD,
                                        (int) VehicleMeasurements.MAX_FRONT_OF_HOOD));

        cbAgreement.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                vAgreement.setSelected(isChecked);
                mReport.adasTermsAccepted(isChecked);
                cbAgreement.setText(isChecked
                                    ? R.string.accepted_terms
                                    : R.string.accept_terms);
            }
        });

        vAgreement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cbAgreement.setChecked(!cbAgreement.isChecked());
            }
        });

        ivHelpCameraHeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "help screen overlay", Toast.LENGTH_SHORT)
                        .show();
            }
        });

        vFocus.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    for (EditText et :  Utils.getAllEditTexts(vRootContainer)) {
                        if (et.isFocused()) {
                            Rect outRect = new Rect();
                            et.getGlobalVisibleRect(outRect);
                            if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                                et.clearFocus();
                                InputMethodManager imm = (InputMethodManager) v.getContext()
                                        .getSystemService(
                                                Context.INPUT_METHOD_SERVICE);
                                if (imm != null) {
                                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                                }
                            }
                        }
                    }
                }
                return false;
            }
        });
    }

    @Override
    protected void updateReport(Report report) {
        saveLastMeasurements();
        mAdasCamera.notes = etNotes.getText()
                .toString();
        mAdasCamera.fieldOfView = spFieldOfView.getSelectedItem();
        report.adas = mAdasCamera;
    }

    @Override
    protected boolean areFieldsValid() {
        boolean validity = tvCalibrated1.getVisibility() == View.VISIBLE
                && tvCalibrated2.getVisibility() == View.VISIBLE
                && tvCalibrated3.getVisibility() == View.VISIBLE;
        if (!validity) {
            Dialogs.showToast(getContext(), getResources().getString(R.string.error),
                              getResources().getString(R.string.error_calibration));
        } else {
            sendAdasStats();
        }
        return validity;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CALIB_REQ_CODE) {
            if (resultCode == RESULT_OK) {
                int returnValue = data.getIntExtra(STEP, -1);
                if (returnValue != -1) {
                    proceedStepFinish(BleConstants.DistanceCaptured.values()[returnValue]);
                }
            } else if (resultCode == RESULT_CANCELED) {
                if (data != null && data.hasExtra(ERROR) && data.getBooleanExtra(ERROR, false)) {
                    proceedStepFinish(null);
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mReport != null) {
            cbAgreement.setChecked(mReport.adasTerms);
            if (mReport.calibration1) {
                proceedStepFinish(BleConstants.DistanceCaptured.DISTANCE_2_0_METERS);
            }
            if (mReport.calibration2) {
                proceedStepFinish(BleConstants.DistanceCaptured.DISTANCE_2_5_METERS);
            }
            if (mReport.calibration3) {
                proceedStepFinish(BleConstants.DistanceCaptured.DISTANCE_3_5_METERS);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAdasCheckResult(GetCameraInfoEvent event) {
        Log.d("ADAS SERIAL", event.getSerialNumber());

        vWrongPort.setVisibility(event.getCameraType() == BleConstants.CameraType.TYPE_ADAS
                                 ? View.GONE
                                 : View.VISIBLE);
        tvState.setText(event.isConnected()
                        ? R.string.connected
                        : R.string.disconnected);
        ivState.setImageResource(event.isConnected()
                              ? R.drawable.ic_green_circle
                              : R.drawable.ic_gray_circle);

        tvCameraSerial.setVisibility(event.isConnected()
                                     ? View.VISIBLE
                                     : View.GONE);

        tvCameraSerial.setText(event.getSerialNumber());
        vContainer.setVisibility(event.isConnected()
                                 ? View.VISIBLE
                                 : View.GONE);
        hideProgress();
    }

    private int getHeaderTextRes() {
        return R.string.adas_camera;
    }

    private boolean checkMeasurements() {
        boolean validity = true;
        if (tvState != null && tvState.getText()
                .toString()
                .contentEquals(getString(R.string.connected))) {
            if (TextUtils.isEmpty(etVehicleWidth.getText())
                    || !mMeasurements.isVehicleWidthValid()) {
                validity = false;
                etVehicleWidth.setSelected(true);
                erVehicleWidth.setVisibility(View.VISIBLE);
                cmVehicleWidth.setTextColor(getResources().getColor(R.color.yellow));
            } else {
                erVehicleWidth.setVisibility(View.GONE);
                cmVehicleWidth.setTextColor(getResources().getColor(R.color.white));
            }
            if (TextUtils.isEmpty(etFrontOfHood.getText())
                    || !mMeasurements.isFrontOfHoodValid()) {
                validity = false;
                etFrontOfHood.setSelected(true);
                erFrontOfHood.setVisibility(View.VISIBLE);
                cmFrontOfHood.setTextColor(getResources().getColor(R.color.yellow));
            } else {
                erFrontOfHood.setVisibility(View.GONE);
                cmFrontOfHood.setTextColor(getResources().getColor(R.color.white));
            }
            if (TextUtils.isEmpty(etCameraOffset.getText())
                    || !mMeasurements.isCameraOffsetValid()) {
                validity = false;
                etCameraOffset.setSelected(true);
                erCameraOffset.setVisibility(View.VISIBLE);
                cmCameraOffset.setTextColor(getResources().getColor(R.color.yellow));
            } else {
                erCameraOffset.setVisibility(View.GONE);
                cmCameraOffset.setTextColor(getResources().getColor(R.color.white));
            }
            if (TextUtils.isEmpty(etCameraHeight.getText())
                    || !mMeasurements.isCameraHeightValid()) {
                validity = false;
                etCameraHeight.setSelected(true);
                erCameraHeight.setVisibility(View.VISIBLE);
                cmCameraHeight.setTextColor(getResources().getColor(R.color.yellow));
            } else {
                erCameraHeight.setVisibility(View.GONE);
                cmCameraHeight.setTextColor(getResources().getColor(R.color.white));
            }
        }
        return validity;
    }

    private void proceedStepFinish(BleConstants.DistanceCaptured step) {
        if (step != null) {
            switch (step) {
                case DISTANCE_2_0_METERS:
                    mReport.imageOneCalibrated(true);
                    bCalibrate1.setVisibility(View.GONE);
                    tvCalibrated1.setVisibility(View.VISIBLE);
                    break;
                case DISTANCE_2_5_METERS:
                    mReport.imageTwoCalibrated(true);
                    bCalibrate2.setVisibility(View.GONE);
                    tvCalibrated2.setVisibility(View.VISIBLE);
                    break;
                case DISTANCE_3_5_METERS:
                    mReport.imageThreeCalibrated(true);
                    bCalibrate3.setVisibility(View.GONE);
                    tvCalibrated3.setVisibility(View.VISIBLE);
                    break;
            }
        }
    }

    private void checkAdas() {
        showProgress(R.string.rescanning_for_cameras);

        if (mReport == null || mReport.adas == null) {
            List<Camera> cameras = CsvParser.parseCameras(Constants.ADAS_CAMERA_SCREEN);
            if (cameras != null && !cameras.isEmpty()) {
                mAdasCamera = cameras.get(0);
            }
        } else {
            mAdasCamera = mReport.adas;
        }

        tvCameraName.setText(mAdasCamera.name);
        etNotes.setText(mAdasCamera.notes);
        spFieldOfView.setSelectedItem(mAdasCamera.fieldOfView);

        bleManager.getCameraInfo(BleConstants.CameraInfo.byValue((byte) mAdasCamera.cameraId));
    }

    private void saveLastMeasurements() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
                getActivity());
        if (mMeasurements != null) {
            sharedPreferences.edit()
                    .putString(LAST_MEASUREMENTS, new Gson().toJson(mMeasurements))
                    .apply();
        }

    }

    private VehicleMeasurements getLastMeasurements() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
                getActivity());
        String json = sharedPreferences.getString(LAST_MEASUREMENTS, null);
        if (json != null) {
            return new Gson().fromJson(json, VehicleMeasurements.class);
        } else {
            return null;
        }
    }

    private void sendAdasStats() {
        bleManager.sendAdasStats(XmlHelper.generateAdasReportXML(mMeasurements));
    }
}
