package com.smartdrive.technician.managers;

import android.util.Log;

import com.smartdrive.technician.events.ReportSentEvent;
import com.smartdrive.technician.models.DebugInfoItem;
import com.smartdrive.technician.models.report.Report;
import com.smartdrive.technician.utils.XmlHelper;

import org.greenrobot.eventbus.EventBus;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPOutputStream;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

import static com.smartdrive.technician.Constants.CACHE_CONTROL_KEY;
import static com.smartdrive.technician.Constants.CACHE_CONTROL_VALUE;
import static com.smartdrive.technician.Constants.CONTENT_TYPE_APPLICATION_XML;
import static com.smartdrive.technician.Constants.SERVER_URL;

public class ReportSendingManager {

    private static ReportSendingManager instance;
    private static final String TAG = XmlHelper.class.getName();
    private ReportSendingManager() {
    }

    public static ReportSendingManager getInstance() {
        if (instance == null) {
            synchronized (ReportSendingManager.class) {
                if (instance == null) {
                    instance = new ReportSendingManager();
                }
            }
        }
        return instance;
    }

    public void send(Report report) {
        List<Report> reports = new ArrayList<>();
        reports.add(report);
        send(reports);
    }

    public void send(final List<Report> reports) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
                List<DebugInfoItem> debugInfo = BleManager.getInstance().getDebugInfo();
                for (Report report : reports) {
                    try {
                        String xml = XmlHelper.serializeReport(report, debugInfo);
                        byte[] gzipByteArray = compress(xml);
                        //String gzipBase64 = Base64.encodeToString(gzipByteArray, Base64.DEFAULT);
                        RequestBody body = RequestBody.create(MediaType.parse(
                                CONTENT_TYPE_APPLICATION_XML), gzipByteArray);
                        Request request = new Request.Builder()
                                .url(SERVER_URL)
                                .post(body)
                                .addHeader("Content-Encoding", "gzip")
                                //.addHeader(AUTH_HASH_KEY, AUTH_HASH_VALUE)
                                .addHeader(CACHE_CONTROL_KEY, CACHE_CONTROL_VALUE)
                                .build();
                        Response response = client.newCall(request).execute();
						Log.e(TAG, "Http report sending return code " + response.code());
                        if (response.isSuccessful()) {
                            onReportsSent(report.getId(), true);
                        } else {
                            onReportsSent(report.getId(), false);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Http report sending exception");
                        e.printStackTrace();
                        onReportsSent(report.getId(), false);
                    }
                }
            }
        }).start();
    }

    private static byte[] compress(String string) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        GZIPOutputStream gos = new GZIPOutputStream(os);
        gos.write(string.getBytes("UTF-8"));
        gos.close();
        byte[] compressed = os.toByteArray();
        os.close();
        return compressed;
    }

    private void onReportsSent(Long reportId, boolean isSuccessfully) {
        EventBus.getDefault().post(new ReportSentEvent(reportId, isSuccessfully));
    }

}