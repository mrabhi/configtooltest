package com.smartdrive.technician.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smartdrive.technician.R;
import com.smartdrive.technician.models.DebugInfoItem;

import butterknife.Bind;

public class DebugInfoAdapter extends BaseAdapter<DebugInfoItem> {

    protected class DebugInfoHolder extends BaseViewHolder {
        @Bind(R.id.tvName)
        TextView tvName;
        @Bind(R.id.tvDetails)
        TextView tvDetails;

        private DebugInfoHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void bind(final int position) {
            DebugInfoItem item = getItem(position);
            tvName.setText(item.name);
            tvDetails.setText(item.details);
            itemView.setPadding(0, position == 0 ? topMargin : 0, 0, 0);
        }
    }

    private int topMargin;

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        topMargin = (int) context.getResources().getDimension(R.dimen.default_padding);
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_simple_list, parent, false);
        return new DebugInfoHolder(itemView);
    }

}