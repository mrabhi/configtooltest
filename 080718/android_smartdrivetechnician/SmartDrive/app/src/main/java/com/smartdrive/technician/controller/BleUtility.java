package com.smartdrive.technician.controller;

import android.support.annotation.Nullable;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by mikeho on 6/13/16.
 */
public class BleUtility {
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String getHexStringFromBytes(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 3];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 3] = hexArray[v >>> 4];
            hexChars[j * 3 + 1] = hexArray[v & 0x0F];
            hexChars[j * 3 + 2] = ' ';
        }
        return new String(hexChars);
    }

    public static byte[] getBytesFromUuid(UUID uuid) {
        ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.putLong(uuid.getMostSignificantBits());
        bb.putLong(uuid.getLeastSignificantBits());

        return bb.array();
    }

    public static int getIntegerFromByte(byte aByte) {
        int unsignedByte = aByte & 0xFF;
        return unsignedByte;
    }

    public static UUID getUuidFromBytes(byte[] bytes) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
        Long high = byteBuffer.getLong();
        Long low = byteBuffer.getLong();

        return new UUID(high, low);
    }

    public static byte calculateChecksum(byte[] payload, int length) {
        int checksum = 0;
        for (int index = 0 ; index < length; index++) {
            checksum += payload[index];
        }

        return (byte) (checksum % 256);
    }

    public static byte[] gzCompress(final byte[] input) throws IOException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        GZIPOutputStream gzipper = new GZIPOutputStream(bout);

        gzipper.write(input, 0, input.length);
        gzipper.close();

        return bout.toByteArray();
    }

    public static byte[] gzDecompress(byte[] bytes) throws IOException {
        GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(bytes));

        byte[] buffer = new byte[1024];
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        int len;
        while ((len = gis.read(buffer)) > 0) {
            out.write(buffer, 0, len);
        }

        gis.close();
        out.close();
        return out.toByteArray();
    }

    public static byte[] getBytesFromByteArrayList(ArrayList<Byte> byteArrayList) {
        int size = byteArrayList.size();
        byte toReturn[] = new byte[size];
        for (int index = 0 ; index < size ; index++) {
            toReturn[index] = byteArrayList.get(index);
        }

        return toReturn;
    }

    /**
     * Follows the definition of the EIR record for advertisementData in
     * section 8.12 in the Core Bluetooth Specification(page 1761 of 2302 in the Core_V4.0.pdf)
     *
     * @param advertisementData byte[]
     * @return UUID
     */
    @Nullable
    public static UUID parseUuidFromAdvertisementData(byte[] advertisementData) {
        for (int index = 0; index < advertisementData.length; index++) {
            int length = advertisementData[index];

            if (length > 0) {
                switch (advertisementData[index + 1]) {
                    case 0x07:
                        if (length == 17) {
                            byte[] uuidBytes = new byte[16];
                            for (int offset = 0; offset < 16; offset++) {
                                uuidBytes[15 - offset] = advertisementData[index + offset + 2];
                            }

                            return getUuidFromBytes(uuidBytes);
                        }
                        break;
                }

                index += length;
            }
        }

        // If we are here, we do not have a UUID
        return null;
    }

    public static int byteArrayToInt(byte[] data){
        int size = 0;
        size += BleUtility.getIntegerFromByte(data[0]) * 256 * 256 * 256;
        size += BleUtility.getIntegerFromByte(data[1]) * 256 * 256;
        size += BleUtility.getIntegerFromByte(data[2]) * 256;
        size += BleUtility.getIntegerFromByte(data[3]);
        return size;
    }

    public static byte[] intToByteArray(int i){
        byte[] result = new byte[4];

        result[0] = (byte) (i >> 24);
        result[1] = (byte) (i >> 16);
        result[2] = (byte) (i >> 8);
        result[3] = (byte) (i /*>> 0*/);

        return result;
    }

    public static String getCameraInfoSerialNumber(byte[] data){
        //remove first 2 index from data array
        byte[] serialByteArray = Arrays.copyOfRange(data, 2, data.length);
        String serialNumber = "";

        try {
            serialNumber = new String(serialByteArray, "UTF-8");
        } catch(UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return serialNumber;
    }
}
