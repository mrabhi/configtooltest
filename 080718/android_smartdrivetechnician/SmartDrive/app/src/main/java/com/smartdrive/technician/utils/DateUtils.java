package com.smartdrive.technician.utils;

import android.text.format.DateFormat;

import com.smartdrive.technician.SmartDriveApp;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public abstract class DateUtils {

    public static final String TIME_FORMAT_12 = "MM/dd/yyyy h:mm:ss a z";
    public static final String TIME_FORMAT_24 = "MM/dd/yyyy HH:mm:ss z";
    private static final String TIME_FROM_FORMAT_12 = "MM/dd/yyyy h:mm a z";
    private static final String TIME_FROM_FORMAT_24 = "MM/dd/yyyy HH:mm z";
    private static final String TIME_TO_FORMAT_12 = "h:mm a z";
    private static final String TIME_TO_FORMAT_24 = "HH:mm z";
    public static final String PENDING_REPORT_TIME_FORMAT_12 = "MM/dd/yyyy h:mm a";
    public static final String PENDING_REPORT_TIME_FORMAT_24 = "MM/dd/yyyy HH:mm";

    public static final String REPORT_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String EVENT_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";

    private static final long DAY_IN_MILLIS = 86400000;

    public static String longToString(long source, String format) {
        return new SimpleDateFormat(format, Locale.getDefault()).format(new Date(source));
    }

    private static String calendarToString(Calendar source, String format) {
        return new SimpleDateFormat(format, Locale.getDefault()).format(source.getTime());
    }

    public static Calendar stringToCalendar(String source, String format) {
        Calendar result = Calendar.getInstance();
        try {
            result.setTime(new SimpleDateFormat(format, Locale.getDefault()).parse(source));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String timeIntervalToString(Calendar fromDate, Calendar toDate) {
        String from = calendarToString(fromDate, is24HourFormat() ? TIME_FROM_FORMAT_24 : TIME_FROM_FORMAT_12);
        String to = calendarToString(toDate, is24HourFormat() ? TIME_TO_FORMAT_24 : TIME_TO_FORMAT_12);
        return from + " - " + to;
    }

    public static boolean isTimeDayOld(long time1, long time2) {
        long delta = time1 - time2;
        return delta > DAY_IN_MILLIS;
    }

    public static boolean is24HourFormat() {
        return DateFormat.is24HourFormat(SmartDriveApp.getContext());
    }

}