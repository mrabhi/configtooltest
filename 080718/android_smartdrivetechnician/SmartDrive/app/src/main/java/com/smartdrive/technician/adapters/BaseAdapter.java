package com.smartdrive.technician.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public abstract class BaseAdapter<T> extends RecyclerView.Adapter<BaseAdapter.BaseViewHolder> {

    public interface OnItemClickListener {
        void onClick(View view, int position);
    }

    protected abstract static class BaseViewHolder extends RecyclerView.ViewHolder {
        protected BaseViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected abstract void bind(int position);

        protected void setListeners(final int position,
                                    final OnItemClickListener onItemClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onClick(itemView, position);
                    }
                }
            });
        }
    }

    protected List<T> list;
    private OnItemClickListener onItemClickListener;

    public BaseAdapter() {
        list = new ArrayList<>();
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.bind(position);
        holder.setListeners(position, onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public T getItem(int position) {
        return list.get(position);
    }

    public void update(List<T> list) {
        this.list.clear();
        addAll(list);
    }

    public void updateItem(T item, int position) {
        list.set(position, item);
        notifyItemChanged(position);
    }

    public List<T> getList() {
        return list;
    }

    private void addAll(List<T> list) {
        if (list != null && !list.isEmpty()) {
            this.list.addAll(list);
        }
        notifyDataSetChanged();
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }

    public void add(T item) {
        list.add(item);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        list.remove(position);
        notifyDataSetChanged();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

}