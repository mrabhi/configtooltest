package com.smartdrive.technician.events.ble;

import com.smartdrive.technician.models.SelfCheck;

public class SelfCheckGeneratedEvent {

    public final SelfCheck selfCheck;

    public SelfCheckGeneratedEvent(SelfCheck selfCheck) {
        this.selfCheck = selfCheck;
    }

}