package com.smartdrive.technician.models.report;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "CommunicationSettings")
public class CommunicationSettings extends Model implements Parcelable {

    public static final Parcelable.Creator<CommunicationSettings> CREATOR = new Parcelable.Creator<CommunicationSettings>() {
        @Override
        public CommunicationSettings createFromParcel(Parcel source) {
            return new CommunicationSettings(source);
        }

        @Override
        public CommunicationSettings[] newArray(int size) {
            return new CommunicationSettings[size];
        }
    };

    public enum Type {
        CELLULAR,
        WIFI,
        ETHERNET;

        @Override
        public String toString() {
            return name().toLowerCase();
        }
    }

    @Column
    public Type type;
    @Column(onUpdate = Column.ForeignKeyAction.CASCADE, onDelete = Column.ForeignKeyAction.CASCADE)
    public Cellular cellular;
    @Column(onUpdate = Column.ForeignKeyAction.CASCADE, onDelete = Column.ForeignKeyAction.CASCADE)
    public WifiSecurity wifiSecurity;
    @Column(onUpdate = Column.ForeignKeyAction.CASCADE, onDelete = Column.ForeignKeyAction.CASCADE)
    public CommunicationProtocol protocol;
    @Column
    public String signalStrength;
    @Column
    public String ping;

    public CommunicationSettings() {
    }

    protected CommunicationSettings(Parcel in) {
        this.type = Type.values()[in.readInt()];
        this.cellular = in.readParcelable(Cellular.class.getClassLoader());
        this.wifiSecurity = in.readParcelable(WifiSecurity.class.getClassLoader());
        this.protocol = in.readParcelable(CommunicationProtocol.class.getClassLoader());
        this.signalStrength = in.readString();
        this.ping = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type.ordinal());
        dest.writeParcelable(this.cellular, flags);
        dest.writeParcelable(this.wifiSecurity, flags);
        dest.writeParcelable(this.protocol, flags);
        dest.writeString(this.signalStrength);
        dest.writeString(this.ping);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommunicationSettings settings = (CommunicationSettings) o;
        return type == settings.type
                && ((cellular != null ? cellular.equals(settings.cellular) : settings.cellular == null)
                && ((wifiSecurity != null ? wifiSecurity.equals(settings.wifiSecurity) : settings.wifiSecurity == null)
                && ((protocol != null ? protocol.equals(settings.protocol) : settings.protocol == null))));
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + (cellular != null ? cellular.hashCode() : 0);
        result = 31 * result + (wifiSecurity != null ? wifiSecurity.hashCode() : 0);
        result = 31 * result + (protocol != null ? protocol.hashCode() : 0);
        return result;
    }

    public void saveToDB() {
        if (cellular != null) {
            cellular.save();
        }
        if (wifiSecurity != null) {
            wifiSecurity.save();
        }
        if (protocol != null) {
            protocol.save();
        }
        save();
    }

    public void deleteFromDB() {
        delete();
        if (cellular != null) {
            cellular.delete();
        }
        if (wifiSecurity != null) {
            wifiSecurity.delete();
        }
        if (protocol != null) {
            protocol.delete();
        }
    }

}