package com.smartdrive.technician.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.controller.BleConstants;
import com.smartdrive.technician.events.ble.BleErrorEvent;
import com.smartdrive.technician.events.ble.LoggedOutEvent;
import com.smartdrive.technician.fragments.BaseFragment;
import com.smartdrive.technician.fragments.installation.calibration.InstructionsFragment;
import com.smartdrive.technician.managers.BleManager;
import com.smartdrive.technician.managers.CalibrationManager;
import com.smartdrive.technician.models.VehicleMeasurements;
import com.smartdrive.technician.utils.Dialogs;
import com.smartdrive.technician.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static com.smartdrive.technician.Constants.PACKAGE;
import static com.smartdrive.technician.Constants.RC_ENABLE_BLE_WITH_RECONNECT;
import static com.smartdrive.technician.Constants.RC_STORAGE;

@Layout(id = R.layout.activity_adas_calibration)
public class AdasCalibrationActivity extends AppCompatActivity {
    public static final String STEP = "calibrate_step";
    public static final String ERROR = "error";
    public static final String MEASUREMENTS = "measurements";

    public CalibrationManager mCalibrationManager;
    protected BleManager bleManager = BleManager.getInstance();

    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.activityProgress)
    View activityProgress;
    @Bind(R.id.fullscreenProgress)
    View fullscreenProgress;
    @Bind(R.id.vContainer)
    View activityContent;

    private BleConstants.DistanceCaptured mStep;
    private VehicleMeasurements mMeasurements;
    private Bitmap mBitmap;
    private String mBitmapPath;
    private Rect mRect;
    private Dialog disconnectDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adas_calibration);
        ButterKnife.bind(this);
        EventBus.getDefault()
                .register(this);

        Intent intent = getIntent();
        if (intent.hasExtra(STEP)) {
            mStep = (BleConstants.DistanceCaptured) intent.getSerializableExtra(STEP);
        }
        if (intent.hasExtra(MEASUREMENTS)) {
            mMeasurements = intent.getParcelableExtra(MEASUREMENTS);

            if (mMeasurements != null) {
                mCalibrationManager = CalibrationManager.getInstance(mMeasurements);
            }

        }
        initTitle();

        isStoragePermissionGranted();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
        EventBus.getDefault()
                .unregister(this);
        mBitmap = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openFragment(new InstructionsFragment());
        } else {
            Dialog dialog = Dialogs.showAlert(this, getString(R.string.attention),
                                              getString(R.string.required_storage_permission), null,
                                              R.string.cancel,
                                              R.string.open_app_settings, true,
                                              new View.OnClickListener() {
                                                  @Override
                                                  public void onClick(View v) {
                                                      Intent intent = new Intent(
                                                              Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                                      intent.setData(Uri.parse(
                                                              String.format(PACKAGE,
                                                                            getPackageName())));
                                                      startActivityForResult(intent, RC_STORAGE);
                                                  }
                                              }
            );
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    finish();
                }
            });
        }
    }

    public Rect getRect() {
        return mRect;
    }

    public void setRect(Rect rect) {
        mRect = rect;
    }

    public CalibrationManager getCalibrationManager() {
        return mCalibrationManager;
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public void saveBitmap(byte[] bitmapBytes) {
        mBitmapPath = Utils.writeToFile(bitmapBytes);
        mBitmap = BitmapFactory.decodeFile(mBitmapPath);
    }


    public String getBitmapPath() {
        return mBitmapPath;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(BleErrorEvent event) {
        Dialogs.showToast(this, getString(R.string.error), event.message);
    }

    public void openFragment(BaseFragment fragment) {
        String fragmentClassName = fragment.getClass()
                .getName();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.vContainer, fragment, fragmentClassName)
                .addToBackStack(fragmentClassName)
                .commit();
    }

    @OnClick(R.id.ivCancelCalibration)
    public void onCancel() {
        setResult(RESULT_CANCELED);
        finish();
    }

    public void finishCalibration() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(STEP, mStep.ordinal());
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    public double getH1() {
        if (mMeasurements != null) {
            return mMeasurements.getH1();
        } else {
            return 0;
        }
    }

    public BleConstants.DistanceCaptured getStep() {
        return mStep;
    }

    @SuppressLint("StringFormatMatches")
    public String getStepString() {
        switch (mStep) {
            case DISTANCE_2_0_METERS:
                return getResources().getString(R.string.meters, formatDouble(2f));
            case DISTANCE_2_5_METERS:
                return getResources().getString(R.string.meters, formatDouble(2.5));
            case DISTANCE_3_5_METERS:
                return getResources().getString(R.string.meters, formatDouble(3.5));
            default:
                return null;
        }
    }

    @SuppressLint("StringFormatMatches")
    public int getStepImage() {
        switch (mStep) {
            case DISTANCE_2_0_METERS:
                return R.drawable.content_assert_10_20;
            case DISTANCE_2_5_METERS:
                return R.drawable.content_assert_10_25;
            case DISTANCE_3_5_METERS:
                return R.drawable.content_assert_10_35;
            default:
                return 0;
        }
    }

    public void showProgress(@StringRes int textRes) {
        if (activityProgress == null || activityContent == null) {
            return;
        }
        ((TextView) activityProgress.findViewById(R.id.tvProgress)).setText(textRes);
        activityProgress.setVisibility(View.VISIBLE);
        activityContent.setVisibility(View.GONE);
    }

    public void hideProgress() {
        if (activityProgress == null || activityContent == null) {
            return;
        }
        activityProgress.setVisibility(View.GONE);
        activityContent.setVisibility(View.VISIBLE);
    }

    public void isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                openFragment(new InstructionsFragment());
            } else {
                ActivityCompat.requestPermissions(this,
                                                  new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                                  1);
            }
        } else {
            openFragment(new InstructionsFragment());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(LoggedOutEvent event) {
        if (isFinishing() || isDestroyed()) {
            return;
        }
        if (disconnectDialog != null && disconnectDialog.isShowing()) {
            Dialogs.hideProgress(disconnectDialog);
        } else {
            disconnectDialog = Dialogs.showAlert(this, getString(R.string.error),
                                                 getString(R.string.connection_interrupted),
                                                 null, R.string.no, R.string.yes, false,
                                                 new View.OnClickListener() {
                                                     @Override
                                                     public void onClick(View v) {
                                                         reconnect();
                                                     }
                                                 });
            disconnectDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    startConnectionActivity();
                }
            });
        }
    }

    private void initTitle() {
        switch (mStep) {
            case DISTANCE_2_0_METERS:
                tvTitle.setText(R.string.calibration_1);
                break;
            case DISTANCE_2_5_METERS:
                tvTitle.setText(R.string.calibration_2);
                break;
            case DISTANCE_3_5_METERS:
                tvTitle.setText(R.string.calibration_3);
                break;
            default:
                break;
        }
    }

    @SuppressLint("DefaultLocale")
    private String formatDouble(double d) {
        if (d == (long) d) {
            return String.format("%d", (long) d);
        } else {
            return String.format("%s", d);
        }
    }

    private void reconnect() {
        if (bleManager.isBluetoothEnabled()) {
            Dialogs.showProgress(disconnectDialog, R.string.connection_in_progress);
            bleManager.reconnect();
        } else {
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),
                                   RC_ENABLE_BLE_WITH_RECONNECT);
        }
    }

    private void startConnectionActivity() {
        finish();
        Intent intent = new Intent(AdasCalibrationActivity.this, ConnectionActivity.class);
        intent.addFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
