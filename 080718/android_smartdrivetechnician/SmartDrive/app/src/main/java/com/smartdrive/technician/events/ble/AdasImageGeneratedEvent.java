package com.smartdrive.technician.events.ble;

public class AdasImageGeneratedEvent {
    private byte[] mBytes;

    public AdasImageGeneratedEvent(byte[] bytes) {
        this.mBytes = bytes;
    }

    public byte[] getBytes() {
        return mBytes;
    }
}
