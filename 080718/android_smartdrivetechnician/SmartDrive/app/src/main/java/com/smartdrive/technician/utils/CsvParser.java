package com.smartdrive.technician.utils;

import com.smartdrive.technician.SmartDriveApp;
import com.smartdrive.technician.models.report.Camera;
import com.smartdrive.technician.models.report.Cellular;
import com.smartdrive.technician.models.report.InterchangeItem;
import com.opencsv.CSVReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public abstract class CsvParser {

    private static List<String[]> parseFile(String fileName) throws IOException {
        return new CSVReader(new InputStreamReader(SmartDriveApp.getContext().getAssets().open(fileName))).readAll();
    }

    private static List<String> parseFileToStringList(String fileName) {
        List<String> list = new ArrayList<>();
        try {
            List<String[]> rows = parseFile(fileName);
            for (int i = 0; i < rows.size(); i++) {
                list.add(rows.get(i)[0].trim());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static List<Cellular> parseCellulars() {
        List<Cellular> list = new ArrayList<>();
        try {
            List<String[]> rows = parseFile("apn.csv");
            for (int i = 1; i < rows.size(); i++) {
                String[] row = rows.get(i);
                Cellular cellular = new Cellular();
                cellular.country = row[0].trim();
                cellular.carrier = row[1].trim();
                cellular.apn = row[2].trim();
                list.add(cellular);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static List<Camera> parseCameras(int screen) {
        List<Camera> list = new ArrayList<>();
        try {
            List<String[]> rows = parseFile("camera.csv");
            for (int i = 1; i < rows.size(); i++) {
                String[] row = rows.get(i);
                if (screen == Integer.parseInt(row[0])) {
                    Camera camera = new Camera();
                    camera.type = Camera.Type.valueOf(row[1].toUpperCase());
                    camera.name = row[2];
                    camera.cameraId = Integer.parseInt(row[3]);
                    list.add(camera);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static List<String> parseCameraMountingLocations() {
        return parseFileToStringList("camera_mounting_location.csv");
    }

    public static List<String> parseControllerMountingLocations() {
        return parseFileToStringList("controller_mounting_location.csv");
    }

    public static List<String> parseEncryption() {
        return parseFileToStringList("encryption.csv");
    }

    public static List<String> parseFieldOfView() {
        return parseFileToStringList("field_of_view.csv");
    }

    public static List<String> parseGpsMountingLocation() {
        return parseFileToStringList("gps_mounting_location.csv");
    }

    public static List<String> parseKeypadMountingLocation() {
        return parseFileToStringList("keypad_mounting_location.csv");
    }

    public static List<String> parseRemotePushButtonMountingLocation() {
        return parseFileToStringList("remote_push_button_mounting_location.csv");
    }

    public static List<String> parseSensorBarMountingLocations() {
        return parseFileToStringList("sensor_bar_mounting_location.csv");
    }

    public static List<String> parseVehicleTypes() {
        return parseFileToStringList("vehicle_types.csv");
    }

    public static List<InterchangeItem> parseInterchange() {
        List<InterchangeItem> list = new ArrayList<>();
        List<String> names = parseFileToStringList("interchange.csv");
        for (String name : names) {
            list.add(new InterchangeItem(name));
        }
        return list;
    }

}