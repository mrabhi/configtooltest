package com.smartdrive.technician.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartdrive.technician.EventsSubscriber;
import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.SmartDriveApp;
import com.smartdrive.technician.activities.MainActivity;
import com.smartdrive.technician.adapters.AdasCheckAdapter;
import com.smartdrive.technician.events.ble.AdasCheckGeneratedEvent;
import com.smartdrive.technician.events.ble.CurrentTimeGotEvent;
import com.smartdrive.technician.models.AdasCheck;
import com.smartdrive.technician.models.report.Report;
import com.smartdrive.technician.utils.DateUtils;
import com.smartdrive.technician.utils.Dialogs;
import com.smartdrive.technician.widgets.BaseRecyclerView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;

import static com.smartdrive.technician.Constants.REPORT;
import static com.smartdrive.technician.Constants.SERIAL_NUMBER;
import static com.smartdrive.technician.Constants.TOOLBAR_TITLE;

@EventsSubscriber
@Layout(id = R.layout.fragment_adas_calibration_status)
public class AdasCalibrationStatusFragment extends BaseFragment {

    @Bind(R.id.conditionContainer)
    View conditionContainer;
    @Bind(R.id.tvCondition)
    TextView tvCondition;
    @Bind(R.id.tvLastRun)
    TextView tvLastRun;
    @Bind(R.id.rvSelfCheck)
    BaseRecyclerView rvSelfCheck;

    private AdasCheckAdapter adapter;
    private long lastRunTime;
    private boolean isInstallationMode;

    @Override
    protected void initView() {
        adapter = new AdasCheckAdapter();
        rvSelfCheck.setAdapter(adapter);
        isInstallationMode = getParentFragment() != null;
        if (isInstallationMode) {
            Report report = null;
            if (getParentFragment().getArguments() != null) {
                report = getParentFragment().getArguments()
                        .getParcelable(REPORT);
            }
            if (report != null && report.selfCheck != null) {
                showData(report.adasCheck);
                return;
            }
        } else {
            if (getActivity() == null) {
                return;
            }
            AdasCheck lastSelfCheck = AdasCheck.getLastFromDB(getActivity().getIntent()
                                                                      .getStringExtra(
                                                                              SERIAL_NUMBER));
            if (lastSelfCheck != null) {
                showData(lastSelfCheck);
                showProgress(R.string.getting_of_current_time);
                bleManager.getCurrentTime();
                return;
            }
        }
        generateAdasCheck();
    }

    @Override
    protected View createToolbarButton() {
        ImageView view = new ImageView(getContext());
        view.setImageResource(R.drawable.selector_btn_refresh);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity activity = (MainActivity) getActivity();
                if (activity != null && !activity.isProgressVisible()) {
                    generateAdasCheck();
                }
            }
        });
        return view;
    }

    public static Fragment newInstance() {
        Fragment fragment = new AdasCalibrationStatusFragment();
        Bundle args = new Bundle();
        args.putString(TOOLBAR_TITLE, SmartDriveApp.getContext()
                .getString(R.string.adas_check));
        fragment.setArguments(args);
        return fragment;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(CurrentTimeGotEvent event) {
        if (isInstallationMode) {
            return;
        }
        hideProgress();
        if (DateUtils.isTimeDayOld(event.currentTime.getTimeInMillis(), lastRunTime)) {
            Dialogs.showAlert(getContext(), null, getString(R.string.adas_run_new_test),
                              null, R.string.no, R.string.yes, true, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            generateAdasCheck();
                        }
                    }
            );
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(AdasCheckGeneratedEvent event) {
        hideProgress();
        showData(event.getAdasCheck());
    }

    private void showData(AdasCheck adasCheck) {
        int colorRes = adasCheck.getBackgroundColorRes();
        if (getContext() != null) {
            conditionContainer.setBackgroundColor(ContextCompat.getColor(getContext(), colorRes));
        }
        tvCondition.setText(getString(R.string.adas_condition, adasCheck.status));
        lastRunTime = adasCheck.runTime;
        String runTimeStr = DateUtils.longToString(lastRunTime, DateUtils.is24HourFormat()
                                                                ?
                                                                DateUtils.TIME_FORMAT_24
                                                                : DateUtils.TIME_FORMAT_12);
        tvLastRun.setText(getString(R.string.last_run, runTimeStr));
        adapter.update(adasCheck.items);
    }

    private void generateAdasCheck() {
        showProgress(R.string.adas_check_progress);
        if (getActivity() != null) {
            bleManager.generateAdasCheck(getActivity().getIntent()
                                                 .getStringExtra(SERIAL_NUMBER));
        }
    }

}