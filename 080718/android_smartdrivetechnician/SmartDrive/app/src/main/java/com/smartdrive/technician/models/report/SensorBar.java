package com.smartdrive.technician.models.report;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "SensorBar")
public class SensorBar extends Model implements Parcelable {

    public static final Parcelable.Creator<SensorBar> CREATOR = new Parcelable.Creator<SensorBar>() {
        @Override
        public SensorBar createFromParcel(Parcel source) {
            return new SensorBar(source);
        }

        @Override
        public SensorBar[] newArray(int size) {
            return new SensorBar[size];
        }
    };

    @Column
    public String mountingLocation;
    @Column
    public String notes;
    @Column
    public String checkResponse;

    public SensorBar() {
    }

    protected SensorBar(Parcel in) {
        this.mountingLocation = in.readString();
        this.notes = in.readString();
        this.checkResponse = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mountingLocation);
        dest.writeString(this.notes);
        dest.writeString(this.checkResponse);
    }

    @Override
    public int describeContents() {
        return 0;
    }
    
}