package com.smartdrive.technician.events;

public class PendingReportsEvent {

    public final int count;

    public PendingReportsEvent(int count) {
        this.count = count;
    }

}