package com.smartdrive.technician.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.smartdrive.technician.R;

import java.util.List;

@Table(name = "AdasCheck")
public class AdasCheck extends Model implements Parcelable {

    public static final Creator<AdasCheck> CREATOR = new Creator<AdasCheck>() {
        @Override
        public AdasCheck createFromParcel(Parcel source) {
            return new AdasCheck(source);
        }

        @Override
        public AdasCheck[] newArray(int size) {
            return new AdasCheck[size];
        }
    };

    private static final String RED = "red";
    private static final String YELLOW = "yellow";
    @Column
    public String serialNumber;
    @Column
    public long runTime;
    @Column
    public String status;
    @Column
    public String background;
    public List<AdasCheckItem> items;
    @Column
    private long reportId;
    public AdasCheck() {
    }

    private AdasCheck(Parcel in) {
        this.serialNumber = in.readString();
        this.runTime = in.readLong();
        this.status = in.readString();
        this.background = in.readString();
        this.items = in.createTypedArrayList(AdasCheckItem.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.serialNumber);
        dest.writeLong(this.runTime);
        dest.writeString(this.status);
        dest.writeString(this.background);
        dest.writeTypedList(this.items);
    }

    public static AdasCheck getLastFromDB(String deviceSerialNumber) {
        if (deviceSerialNumber == null) {
            return null;
        }
        AdasCheck adasCheck = new Select().from(AdasCheck.class)
                .where("serialNumber = ?", deviceSerialNumber)
                .orderBy("runTime DESC")
                .executeSingle();
        if (adasCheck != null) {
            adasCheck.items = AdasCheckItem.getListFromDB(adasCheck.getId());
        }
        return adasCheck;
    }

    public void setReportId(long reportId) {
        this.reportId = reportId;
    }

    public int getBackgroundColorRes() {
        if (!TextUtils.isEmpty(background)) {
            if (background.equals(YELLOW)) {
                return R.color.yellow;
            } else if (background.equals(RED)) {
                return android.R.color.holo_red_dark;
            }
        }
        return R.color.green;
    }

    public void saveToDB() {
        deleteOldFromDB();
        save();
        if (items != null) {
            for (AdasCheckItem item : items) {
                item.selfCheckId = getId();
                item.save();
            }
        }
    }

    public void deleteFromDB() {
        delete();
        if (items != null) {
            for (AdasCheckItem item : items) {
                item.delete();
            }
        }
    }

    private void deleteOldFromDB() {
        if (serialNumber == null) {
            return;
        }
        AdasCheck oldCheck = new Select().from(AdasCheck.class)
                .where("reportId = ?", reportId)
                .and("serialNumber = ?", serialNumber)
                .executeSingle();
        if (oldCheck != null) {
            oldCheck.items = AdasCheckItem.getListFromDB(oldCheck.getId());
            oldCheck.deleteFromDB();
        }
    }

}