package com.smartdrive.technician.activities;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.events.PendingReportsEvent;
import com.smartdrive.technician.events.ble.BleErrorEvent;
import com.smartdrive.technician.fragments.PendingReportsFragment;
import com.smartdrive.technician.managers.BleManager;
import com.smartdrive.technician.utils.Dialogs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.smartdrive.technician.Constants.PACKAGE;
import static com.smartdrive.technician.Constants.RC_ENABLE_BLE;

public abstract class BaseActivity extends AppCompatActivity {

    protected BleManager bleManager = BleManager.getInstance();

    @Bind(R.id.activityProgress)
    View activityProgress;
    @Bind(R.id.fullscreenProgress)
    View fullscreenProgress;
    @Bind(R.id.activityContent)
    View activityContent;
    @Bind(R.id.pendingReportsPanel)
    View pendingReportsPanel;
    @Bind(R.id.tvPendingReports)
    TextView tvPendingReports;
    @Bind(R.id.bottomProgress)
    View bottomProgress;
    @Bind(R.id.tvBottomProgress)
    TextView tvBottomProgress;

    private PendingReportsFragment pendingReportsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        Class cls = getClass();
        if (!cls.isAnnotationPresent(Layout.class)) {
            throw new RuntimeException("Activity must use @Layout (R.layout.xxx) annotation");
        }
        Layout layout = (Layout) cls.getAnnotation(Layout.class);
        View contentView = getLayoutInflater().inflate(layout.id(), null);
        ViewGroup contentContainer = findViewById(R.id.contentContainer);
        if (contentContainer != null) {
            contentContainer.addView(contentView);
        }
        ButterKnife.bind(this);
        initView();
        EventBus.getDefault()
                .register(this);
        initPendingReportsFragment();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
        EventBus.getDefault()
                .unregister(this);
    }

    @Override
    public void onBackPressed() {
        if (!pendingReportsFragment.isHidden()) {
            hidePendingReportsFragment();
        } else {
            super.onBackPressed();
        }
    }

    protected abstract void initView();

    @OnClick(R.id.pendingReportsPanel)
    public void onClickPendingReports() {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter_slide, R.anim.exit_slide)
                .show(pendingReportsFragment)
                .commit();
    }

    @OnClick(R.id.ivCancel)
    public void onIvCancelPressed() {
        hideFullscreenProgress();
    }

    public void hidePendingReportsFragment() {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter_slide, R.anim.exit_slide)
                .hide(pendingReportsFragment)
                .commit();
    }

    public void showBottomProgress(@StringRes int textRes) {
        pendingReportsPanel.setClickable(false);
        tvBottomProgress.setText(textRes);
        bottomProgress.setVisibility(View.VISIBLE);
    }

    public void hideBottomProgress() {
        pendingReportsPanel.setClickable(true);
        tvBottomProgress.setText(null);
        bottomProgress.setVisibility(View.GONE);
    }

    public void showProgress(@StringRes int textRes) {
        if (activityProgress == null || activityContent == null) {
            return;
        }
        ((TextView) activityProgress.findViewById(R.id.tvProgress)).setText(textRes);
        activityProgress.setVisibility(View.VISIBLE);
        activityContent.setVisibility(View.GONE);
    }

    public void hideProgress() {
        if (activityProgress == null || activityContent == null) {
            return;
        }
        activityProgress.setVisibility(View.GONE);
        activityContent.setVisibility(View.VISIBLE);
    }

    public void hideFullscreenProgress() {
        if (fullscreenProgress == null || activityContent == null) {
            return;
        }
        fullscreenProgress.setVisibility(View.GONE);
        activityContent.setVisibility(View.VISIBLE);
    }

    public void showFullscreenProgress(@DrawableRes int drawableRes, @StringRes int messageString) {
        if (fullscreenProgress == null || activityContent == null) {
            return;
        }
        ((ImageView) fullscreenProgress.findViewById(R.id.ivCancel)).setImageResource(drawableRes);
        ((TextView) fullscreenProgress.findViewById(R.id.tvProgress)).setText(messageString);
        fullscreenProgress.setVisibility(View.VISIBLE);
        activityContent.setVisibility(View.GONE);
    }

    public boolean isProgressVisible() {
        return activityProgress != null
                && activityContent != null
                && activityProgress.getVisibility() == View.VISIBLE;
    }

    public void showAppSettings(int requestCode) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse(String.format(PACKAGE, getPackageName())));
        startActivityForResult(intent, requestCode);
    }

    @Subscribe()
    public void onEvent(PendingReportsEvent event) {
        if (isFinishing()) {
            return;
        }
        if (event.count > 0) {
            pendingReportsPanel.setVisibility(View.VISIBLE);
            if (event.count == 1) {
                tvPendingReports.setText(R.string.pending_installation_report);
            } else {
                tvPendingReports.setText(
                        getString(R.string.pending_installation_reports, event.count));
            }
        } else {
            tvPendingReports.setText(null);
            pendingReportsPanel.setVisibility(View.GONE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(BleErrorEvent event) {
        hideProgress();
        Dialogs.showToast(this, getString(R.string.error), event.message);
    }

    protected boolean checkBluetoothEnabled() {
        if (!bleManager.isBluetoothEnabled()) {
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),
                                   RC_ENABLE_BLE);
            return false;
        }
        return true;
    }

    private void initPendingReportsFragment() {
        FragmentManager fm = getSupportFragmentManager();
        pendingReportsFragment = (PendingReportsFragment) fm.findFragmentById(
                R.id.pendingReportsFragment);
        fm.beginTransaction()
                .hide(pendingReportsFragment)
                .commit();
    }

}