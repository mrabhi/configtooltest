package com.smartdrive.technician.events.ble.installation;

public class CommunicationSettingsTestedEvent {

    public final String signalStrength;
    public final String ping;

    public CommunicationSettingsTestedEvent(String signalStrength, String ping) {
        this.signalStrength = signalStrength;
        this.ping = ping;
    }

}