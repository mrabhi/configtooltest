package com.smartdrive.technician.utils;

import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;

import com.smartdrive.technician.Constants;
import com.smartdrive.technician.SmartDriveApp;
import com.smartdrive.technician.models.AdasCheck;
import com.smartdrive.technician.models.AdasCheckItem;
import com.smartdrive.technician.models.DebugInfoItem;
import com.smartdrive.technician.models.Event;
import com.smartdrive.technician.models.SelfCheck;
import com.smartdrive.technician.models.SelfCheckItem;
import com.smartdrive.technician.models.VehicleMeasurements;
import com.smartdrive.technician.models.report.Accessories;
import com.smartdrive.technician.models.report.Camera;
import com.smartdrive.technician.models.report.Cellular;
import com.smartdrive.technician.models.report.CommunicationProtocol;
import com.smartdrive.technician.models.report.CommunicationSettings;
import com.smartdrive.technician.models.report.InterchangeItem;
import com.smartdrive.technician.models.report.Report;
import com.smartdrive.technician.models.report.WifiSecurity;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import static com.smartdrive.technician.Constants.REPORT;
import static com.smartdrive.technician.Constants.SLASH;
import static com.smartdrive.technician.Constants.SPACE;
import static com.smartdrive.technician.utils.DateUtils.EVENT_TIME_FORMAT;

public abstract class XmlHelper {

    private static final String TAG = XmlHelper.class.getName();

    private static final String ITEM = "item";
    private static final String LABEL = "label";

    private static final String EVENT = "event";
    private static final String NAME = "name";
    private static final String DATE = "date";

    private static final String STATUS = "status";
    private static final String BACKGROUND = "background";
    private static final String ALERT_FLAG = "alertFlag";

    private static final String NETWORK = "network";
    private static final String TYPE = "type";
    private static final String COUNTRY = "country";
    private static final String CARRIER = "carrier";
    private static final String APN = "apn";
    private static final String SSID = "ssid";
    private static final String ENCRYPTION = "encryption";
    private static final String PASSWORD = "password";
    private static final String PROTOCOL = "protocol";
    private static final String IP_ADDRESS = "ipAddress";
    private static final String SUBNET_MASK = "subnetMask";
    private static final String GATEWAY = "gateway";
    private static final String DNS = "dns";

    private static final String LATITUDE = "Latitude";
    private static final String LONGITUDE = "Longitude";
    private static final String VIDEO_FILE = "VideoFile";
    private static final String VIDEO_FILE_NAME = "name";
    private static final String CAMERA_REFERENCE = "CameraReference";
    private static final String CAMERA_ID = "CameraID";
    private static final String CAMERA_POSITION = "Position";
    private static final String CAMERA_FORWARD = "Forward";
    private static final String CAMERA_DRIVER = "Driver";
    private static final String CAMERA_WITH_ID = "Camera %1$s";

    private static final String INSTALLATION_REPORT = "InstallationReport";
    private static final String REPORT_CREATION_DATE = "ReportCreationDate";
    private static final String SR4_SERIAL_NUMBER = "SR4SerialNumber";
    private static final String VEHICLE_ID = "VehicleID";
    private static final String FIRST_NAME = "First name";
    private static final String LAST_NAME = "Last name";
    private static final String PHONE_NUMBER = "Phone Number";
    private static final String USER_ROLE = "User Role";
    private static final String SALES_ORDER_NUMBER = "Sales order number";
    private static final String COMPONENTS_INSTALLED = "Components installed";
    private static final String VEHICLE_TYPE = "Vehicle type";
    private static final String YEAR = "Year";
    private static final String MAKE = "Make";
    private static final String MODEL = "Model";
    private static final String VIN = "VIN";
    private static final String COMPANY = "Company";
    private static final String CONTROLLER_MOUNTING_LOCATION = "Controller mounting location";
    private static final String CONTROLLER_MOUNTING_NOTES = "Controller mounting notes";
    private static final String SENSORBAR_MOUNTING_LOCATION = "SensorBar mounting location";
    private static final String SENSORBAR_MOUNTING_NOTES = "SensorBar mounting notes";
    private static final String SENSORBAR_CHECK = "SensorBar check";
    private static final String COMMUNICATION_INTERFACE = "Communication interface";
    private static final String CELLULAR_COUNTRY = "Cellular Country";
    private static final String CELLULAR_CARRIER = "Cellular Carrier";
    private static final String CELLULAR_SIGNAL_STRENGTH = "Cellular signal strength";
    private static final String WIFI_IP_MODE = "Wifi IP mode";
    private static final String WIFI_STATIC_IP_ADDRESS = "Wifi Static IP address";
    private static final String WIFI_SUBNET_MASK = "Wifi subnet mask";
    private static final String WIFI_GATEWAY = "Wifi gateway";
    private static final String WIFI_DNS = "Wifi dns";
    private static final String WIFI_SSID = "Wifi ssid";
    private static final String WIFI_ENCRYPTION = "Wifi encryption";
    private static final String WIFI_PASSWORD = "Wifi password";
    private static final String WIFI_SIGNAL_STRENGTH = "Wifi Signal strength";
    private static final String ETHERNET_IP_MODE = "Ethernet IP mode";
    private static final String ETHERNET_STATIC_IP_ADDRESS = "Ethernet Static IP address";
    private static final String ETHERNET_SUBNET_MASK = "Ethernet subnet mask";
    private static final String ETHERNET_GATEWAY = "Ethernet gateway";
    private static final String ETHERNET_DNS = "Ethernet dns";
    private static final String PING_TO_SMARTDRIVE_SERVER = "Ping to SmartDrive server";
    private static final String CAMERA_CONNECTED = "%1$s connected";
    private static final String CAMERA_MOUNTING_LOCATION = "%1$s mounting location";
    private static final String CAMERA_FIELD_OF_VIEW = "%1$s field of view";
    private static final String CAMERA_NOTES = "%1$s Notes";
    private static final String CAMERA_TEST_IMAGE = "%1$s Test image";
    private static final String KEYPAD_CONNECTED = "Keypad connected";
    private static final String KEYPAD_MOUNTING_LOCATION = "Keypad Mounting location";
    private static final String GPS_PUCK_CONNECTED = "GPS Puck connected";
    private static final String GPS_PUCK_MOUNTING_LOCATION = "GPS Puck mounting location";
    private static final String GPS_PUCK_NOTES = "GPS Puck notes";
    private static final String GPS_PUCK_STATUS = "GPS Puck status";
    private static final String REMOTE_PUSH_BUTTON_CONNECTED = "Remote Push Button connected";
    private static final String REMOTE_PUSH_BUTTON_MOUTING_LOCATION = "Remote Push Button Mouting location";
    private static final String INPUT_CONNECTED = "%1$s Wire Input connected";
    private static final String INPUT_NOTES = "%1$s Wire Install notes";
    private static final String SELF_CHECK_REPORT_DATE = "Self Check Report Date";
    private static final String SELF_CHECK = "selfCheck";
    private static final String DEBUG_INFO = "debugInformation";
    private static final String NA = "N/A";
    private static final String YES = "Yes";
    private static final String NO = "No";
    private static final String STATIC_IP = "Static IP";
    private static final String DYNAMIC_IP = "Dynamic IP";
    private static final String NOT_CONNECTED = "Not connected";
    private static final String CAMERA = "Camera";
    private static final String CAM = "Cam";


    //---Parsing---

    private static XmlPullParser createParser(String xml) throws XmlPullParserException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser xpp = factory.newPullParser();
        xpp.setInput(new StringReader(xml));
        return xpp;
    }

    public static List<Event> parseEventsLibrary(String xml) {
        List<Event> items = new ArrayList<>();
        try {
            XmlPullParser xpp = createParser(xml);
            int eventType = xpp.getEventType();
            Event item = null;
            String tagName = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    tagName = xpp.getName();
                    if (tagName.equals(EVENT)) {
                        item = new Event();
                        item.id = xpp.getAttributeValue(0);
                        items.add(item);
                    }
                } else if (eventType == XmlPullParser.TEXT && item != null) {
                    switch (tagName) {
                        case NAME:
                            item.name = xpp.getText();
                            break;
                        case DATE:
                            item.time = DateUtils.stringToCalendar(xpp.getText(), EVENT_TIME_FORMAT)
                                    .getTimeInMillis();
                            break;
                    }
                }
                eventType = xpp.next();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return items;
    }

    public static List<DebugInfoItem> parseDebugInfo(String xml) {
        List<DebugInfoItem> items = new ArrayList<>();
        try {
            XmlPullParser xpp = createParser(xml);
            int eventType = xpp.getEventType();
            DebugInfoItem item = null;
            String tagName = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    tagName = xpp.getName();
                    if (tagName.equals(ITEM)) {
                        item = new DebugInfoItem();
                        item.name = xpp.getAttributeValue(0);
                        items.add(item);
                    }
                } else if (eventType == XmlPullParser.TEXT && item != null && tagName.equals(
                        ITEM)) {
                    item.details = xpp.getText();
                }
                eventType = xpp.next();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return items;
    }

    public static SelfCheck parseSelfCheck(String xml) {
        SelfCheck selfCheck = new SelfCheck();
        selfCheck.items = new ArrayList<>();
        try {
            XmlPullParser xpp = createParser(xml);
            int eventType = xpp.getEventType();
            SelfCheckItem item = null;
            String tagName = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    tagName = xpp.getName();
                    if (tagName.equals(ITEM)) {
                        item = new SelfCheckItem();
                        item.name = xpp.getAttributeValue("", LABEL);
                        item.background = xpp.getAttributeValue("", BACKGROUND);
                        selfCheck.items.add(item);
                    }  else if (tagName.equals(REPORT)) {
                        selfCheck.status = xpp.getAttributeValue("", STATUS);
                        if (selfCheck.status == null) {
                            selfCheck.status = "";
                        }
                        selfCheck.background = xpp.getAttributeValue("", BACKGROUND);
                    }
                } else if (eventType == XmlPullParser.TEXT && item != null && tagName.equals(
                        ITEM)) {
                    item.details = xpp.getText();
                }
                eventType = xpp.next();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return selfCheck;
    }

    public static AdasCheck parseAdasCheck(String xml) {
        AdasCheck adasCheck = new AdasCheck();
        adasCheck.items = new ArrayList<>();
        try {
            XmlPullParser xpp = createParser(xml);
            int eventType = xpp.getEventType();
            AdasCheckItem item = null;
            String tagName = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    tagName = xpp.getName();
                    if (tagName.equals(ITEM)) {
                        item = new AdasCheckItem();
                        item.name = xpp.getAttributeValue("", LABEL);
                        item.background = xpp.getAttributeValue("", BACKGROUND);
                        adasCheck.items.add(item);
                    } else if (tagName.equals(REPORT)) {
                        adasCheck.status = xpp.getAttributeValue("", STATUS);
                        if (adasCheck.status == null) {
                            adasCheck.status = "";
                        }
                        adasCheck.background = xpp.getAttributeValue("", BACKGROUND);
                    }
                } else if (eventType == XmlPullParser.TEXT && item != null && tagName.equals(
                        ITEM)) {
                    item.details = xpp.getText();
                }
                eventType = xpp.next();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return adasCheck;
    }

    public static CommunicationSettings parseCommunicationSettings(String xml) {
        CommunicationSettings settings = new CommunicationSettings();
        try {
            XmlPullParser xpp = createParser(xml);
            int eventType = xpp.getEventType();
            String tagName = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    tagName = xpp.getName();
                    switch (tagName) {
                        case NETWORK:
                            parseNetworkType(xpp.getAttributeValue(0), settings);
                            break;
                        case ENCRYPTION:
                            settings.wifiSecurity.encryption = xpp.getAttributeValue(0);
                            break;
                        case PROTOCOL:
                            String protocolType = xpp.getAttributeValue(0);
                            settings.protocol.type = CommunicationProtocol.Type.valueOf(
                                    protocolType.toUpperCase());
                            break;
                    }
                } else if (eventType == XmlPullParser.TEXT && tagName != null) {
                    switch (tagName) {
                        case COUNTRY:
                            settings.cellular.country = xpp.getText();
                            break;
                        case CARRIER:
                            settings.cellular.carrier = xpp.getText();
                            break;
                        case APN:
                            settings.cellular.apn = xpp.getText();
                            break;
                        case SSID:
                            settings.wifiSecurity.ssid = xpp.getText();
                            break;
                        case PASSWORD:
                            settings.wifiSecurity.password = xpp.getText();
                            break;
                        case IP_ADDRESS:
                            settings.protocol.ipAddress = xpp.getText();
                            break;
                        case SUBNET_MASK:
                            settings.protocol.subnetMask = xpp.getText();
                            break;
                        case GATEWAY:
                            settings.protocol.gateway = xpp.getText();
                            break;
                        case DNS:
                            settings.protocol.dns = xpp.getText();
                            break;
                    }
                }
                eventType = xpp.next();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return settings;
    }

    private static void parseNetworkType(String networkType, CommunicationSettings settings) {
        settings.type = CommunicationSettings.Type.valueOf(networkType.toUpperCase());
        switch (settings.type) {
            case CELLULAR:
                settings.cellular = new Cellular();
                break;
            case WIFI:
                settings.wifiSecurity = new WifiSecurity();
                settings.protocol = new CommunicationProtocol();
                break;
            case ETHERNET:
                settings.protocol = new CommunicationProtocol();
                break;
        }
    }

    public static Event parseVideoFilesHeader(String eventId, File headerXmlFile) {
        try {
            Event event = new Event();
            event.id = eventId;
            event.cameras = new ArrayList<>();
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(new InputStreamReader(new FileInputStream(headerXmlFile)));
            int eventType = xpp.getEventType();
            String tagName = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    tagName = xpp.getName();
                    if (tagName.equals(VIDEO_FILE)) {
                        Camera camera = new Camera();
                        String fileName = xpp.getAttributeValue("", VIDEO_FILE_NAME)
                                .toLowerCase();
                        camera.videoUrl = headerXmlFile.getParent() + SLASH + fileName;
                        event.cameras.add(camera);
                    } else if (tagName.equals(CAMERA_REFERENCE) && !event.cameras.isEmpty()) {
                        String cameraId = xpp.getAttributeValue("", CAMERA_ID);
                        String position = xpp.getAttributeValue("", CAMERA_POSITION);
                        Camera camera = event.cameras.get(event.cameras.size() - 1);
                        camera.cameraId = Integer.parseInt(cameraId);
                        if (CAMERA_FORWARD.equals(position) || CAMERA_DRIVER.equals(position)) {
                            camera.name = position;
                        } else {
                            String mountingLocation = SmartDriveApp.getCameraMountingLocation(
                                    camera.cameraId);
                            if (!TextUtils.isEmpty(mountingLocation)) {
                                camera.name = mountingLocation;
                            } else {
                                camera.name = String.format(CAMERA_WITH_ID, cameraId);
                            }
                        }
                    }
                } else if (tagName != null && eventType == XmlPullParser.TEXT) {
                    if (event.latitude == 0 && tagName.equals(LATITUDE)) {
                        event.latitude = Double.parseDouble(xpp.getText());
                    } else if (event.longitude == 0 & tagName.equals(LONGITUDE)) {
                        event.longitude = Double.parseDouble(xpp.getText());
                    }
                }
                eventType = xpp.next();
            }
            return event;
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
    }


    //---Serializing---

    public static String serializeCommunicationSettings(CommunicationSettings settings) {
        try {
            XmlSerializer xmlSerializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            xmlSerializer.setOutput(writer);
            xmlSerializer.startTag("", NETWORK);
            xmlSerializer.attribute("", TYPE, settings.type.toString());
            switch (settings.type) {
                case CELLULAR:
                    if (!TextUtils.isEmpty(settings.cellular.country)) {
                        addTag(xmlSerializer, COUNTRY, settings.cellular.country);
                    }
                    if (!TextUtils.isEmpty(settings.cellular.carrier)) {
                        addTag(xmlSerializer, CARRIER, settings.cellular.carrier);
                    }
                    if (!TextUtils.isEmpty(settings.cellular.apn)) {
                        addTag(xmlSerializer, APN, settings.cellular.apn);
                    }
                    break;
                case WIFI:
                    if (!TextUtils.isEmpty(settings.wifiSecurity.ssid)) {
                        addTag(xmlSerializer, SSID, settings.wifiSecurity.ssid);
                    }
                    if (!TextUtils.isEmpty(settings.wifiSecurity.encryption)) {
                        xmlSerializer.startTag("", ENCRYPTION);
                        xmlSerializer.attribute("", TYPE, settings.wifiSecurity.encryption);
                        if (!settings.wifiSecurity.isEncryptionNone() && !TextUtils.isEmpty(
                                settings.wifiSecurity.password)) {
                            addTag(xmlSerializer, PASSWORD, settings.wifiSecurity.password);
                        }
                        xmlSerializer.endTag("", ENCRYPTION);
                    }
                    serializeCommunicationProtocol(xmlSerializer, settings.protocol);
                    break;
                case ETHERNET:
                    serializeCommunicationProtocol(xmlSerializer, settings.protocol);
                    break;
            }
            xmlSerializer.endTag("", NETWORK);
            xmlSerializer.endDocument();
            return writer.toString();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return "";
    }

    private static void serializeCommunicationProtocol(XmlSerializer xmlSerializer,
                                                       CommunicationProtocol protocol) throws IOException {
        if (protocol == null || protocol.type == null) {
            return;
        }
        xmlSerializer.startTag("", PROTOCOL);
        xmlSerializer.attribute("", TYPE, protocol.type.toString());
        if (protocol.type.equals(CommunicationProtocol.Type.STATIC)) {
            if (!TextUtils.isEmpty(protocol.ipAddress)) {
                addTag(xmlSerializer, IP_ADDRESS, protocol.ipAddress);
            }
            if (!TextUtils.isEmpty(protocol.subnetMask)) {
                addTag(xmlSerializer, SUBNET_MASK, protocol.subnetMask);
            }
            if (!TextUtils.isEmpty(protocol.gateway)) {
                addTag(xmlSerializer, GATEWAY, protocol.gateway);
            }
            if (!TextUtils.isEmpty(protocol.dns)) {
                addTag(xmlSerializer, DNS, protocol.dns);
            }
        }
        xmlSerializer.endTag("", PROTOCOL);
    }

    public static String serializeReport(Report report,
                                         List<DebugInfoItem> debugInfo) throws IOException {
        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        xmlSerializer.setOutput(writer);
        xmlSerializer.startTag("", INSTALLATION_REPORT);
        addTag(xmlSerializer, REPORT_CREATION_DATE,
               DateUtils.longToString(report.time, DateUtils.REPORT_TIME_FORMAT));
        addTag(xmlSerializer, SR4_SERIAL_NUMBER, report.serialNumber);
        addTag(xmlSerializer, VEHICLE_ID, report.vehicleId);
        addItemTag(xmlSerializer, FIRST_NAME, report.firstName);
        addItemTag(xmlSerializer, LAST_NAME, report.lastName);
        addItemTag(xmlSerializer, PHONE_NUMBER, report.phoneNumber);
        addItemTag(xmlSerializer, USER_ROLE, report.userMode);
        addItemTag(xmlSerializer, SALES_ORDER_NUMBER, report.son);
        addItemTag(xmlSerializer, COMPONENTS_INSTALLED, report.selectedSettings);
        addItemTag(xmlSerializer, VEHICLE_TYPE, report.vehicleType);
        addItemTag(xmlSerializer, YEAR, report.year);
        addItemTag(xmlSerializer, MAKE, report.make);
        addItemTag(xmlSerializer, MODEL, report.model);
        addItemTag(xmlSerializer, VIN, report.vin);
        addItemTag(xmlSerializer, COMPANY, report.company);
        if (report.controller != null) {
            addItemTag(xmlSerializer, CONTROLLER_MOUNTING_LOCATION,
                       report.controller.mountingLocation);
            addItemTag(xmlSerializer, CONTROLLER_MOUNTING_NOTES, report.controller.notes);
        } else {
            addEmptyItemTag(xmlSerializer, CONTROLLER_MOUNTING_LOCATION);
            addEmptyItemTag(xmlSerializer, CONTROLLER_MOUNTING_NOTES);
        }
        if (report.sensorBar != null) {
            addItemTag(xmlSerializer, SENSORBAR_MOUNTING_LOCATION,
                       report.sensorBar.mountingLocation);
            addItemTag(xmlSerializer, SENSORBAR_MOUNTING_NOTES, report.sensorBar.notes);
            addItemTag(xmlSerializer, SENSORBAR_CHECK, report.sensorBar.checkResponse, "");
        } else {
            addEmptyItemTag(xmlSerializer, SENSORBAR_MOUNTING_LOCATION);
            addEmptyItemTag(xmlSerializer, SENSORBAR_MOUNTING_NOTES);
            addEmptyItemTag(xmlSerializer, SENSORBAR_CHECK);
        }
        addCommunicationSettingsTags(xmlSerializer, report.communicationSettings);
        boolean configured = report.cameras != null;
        List<Camera> configuredCameras = configured
                                         ? report.cameras
                                         : CsvParser.parseCameras(
                                                 Constants.DIGITAL_CAMERAS_SCREEN);
        for (Camera camera : configuredCameras) {
            addCameraTags(xmlSerializer, camera, configured);
        }
        configured = report.otherCameras != null;
        configuredCameras = configured
                            ? report.otherCameras
                            : CsvParser.parseCameras(Constants.ANALOG_CAMERAS_SCREEN);
        for (Camera camera : configuredCameras) {
            addCameraTags(xmlSerializer, camera, configured);
        }
        Accessories accessories = report.accessories;
        if (accessories != null) {
            addItemTag(xmlSerializer, KEYPAD_CONNECTED, connectedStr(accessories.keypadConnected));
            addItemTag(xmlSerializer, KEYPAD_MOUNTING_LOCATION, accessories.keypadMountingLocation);
            addItemTag(xmlSerializer, GPS_PUCK_CONNECTED,
                       connectedStr(accessories.gpsPuckConnected));
            addItemTag(xmlSerializer, GPS_PUCK_MOUNTING_LOCATION,
                       accessories.gpsPuckMountingLocation);
            addItemTag(xmlSerializer, GPS_PUCK_NOTES, accessories.gpsPuckNotes);
            addItemTag(xmlSerializer, GPS_PUCK_STATUS, accessories.gpsConnection, NOT_CONNECTED);
            addItemTag(xmlSerializer, REMOTE_PUSH_BUTTON_CONNECTED,
                       connectedStr(accessories.remotePushButtonConnected));
            addItemTag(xmlSerializer, REMOTE_PUSH_BUTTON_MOUTING_LOCATION,
                       accessories.remotePushButtonMountingLocation);
        } else {
            addEmptyItemTag(xmlSerializer, KEYPAD_CONNECTED);
            addEmptyItemTag(xmlSerializer, KEYPAD_MOUNTING_LOCATION);
            addEmptyItemTag(xmlSerializer, GPS_PUCK_CONNECTED);
            addEmptyItemTag(xmlSerializer, GPS_PUCK_MOUNTING_LOCATION);
            addEmptyItemTag(xmlSerializer, GPS_PUCK_NOTES);
            addEmptyItemTag(xmlSerializer, GPS_PUCK_STATUS);
            addEmptyItemTag(xmlSerializer, REMOTE_PUSH_BUTTON_CONNECTED);
            addEmptyItemTag(xmlSerializer, REMOTE_PUSH_BUTTON_MOUTING_LOCATION);
        }
        if (report.interchangeItems != null) {
            for (InterchangeItem item : report.interchangeItems) {
                addItemTag(xmlSerializer, String.format(INPUT_CONNECTED, item.name),
                           connectedStr(item.connected));
                addItemTag(xmlSerializer, String.format(INPUT_NOTES, item.name), item.notes);
            }
        } else {
            for (InterchangeItem item : CsvParser.parseInterchange()) {
                addEmptyItemTag(xmlSerializer, String.format(INPUT_CONNECTED, item.name));
                addEmptyItemTag(xmlSerializer, String.format(INPUT_NOTES, item.name));
            }
        }
        SelfCheck selfCheck = report.selfCheck;
        if (selfCheck != null) {
            addItemTag(xmlSerializer, SELF_CHECK_REPORT_DATE,
                       DateUtils.longToString(selfCheck.runTime, DateUtils.REPORT_TIME_FORMAT));
            xmlSerializer.startTag("", SELF_CHECK);
            if (!TextUtils.isEmpty(selfCheck.status)) {
                xmlSerializer.attribute("", STATUS, selfCheck.status);
            }
            if (!TextUtils.isEmpty(selfCheck.background)) {
                xmlSerializer.attribute("", BACKGROUND, selfCheck.background);
            }
            if (selfCheck.items != null) {
                for (SelfCheckItem item : selfCheck.items) {
                    xmlSerializer.startTag("", ITEM);
                    xmlSerializer.attribute("", ALERT_FLAG, String.valueOf(item.background));
                    xmlSerializer.attribute("", LABEL, item.name);
                    xmlSerializer.text(item.details);
                    xmlSerializer.endTag("", ITEM);
                }
            }
            xmlSerializer.endTag("", SELF_CHECK);
        }
        if (debugInfo != null) {
            xmlSerializer.startTag("", DEBUG_INFO);
            for (DebugInfoItem item : debugInfo) {
                addItemTag(xmlSerializer, item.name, item.details, "");
            }
            xmlSerializer.endTag("", DEBUG_INFO);
        }
        xmlSerializer.endTag("", INSTALLATION_REPORT);
        xmlSerializer.endDocument();
        return writer.toString();
    }

    private static void addCommunicationSettingsTags(XmlSerializer xmlSerializer,
                                                     CommunicationSettings settings) throws IOException {
        if (settings != null && settings.type != null) {
            String type = settings.type.name()
                    .substring(0, 1) + settings.type.name()
                    .substring(1)
                    .toLowerCase();
            addItemTag(xmlSerializer, COMMUNICATION_INTERFACE, type);
            switch (settings.type) {
                case CELLULAR:
                    addItemTag(xmlSerializer, CELLULAR_COUNTRY, settings.cellular.country);
                    addItemTag(xmlSerializer, CELLULAR_CARRIER, settings.cellular.carrier);
                    addItemTag(xmlSerializer, CELLULAR_SIGNAL_STRENGTH, settings.signalStrength,
                               "");
                    addEmptyWifiTags(xmlSerializer, true);
                    addEmptyEthernetTags(xmlSerializer, true);
                    break;
                case WIFI:
                    addEmptyCellularTags(xmlSerializer);
                    addItemTag(xmlSerializer, WIFI_IP_MODE, ipTypeStr(settings.protocol.type));
                    if (settings.protocol.type.equals(CommunicationProtocol.Type.STATIC)) {
                        addItemTag(xmlSerializer, WIFI_STATIC_IP_ADDRESS,
                                   settings.protocol.ipAddress);
                        addItemTag(xmlSerializer, WIFI_SUBNET_MASK, settings.protocol.subnetMask);
                        addItemTag(xmlSerializer, WIFI_GATEWAY, settings.protocol.gateway);
                        addItemTag(xmlSerializer, WIFI_DNS, settings.protocol.dns);
                    } else {
                        addEmptyWifiTags(xmlSerializer, false);
                    }
                    addItemTag(xmlSerializer, WIFI_SSID, settings.wifiSecurity.ssid);
                    addItemTag(xmlSerializer, WIFI_ENCRYPTION, settings.wifiSecurity.encryption);
                    addItemTag(xmlSerializer, WIFI_PASSWORD, settings.wifiSecurity.password);
                    addItemTag(xmlSerializer, WIFI_SIGNAL_STRENGTH, settings.signalStrength, "");
                    addEmptyEthernetTags(xmlSerializer, true);
                    break;
                case ETHERNET:
                    addEmptyCellularTags(xmlSerializer);
                    addEmptyWifiTags(xmlSerializer, true);
                    addItemTag(xmlSerializer, ETHERNET_IP_MODE, ipTypeStr(settings.protocol.type));
                    if (settings.protocol.type.equals(CommunicationProtocol.Type.STATIC)) {
                        addItemTag(xmlSerializer, ETHERNET_STATIC_IP_ADDRESS,
                                   settings.protocol.ipAddress);
                        addItemTag(xmlSerializer, ETHERNET_SUBNET_MASK,
                                   settings.protocol.subnetMask);
                        addItemTag(xmlSerializer, ETHERNET_GATEWAY, settings.protocol.gateway);
                        addItemTag(xmlSerializer, ETHERNET_DNS, settings.protocol.dns);
                    } else {
                        addEmptyEthernetTags(xmlSerializer, false);
                    }
                    break;
            }
            addItemTag(xmlSerializer, PING_TO_SMARTDRIVE_SERVER, settings.ping, "");
        } else {
            addEmptyItemTag(xmlSerializer, COMMUNICATION_INTERFACE);
            addEmptyCellularTags(xmlSerializer);
            addEmptyWifiTags(xmlSerializer, true);
            addEmptyEthernetTags(xmlSerializer, true);
            addEmptyItemTag(xmlSerializer, PING_TO_SMARTDRIVE_SERVER);
        }
    }

    private static void addEmptyCellularTags(XmlSerializer xmlSerializer) throws IOException {
        addEmptyItemTag(xmlSerializer, CELLULAR_COUNTRY);
        addEmptyItemTag(xmlSerializer, CELLULAR_CARRIER);
        addEmptyItemTag(xmlSerializer, CELLULAR_SIGNAL_STRENGTH);
    }

    private static void addEmptyWifiTags(XmlSerializer xmlSerializer,
                                         boolean needAddAllTags) throws IOException {
        if (needAddAllTags) {
            addEmptyItemTag(xmlSerializer, WIFI_IP_MODE);
        }
        addEmptyItemTag(xmlSerializer, WIFI_STATIC_IP_ADDRESS);
        addEmptyItemTag(xmlSerializer, WIFI_SUBNET_MASK);
        addEmptyItemTag(xmlSerializer, WIFI_GATEWAY);
        addEmptyItemTag(xmlSerializer, WIFI_DNS);
        if (needAddAllTags) {
            addEmptyItemTag(xmlSerializer, WIFI_SSID);
            addEmptyItemTag(xmlSerializer, WIFI_ENCRYPTION);
            addEmptyItemTag(xmlSerializer, WIFI_PASSWORD);
            addEmptyItemTag(xmlSerializer, WIFI_SIGNAL_STRENGTH);
        }
    }

    private static void addEmptyEthernetTags(XmlSerializer xmlSerializer,
                                             boolean needAddAllTags) throws IOException {
        if (needAddAllTags) {
            addEmptyItemTag(xmlSerializer, ETHERNET_IP_MODE);
        }
        addEmptyItemTag(xmlSerializer, ETHERNET_STATIC_IP_ADDRESS);
        addEmptyItemTag(xmlSerializer, ETHERNET_SUBNET_MASK);
        addEmptyItemTag(xmlSerializer, ETHERNET_GATEWAY);
        addEmptyItemTag(xmlSerializer, ETHERNET_DNS);
    }

    private static void addCameraTags(XmlSerializer xmlSerializer, Camera camera,
                                      boolean configured) throws IOException {
        String name;
        switch (camera.type) {
            case DIGITAL:
                name = camera.type.toString() + SPACE + camera.name.replaceAll(CAMERA, CAM);
                break;
            case ANALOG:
                name = camera.name.replaceAll(CAMERA, CAM);
                break;
            default:
                name = camera.name.substring(0, camera.name.indexOf(CAM));
                name = name.replaceAll(camera.type.name(), "");
                name = name.replaceAll("\\s", "");
        }
        if (configured) {
            addItemTag(xmlSerializer, String.format(CAMERA_CONNECTED, name),
                       connectedStr(camera.connected));
            if (!camera.type.equals(Camera.Type.WABCO)) {
                addItemTag(xmlSerializer, String.format(CAMERA_MOUNTING_LOCATION, name),
                           camera.mountingLocation);
                addItemTag(xmlSerializer, String.format(CAMERA_FIELD_OF_VIEW, name),
                           camera.fieldOfView);
            }
            addItemTag(xmlSerializer, String.format(CAMERA_NOTES, name), camera.notes);
        } else {
            addEmptyItemTag(xmlSerializer, String.format(CAMERA_CONNECTED, name));
            if (!camera.type.equals(Camera.Type.WABCO)) {
                addEmptyItemTag(xmlSerializer, String.format(CAMERA_MOUNTING_LOCATION, name));
                addEmptyItemTag(xmlSerializer, String.format(CAMERA_FIELD_OF_VIEW, name));
            }
            addEmptyItemTag(xmlSerializer, String.format(CAMERA_NOTES, name));
        }
        addItemTag(xmlSerializer, String.format(CAMERA_TEST_IMAGE, name), camera.testImageBase64,
                   "");
    }

    private static void addTag(XmlSerializer xmlSerializer, String tagName,
                               String tagText) throws IOException {
        xmlSerializer.startTag("", tagName);
        xmlSerializer.text(TextUtils.isEmpty(tagText)
                           ? NA
                           : tagText);
        xmlSerializer.endTag("", tagName);
    }

    private static void addItemTag(XmlSerializer xmlSerializer, String attrText, String tagText,
                                   String defaultValue) throws IOException {
        xmlSerializer.startTag("", ITEM);
        xmlSerializer.attribute("", LABEL, attrText);
        xmlSerializer.text(TextUtils.isEmpty(tagText)
                           ? defaultValue
                           : tagText);
        xmlSerializer.endTag("", ITEM);
    }

    private static void addItemTag(XmlSerializer xmlSerializer, String attrText,
                                   String tagText) throws IOException {
        addItemTag(xmlSerializer, attrText, tagText, NA);
    }

    private static void addEmptyItemTag(XmlSerializer xmlSerializer,
                                        String attrText) throws IOException {
        addItemTag(xmlSerializer, attrText, "", "");
    }

    private static String connectedStr(boolean connected) {
        return connected
               ? YES
               : NO;
    }

    private static String ipTypeStr(CommunicationProtocol.Type type) {
        return type.equals(CommunicationProtocol.Type.STATIC)
               ? STATIC_IP
               : DYNAMIC_IP;
    }

    public static String generateAdasReportXML(VehicleMeasurements vehicleMeasurements) {
        try {
            XmlSerializer xmlSerializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            xmlSerializer.setOutput(writer);
            //Open Tag <file>
            xmlSerializer.startTag("", "root");
            xmlSerializer.startTag("", "model");
            xmlSerializer.startTag("", "default");
            xmlSerializer.startTag("", "front");
            xmlSerializer.startTag("", "BoardHeight");
            xmlSerializer.text(String.valueOf(vehicleMeasurements.getBoardHeightFloat()));
            xmlSerializer.endTag("", "BoardHeight");
            xmlSerializer.startTag("", "vehicle_width");
            xmlSerializer.text(String.valueOf(vehicleMeasurements.getVehicleWidthFloat()));
            xmlSerializer.endTag("", "vehicle_width");
            xmlSerializer.startTag("", "tx");
            xmlSerializer.text(String.valueOf(vehicleMeasurements.getTx()));
            xmlSerializer.endTag("", "tx");
            xmlSerializer.startTag("", "ty");
            xmlSerializer.text(String.valueOf(vehicleMeasurements.getTy()));
            xmlSerializer.endTag("", "ty");
            xmlSerializer.startTag("", "tz");
            xmlSerializer.text(String.valueOf(vehicleMeasurements.getTz()));
            xmlSerializer.endTag("", "tz");
            xmlSerializer.endTag("", "front");
            xmlSerializer.endTag("", "default");
            xmlSerializer.endTag("", "model");
            xmlSerializer.endTag("", "root");

            xmlSerializer.endDocument();
            return writer.toString();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return "";
    }
}