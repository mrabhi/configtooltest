package com.smartdrive.technician.events.ble.installation;

public class CheckTurnSignalEvent {

    public boolean isTurnSignalAvailable;

    public CheckTurnSignalEvent(boolean isTurnSignalAvailable) {
        this.isTurnSignalAvailable = isTurnSignalAvailable;
    }
}
