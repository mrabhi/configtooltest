package com.smartdrive.technician.events.ble.installation;

import com.smartdrive.technician.models.report.CommunicationSettings;

public class CommunicationSettingsSetEvent {

    public final CommunicationSettings settings;
    public final boolean failed;
    public final boolean needTestCommunicationSettings;

    public CommunicationSettingsSetEvent(CommunicationSettings settings, boolean failed, boolean needTestCommunicationSettings) {
        this.settings = settings;
        this.failed = failed;
        this.needTestCommunicationSettings = needTestCommunicationSettings;
    }

}