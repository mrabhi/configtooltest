package com.smartdrive.technician.fragments.installation;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.smartdrive.technician.EventsSubscriber;
import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.events.ble.installation.SensorBarCheckedEvent;
import com.smartdrive.technician.models.report.Report;
import com.smartdrive.technician.models.report.SensorBar;
import com.smartdrive.technician.utils.CsvParser;
import com.smartdrive.technician.widgets.BaseSpinner;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;

import static com.smartdrive.technician.Constants.REPORT;

@EventsSubscriber
@Layout(id = R.layout.fragment_sensor_bar)
public class SensorBarFragment extends ChildInstallationFragment {

    @Bind(R.id.scrollSensorBar)
    ScrollView scrollSensorBar;
    @Bind(R.id.spinMountingLocation)
    BaseSpinner spinMountingLocation;
    @Bind(R.id.etNotes)
    EditText etNotes;
    @Bind(R.id.btnCheckSensorBar)
    TextView btnCheckSensorBar;
    @Bind(R.id.tvSensorBarResponse)
    TextView tvSensorBarResponse;

    @Override
    protected void initView() {
        spinMountingLocation.init(CsvParser.parseSensorBarMountingLocations(), R.string.mounting_location, false);
        btnCheckSensorBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(R.string.checking_sensor_bar);
                bleManager.checkSensorBar();
            }
        });
        Report report = getArguments().getParcelable(REPORT);
        if (report != null && report.sensorBar != null) {
            spinMountingLocation.setSelectedItem(report.sensorBar.mountingLocation);
            etNotes.setText(report.sensorBar.notes);
            showSensorBarResponse(report.sensorBar.checkResponse);
        }
    }

    @Override
    protected void updateReport(Report report) {
        report.sensorBar = new SensorBar();
        report.sensorBar.mountingLocation = spinMountingLocation.getSelectedItem();
        report.sensorBar.notes = etNotes.getText().toString().trim();
        report.sensorBar.checkResponse = tvSensorBarResponse.getText().toString();
    }

    private void showSensorBarResponse(String checkResponse) {
        if (!TextUtils.isEmpty(checkResponse)) {
            tvSensorBarResponse.setVisibility(View.VISIBLE);
            tvSensorBarResponse.setText(checkResponse);
            scrollSensorBar.post(new Runnable() {
                @Override
                public void run() {
                    scrollSensorBar.fullScroll(View.FOCUS_DOWN);
                }
            });
        } else {
            tvSensorBarResponse.setVisibility(View.GONE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(SensorBarCheckedEvent event) {
        hideProgress();
        showSensorBarResponse(event.message);
    }

}