package com.smartdrive.technician.models.report;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "Controller")
public class Controller extends Model implements Parcelable {

    public static final Parcelable.Creator<Controller> CREATOR = new Parcelable.Creator<Controller>() {
        @Override
        public Controller createFromParcel(Parcel source) {
            return new Controller(source);
        }

        @Override
        public Controller[] newArray(int size) {
            return new Controller[size];
        }
    };

    @Column
    public String mountingLocation;
    @Column
    public String notes;

    public Controller() {
    }

    protected Controller(Parcel in) {
        this.mountingLocation = in.readString();
        this.notes = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mountingLocation);
        dest.writeString(this.notes);
    }

    @Override
    public int describeContents() {
        return 0;
    }

}