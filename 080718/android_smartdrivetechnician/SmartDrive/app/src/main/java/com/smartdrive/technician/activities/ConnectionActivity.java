package com.smartdrive.technician.activities;

import android.Manifest;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.SmartDriveApp;
import com.smartdrive.technician.adapters.BaseAdapter;
import com.smartdrive.technician.adapters.SmartRecordersAdapter;
import com.smartdrive.technician.events.ble.BleErrorEvent;
import com.smartdrive.technician.events.ble.LoggedInEvent;
import com.smartdrive.technician.events.ble.LoggedOutEvent;
import com.smartdrive.technician.events.ble.ScanCompletedEvent;
import com.smartdrive.technician.events.ble.ScanResultEvent;
import com.smartdrive.technician.managers.BleManager;
import com.smartdrive.technician.managers.VideoManager;
import com.smartdrive.technician.models.Device;
import com.smartdrive.technician.utils.Dialogs;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;
import butterknife.OnClick;

import static com.smartdrive.technician.Constants.AUTO_SCAN;
import static com.smartdrive.technician.Constants.PASSWORD_REGEXP;
import static com.smartdrive.technician.Constants.RC_ENABLE_BLE_WITH_SCAN;
import static com.smartdrive.technician.Constants.RC_LOCATION;
import static com.smartdrive.technician.Constants.SERIAL_NUMBER;
import static com.smartdrive.technician.Constants.USER_MODE;
import static com.smartdrive.technician.Constants.VEHICLE_ID;

@Layout(id = R.layout.activity_connection)
public class ConnectionActivity extends BaseActivity {

    @Bind(R.id.tvDisconnectedMessage)
    TextView tvDisconnectedMessage;
    @Bind(R.id.tvAvailableSmartRecorders)
    TextView tvAvailableSmartRecorders;
    @Bind(R.id.rvSmartRecorders)
    RecyclerView rvSmartRecorders;
    @Bind(R.id.emptySmartRecorders)
    View emptySmartRecorders;

    private SmartRecordersAdapter adapter;
    private Dialog connectionDialog;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == RC_ENABLE_BLE_WITH_SCAN && resultCode == RESULT_OK)
                || requestCode == RC_LOCATION) {
            startScan();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == RC_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startScan();
            } else {
                Dialogs.showAlert(this, getString(R.string.error),
                                  getString(R.string.required_location_permission),
                                  null, R.string.cancel, R.string.open_app_settings, true,
                                  new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          showAppSettings(RC_LOCATION);
                                      }
                                  }
                );
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        VideoManager.getInstance()
                .deleteAllSessionVideos();
        BleManager.getInstance()
                .clear();
        SmartDriveApp.clearCamerasMountingLocations();
    }

    @Override
    protected void initView() {
        adapter = new SmartRecordersAdapter();
        adapter.setOnConnectClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                showConnectionDialog(adapter.getItem(position), position);
            }
        });
        rvSmartRecorders.setAdapter(adapter);
        if (getIntent().getBooleanExtra(AUTO_SCAN, false)) {
            startScan();
        } else {
            tvDisconnectedMessage.setVisibility(View.VISIBLE);
        }
    }

    @Override
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(BleErrorEvent event) {
        Dialogs.hideProgress(connectionDialog);
        super.onBleEvent(event);
    }

    @OnClick(R.id.refresh)
    public void onRefresh() {
        tvDisconnectedMessage.setVisibility(View.GONE);
        startScan();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(ScanResultEvent event) {
        if (tvDisconnectedMessage.getVisibility() == View.VISIBLE) {
            return;
        }
        if (adapter.isEmpty()) {
            hideProgress();
            showBottomProgress(R.string.searching_for_additional_smart_recorders);
            tvAvailableSmartRecorders.setVisibility(View.VISIBLE);
            rvSmartRecorders.setVisibility(View.VISIBLE);
        }
        adapter.add(new Device(event.deviceName));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(ScanCompletedEvent event) {
        if (tvDisconnectedMessage.getVisibility() == View.VISIBLE) {
            return;
        }
        if (adapter.isEmpty()) {
            hideProgress();
            tvAvailableSmartRecorders.setVisibility(View.VISIBLE);
            emptySmartRecorders.setVisibility(View.VISIBLE);
        } else {
            hideBottomProgress();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(LoggedInEvent event) {
        if (event.isInvalidPassword()) {
            Dialogs.hideProgress(connectionDialog);
            showPasswordError(false);
        } else {
            if (connectionDialog != null) {
                connectionDialog.dismiss();
            }
            finish();
            Intent intent = new Intent(ConnectionActivity.this, MainActivity.class);
            intent.putExtra(USER_MODE, event.mode);
            intent.putExtra(SERIAL_NUMBER, event.device.serialNumber);
            String vehicleId = event.device.vehicleId;
            if (vehicleId == null) {
                vehicleId = "";
            }
            intent.putExtra(VEHICLE_ID, vehicleId);
            startActivity(intent);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleEvent(LoggedOutEvent event) {
        Dialogs.hideProgress(connectionDialog);
        if (event.message.toLowerCase()
                .contains("timeout")) {
            Dialogs.showAlert(this, getString(R.string.error), event.message, null, R.string.ok, -1,
                              false, null);
        } else {
            Dialogs.showToast(this, getString(R.string.error), event.message);
        }
    }

    private void showConnectionDialog(final Device device, final int position) {
        final View view = getLayoutInflater().inflate(R.layout.dialog_connection, null);
        final EditText etPassword = view.findViewById(R.id.etPassword);
        CheckBox cbShowPassword = view.findViewById(R.id.cbShowPassword);
        cbShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                } else {
                    etPassword.setInputType(
                            InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });
        connectionDialog = Dialogs.showAlert(this, device.serialNumber, null, view, R.string.cancel,
                                             R.string.login, false, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String password = etPassword.getText()
                                .toString();
                        if (isPasswordValid(password)) {
                            if (checkBluetoothEnabled()) {
                                hideBottomProgress();
                                Dialogs.showProgress(connectionDialog, R.string.login_in_progress);
                                bleManager.login(position, device, password);
                            }
                        } else {
                            showPasswordError(true);
                        }
                    }
                }
        );
    }

    private boolean isPasswordValid(String password) {
        return password.matches(PASSWORD_REGEXP);
    }

    private void showPasswordError(boolean localValidation) {
        if (connectionDialog == null) {
            return;
        }
        EditText etPassword = connectionDialog.findViewById(R.id.etPassword);
        etPassword.setText(null);
        etPassword.setHintTextColor(ContextCompat.getColor(this, R.color.yellow));
        etPassword.setHint(localValidation
                           ? R.string.incorrect_password
                           : R.string.incorrect_password);
        connectionDialog.findViewById(R.id.ivError)
                .setVisibility(View.VISIBLE);
        connectionDialog.findViewById(R.id.divider)
                .setBackgroundColor(ContextCompat.getColor(this, R.color.yellow));
    }

    private void startScan() {
        if (!isLocationAvailable()) {
            return;
        }
        if (bleManager.isBluetoothEnabled()) {
            tvAvailableSmartRecorders.setVisibility(View.GONE);
            rvSmartRecorders.setVisibility(View.GONE);
            emptySmartRecorders.setVisibility(View.GONE);
            adapter.clear();
            hideBottomProgress();
            showProgress(R.string.searching_smart_recorders);
            bleManager.startScan();
        } else {
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),
                                   RC_ENABLE_BLE_WITH_SCAN);
        }
    }

    private boolean isLocationAvailable() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_DENIED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                                   RC_LOCATION);
                return false;
            }
            LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (lm != null && !lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
                    && !lm.isProviderEnabled(
                    LocationManager.NETWORK_PROVIDER)) {
                Dialogs.showAlert(this, getString(R.string.error),
                                  getString(R.string.location_source_is_disabled),
                                  null, R.string.cancel, R.string.open_location_settings, true,
                                  new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          Intent intent = new Intent(
                                                  android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                          startActivityForResult(intent, RC_LOCATION);
                                      }
                                  }
                );
                return false;
            }
        }
        return true;
    }

}