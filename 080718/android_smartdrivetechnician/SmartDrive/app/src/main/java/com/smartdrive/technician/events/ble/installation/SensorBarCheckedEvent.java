package com.smartdrive.technician.events.ble.installation;

public class SensorBarCheckedEvent {

    public final String message;

    public SensorBarCheckedEvent(String message) {
        this.message = message;
    }

}