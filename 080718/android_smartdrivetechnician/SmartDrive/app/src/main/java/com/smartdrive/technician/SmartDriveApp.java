package com.smartdrive.technician;

import android.app.Application;
import android.content.Context;
import android.util.SparseArray;

import com.activeandroid.ActiveAndroid;

public class SmartDriveApp extends Application {

    private static Context context;

    public static String firstName;
    public static String lastName;
    public static String phoneNumber;
    public static String company;
    public static long inputUserDataTime;

    private static SparseArray<String> camerasMountingLocations = new SparseArray<>();

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        ActiveAndroid.initialize(context);
    }

    public static Context getContext() {
        return context;
    }

    public static void clearUserData() {
        firstName = null;
        lastName = null;
        phoneNumber = null;
        company = null;
        inputUserDataTime = 0;
    }

    public static String getCameraMountingLocation(int cameraId) {
        return camerasMountingLocations.get(cameraId, null);
    }

    public static void addCameraMountingLocation(int cameraId, String mountingLocation) {
        camerasMountingLocations.put(cameraId, mountingLocation);
    }

    public static void clearCamerasMountingLocations() {
        camerasMountingLocations.clear();
    }

}