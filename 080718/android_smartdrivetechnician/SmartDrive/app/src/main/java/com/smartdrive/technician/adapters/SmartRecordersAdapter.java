package com.smartdrive.technician.adapters;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smartdrive.technician.R;
import com.smartdrive.technician.models.Device;

import butterknife.Bind;

public class SmartRecordersAdapter extends BaseAdapter<Device> {

    protected class SmartRecordersHolder extends BaseViewHolder {
        @Bind(R.id.tvName)
        TextView tvName;
        @Bind(R.id.tvVehicle)
        TextView tvVehicle;
        @Bind(R.id.tvConnect)
        TextView tvConnect;
        @Bind(R.id.divider)
        View divider;

        private SmartRecordersHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void bind(final int position) {
            Device item = getItem(position);
            tvName.setText(item.serialNumber);
            if (TextUtils.isEmpty(item.vehicleId)) {
                tvVehicle.setVisibility(View.GONE);
            } else {
                tvVehicle.setText(itemView.getContext().getString(R.string.assigned_vehicle, item.vehicleId));
                tvVehicle.setVisibility(View.VISIBLE);
            }
            divider.setVisibility(position == (getItemCount() - 1) ? View.GONE : View.VISIBLE);
        }

        @Override
        protected void setListeners(final int position, OnItemClickListener onItemClickListener) {
            tvConnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onConnectClickListener.onClick(v, position);
                }
            });
        }
    }

    private OnItemClickListener onConnectClickListener;

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_smart_recorders, parent, false);
        return new SmartRecordersHolder(itemView);
    }

    public void setOnConnectClickListener(OnItemClickListener onConnectClickListener) {
        this.onConnectClickListener = onConnectClickListener;
    }

}