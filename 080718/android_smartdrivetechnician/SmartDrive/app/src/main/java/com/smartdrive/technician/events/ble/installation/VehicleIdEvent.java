package com.smartdrive.technician.events.ble.installation;

public class VehicleIdEvent {

    public final String vehicleId;

    public VehicleIdEvent(String vehicleId) {
        this.vehicleId = vehicleId;
    }

}