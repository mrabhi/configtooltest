package com.smartdrive.technician.models.report;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "Accessories")
public class Accessories extends Model implements Parcelable {

    public static final Parcelable.Creator<Accessories> CREATOR = new Parcelable.Creator<Accessories>() {
        @Override
        public Accessories createFromParcel(Parcel source) {
            return new Accessories(source);
        }

        @Override
        public Accessories[] newArray(int size) {
            return new Accessories[size];
        }
    };

    @Column
    public boolean keypadConnected;
    @Column
    public String keypadMountingLocation;
    @Column
    public boolean gpsPuckConnected;
    @Column
    public String gpsPuckMountingLocation;
    @Column
    public String gpsPuckNotes;
    @Column
    public String gpsConnection;
    @Column
    public String gpsSignalStrength;
    @Column
    public boolean remotePushButtonConnected;
    @Column
    public String remotePushButtonMountingLocation;

    public Accessories() {
    }

    protected Accessories(Parcel in) {
        this.keypadConnected = in.readByte() != 0;
        this.keypadMountingLocation = in.readString();
        this.gpsPuckConnected = in.readByte() != 0;
        this.gpsPuckMountingLocation = in.readString();
        this.gpsPuckNotes = in.readString();
        this.gpsConnection = in.readString();
        this.gpsSignalStrength = in.readString();
        this.remotePushButtonConnected = in.readByte() != 0;
        this.remotePushButtonMountingLocation = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.keypadConnected ? (byte) 1 : (byte) 0);
        dest.writeString(this.keypadMountingLocation);
        dest.writeByte(this.gpsPuckConnected ? (byte) 1 : (byte) 0);
        dest.writeString(this.gpsPuckMountingLocation);
        dest.writeString(this.gpsPuckNotes);
        dest.writeString(this.gpsConnection);
        dest.writeString(this.gpsSignalStrength);
        dest.writeByte(this.remotePushButtonConnected ? (byte) 1 : (byte) 0);
        dest.writeString(this.remotePushButtonMountingLocation);
    }

    @Override
    public int describeContents() {
        return 0;
    }

}