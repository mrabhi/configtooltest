package com.smartdrive.technician.events.ble.installation;

import android.graphics.Bitmap;

public class ImageGeneratedEvent {

    public Bitmap image;

    public ImageGeneratedEvent(Bitmap image) {
        this.image = image;
    }
}
