package com.smartdrive.technician.events;

import com.smartdrive.technician.models.Event;

import java.util.ArrayList;
import java.util.List;

public class VideosLoadedEvent {

    public final List<Event> events;

    public VideosLoadedEvent(Event event) {
        events = new ArrayList<>();
        events.add(event);
    }

    public VideosLoadedEvent(List<Event> events) {
        this.events = events;
    }

}