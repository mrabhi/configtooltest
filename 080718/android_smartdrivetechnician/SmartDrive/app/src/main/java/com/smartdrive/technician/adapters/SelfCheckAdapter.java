package com.smartdrive.technician.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smartdrive.technician.R;
import com.smartdrive.technician.models.SelfCheckItem;

import butterknife.Bind;

public class SelfCheckAdapter extends BaseAdapter<SelfCheckItem> {

    protected class SelfCheckHolder extends BaseViewHolder {
        @Bind(R.id.vRoot)
        LinearLayout vRoot;
        @Bind(R.id.tvName)
        TextView tvName;
        @Bind(R.id.tvDetails)
        TextView tvDetails;

        private SelfCheckHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void bind(final int position) {
            SelfCheckItem item = getItem(position);
            tvName.setText(item.name);
            tvDetails.setText(item.details);
            tvName.setTextColor(ContextCompat.getColor(itemView.getContext(),
                    item.background!=null ? R.color.xxx_dark_gray : R.color.hint));
            tvDetails.setTextColor(ContextCompat.getColor(itemView.getContext(),
                    item.background!=null ? R.color.xxx_dark_gray : android.R.color.white));
            vRoot.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), item.getBackgroundColorRes()));
            itemView.setPadding(0, position == 0 ? topMargin : 0, 0, 0);
        }
    }

    private int topMargin;

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        topMargin = (int) context.getResources().getDimension(R.dimen.default_padding);
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_simple_list, parent, false);
        return new SelfCheckHolder(itemView);
    }
}