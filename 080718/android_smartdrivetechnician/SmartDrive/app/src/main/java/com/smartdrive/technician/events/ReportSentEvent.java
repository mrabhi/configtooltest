package com.smartdrive.technician.events;

public class ReportSentEvent {

    public final Long reportId;
    public final boolean isSuccessfully;

    public ReportSentEvent(Long reportId, boolean isSuccessfully) {
        this.reportId = reportId;
        this.isSuccessfully = isSuccessfully;
    }

}