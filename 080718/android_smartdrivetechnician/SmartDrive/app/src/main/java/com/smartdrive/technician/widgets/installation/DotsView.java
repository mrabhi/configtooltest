package com.smartdrive.technician.widgets.installation;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.smartdrive.technician.R;

public class DotsView extends LinearLayout {

    private int padding;
    private int selectedDotIndex = -1;

    public DotsView(Context context) {
        super(context);
        init();
    }

    public DotsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DotsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        padding = (int) getContext().getResources().getDimension(R.dimen.x4_small_padding);
    }

    public void showDots(int dotsCount) {
        if (dotsCount <= 0) return;
        for (int i = 0; i < dotsCount; i++) {
            ImageView dot = new ImageView(getContext());
            dot.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            dot.setImageResource(i == 0 ? R.drawable.ic_lownav_green : R.drawable.ic_lownav_gray);
            dot.setPadding(padding, 0, padding, 0);
            addView(dot);
        }
        selectedDotIndex = 0;
    }

    public void selectNext() {
        if (selectedDotIndex < (getChildCount() - 1)) {
            selectedDotIndex++;
            ((ImageView) getChildAt(selectedDotIndex)).setImageResource(R.drawable.ic_lownav_green);
        }
    }

    public void selectPrevious() {
        if (selectedDotIndex == 0) {
            removeAllViews();
            selectedDotIndex = -1;
        } else {
            ((ImageView) getChildAt(selectedDotIndex)).setImageResource(R.drawable.ic_lownav_gray);
            selectedDotIndex--;
        }
    }

    public boolean hasDots() {
        return getChildCount() != 0;
    }

    public boolean isEndDotSelected() {
        return selectedDotIndex == (getChildCount() - 1);
    }

    public int getSelectedDotIndex() {
        return selectedDotIndex;
    }

}