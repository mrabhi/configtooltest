package com.smartdrive.technician.fragments.installation.calibration;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.view.View;

import com.neusoft.smartdrive.CalibrationBoardRect;
import com.smartdrive.technician.Layout;
import com.smartdrive.technician.R;
import com.smartdrive.technician.activities.AdasCalibrationActivity;
import com.smartdrive.technician.controller.BleConstants;
import com.smartdrive.technician.fragments.BaseFragment;
import com.smartdrive.technician.managers.CalibrationManager;
import com.theartofdev.edmodo.cropper.CropImageView;

import butterknife.Bind;

@Layout(id = R.layout.fragment_adas_box_draw)
public class BoxDrawFragment extends BaseFragment {
    @Bind(R.id.vYes)
    View vYes;
    @Bind(R.id.vReset)
    View vReset;
    @Bind(R.id.ivGeneratedImage)
    CropImageView ivGeneratedImage;
    AdasCalibrationActivity mActivity;
    CalibrationManager mCalibrationManager;
    Bitmap mBitmap;


    @Override
    protected void initView() {
        mActivity = (AdasCalibrationActivity) getActivity();
        if (mActivity == null) {
            return;
        }
        mBitmap = mActivity.getBitmap();
        mCalibrationManager = mActivity.getCalibrationManager();
        vYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.setRect(ivGeneratedImage.getCropRect());

                if (mActivity.getStep()
                        == BleConstants.DistanceCaptured.DISTANCE_3_5_METERS) {
                    calibrate();
                } else {
                    mActivity.openFragment(
                            new PatternCheckFragment());
                }
            }
        });
        vReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivGeneratedImage.resetCropRect();
            }
        });
        ivGeneratedImage.setImageBitmap(mBitmap);

        Rect r=new Rect();
        r.left=148;
        r.bottom=245;
        r.right=284;
        r.top=110;

        ivGeneratedImage.setCropRect(r);
    }


    void calibrate() {
        Rect r = ivGeneratedImage.getCropRect();
        CalibrationBoardRect rect = new CalibrationBoardRect();
        rect.setiLeft((short) r.left);
        rect.setiBottom((short) r.bottom);
        rect.setiRight((short) r.right);
        rect.setiTop((short) r.top);

        mActivity.openFragment(MessageFragment.newInstance(
                mCalibrationManager.calibrate(rect, mActivity.getBitmapPath(),
                                              mActivity.getStep())));
    }

}