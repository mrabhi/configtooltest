package com.smartdrive.technician.events.ble;

import com.smartdrive.technician.models.Event;

import java.util.List;

public class EventsLibraryLoadedEvent {

    public final List<Event> eventsLibrary;

    public EventsLibraryLoadedEvent(List<Event> eventsLibrary) {
        this.eventsLibrary = eventsLibrary;
    }

}